package com.mutumbakato.batchmanager.ui;

import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.BaseDataSource;
import com.mutumbakato.batchmanager.data.models.Vaccination;
import com.mutumbakato.batchmanager.data.models.VaccinationCheck;
import com.mutumbakato.batchmanager.fragments.BaseView;
import com.mutumbakato.batchmanager.presenters.BasePresenter;

import java.util.List;
import java.util.Map;

public interface VaccinationContract {

    interface View extends BaseView<Presenter> {

        void showVaccinations(Map<Integer, List<Vaccination>> vaccinations);

        void showLoadingVaccinationsIndicator(boolean isLoading);

        void showVaccinations(List<Vaccination> vaccinations);

        void showForm(String id);

        void showVaccinationDetailsUi(String batchId);

        void showLoadingVaccinationError(String message);

        void showNoVaccinations();

        void showSuccessfullySavedMessage();

        void showUndoDelete(Vaccination vaccination, int mills);

        void showDeleteUndoItem(Vaccination vaccination);

        void showTotalVaccinations(String totalVaccinations);

        void showScheduleDetails(String description);

        void showChecks(List<VaccinationCheck> checks);

        interface ListInteractionListener {

            void showForm(String is);

            void hideFab(String total);

            void edit(String scheduleId, String vaccinationId);
        }

        interface CheckListener {
            void onChecked(boolean isChecked, Vaccination vaccination);
        }

        interface VaccinationItemInteraction {
            void onDelete(Vaccination vaccination);

            void onEdit(Vaccination vaccination);
        }

    }

    interface Presenter extends BasePresenter {
        void loadVaccination(BaseDataSource.DataLoadCallback<Vaccination> callback);

        void result(int requestCode, int resultCode);

        void editVaccination(Vaccination vaccination);

        void addVaccination();

        void editSchedule();

        void setSchedule(String scheduleId);

        void addToComplete(Vaccination vaccination);

        void removeFromComplete(Vaccination vaccination);

        void removeSchedule(String scheduleId);

        void openVaccinationDetails(@NonNull Vaccination requestedVaccination);

        void refreshVaccinations();

        void LoadMoreVaccinations();

        void saveVaccination(Vaccination vaccination);

        void deleteVaccination(Vaccination vaccination);

        void undoDelete(String id);

        String getBatchId();
    }

}
