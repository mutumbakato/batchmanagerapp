package com.mutumbakato.batchmanager.ui.components

import android.app.DatePickerDialog
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.DatePicker
import com.google.android.material.textfield.TextInputEditText
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.utils.Validator
import java.util.*

class DatePickerEditText : TextInputEditText, View.OnClickListener, DatePickerDialog.OnDateSetListener, ValidatorEditText {

    private val mCalendar = Calendar.getInstance()
    private val day = if (mCalendar.get(Calendar.DAY_OF_MONTH) > 9) mCalendar.get(Calendar.DAY_OF_MONTH).toString() else "0" + mCalendar.get(Calendar.DAY_OF_MONTH)
    private val month = if (mCalendar.get(Calendar.MONTH) + 1 > 9) (mCalendar.get(Calendar.MONTH) + 1).toString() else "0" + (mCalendar.get(Calendar.MONTH) + 1)
    private val today = mCalendar.get(Calendar.YEAR).toString() + "-" + month + "-" + day
    private var mListener: ValidatorEditText.ErrorListener? = null

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        isClickable = true
        setOnClickListener(this)
        isFocusable = false
        setText(today)
        setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_calendar, 0)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        isClickable = true
        setOnClickListener(this)
        isFocusable = false
        setText(today)
    }

    override fun onClick(view: View) {
        if (mListener != null)
            mListener!!.onRemoveError()
        val date = text.toString().trim().split("-")
        val y = if (date.size == 3) date[0].toInt() else mCalendar.get(Calendar.YEAR)
        val m = if (date.size == 3) (date[1].toInt() - 1) else mCalendar.get(Calendar.MONTH)
        val d = if (date.size == 3) date[2].toInt() else mCalendar.get(Calendar.DAY_OF_MONTH)
        DatePickerDialog(context, this, y, m, d).apply {
            setOnDismissListener { isValid }
            show()
        }
    }

    override fun onDateSet(datePicker: DatePicker, year: Int, m: Int, day: Int) {
        var month = m
        month += 1
        val date = year.toString() + "-" + (if (month > 9) month else "0$month") + "-" + if (day > 9) day else "0$day"
        setText(date)
    }

    override fun isValid(): Boolean {
        if (!Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
            if (mListener != null) {
                mListener!!.onShowError("Please select a Date")
            }
            return false
        }
        return true
    }

    override fun setOnErrorListener(listener: ValidatorEditText.ErrorListener) {
        mListener = listener
    }
}
