package com.mutumbakato.batchmanager.ui


import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter

interface EggsFormContract {

    interface View : BaseView<Presenter> {

        fun showDate(date: String)

        fun showTreys(treys: String)

        fun showEggs(eggs: String)

        fun showDamage(damage: String)

        fun clearInputs()

        fun showEggError()

        fun toggleExpansion(expand: Boolean)

        interface FormInteractionListener {
            fun onDoneEditing()

            fun onStartEditing()
        }
    }

    interface Presenter : BasePresenter {

        val isNew: Boolean

        fun save(date: String, treys: Int, eggs: Int, damage: Int)

        fun populate()

        fun startEditing(id: String?)

        fun stopEditing()
    }
}
