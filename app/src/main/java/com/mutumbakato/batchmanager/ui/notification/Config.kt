package com.mutumbakato.batchmanager.ui.notification

object Config {
    // name to handle the notification in the notification tray
    const val NOTIFICATION_ID = 100
    const val NOTIFICATION_ID_BIG_IMAGE = 101

}
