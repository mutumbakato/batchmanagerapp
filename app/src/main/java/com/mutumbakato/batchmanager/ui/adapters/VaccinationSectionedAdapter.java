package com.mutumbakato.batchmanager.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.models.Vaccination;
import com.mutumbakato.batchmanager.data.models.VaccinationCheck;
import com.mutumbakato.batchmanager.ui.VaccinationContract;
import com.mutumbakato.batchmanager.utils.DateUtils;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VaccinationSectionedAdapter extends SectioningAdapter {

    private final VaccinationContract.View.CheckListener mListener;
    private Map<Integer, List<Vaccination>> mVaccinations;
    private List<Integer> mCategories;
    private List<VaccinationCheck> vaccinationChecks;
    private String dateOfBirth;
    private RecyclerView mRecyclerView;

    public VaccinationSectionedAdapter(Map<Integer, List<Vaccination>> mVaccinations,
                                       String dateOfBirth, List<VaccinationCheck> checks,
                                       VaccinationContract.View.CheckListener mListener,
                                       RecyclerView recyclerView) {

        this.mVaccinations = mVaccinations;
        this.mCategories = new ArrayList<>(mVaccinations.keySet());
        this.vaccinationChecks = checks;
        this.dateOfBirth = dateOfBirth;
        this.mListener = mListener;
        this.mRecyclerView = recyclerView;
    }

    @Override
    public GhostHeaderViewHolder onCreateGhostHeaderViewHolder(ViewGroup parent) {
        final View ghostView = new View(parent.getContext());
        ghostView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        return new GhostHeaderViewHolder(ghostView);
    }

    @Override
    public int getNumberOfSections() {
        return mCategories.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return mVaccinations.get(mCategories.get(sectionIndex)).size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public SectioningAdapter.HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.vaccine_section_header, parent, false);
        return new HeaderViewHolder(v);
    }

    @Override
    public SectioningAdapter.ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.fragment_vaccination_item, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex,
                                       int headerType) {
        HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
        headerViewHolder.titleTextView.setText(mCategories.get(sectionIndex) + (mCategories.get(sectionIndex) == 1 ? " Week" : " Weeks"));
        headerViewHolder.totalTextView.setText(DateUtils.INSTANCE.dateFromAge(dateOfBirth, mCategories.get(sectionIndex)));

        int age = DateUtils.INSTANCE.toWeeks(dateOfBirth);
        int week = mCategories.get(sectionIndex);

        if (week == age) {
            mRecyclerView.scrollToPosition(getAdapterPositionForSectionHeader(sectionIndex));
        }
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, final int sectionIndex,
                                     int itemIndex, int itemType) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
        itemViewHolder.bind(mCategories.get(sectionIndex), itemIndex);

    }

    public void addChecks(List<VaccinationCheck> checks) {
        vaccinationChecks = checks;
        notifyDataSetChanged();
    }

    public void updateData(Map<Integer, List<Vaccination>> batches) {
        mVaccinations = batches;
        mCategories = new ArrayList<>(batches.keySet());
        notifyAllSectionsDataSetChanged();
    }

    private boolean isChecked(Vaccination item) {

        if (vaccinationChecks == null)
            return false;

        for (VaccinationCheck check : vaccinationChecks) {
            if (check.getVaccinationId().equals(item.getId())) {
                return true;
            }
        }
        return false;
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {

        private TextView titleTextView;
        private TextView totalTextView;

        HeaderViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.week_age_text);
            totalTextView = itemView.findViewById(R.id.week_header_date);
        }
    }

    public class ItemViewHolder extends SectioningAdapter.ItemViewHolder implements View.OnClickListener {
        final View mView;
        final TextView mDateTextView;
        final TextView mAgeTextView;
        final TextView mDiseaseTextView;
        final TextView mVaccine;
        final TextView mMethod;
        final CheckBox isComplete;
        Vaccination mItem;

        ItemViewHolder(View view) {
            super(view);
            mView = view;
            mDateTextView = view.findViewById(R.id.vaccination_date_textView);
            mAgeTextView = view.findViewById(R.id.vaccination_age_textView);
            mDiseaseTextView = view.findViewById(R.id.total_quantity_textView);
            mVaccine = view.findViewById(R.id.day_textView);
            mMethod = view.findViewById(R.id.date_textView);
            isComplete = view.findViewById(R.id.vaccination_complete_checkbox);
            mView.setOnClickListener(this);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDiseaseTextView.getText() + "'";
        }

        public void bind(int section, int position) {
            mItem = mVaccinations.get(section).get(position);
            mDateTextView.setText(DateUtils.INSTANCE.dateFromAge("2017-08-08", mItem.getAge()));
            mVaccine.setText(mItem.getVaccine());
            mDiseaseTextView.setText(mItem.getInfection());
            mAgeTextView.setText(mItem.getAge() + (mItem.getAge() == 1 ? " Week" : " Weeks"));
            mMethod.setText(mItem.getMethod());
            isComplete.setOnCheckedChangeListener(null);
            isComplete.setChecked(isChecked(mItem));
            isComplete.setOnCheckedChangeListener((compoundButton, b) -> mListener.onChecked(b, mItem));
        }

        @Override
        public void onClick(View view) {
//            mListener.onBatchListFragmentInteraction(mItem);
        }
    }
}
