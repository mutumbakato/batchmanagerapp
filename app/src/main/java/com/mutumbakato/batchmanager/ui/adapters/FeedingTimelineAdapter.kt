package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Feeds
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.fragments.feeds.FeedingListFragment
import com.mutumbakato.batchmanager.ui.components.WeekPickerDialogFragment
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.Standards
import com.mutumbakato.batchmanager.utils.UserRoles
import kotlinx.android.synthetic.main.fragment_timeline_item.view.*
import net.cachapa.expandablelayout.ExpandableLayout
import org.zakariya.stickyheaders.SectioningAdapter

private const val UNSELECTED = -1

class FeedingTimelineAdapter(
        private val dateOfBirth: String,
        private val listener: FeedingListFragment.OnFeedsItemInteraction,
        private val mRecyclerView: RecyclerView,
        private val weekPickerFragmentManager: FragmentManager,
        closeDate: String? = null,
        private val batchType: String) : SectioningAdapter() {

    private var mTimeline: Map<Int, List<Int>> = DateUtils.getTimeLine(dateOfBirth, closeDate)
    private var mCategories: List<Int>? = null
    private var mFeeds: Map<Int, List<Feeds>> = HashMap()
    private var role: String = UserRoles.USER_GUEST
    private var selectedItem = UNSELECTED

    init {
        this.mCategories = ArrayList(mTimeline.keys).sortedWith(Comparator { t, t2 -> t.compareTo(t2) })
    }

    override fun onCreateGhostHeaderViewHolder(parent: ViewGroup): GhostHeaderViewHolder {
        val ghostView = View(parent.context)
        ghostView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        return GhostHeaderViewHolder(ghostView)
    }

    override fun getNumberOfSections(): Int {
        return mCategories!!.size
    }

    override fun getNumberOfItemsInSection(sectionIndex: Int): Int {
        return (mTimeline[mCategories!![sectionIndex]] ?: error("")).size
    }

    override fun doesSectionHaveHeader(sectionIndex: Int): Boolean {
        return true
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup?, headerType: Int): SectioningAdapter.HeaderViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        val v = inflater.inflate(R.layout.timeline_section_header, parent, false)
        return HeaderViewHolder(v)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup?, itemType: Int): SectioningAdapter.ItemViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        val v = inflater.inflate(R.layout.fragment_timeline_item, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindHeaderViewHolder(viewHolder: SectioningAdapter.HeaderViewHolder?, sectionIndex: Int, headerType: Int) {
        val headerViewHolder = viewHolder as HeaderViewHolder?
        headerViewHolder!!.titleTextView.text = headerViewHolder.titleTextView.context.getString(R.string.age_week, mCategories!![sectionIndex])
        headerViewHolder.totalTextView.text = DateUtils.dateFromAge(dateOfBirth, mCategories!![sectionIndex])
        headerViewHolder.titleTextView.setOnClickListener {
            WeekPickerDialogFragment.newInstance(sectionIndex, dateOfBirth).apply {
                setWeekSelectedListener(object : WeekPickerDialogFragment.OnListFragmentInteractionListener {
                    override fun onGoToWeek(week: Int?) {
                        goToWeek(week!!)
                        dismiss()
                    }
                })
                show(weekPickerFragmentManager, "Pick a Week")
            }
        }
    }

    override fun onBindItemViewHolder(viewHolder: SectioningAdapter.ItemViewHolder?, sectionIndex: Int, itemIndex: Int, itemType: Int) {
        val itemViewHolder = viewHolder as ItemViewHolder?
        itemViewHolder!!.bind(mCategories!![sectionIndex], itemIndex)
    }

    fun updateData(feeds: Map<Int, List<Feeds>>) {
        mFeeds = feeds
        notifyAllSectionsDataSetChanged()
    }

    fun goToWeek(week: Int = DateUtils.ageWeeksFromDate(dateOfBirth)) {
        val position = getWeekPosition(week)
        mRecyclerView.scrollToPosition(getAdapterPositionForSectionHeader(position))
    }

    fun applyPermissions(role: String) {
        this.role = role
        notifyDataSetChanged()
    }

    private fun getWeekPosition(week: Int): Int {
        var position = 0
        for (i in mCategories!!.indices) {
            if (mCategories!![i] == week)
                position = mCategories!![i]
        }
        return position
    }

    inner class HeaderViewHolder internal constructor(itemView: View) : SectioningAdapter.HeaderViewHolder(itemView) {
        val titleTextView: TextView = itemView.findViewById(R.id.week_age_text)
        val totalTextView: TextView = itemView.findViewById(R.id.week_header_date)
    }

    inner class ItemViewHolder internal constructor(internal val mView: View) : SectioningAdapter.ItemViewHolder(mView) {

        private val mWeightTextView: TextView = mView.findViewById(R.id.total_quantity_textView)
        private val mDayTextView: TextView = mView.findViewById(R.id.day_textView)
        private val mDateTextView: TextView = mView.findViewById(R.id.date_textView)
        private val mItemContainerView: View = mView.findViewById(R.id.section_item_container)
        private val expandablePaned: ExpandableLayout = mView.findViewById(R.id.details_expansion)
        private val mAverageTextView: TextView = mView.findViewById(R.id.weight_aim_text)
        private val errorMessage: TextView = mView.empty_message_text
        private val itemsList: RecyclerView = mView.items_list
        private val mNewEntry: ImageButton = mView.new_entry
        private val totalLayout: LinearLayout = mView.total_layout
        private val totalView: TextView = mView.timeline_item_total
        private val itemsLayout: LinearLayout = mView.timeline_items_layout
        internal var mItem: Int = 0

        override fun toString(): String {
            return super.toString() + " '" + mWeightTextView.text + "'"
        }

        fun bind(section: Int, position: Int) {
            mItem = (mTimeline[section] ?: error(""))[position]
            val ageInDays = DateUtils.ageDaysFromDate(dateOfBirth, DateUtils.today())
            val itemAge = (mCategories!![section] * 7) + mItem
            val date = DateUtils.rawDateFromAgeDays(dateOfBirth, itemAge)
            val mItems = mFeeds[itemAge] ?: ArrayList()
            val average = getAverage(mItems)
            val aim = Standards.getDailyFeedsTarget(itemAge, batchType)

            expandablePaned.apply {
                duration = 0
                isExpanded = selectedItem == adapterPosition
                duration = 300
            }

            mDayTextView.text = "Day ${itemAge + 1}"
            mWeightTextView.text = String.format("Avg %.0f %s", average, PreferenceUtils.weightUnits)
            mAverageTextView.text = String.format("Aim %.0f %s", aim, PreferenceUtils.weightUnits)
            //mAverageTextView.text = String.format("Avg %.0f %s", average, PreferenceUtils.weightUnits)

            if (mItems.isNotEmpty()) {
                totalView.text = "${getTotalFeeding(mItems)} Kg"
                itemsLayout.visibility = View.VISIBLE
                if (mItems.size > 1) {
                    totalLayout.visibility = View.VISIBLE
                } else {
                    totalLayout.visibility = View.GONE
                }
            } else {
                itemsLayout.visibility = View.GONE
            }

            mNewEntry.setOnClickListener {
                listener.onAdd(date)
            }

            errorMessage.visibility = if (mItems.isEmpty()) View.VISIBLE else View.GONE
            itemsList.apply {
                layoutManager = LinearLayoutManager(mView.context)
                adapter = FeedingRecyclerViewAdapter(mItems, listener, itemsList).apply {
                    applyPermissions(role)
                }
            }

            if (date == DateUtils.today()) {
                mDateTextView.text = DateUtils.getDateFromAge(dateOfBirth, mCategories!![section], mItem) + " - " + mView.context.getString(R.string.today)
                mDateTextView.setTextColor(ContextCompat.getColor(mDateTextView.context, R.color.colorOrange))
            } else {
                mDateTextView.text = DateUtils.getDateFromAge(dateOfBirth, mCategories!![section], mItem)
                mDateTextView.setTextColor(ContextCompat.getColor(mDateTextView.context, R.color.textGray))
            }

            mItemContainerView.setBackgroundResource(when (mItem) {
                0 -> R.drawable.section_top_background
                6 -> R.drawable.section_bottom_background
                else -> R.drawable.section_background
            })

            if (itemAge > ageInDays) {
                if (DateUtils.ageWeeksFromDate(dateOfBirth) == mCategories!![section])
                    mView.alpha = 0.7F
                else {
                    mView.alpha = getFade(mItem)
                }
            } else {
                mView.alpha = 1f
            }

            if (itemAge > ageInDays) {
                mItemContainerView.setOnClickListener {
                    Toast.makeText(mItemContainerView.context, "This day has not reached", Toast.LENGTH_SHORT).show()
                }
            } else {
                mItemContainerView.setOnClickListener {
                    val holder: ItemViewHolder? = mRecyclerView.findViewHolderForAdapterPosition(selectedItem) as ItemViewHolder?
                    holder?.apply {
                        collapse()
                    }
                    if (adapterPosition == selectedItem) {
                        selectedItem = UNSELECTED
                    } else {
                        expand()
                    }
                }
            }
        }

        fun expand() {
            expandablePaned.expand()
            selectedItem = adapterPosition
        }

        fun collapse() {
            expandablePaned.collapse()
        }
    }

    private fun getAverage(mItems: List<Feeds>): Float {
        var avg = 0f
        for (feed in mItems) {
            avg += feed.average
        }
        return (avg * 1000)
    }

    private fun getFade(day: Int): Float {
        return when (day) {
            0 -> 0.55f
            1 -> 0.4f
            2 -> 0.3f
            3 -> 0.15f
            else -> 0.07f
        }
    }

    private fun getTotalFeeding(feeds: List<Feeds>): Float {
        var total = 0f
        feeds.map {
            total += it.quantity
        }
        return total
    }
}
