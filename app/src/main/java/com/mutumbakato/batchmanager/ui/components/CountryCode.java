package com.mutumbakato.batchmanager.ui.components;

/**
 * Created by cato on 7/22/17.
 */

public class CountryCode {
    public String name;
    public String code;

    public CountryCode(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String toString() {
        return name + " " + code;
    }
}
