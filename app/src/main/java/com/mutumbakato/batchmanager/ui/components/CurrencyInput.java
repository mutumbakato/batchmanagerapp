package com.mutumbakato.batchmanager.ui.components;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputEditText;
import com.mutumbakato.batchmanager.utils.Validator;

import java.text.DecimalFormat;

public class CurrencyInput extends TextInputEditText implements ValidatorEditText, View.OnFocusChangeListener {
    private ErrorListener mListener;

    public CurrencyInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        addTextChangedListener(new NumberTextWatcher(this));
        setOnFocusChangeListener(this);
    }

    @Override
    public boolean isValid() {
        if (!Validator.INSTANCE.isValidNumber(super.getText().toString().trim())) {
            if (mListener != null) {
                mListener.onShowError("Invalid amount.");
            }
            return false;
        }
        return true;
    }

    @Override
    public void setOnErrorListener(ErrorListener listener) {
        this.mListener = listener;
    }


    @Override
    public void onFocusChange(View view, boolean isFocused) {
        if (!isFocused) {
            isValid();
        } else {
            if (mListener != null)
                mListener.onRemoveError();
        }
    }

    @Override
    public Editable getText() {
        return super.getText();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
    }

    public class NumberTextWatcher implements TextWatcher {

        @SuppressWarnings("unused")
        private static final String TAG = "NumberTextWatcher";
        private DecimalFormat decimalFormat;
        private DecimalFormat decimalFormatReal;
        private boolean hasFractionalPart;
        private EditText editText;

        NumberTextWatcher(EditText editText) {
            decimalFormat = new DecimalFormat("#,###.##");
            decimalFormat.setDecimalSeparatorAlwaysShown(true);
            decimalFormatReal = new DecimalFormat("#,###");
            this.editText = editText;
            hasFractionalPart = false;
        }

        @Override
        public void afterTextChanged(Editable s) {
//            editText.removeTextChangedListener(this);
//            try {
//                int inilen, endlen;
//                inilen = editText.getText().length();
////                String v = s.toString().replace(String.valueOf(decimalFormat.getDecimalFormatSymbols().getGroupingSeparator()), "");
////                Number n = decimalFormat.parse(v);
//                float n = Float.parseFloat(s.toString().replace(",", ""));
//                int cp = editText.getSelectionStart();
//
//                if (hasFractionalPart) {
//                    editText.setText(decimalFormat.format(n));
//                } else {
//                    editText.setText(decimalFormatReal.format(n));
//                }
//
//                endlen = editText.getText().length();
//                int sel = (cp + (endlen - inilen));
//
//                if (sel > 0 && sel <= editText.getText().length()) {
//                    editText.setSelection(sel);
//                } else {
//                    // place cursor at the end?
//                    editText.setSelection(editText.getText().length() - 1);
//                }
//
//            } catch (NumberFormatException nfe) {
//                // do nothing?
//            }
//            editText.addTextChangedListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            hasFractionalPart = s.toString().contains(String.valueOf(decimalFormat.getDecimalFormatSymbols().getDecimalSeparator()));
        }
    }
}
