package com.mutumbakato.batchmanager.ui

import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter

interface AddUserContract {

    interface View : BaseView<Presenter> {

        fun showBatchUsers(users: List<BatchUser>)

        fun showEmailDialog()

        fun showRoleDialog(user: String, role: String, type: String, extra: String)

        fun showSourceChooser()

        fun showProgress(isLoading: Boolean)

        fun showError(message: String)

        fun showSuccessMessage(s: String)

        fun showNoUsers()

        fun applyPermissions(role: String)

        fun showBatches()

        fun hideLeaveButton()

    }

    interface Presenter : BasePresenter {

        fun addUser(user: String, role: String, type: String, extra: String)

        fun addUserByEmail(email: String, role: String)

        fun addUserFromEmployees(id: String, role: String, extra: String)

        fun changeRole(user: BatchUser, role: String)

        fun removeUser(user: BatchUser)

        fun leave(userId: String)

        fun chooseSource()

        fun addyEmail()

        fun chooseFromEmployees()

        fun loadBatchUsers(batchId: String)

        fun applyPermissions(role: String)

    }

}
