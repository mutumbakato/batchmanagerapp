package com.mutumbakato.batchmanager.ui

import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter

/**
 * Created by cato on 8/8/17.
 */

interface BatchDetailsContract {

    interface View : BaseView<Presenter> {

        fun showDetails(batch: Batch)

        fun showName(name: String)

        fun showDate(date: String)

        fun showQuantity(quantity: String)

        fun showSupplier(supplier: String)

        fun showRate(rate: String)

        fun showBreed(breed: String)

        fun showAge(age: String)

        fun showEditBatch(batchId: String)

        fun showLoadingBatch(b: Boolean)

        fun showNotFound()

        fun showErrorLoading(message: String)

        fun applyPermissions(role: String)

        fun showAddUser(mBatchId: String, name: String)
    }

    interface Presenter : BasePresenter {

        fun closeBatch(archive: Boolean, users: List<String>)

        fun deleteBatch(id: String)

        fun start(batchId: String)

        fun addUser()
    }


}
