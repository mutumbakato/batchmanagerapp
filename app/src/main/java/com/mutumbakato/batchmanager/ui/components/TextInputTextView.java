package com.mutumbakato.batchmanager.ui.components;

import android.content.Context;
import android.text.InputType;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import com.mutumbakato.batchmanager.R;

public class TextInputTextView extends androidx.appcompat.widget.AppCompatTextView {

    private TextSavedListener mListener;
    private String mTitle = "Enter new value";
    private int inputType = InputType.TYPE_CLASS_TEXT;

    public TextInputTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        setOnClickListener(v -> showInput(getText().toString().trim()));
    }

    public void setTextSavedListener(TextSavedListener listener) {
        mListener = listener;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public void setCustomInputType(int type) {
        inputType = type;
    }

    public void showInput() {
        showInput(getText().toString().trim());
    }

    private void showInput(String value) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(mTitle);

        // Set up the input
        View view = inflate(getContext(), R.layout.dialog_input_view, null);
        final EditText input = view.findViewById(R.id.dialog_input);
        input.setInputType(inputType);
        input.setText(value);
        builder.setView(view);

        // Set up the buttons
        builder.setPositiveButton("Save", (dialog, which) -> {
            String text = input.getText().toString().trim();
            if (mListener != null && !TextUtils.isEmpty(text)) {
                mListener.onSave(text);
                dialog.cancel();
            } else if (TextUtils.isEmpty(text)) {
                input.setError("Field must not be empty!");
            } else {
                dialog.cancel();
            }

        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    public interface TextSavedListener {
        void onSave(String value);
    }
}
