package com.mutumbakato.batchmanager.ui.components;

import android.content.Context;
import androidx.appcompat.app.AlertDialog;
import android.util.AttributeSet;

import com.mutumbakato.batchmanager.data.models.BatchUser;

/**
 */
public class RoleTextView extends androidx.appcompat.widget.AppCompatTextView {

    private OnItemSelectedListener onItemSelectedListener;
    private boolean isEnabled = true;

    public RoleTextView(Context context) {
        super(context);
        init();
    }

    public RoleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RoleTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setOnClickListener(view -> showAlert(getText().toString().trim()));
    }

    private void showAlert(String role) {
        if (!isEnabled)
            return;
        String[] roles = BatchUser.ROLES;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setSingleChoiceItems(roles, indexOf(role, roles), (dialog, which) -> {
            if (onItemSelectedListener != null)
                onItemSelectedListener.onItem(roles[which]);
            dialog.dismiss();
        }).create().show();
    }

    private int indexOf(String role, String[] roles) {
        for (int i = 0; i < roles.length; i++) {
            if (roles[i].equals(role))
                return i;
        }
        return 0;
    }

    public void setOnItemSelectedListener(OnItemSelectedListener mListener) {
        this.onItemSelectedListener = mListener;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public interface OnItemSelectedListener {
        void onItem(String item);
    }

}
