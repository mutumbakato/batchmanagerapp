package com.mutumbakato.batchmanager.ui.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.models.Batch;
import com.mutumbakato.batchmanager.fragments.batch.BatchListFragment.OnBatchListInteractionListener;
import com.mutumbakato.batchmanager.utils.DateUtils;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Batch} and makes a call to the
 * specified {@link OnBatchListInteractionListener}.
 */
public class BatchListAdapter extends RecyclerView.Adapter<BatchListAdapter.BatchViewHolder> {

    private final OnBatchListInteractionListener mListener;
    private List<Batch> mBatches;

    public BatchListAdapter(List<Batch> items, OnBatchListInteractionListener listener) {
        mBatches = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public BatchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_batch, parent, false);
        return new BatchViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final BatchViewHolder holder, int position) {
        holder.mItem = mBatches.get(position);
        holder.mSupplierTextView.setText(String.valueOf(mBatches.get(position).getSupplier()));
        holder.mDateTextView.setText(DateUtils.INSTANCE.parseDateToddMMyyyy(mBatches.get(position).getDate()));
        holder.mNameTextView.setText(mBatches.get(position).getName());
        holder.mTypeImage.setImageResource(holder.mItem.getType().equals("Layers") ? R.drawable.ic_type_eggs : holder.mItem.getType().equals("Broilers") ? R.drawable.ic_type_broiler : R.drawable.ic_type_chicken);
        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onBatchListFragmentInteraction(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBatches.size();
    }

    public void updateData(List<Batch> batches) {
        this.mBatches = batches;
        notifyDataSetChanged();
    }

    public class BatchViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final TextView mSupplierTextView;
        final TextView mNameTextView;
        final TextView mDateTextView;
        final ImageView mTypeImage;

        Batch mItem;

        BatchViewHolder(View view) {
            super(view);
            mView = view;
            mSupplierTextView = view.findViewById(R.id.batch_textView_supplier);
            mDateTextView = view.findViewById(R.id.batch_textView_quantity);
            mNameTextView = view.findViewById(R.id.batch_textView_date);
            mTypeImage = view.findViewById(R.id.chicken_type_icon);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mDateTextView.getText() + "'";
        }
    }
}
