package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.fragments.feeds.FeedingListFragmentOld
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [Feeding] and makes a call to the
 */
class FeedingOldRecyclerViewAdapter(private var mFeedings: List<Feeding>?,
                                    private val mListener: FeedingListFragmentOld.OnFeedsItemInteraction,
                                    private val recyclerView: RecyclerView) : RecyclerView.Adapter<FeedingOldRecyclerViewAdapter.ViewHolder>() {

    private var selectedItem = UNSELECTED
    private var role = UserRoles.USER_GUEST

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_feeding_old, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mFeedings!!.size
    }

    fun updateData(feedings: List<Feeding>) {
        selectedItem = UNSELECTED
        mFeedings = feedings
        Collections.sort(mFeedings!!) { feeding1, feeding2 -> feeding2.date.compareTo(feeding1.date) }
        notifyDataSetChanged()
    }

    fun applyPermissions(role: String) {
        this.role = role
        notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView), View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {

        private val mTypeTextView: TextView = mView.findViewById(R.id.death_textView_reason)
        private val mCountTextView: TextView = mView.findViewById(R.id.death_textView_count)

        val mDateTextView: TextView = mView.findViewById(R.id.death_textView_date)
        val expandableLayout: ExpandableLayout = mView.findViewById(R.id.death_details_expansion)
        val mEdit: ImageButton = mView.findViewById(R.id.death_button_edit)
        val mDelete: ImageButton = mView.findViewById(R.id.death_button_delete)

        lateinit var mItem: Feeding

        override fun toString(): String {
            return super.toString() + " '" + mTypeTextView.text + "'"
        }

        override fun onClick(view: View) {
            when (view.id) {
                R.id.death_button_edit -> {
                    selectedItem = UNSELECTED
                    mListener.onEdit(mItem)
                    collapse()
                }
                R.id.death_button_delete -> {
                    mListener.onDelete(mFeedings!![adapterPosition])
                    collapse()
                    selectedItem = UNSELECTED
                }
                else -> {
                    val holder =
                            recyclerView.findViewHolderForAdapterPosition(selectedItem) as ViewHolder?
                    holder?.collapse()
                    if (adapterPosition == selectedItem) {
                        selectedItem = UNSELECTED
                    } else {
                        expand()
                    }
                }
            }
        }

        fun bind(position: Int) {
            mItem = mFeedings!![position]
            mDateTextView.text = DateUtils.parseDateToddMMyyyy(mItem.date)
            mTypeTextView.text = mItem.item
            mCountTextView.text = String.format("%s %s", mItem.quantity.toString(), PreferenceUtils.weightUnits)
            expandableLayout.visibility = View.GONE
            expandableLayout.setOnExpansionUpdateListener(this)
            mDelete.setOnClickListener(this)
            mEdit.setOnClickListener(this)
            mView.setOnClickListener(this)
            mEdit.visibility = if (UserRoles.canEdit(role)) View.VISIBLE else View.GONE
            mDelete.visibility = if (UserRoles.canDelete(role)) View.VISIBLE else View.GONE
        }

        private fun expand() {
            mView.isSelected = true
            expandableLayout.expand()
            selectedItem = adapterPosition
        }

        private fun collapse() {
            mView.isSelected = false
            expandableLayout.collapse()
        }

        override fun onExpansionUpdate(expansionFraction: Float, state: Int) {
            try {
                recyclerView.smoothScrollToPosition(adapterPosition)
            } catch (ignored: Exception) {
            }
        }
    }

    companion object {
        private const val UNSELECTED = -1
    }
}
