package com.mutumbakato.batchmanager.ui

import androidx.lifecycle.LiveData
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchCount
import com.mutumbakato.batchmanager.data.models.EggCount
import com.mutumbakato.batchmanager.data.models.EntryItem
import com.mutumbakato.batchmanager.data.models.exports.ExportConfig
import com.mutumbakato.batchmanager.data.models.exports.ExportData

interface StatisticsContract {

    fun getBatch(batchId: String): LiveData<Batch>

    fun getCount(batchId: String): LiveData<Int>

    fun getBatchCount(batchId: String): LiveData<BatchCount>

    fun getSold(batchId: String): LiveData<Int>

    fun mortality(batchId: String): LiveData<Int>

    fun totalSales(batchId: String): LiveData<Float>

    fun totalExpenses(batchId: String): LiveData<Float>

    fun totalFeeds(batchId: String): LiveData<Float>

    fun totalWater(batchId: String): LiveData<Float>

    fun getEggCount(batchID: String): LiveData<EggCount>

    fun sales(batchId: String): LiveData<List<EntryItem>>

    fun deaths(batchId: String): LiveData<List<EntryItem>>

    fun dailyExpenses(batchID: String): LiveData<List<EntryItem>>

    fun dailyEggs(batchId: String): LiveData<List<EntryItem>>

    fun dailyHenDay(batchId: String): LiveData<List<EntryItem>>

    fun dailyFeeding(batchId: String): LiveData<List<EntryItem>>

    fun dailyWater(batchId: String): LiveData<List<EntryItem>>

    fun dailyAverageWeight(batchId: String): LiveData<List<EntryItem>>

    fun averageWeight(batchId: String): LiveData<Float>

    fun costPerBird(batchId: String): LiveData<Float>

    fun getLastWaterTotal(batchId: String): LiveData<Float>

    fun getLastFeedsTotal(batchId: String): LiveData<Float>

    fun getEggsPercentage(batchId: String): LiveData<Float>

    fun getExportData(config: ExportConfig, onFinish: (data: ExportData) -> Unit, onError: (error: String) -> Unit)

}

