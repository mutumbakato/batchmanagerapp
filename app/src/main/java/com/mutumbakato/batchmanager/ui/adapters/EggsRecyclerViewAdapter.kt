package com.mutumbakato.batchmanager.ui.adapters

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Eggs
import com.mutumbakato.batchmanager.ui.EggsListContract
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [Eggs] and makes a call to the
 */
class EggsRecyclerViewAdapter(private var mEggs: List<Eggs>,
                              private val mRecyclerView: RecyclerView,
                              private val mListener: EggsListContract.View.EggsItemInteractionListener) : RecyclerView.Adapter<EggsRecyclerViewAdapter.ViewHolder>() {

    private var selectedItem = UNSELECTED
    private var role = UserRoles.USER_GUEST

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_eggs, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mEggs.size
    }

    fun updateData(eggs: List<Eggs>) {
        mEggs = eggs
        notifyDataSetChanged()
    }

    fun applyPermissions(role: String) {
        this.role = role
        notifyDataSetChanged()
    }

    inner class ViewHolder internal constructor(private val mView: View) : RecyclerView.ViewHolder(mView),
            View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {

        private val mDateTextView: TextView = mView.findViewById(R.id.eggs_textView_date)
        private val mTreysTextView: TextView = mView.findViewById(R.id.eggs_textView_treys)
        private val mDamageTextView: TextView = mView.findViewById(R.id.eggs_textView_damage)
        private val mPercentageTextView: TextView = mView.findViewById(R.id.eggs_textView_percentage)
        private val mDirection: ImageView = mView.findViewById(R.id.eggs_image_direction)
        private val mEdit: ImageButton = mView.findViewById(R.id.eggs_button_edit)
        private val mDelete: ImageButton = mView.findViewById(R.id.eggs_button_delete)
        private val expandableLayout: ExpandableLayout = mView.findViewById(R.id.eggs_details_expansion)
        private lateinit var mItem: Eggs

        fun bind(position: Int) {
            collapse()
            mItem = mEggs[position]
            mDateTextView.text = DateUtils.parseDateToddMMyyyy(mItem.date)
            mTreysTextView.text = String.format("%s Trays${if (mItem.remainderEggs > 0) ", ${mItem.remainderEggs} Eggs" else ""}", mItem.treys.toString())
            mDamageTextView.text = String.format("%s Damaged", mItem.damage.toString())

            if (position + 1 < mEggs.size) {
                val prev = position + 1
                val diff = mItem.percentage - mEggs[prev].percentage
                val direction = if (diff >= 0) R.drawable.icon_percentage_up else R.drawable.icon_percentage_down
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    mDirection.setImageDrawable(mDirection.context.getDrawable(direction))
                } else {
                    mDirection.setImageResource(direction)
                }
            }

            val percentage = String.format(Locale.ENGLISH, "%.1f", mItem.percentage)
            mPercentageTextView.text = String.format("%s%%", percentage)
            expandableLayout.setOnExpansionUpdateListener(this)

            mView.setOnClickListener(this)
            mDelete.setOnClickListener(this)
            mEdit.setOnClickListener(this)

            mEdit.visibility = if (UserRoles.canEdit(role)) View.VISIBLE else View.GONE
            mDelete.visibility = if (UserRoles.canDelete(role)) View.VISIBLE else View.GONE
        }

        override fun toString(): String {
            return super.toString() + " '" + mTreysTextView.text + "'"
        }

        override fun onClick(view: View) {
            when (view.id) {
                R.id.eggs_button_edit -> {
                    selectedItem = UNSELECTED
                    mListener.onEdit(mItem)
                    collapse()
                }
                R.id.eggs_button_delete -> {
                    mListener.onDelete(mItem, adapterPosition)
                    collapse()
                    selectedItem = UNSELECTED
                }
                else -> {
                    val holder = mRecyclerView.findViewHolderForAdapterPosition(selectedItem) as ViewHolder?
                    holder?.collapse()
                    if (adapterPosition == selectedItem) {
                        selectedItem = UNSELECTED
                    } else {
                        expand()
                    }
                }
            }
        }

        fun expand() {
            mView.isSelected = true
            expandableLayout.expand()
            selectedItem = adapterPosition
        }

        fun collapse() {
            mView.isSelected = false
            expandableLayout.collapse()
        }

        override fun onExpansionUpdate(expansionFraction: Float, state: Int) {
            try {
                mRecyclerView.smoothScrollToPosition(selectedItem)
            } catch (ignore: Exception) {

            }
        }
    }

    companion object {
        private const val UNSELECTED = -1
    }
}
