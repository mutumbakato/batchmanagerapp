package com.mutumbakato.batchmanager.ui.components;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.textfield.TextInputEditText;
import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static android.view.LayoutInflater.from;

public class CurrencySymbolInput extends TextInputEditText implements
        View.OnClickListener, ValidatorEditText {

    private ErrorListener mListener;
    private FragmentManager fragmentManager;
    private CurrencyDialog.OnOptionItemClicked onOptionItemClicked;

    public CurrencySymbolInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        setClickable(true);
        setOnClickListener(this);
        setFocusable(false);
        setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    @Override
    public void onClick(View view) {
        showOptions();
    }

    @Override
    public boolean isValid() {
        if (Validator.INSTANCE.isValidWord(getText().toString().trim())) {
            return true;
        } else {
            if (mListener != null) {
                mListener.onShowError("Invalid Category");
            }
            return false;
        }
    }

    @Override
    public void setOnErrorListener(ErrorListener listener) {
        mListener = listener;
    }

    public void setFragmentManager(FragmentManager manager) {
        fragmentManager = manager;
    }

    public void setOnOptionItemClicked(CurrencyDialog.OnOptionItemClicked onOptionItemClicked) {
        this.onOptionItemClicked = onOptionItemClicked;
    }

    public void showOptions() {

        if (mListener != null) {
            mListener.onRemoveError();
        }

        final CurrencyDialog dialog = new CurrencyDialog();
        dialog.setOnOptionItemClickListener(item -> {
            if (onOptionItemClicked != null)
                onOptionItemClicked.onItem(item);
            dialog.dismiss();
        });

        if (fragmentManager != null) {
            dialog.show(fragmentManager, "");
            dialog.setOnDismissListener(dialogInterface -> {
                if (mListener != null && !Validator.INSTANCE.isValidWord(getText().toString().trim())) {
                    mListener.onShowError("Please select a currency.");
                }
            });
        }
    }

    public static class CurrencyDialog extends DialogFragment {

        private CurrencyAdapter adapter;
        private ListView listView;
        private OnOptionItemClicked mListener;
        private List<Currency> currencies;
        private DialogInterface.OnDismissListener dismissListener;
        private EditText filterEditText;
        private ImageButton close;

        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.currency_list_view, container, false);
            listView = view.findViewById(R.id.options_list);
            filterEditText = view.findViewById(R.id.filter_edit_text);
            close = view.findViewById(R.id.close_currency_list);
            close.setOnClickListener(view12 -> dismiss());

            listView.setAdapter(adapter);
            listView.setOnItemClickListener((adapterView, view1, i, l) -> mListener.onItem(adapter.getItem(i).symbol));
            getDialog().setTitle("Select Currency");

            filterEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    adapter.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            if (dismissListener != null) {
                getDialog().setOnDismissListener(dismissListener);
            }
            return view;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle);
            currencies = getFromAssets(getContext());
            adapter = new CurrencyAdapter(getActivity(), currencies);
        }

        @Override
        public void onPause() {
            super.onPause();
            dismiss();
        }

        @Override
        public void onStart() {
            super.onStart();
            Dialog dialog = getDialog();
            if (dialog != null) {
                int width = ViewGroup.LayoutParams.MATCH_PARENT;
                int height = ViewGroup.LayoutParams.MATCH_PARENT;
                dialog.getWindow().setLayout(width, height);
            }
        }

        void setOnOptionItemClickListener(OnOptionItemClicked listener) {
            mListener = listener;
        }

        void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            dismissListener = onDismissListener;
        }

        private List<Currency> getFromAssets(Context context) {
            List<Currency> countryCodes = new ArrayList<>();
            StringBuilder buf = new StringBuilder();
            InputStream json;
            try {
                json = context.getAssets().open("currencies.json");
                BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
                String str;
                while ((str = in.readLine()) != null) {
                    buf.append(str);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                JSONArray jsonArray = new JSONArray(buf.toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    countryCodes.add(new Currency(
                            jsonArray.getJSONObject(i).getString("name"),
                            jsonArray.getJSONObject(i).getString("symbol"),
                            jsonArray.getJSONObject(i).getString("cc")));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return countryCodes;
        }

        public interface OnOptionItemClicked {
            void onItem(String item);
        }
    }

    public static class CurrencyAdapter extends ArrayAdapter<Currency> {

        private final List<Currency> original;
        private List<Currency> values;
        private Context context;

        CurrencyAdapter(Context context, List<Currency> values) {
            super(context, android.R.layout.simple_list_item_1, values);
            this.values = values;
            this.original = values;
            this.context = context;
        }

        @Override
        public int getCount() {
            return values.size();
        }

        @Override
        public Currency getItem(int i) {
            return values.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @NonNull
        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            TextView itemView = (TextView) from(context).inflate(android.R.layout.simple_list_item_1, viewGroup, false);
            itemView.setText(values.get(i).toString());
            return itemView;
        }

        @NonNull
        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();
                    List<Currency> filteredArrayNames = new ArrayList<>();

                    if (constraint != null && constraint.length() > 0) {
                        constraint = constraint.toString().toLowerCase();
                        for (int i = 0; i < values.size(); i++) {
                            Currency dataNames = values.get(i);
                            if (dataNames.name.toLowerCase().contains(constraint.toString()) || dataNames.cc.toLowerCase().contains(constraint.toString())) {
                                filteredArrayNames.add(dataNames);
                            }
                        }
                    } else {
                        filteredArrayNames = original;
                    }
                    results.count = filteredArrayNames.size();
                    results.values = filteredArrayNames;
                    return results;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    values = (List<Currency>) results.values;
                    notifyDataSetChanged();
                }
            };
        }
    }
}
