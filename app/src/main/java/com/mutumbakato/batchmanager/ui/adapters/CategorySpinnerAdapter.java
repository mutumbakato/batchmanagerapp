package com.mutumbakato.batchmanager.ui.adapters;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.ThemedSpinnerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mutumbakato.batchmanager.data.models.ExpenseCategory;

import java.util.ArrayList;
import java.util.List;

public class CategorySpinnerAdapter extends ArrayAdapter<ExpenseCategory> {

    private final ThemedSpinnerAdapter.Helper mDropDownHelper;
    private List<ExpenseCategory> mCategories;

    public CategorySpinnerAdapter(Context context, ArrayList<ExpenseCategory> categories) {
        super(context, android.R.layout.simple_list_item_1, categories);
        mDropDownHelper = new ThemedSpinnerAdapter.Helper(context);
        mCategories = categories;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            // Inflate the drop down using the helper's LayoutInflater
            LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        } else {
            view = convertView;
        }
        TextView textView = view.findViewById(android.R.id.text1);
        textView.setText(getItem(position).toString());
        return view;
    }

    @Nullable
    @Override
    public ExpenseCategory getItem(int position) {
        return mCategories.get(position);
    }

    @Override
    public int getCount() {
        return mCategories.size();
    }

    public void updateData(List<ExpenseCategory> categories) {
        mCategories = categories;
        notifyDataSetChanged();
    }
}


