package com.mutumbakato.batchmanager.ui.components;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.utils.Validator;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CountryCodeInput extends TextInputEditText implements
        View.OnClickListener, ValidatorEditText {

    private ErrorListener mListener;
    private FragmentManager fragmentManager;

    public CountryCodeInput(Context context, AttributeSet attrs) {
        super(context, attrs);
        setClickable(true);
        setOnClickListener(this);
        setFocusable(false);
        setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_drop_down, 0);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onRemoveError();
        }
        final CountryCodeDialog dialog = new CountryCodeDialog();
        dialog.setOnOptionItemClickListener(item -> {

            setText(item);
            dialog.dismiss();
        });
        if (fragmentManager != null) {
            dialog.show(fragmentManager, "");
            dialog.setOnDismissListener(dialogInterface -> {
                if (mListener != null && !Validator.INSTANCE.isValidWord(getText().toString().trim())) {
                    mListener.onShowError("Please select a category.");
                }
            });
        }
    }

    @Override
    public boolean isValid() {
        if (Validator.INSTANCE.isValidWord(getText().toString().trim())) {
            return true;
        } else {
            if (mListener != null) {
                mListener.onShowError("Invalid Category");
            }
            return false;
        }
    }

    @Override
    public void setOnErrorListener(ErrorListener listener) {
        mListener = listener;
    }

    public void setFragmentManager(FragmentManager manager) {
        fragmentManager = manager;
    }

    public static class CountryCodeDialog extends DialogFragment {

        private OptionsAdapter adapter;
        private ListView listView;
        private OnOptionItemClicked mListener;
        private List<CountryCode> codes;
        private DialogInterface.OnDismissListener dismissListener;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.options_list_view, container, false);

            listView = view.findViewById(R.id.options_list);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener((adapterView, view1, i, l) -> mListener.onItem(codes.get(i).code));

            getDialog().setTitle("Select Code");
            final EditText newCodeInput = view.findViewById(R.id.new_category_input);
            newCodeInput.setImeOptions(EditorInfo.IME_ACTION_DONE);
            newCodeInput.setSingleLine(true);
            newCodeInput.setVisibility(GONE);

            if (dismissListener != null) {
                getDialog().setOnDismissListener(dismissListener);
            }
            return view;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            codes = getFromAssets(getContext());
            adapter = new OptionsAdapter(getActivity(), getCodes(codes));
        }

        public String[] getCodes(List<CountryCode> categories) {
            Collections.sort(categories, (t, t1) -> t.name.compareTo(t1.name));

            List<String> c = new ArrayList<>();
            String[] categs = new String[categories.size()];
            for (CountryCode category : categories) {
                c.add(category.name);
            }
            return c.toArray(categs);
        }

        void setOnOptionItemClickListener(OnOptionItemClicked listener) {
            mListener = listener;
        }

        void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            dismissListener = onDismissListener;
        }

        private List<CountryCode> getFromAssets(Context context) {

            List<CountryCode> countryCodes = new ArrayList<>();
            StringBuilder buf = new StringBuilder();
            InputStream json;

            try {
                json = context.getAssets().open("codes.json");
                BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));
                String str;
                while ((str = in.readLine()) != null) {
                    buf.append(str);
                }
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                JSONArray jsonArray = new JSONArray(buf.toString());
                for (int i = 0; i < jsonArray.length(); i++) {
                    countryCodes.add(new CountryCode(jsonArray.getJSONObject(i).getString("name"),
                            jsonArray.getJSONObject(i).getString("dial_code")));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
            return countryCodes;
        }

        private int ug(ArrayList<CountryCode> codes) {
            for (int i = 0; i < codes.size(); i++) {
                if (codes.get(i).name.equals("Uganda"))
                    return i;
            }
            return 0;
        }

        public interface OnOptionItemClicked {
            void onItem(String item);
        }
    }


}
