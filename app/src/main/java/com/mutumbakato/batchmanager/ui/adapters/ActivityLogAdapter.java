package com.mutumbakato.batchmanager.ui.adapters;

import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.models.ActivityLog;
import com.mutumbakato.batchmanager.utils.DateUtils;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ActivityLogAdapter extends RecyclerView.Adapter<ActivityLogAdapter.ViewHolder> {

    private List<ActivityLog> mValues;

    public ActivityLogAdapter(List<ActivityLog> items, int newCount) {
        mValues = items;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_activity_log, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        Spanned log = Html.fromHtml(holder.mItem.getActor() + " <strong>" + holder.mItem.getAction() + "</strong>  "
                + holder.mItem.getDescription() + ": <strong>" + holder.mItem.getBatchName() + "</strong>");
        holder.mIdView.setText(log);
        holder.mContentView.setText(DateUtils.INSTANCE.parseDateToddMMyyyyTT(holder.mItem.getTime()));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void addItems(List<ActivityLog> logs, int newCount) {
        this.mValues = logs;
        Set<ActivityLog> clean = new HashSet<>(mValues);
        mValues.clear();
        mValues.addAll(clean);
        Collections.sort(mValues, (log, t1) -> t1.getTime().compareTo(log.getTime()));
        notifyDataSetChanged();
    }

    public String getLastDate() {
        return mValues.get(mValues.size() - 1).getTime();
    }

    public void topUp(List<ActivityLog> logs) {
        this.mValues.addAll(logs);
        Set<ActivityLog> clean = new HashSet<>(mValues);
        mValues.clear();
        mValues.addAll(clean);
        Collections.sort(mValues, (log, t1) -> t1.getTime().compareTo(log.getTime()));
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        final TextView mIdView;
        final TextView mContentView;
        public ActivityLog mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = view.findViewById(R.id.item_number);
            mContentView = view.findViewById(R.id.content);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}

