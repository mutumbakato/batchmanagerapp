package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.Features
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.fragments.batch.BatchHomeFragment

class BatchFeaturesAdapter(private var mValues: ArrayList<Features.FeatureItem>?,
                           private val mListener: BatchHomeFragment.OnFeaturesFragmentInteractionListener?,
                           private val doc: Int,
                           private val batchType: String)
    : RecyclerView.Adapter<BatchFeaturesAdapter.ViewHolder>() {

    private var count = 0
    private var eggs = 0f
    private var feeds = 0f
    private var water = 0f
    private var weight = 0f

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_feature_grid, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mValues!!.size
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {

        private val mContentView: TextView = mView.findViewById(R.id.name)
        private val mSummary: TextView = mView.findViewById(R.id.feature_summary)
        private val mIcon: ImageView = mView.findViewById(R.id.feature_icon)
        private lateinit var mItem: Features.FeatureItem

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }

        fun bind(position: Int) {
            mItem = mValues!![position]
            mContentView.text = mValues!![position].name
            mIcon.setImageResource(mValues!![position].icon)
            when (mItem.name) {
                "Mortality" -> {
                    mSummary.text = String.format("%s/%d Alive", count.toString(), doc)
                }
                "Vaccination" -> {
                    mSummary.text = "---"
                }
                "Eggs" -> {
                    mSummary.text = "$eggs%"
                }
                "Feeding" -> {
                    mSummary.text = String.format("%.0f %s/bird", feeds, PreferenceUtils.weightUnits)
                }
                "Body Weight" -> {
                    mSummary.text = String.format("Avg: %.0f %s", weight, PreferenceUtils.weightUnits)
                }
                "Water" -> {
                    mSummary.text = String.format("%.0f ml/bird", water)
                }
            }
            mView.setOnClickListener {
                mListener?.onFeaturesFragmentInteraction(mItem)
            }
        }
    }

    fun setBatch(batch: Batch) {
        mValues = ArrayList(mValues!!)
        if (batch.type == "Broilers") {
            for (feat in mValues!!) {
                if (feat.name == "Eggs")
                    mValues!!.remove(feat)
            }
        }
        notifyDataSetChanged()
    }

    fun setRemaining(count: Int) {
        this.count = count
        notifyDataSetChanged()
    }

    fun setEggsCount(count: Float) {
        eggs = count
        notifyDataSetChanged()
    }

    fun setFeeds(quantity: Float) {
        feeds = quantity
        notifyDataSetChanged()
    }

    fun setWater(quantity: Float) {
        water = quantity
        notifyDataSetChanged()
    }

    fun setWeight(quantity: Float) {
        weight = quantity
        notifyDataSetChanged()
    }
}
