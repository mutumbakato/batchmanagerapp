package com.mutumbakato.batchmanager.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.models.Death;
import com.mutumbakato.batchmanager.ui.DeathListContract;
import com.mutumbakato.batchmanager.utils.DateUtils;
import com.mutumbakato.batchmanager.utils.UserRoles;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.Collections;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Death} and makes a call to the
 */
public class DeathRecyclerViewAdapter extends RecyclerView.Adapter<DeathRecyclerViewAdapter.ViewHolder> {

    private static final int UNSELECTED = -1;
    private final DeathListContract.View.OnDeathItemInteraction mListener;
    private List<Death> mDeaths;
    private RecyclerView recyclerView;
    private int selectedItem = UNSELECTED;
    private String role = UserRoles.USER_GUEST;

    public DeathRecyclerViewAdapter(List<Death> items, DeathListContract.View.OnDeathItemInteraction listener, RecyclerView recyclerView) {
        mDeaths = items;
        mListener = listener;
        this.recyclerView = recyclerView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_death, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mDeaths.size();
    }

    public void updateData(List<Death> deaths) {
        selectedItem = UNSELECTED;
        mDeaths = deaths;
        Collections.sort(mDeaths, (death1, death2) -> death2.getDate().compareTo(death1.getDate()));
        notifyDataSetChanged();
    }

    public void applyPermissions(String role) {
        this.role = role;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {
        final View mView;
        final TextView mDateTextView;
        final TextView mReasonTextView;
        final TextView mCountTextView;
        final ExpandableLayout expandableLayout;
        final ImageButton mEdit, mDelete;
        Death mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mDateTextView = view.findViewById(R.id.death_textView_date);
            mReasonTextView = view.findViewById(R.id.death_textView_reason);
            mCountTextView = view.findViewById(R.id.death_textView_count);
            expandableLayout = view.findViewById(R.id.death_details_expansion);
            mEdit = view.findViewById(R.id.death_button_edit);
            mDelete = view.findViewById(R.id.death_button_delete);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mReasonTextView.getText() + "'";
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.death_button_edit: {
                    selectedItem = UNSELECTED;
                    mListener.onEdit(mItem);
                    collapse();
                    break;
                }
                case R.id.death_button_delete: {
                    mListener.onDelete(mDeaths.get(getAdapterPosition()));
                    collapse();
                    selectedItem = UNSELECTED;
                }
                break;
                default: {
                    ViewHolder holder = (ViewHolder) recyclerView.
                            findViewHolderForAdapterPosition(selectedItem);
                    if (holder != null) {
                        holder.collapse();
                    }
                    if (getAdapterPosition() == selectedItem) {
                        selectedItem = UNSELECTED;
                    } else {
                        expand();
                    }
                }
            }
        }

        public void bind(int position) {
            collapse();
            mItem = mDeaths.get(position);
            int count = mDeaths.get(position).getCount();
            mDateTextView.setText(DateUtils.INSTANCE.parseDateToddMMyyyy(mDeaths.get(position).getDate()));
            mReasonTextView.setText(String.format("Cause: %s", mDeaths.get(position).getComment()));
            mCountTextView.setText(count < 10 ? ("0" + count) : String.valueOf(count));
            expandableLayout.setOnExpansionUpdateListener(this);
            mDelete.setOnClickListener(this);
            mEdit.setOnClickListener(this);
            mView.setOnClickListener(this);
            mDelete.setVisibility(UserRoles.INSTANCE.canDelete(role) ? View.VISIBLE : View.GONE);
            mEdit.setVisibility(UserRoles.INSTANCE.canEdit(role) ? View.VISIBLE : View.GONE);
        }

        private void expand() {
            mView.setSelected(true);
            expandableLayout.expand();
            selectedItem = getAdapterPosition();
        }

        private void collapse() {
            mView.setSelected(false);
            expandableLayout.collapse();
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            try {
                recyclerView.smoothScrollToPosition(getAdapterPosition());
            } catch (Exception ignored) {
            }
        }
    }
}
