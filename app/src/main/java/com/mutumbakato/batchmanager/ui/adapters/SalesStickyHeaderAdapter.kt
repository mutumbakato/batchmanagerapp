package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.ui.SalesListContract
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import net.cachapa.expandablelayout.ExpandableLayout
import org.zakariya.stickyheaders.SectioningAdapter
import org.zakariya.stickyheaders.SectioningAdapter.ItemViewHolder
import java.util.*

class SalesStickyHeaderAdapter(mSales: HashMap<String, List<Sale>>,
                               private var mCategories: List<String>?,
                               private val mListener: SalesListContract.View.SalesItemInteractionListener,
                               private val recyclerView: RecyclerView) : SectioningAdapter() {

    private var mSales: Map<String, List<Sale>>
    private var selectedItem = UNSELECTED
    private var role = UserRoles.USER_ADMIN

    init {
        this.mSales = mSales
    }

    override fun onCreateGhostHeaderViewHolder(parent: ViewGroup): GhostHeaderViewHolder {
        val ghostView = View(parent.context)
        ghostView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        return GhostHeaderViewHolder(ghostView)
    }

    override fun getNumberOfSections(): Int {
        return mCategories!!.size
    }

    override fun getNumberOfItemsInSection(sectionIndex: Int): Int {
        return (mSales[mCategories!![sectionIndex]] ?: error("")).size
    }

    override fun doesSectionHaveHeader(sectionIndex: Int): Boolean {
        return true
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup, headerType: Int): SectioningAdapter.HeaderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.section_header_item, parent, false)
        return HeaderViewHolder(v)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, itemType: Int): SectioningAdapter.ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.fragment_sales, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindHeaderViewHolder(viewHolder: SectioningAdapter.HeaderViewHolder?, sectionIndex: Int,
                                        headerType: Int) {
        val headerViewHolder = viewHolder as HeaderViewHolder?
        headerViewHolder!!.titleTextView.text = DateUtils.parseDateToddMMyyyy(mCategories!![sectionIndex])
        headerViewHolder.totalTextView.text = String.format("%s %s", PreferenceUtils.currency, getSectionTotal(mCategories!![sectionIndex]))
    }

    private fun getSectionTotal(s: String): String {
        var total = 0f
        for (sale in mSales[s] ?: error(""))
            total += sale.totalAmount
        return Currency.format(total)
    }

    override fun onBindItemViewHolder(viewHolder: SectioningAdapter.ItemViewHolder?, sectionIndex: Int,
                                      itemIndex: Int, itemType: Int) {
        val itemViewHolder = viewHolder as ItemViewHolder?
        itemViewHolder!!.bind(mCategories!![sectionIndex], itemIndex)
    }

    fun updateData(sales: Map<String, List<Sale>>, categories: List<String>) {
        selectedItem = UNSELECTED
        mSales = sales
        mCategories = categories
        notifyAllSectionsDataSetChanged()
    }

    fun applyPermissions(role: String) {
        this.role = role
        notifyDataSetChanged()
    }

    private inner class HeaderViewHolder internal constructor(itemView: View) : SectioningAdapter.HeaderViewHolder(itemView) {
        val titleTextView: TextView = itemView.findViewById(R.id.expenses_header_title)
        val totalTextView: TextView = itemView.findViewById(R.id.expenses_header_total)
    }

    inner class ItemViewHolder private constructor(private val mView: View) : SectioningAdapter.ItemViewHolder(mView), View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {

        private val mItemTextView: TextView = mView.findViewById(R.id.sales_textView_item)
        private val mTotalTextView: TextView = mView.findViewById(R.id.sales_textView_total)
        private val mRateTextView: TextView = mView.findViewById(R.id.sales_textView_rate)
        private val mEdit: ImageButton = mView.findViewById(R.id.sales_button_edit)
        private val mDelete: ImageButton = mView.findViewById(R.id.sales_button_delete)
        private val expandableLayout: ExpandableLayout = mView.findViewById(R.id.sales_details_expansion)
        private val mDivider: View = mView.findViewById(R.id.sales_divider)
        private var mItem: Sale? = null

        override fun toString(): String {
            return super.toString() + " '" + mItemTextView.text + "'"
        }

        fun bind(section: String, position: Int) {
            collapse()
            mItem = (mSales[section] ?: error(""))[position]
            var item = mItem!!.item
            item = if (item.equals("Eggs", ignoreCase = true)) "Treys" else item
            mItemTextView.text = String.format("%s %s", mItem!!.quantity.toString(), item)
            mTotalTextView.text = Currency.format(mItem!!.quantity * mItem!!.rate)
            mRateTextView.text = String.format("Rate: %s %s", PreferenceUtils.currency, Currency.format(mItem!!.rate))

            expandableLayout.setOnExpansionUpdateListener(this)
            mView.setOnClickListener(this)
            mEdit.setOnClickListener(this)
            mDelete.setOnClickListener(this)

            mEdit.visibility = if (UserRoles.canEdit(role)) View.VISIBLE else View.GONE
            mDelete.visibility = if (UserRoles.canDelete(role)) View.VISIBLE else View.GONE

            if (position == (mSales[section] ?: error("")).size - 1) {
                mDivider.visibility = View.GONE
            } else {
                mDivider.visibility = View.VISIBLE
            }
        }

        fun expand() {
            mView.isSelected = true
            expandableLayout.expand()
            selectedItem = adapterPosition
        }

        fun collapse() {
            mView.isSelected = false
            expandableLayout.collapse()
        }

        override fun onClick(view: View) {
            when (view.id) {
                R.id.sales_button_edit -> {
                    selectedItem = UNSELECTED
                    mListener.onEdit(mItem)
                    collapse()
                }
                R.id.sales_button_delete -> {
                    mListener.onDelete(mItem)
                    collapse()
                    selectedItem = UNSELECTED
                }
                else -> {
                    val holder =
                            recyclerView.findViewHolderForAdapterPosition(selectedItem) as ItemViewHolder?
                    holder?.collapse()
                    if (adapterPosition == selectedItem) {
                        selectedItem = UNSELECTED
                    } else {
                        expand()
                    }
                }
            }
        }

        override fun onExpansionUpdate(expansionFraction: Float, state: Int) {
            try {
                recyclerView.smoothScrollToPosition(adapterPosition)
            } catch (ignore: Exception) {
            }

        }
    }

    companion object {
        private const val UNSELECTED = -1
    }
}
