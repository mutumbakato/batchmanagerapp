package com.mutumbakato.batchmanager.ui

import com.mutumbakato.batchmanager.data.models.Death
import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter

interface DeathListContract {

    interface View : BaseView<Presenter> {

        fun showProgress(active: Boolean)

        fun showDeaths(deaths: List<Death>)

        fun showSectionedDeaths(deaths: Map<Int, List<Death>>)

        fun showLoadingDeathError(message: String)

        fun showNoDeath()

        fun showSuccessfullySavedMessage()

        fun showWeek(week: Int)

        fun showConfirmDelete(death: Death, mills: Int)

        fun showDeleteUndoItem(death: Death)

        fun showMortalityRate(count: String)

        fun showEdit(id: String, date: String)

        fun applyPermissions(role: String)

        fun toggleAdapter()

        interface ListInteractionListener {
            fun add(date: String)

            fun showForm(`is`: String, date: String)

            fun showTotal(total: String)

            fun showEmpty(isVisible: Boolean)

            fun showProgress(isLoading: Boolean)

            fun applyPermissions(role: String)
        }

        interface OnDeathItemInteraction {

            fun onAdd(date: String)

            fun onEdit(death: Death)

            fun onDelete(death: Death)
        }

    }

    interface Presenter : BasePresenter {

        fun getBatchId(): String

        fun result(requestCode: Int, resultCode: Int)

        fun loadDeath(forceUpdate: Boolean, showProgress: Boolean)

        fun goToWeek(week: Int)

        fun refreshDeath()

        fun deleteDeath(death: Death)

        fun delete(death: Death)

        fun edit(death: Death)

        fun toggleAdapter(type: Int)
    }
}

