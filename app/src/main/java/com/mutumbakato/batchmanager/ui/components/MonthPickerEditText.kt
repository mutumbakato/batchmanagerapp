package com.mutumbakato.batchmanager.ui.components

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.Validator

private var months = arrayOf(DateUtils.thisMonth())

class MonthPickerEditText(context: Context, attrs: AttributeSet) : AppCompatEditText(context, attrs),
        View.OnClickListener, ValidatorEditText {

    private var mListener: ValidatorEditText.ErrorListener? = null
    private var fragmentManager: FragmentManager? = null
    private var onMonthPicked: ((date: String) -> Unit)? = null

    init {
        isClickable = true
        setOnClickListener(this)
        isFocusable = false
        setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_drop_down, 0)
        setText(DateUtils.formatMonth(DateUtils.thisMonth()))
    }

    override fun onClick(view: View) {
        if (mListener != null) {
            mListener!!.onRemoveError()
        }
        val dialog = ExpenseCategoryDialog()
        dialog.setOnOptionItemClickListener(object : OnOptionItemClicked {
            override fun onItem(item: String) {
                isFocusable = false
                setText(DateUtils.formatMonth(item))
                onMonthPicked?.let { it(item) }
                dialog.dismiss()
            }
        })
        dialog.setOnDismissListener(DialogInterface.OnDismissListener {
            if (!Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
                if (mListener != null) {
                    mListener!!.onShowError("Please select an item")
                }
            }
        })
        if (fragmentManager != null) {
            dialog.show(fragmentManager!!, "")
        }
    }

    override fun isValid(): Boolean {
        if (!Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
            if (mListener != null) {
                mListener!!.onShowError("Please select an item")
            }
            return false
        }
        return true
    }

    override fun setOnErrorListener(listener: ValidatorEditText.ErrorListener) {
        mListener = listener
    }

    fun setFragmentManager(manager: FragmentManager) {
        fragmentManager = manager
    }

    fun setOnMonthPicked(onPick: (date: String) -> Unit) {
        onMonthPicked = onPick
    }

    fun setMonths(m: Array<String>) {
        months = m
    }

    class ExpenseCategoryDialog : DialogFragment() {

        private var adapter: OptionsAdapter? = null
        private var mListener: OnOptionItemClicked? = null
        private var onDismissListener: DialogInterface.OnDismissListener? = null

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.options_list_view, container, false)
            val listView = view.findViewById<ListView>(R.id.options_list)
            listView.adapter = adapter
            listView.setOnItemClickListener { _, _, i, _ ->
                if (mListener != null)
                    mListener!!.onItem(months[i])
            }
            dialog?.setTitle("Select Month")
            view.findViewById<View>(R.id.option_input_layout).visibility = View.GONE
            if (onDismissListener != null) {
                dialog?.setOnDismissListener(onDismissListener)
            }
            return view
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            adapter = OptionsAdapter(activity, getMonths(months))
        }

        private fun getMonths(months: Array<String>): Array<String> {
            val mm = months.map {
                DateUtils.formatMonth(it)
            }
            return mm.toTypedArray()
        }

        internal fun setOnOptionItemClickListener(listener: OnOptionItemClicked) {
            mListener = listener
        }

        internal fun setOnDismissListener(onDismissListener: DialogInterface.OnDismissListener) {
            this.onDismissListener = onDismissListener
        }
    }

    interface OnOptionItemClicked {
        fun onItem(item: String)
    }
}
