package com.mutumbakato.batchmanager.ui;


import com.mutumbakato.batchmanager.fragments.BaseView;
import com.mutumbakato.batchmanager.presenters.BasePresenter;

public interface SalesFormContract {

    interface View extends BaseView<Presenter> {
        void setDate(String date);

        void setItem(String item);

        void setRate(String rate);

        void setQuantity(String quantity);

        void setTotal(String total);

        void showExpenseList();

        void clearInputs();

        void showError();

        void toggleExpantion(boolean expand);

        interface FormInteractionListener {
            void onDoneEditing();

            void onStartEditing();
        }
    }

    interface Presenter extends BasePresenter {
        void save(String date, String item, float rate, int quantity);

        boolean isNew();

        void populate();

        void startEditing(String id);

        void stopEditing();


    }


}

