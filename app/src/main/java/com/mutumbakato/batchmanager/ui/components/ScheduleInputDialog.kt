package com.mutumbakato.batchmanager.ui.components

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.mutumbakato.batchmanager.R
import kotlinx.android.synthetic.main.schedule_input_dialog.*

/**
 * Created by cato on 11/27/17.
 */

class ScheduleInputDialog : DialogFragment(), View.OnClickListener {

    private var mListener: OnSubmitSchedule? = null
    private var mTitle: String? = null
    private var mDescription: String? = null

    private val isDataValid: Boolean
        get() = !TextUtils.isEmpty(schedule_description_input.text.toString().trim { it <= ' ' }) && !TextUtils.isEmpty(schedule_title_input.text.toString().trim { it <= ' ' })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FullScreenDialogStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.schedule_input_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.title = context?.getString(R.string.new_schedule)
        toolbar.setNavigationIcon(R.drawable.ic_action_close)
        toolbar.setNavigationOnClickListener {
            dismiss()
        }

        schedule_submit.setOnClickListener(this)
        schedule_cancel.setOnClickListener(this)
        schedule_delete.setOnClickListener(this)

        if (mTitle != null)
            schedule_title_input.setText(mTitle)

        if (mDescription != null)
            schedule_description_input.setText(mDescription)

        if (mDescription != null || mTitle != null)
            schedule_delete.visibility = View.VISIBLE

    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.schedule_submit -> {
                if (isDataValid && mListener != null)
                    mListener!!.submit(schedule_title_input.text.toString().trim { it <= ' ' }, schedule_description_input.text.toString().trim { it <= ' ' })
            }
            R.id.schedule_cancel -> {
                dismiss()
            }
            R.id.schedule_delete -> run {
                if (mTitle != null || mDescription != null && mListener != null) {
                    mListener!!.delete()
                }
            }
            else -> {
            }
        }
    }

    fun setListener(listener: OnSubmitSchedule) {
        mListener = listener
    }

    fun edit(tt: String, dd: String) {
        mTitle = tt
        mDescription = dd
        if (schedule_description_input != null) {
            schedule_description_input.setText(dd)
            schedule_delete.visibility = View.VISIBLE
        }
        if (schedule_title_input != null) {
            schedule_title_input.setText(dd)
            schedule_delete.visibility = View.VISIBLE
        }
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    interface OnSubmitSchedule {
        fun submit(title: String, description: String)
        fun delete()
    }
}
