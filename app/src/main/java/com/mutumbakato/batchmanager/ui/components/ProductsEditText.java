package com.mutumbakato.batchmanager.ui.components;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.utils.Validator;

public class ProductsEditText extends androidx.appcompat.widget.AppCompatEditText implements
        View.OnClickListener, ValidatorEditText {

    private ValidatorEditText.ErrorListener mListener;
    private FragmentManager fragmentManager;

    public ProductsEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setClickable(true);
        setOnClickListener(this);
        setFocusable(false);
        setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_drop_down, 0);
    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onRemoveError();
        }
        final ExpenseCategoryDialog dialog = new ExpenseCategoryDialog();
        dialog.setOnOptionItemClickListener(item -> {
            setFocusable(false);
            setText(item);
            dialog.dismiss();
        });
        dialog.setOnDismissListener(dialogInterface -> {
            if (!Validator.INSTANCE.isValidWord(getText().toString().trim())) {
                if (mListener != null) {
                    mListener.onShowError("Please select a category");
                }
            }
        });

        if (fragmentManager != null) {
            dialog.show(fragmentManager, "");
        }
    }

    @Override
    public boolean isValid() {
        if (!Validator.INSTANCE.isValidWord(getText().toString().trim())) {
            if (mListener != null) {
                mListener.onShowError("Please select a product");
            }
            return false;
        }
        return true;
    }

    @Override
    public void setOnErrorListener(ErrorListener listener) {
        mListener = listener;
    }

    public void setFragmentManager(FragmentManager manager) {
        fragmentManager = manager;
    }

    public static class ExpenseCategoryDialog extends DialogFragment {
        private OptionsAdapter adapter;
        private ListView listView;
        private OnOptionItemClicked mListener;
        private String[] products = {"Chicken", "Eggs", "Others"};
        private DialogInterface.OnDismissListener onDismissListener;

        @Nullable
        @Override
        public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.options_list_view, container, false);
            listView = view.findViewById(R.id.options_list);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener((adapterView, view1, i, l) -> {
                if (mListener != null)
                    mListener.onItem(products[i]);
            });
            getDialog().setTitle("Select Product");
            view.findViewById(R.id.option_input_layout).setVisibility(GONE);
            if (onDismissListener != null) {
                getDialog().setOnDismissListener(onDismissListener);
            }

            return view;
        }

        @Override
        public void onCreate(@Nullable Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            adapter = new OptionsAdapter(getActivity(), products);
        }

        void setOnOptionItemClickListener(OnOptionItemClicked listener) {
            mListener = listener;
        }

        void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
            this.onDismissListener = onDismissListener;
        }

        public interface OnOptionItemClicked {
            void onItem(String item);
        }
    }

}
