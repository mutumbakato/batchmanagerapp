package com.mutumbakato.batchmanager.ui


import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter
import com.mutumbakato.batchmanager.utils.DateUtils

interface DeathFormContract {

    interface View : BaseView<Presenter> {

        fun setDate(date: String)

        fun setCount(count: String)

        fun setComment(comment: String)

        fun setLoadingError()

        fun showDateError(error: String)

        fun showCountError(error: String)

        fun showCommentError(error: String)

        fun toggleExpansion(expand: Boolean)

        fun clearInputs()

        interface FormInteractionListener {
            fun onDoneEditing()

            fun onStartEditing()
        }
    }

    interface Presenter : BasePresenter {

        val isNew: Boolean

        fun populate()

        fun save(date: String, count: Int, comment: String)

        fun startEditing(id: String?, date: String)

        fun stopEditing()

    }

}
