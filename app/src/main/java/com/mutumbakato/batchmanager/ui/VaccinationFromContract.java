package com.mutumbakato.batchmanager.ui;

import com.mutumbakato.batchmanager.fragments.BaseView;
import com.mutumbakato.batchmanager.presenters.BasePresenter;

/**
 * Created by cato on 8/8/17.
 */
public interface VaccinationFromContract {

    interface View extends BaseView<Presenter> {

        void showEmptyVaccinationError();

        void showVaccinationList();

        void setVaccine(String name);

        void setDisease(String description);

        void setMethod(String rate);

        void setAge(String age);

        void setFrom(String from);

        void setThenEvery(String every);

        void setDescription(String description);

    }

    interface Presenter extends BasePresenter {


        void saveVaccination(String age,
                             String thenEvery, String infection, String vaccine, String method, boolean recurrent);

        void deleteVaccination(String vaccinationId);

        void populateVaccination();

    }
}
