package com.mutumbakato.batchmanager.ui;

import com.mutumbakato.batchmanager.data.models.Eggs;
import com.mutumbakato.batchmanager.fragments.BaseView;
import com.mutumbakato.batchmanager.presenters.BasePresenter;

import java.util.List;

public interface EggsListContract {


    interface View extends BaseView<EggsListContract.Presenter> {

        void setLoadingIndicator(boolean active);

        void showEggs(List<Eggs> eggs);

        void showNoEggs();

        void showSuccessfullySavedMessage();

        void showConfirmDelete(Eggs eggs, int mills);

        void showEdit(String id);

        void showTotalEggs(String totals);

        void applyPermissions(String userAdmin);

        interface ListInteractionListener {
            void showForm(String is);

            void showTotal(String total);

            void showEmpty(boolean visible);

            void showProgress(boolean isLoading);

            void applyPermissions(String role);
        }

        interface EggsItemInteractionListener {
            void onDelete(Eggs eggs, int position);

            void onEdit(Eggs eggs);
        }

    }

    interface Presenter extends BasePresenter {

        void result(int requestCode, int resultCode);

        void loadEggs(boolean forceUpdate);

        void deleteEggs(Eggs eggs);

        void delete(Eggs eggs);

        void edit(Eggs eggs);

    }
}
