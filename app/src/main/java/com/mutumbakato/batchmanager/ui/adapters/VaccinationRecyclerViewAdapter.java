package com.mutumbakato.batchmanager.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.models.Feeding;
import com.mutumbakato.batchmanager.data.models.Vaccination;
import com.mutumbakato.batchmanager.ui.VaccinationContract;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Feeding} and makes a call to the
 */
public class VaccinationRecyclerViewAdapter extends RecyclerView.Adapter<VaccinationRecyclerViewAdapter.ViewHolder> {

    private final VaccinationContract.View.ListInteractionListener mListener;
    private List<Vaccination> vaccinations;

    public VaccinationRecyclerViewAdapter(List<Vaccination> items, VaccinationContract.View.ListInteractionListener listener) {
        vaccinations = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_vaccination_schedule_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return vaccinations.size();
    }

    public void updateData(List<Vaccination> feedings) {
        vaccinations = feedings;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        final View mView;
        final TextView mAgeTextView;
        final TextView mDiseaseTextView;
        final TextView mMethod;
        Vaccination mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mAgeTextView = view.findViewById(R.id.vaccination_age_textView);
            mDiseaseTextView = view.findViewById(R.id.total_quantity_textView);
            mMethod = view.findViewById(R.id.date_textView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mAgeTextView.getText() + "'";
        }

        public void bind(int position) {
            mItem = vaccinations.get(position);
            int age = mItem.getAge();
            mDiseaseTextView.setText(mItem.getInfection());
            mAgeTextView.setText(age + (age == 1 ? " Week" : " Weeks"));
            mMethod.setText(mItem.getMethod());
            mView.setOnClickListener(view -> {
                if (mListener != null)
                    mListener.edit(mItem.getScheduleId(), mItem.getId());
            });
        }
    }
}
