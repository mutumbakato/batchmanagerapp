package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.ui.SalesListContract
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import kotlinx.android.synthetic.main.fragment_sales.view.*
import net.cachapa.expandablelayout.ExpandableLayout

class SalesRecyclerAdapter(private var mSales: List<Sale>,
                           private val mListener: SalesListContract.View.SalesItemInteractionListener,
                           private val recyclerView: RecyclerView) : RecyclerView.Adapter<SalesRecyclerAdapter.ItemViewHolder>() {

    private var selectedItem = UNSELECTED
    private var role = UserRoles.USER_ADMIN

    fun updateData(expenses: List<Sale>) {
        mSales = expenses
        notifyDataSetChanged()
    }

    fun applyPermissions(role: String) {
        this.role = role
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.fragment_sales, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mSales.size
    }

    inner class ItemViewHolder internal constructor(private val mView: View) : RecyclerView.ViewHolder(mView),
            View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {

        private val mItemTextView: TextView = mView.findViewById(R.id.sales_textView_item)
        private val mTotalTextView: TextView = mView.findViewById(R.id.sales_textView_total)
        private val mRateTextView: TextView = mView.findViewById(R.id.sales_textView_rate)
        private val mSalesDate: TextView = mView.sales_textView_date
        private val mEdit: ImageButton = mView.findViewById(R.id.sales_button_edit)
        private val mDelete: ImageButton = mView.findViewById(R.id.sales_button_delete)
        private val expandableLayout: ExpandableLayout = mView.findViewById(R.id.sales_details_expansion)
        private val mDivider: View = mView.findViewById(R.id.sales_divider)
        private var mItem: Sale? = null

        override fun toString(): String {
            return super.toString() + " '" + mItemTextView.text + "'"
        }

        fun bind(position: Int) {
            collapse()
            mItem = mSales[position]
            var item = mItem!!.item

            item = if (item.equals("Eggs", ignoreCase = true)) "Treys" else item
            mItemTextView.text = String.format("%s %s", mItem!!.quantity.toString(), item)
            mTotalTextView.text = "${PreferenceUtils.currency} ${Currency.format(mItem!!.totalAmount)}"
            mRateTextView.text = String.format("Rate: %s %s", PreferenceUtils.currency, Currency.format(mItem!!.rate))
            mSalesDate.text = DateUtils.parseDateToddMMyyyy(mItem!!.date)

            expandableLayout.setOnExpansionUpdateListener(this)
            mView.setOnClickListener(this)
            mEdit.setOnClickListener(this)
            mDelete.setOnClickListener(this)

            mEdit.visibility = if (UserRoles.canEdit(role)) View.VISIBLE else View.GONE
            mDelete.visibility = if (UserRoles.canDelete(role)) View.VISIBLE else View.GONE

            if (position == mSales.size - 1) {
                mDivider.visibility = View.GONE
            } else {
                mDivider.visibility = View.VISIBLE
            }
        }

        fun expand() {
            mView.isSelected = true
            expandableLayout.expand()
            selectedItem = adapterPosition
        }

        fun collapse() {
            mView.isSelected = false
            expandableLayout.collapse()
        }

        override fun onClick(view: View) {
            when (view.id) {
                R.id.sales_button_edit -> {
                    selectedItem = UNSELECTED
                    mListener.onEdit(mItem)
                    collapse()
                }
                R.id.sales_button_delete -> {
                    mListener.onDelete(mItem)
                    collapse()
                    selectedItem = UNSELECTED
                }
                else -> {
                    val holder =
                            recyclerView.findViewHolderForAdapterPosition(selectedItem) as ItemViewHolder?
                    holder?.collapse()
                    if (adapterPosition == selectedItem) {
                        selectedItem = UNSELECTED
                    } else {
                        expand()
                    }
                }
            }
        }

        override fun onExpansionUpdate(expansionFraction: Float, state: Int) {
            try {
                recyclerView.smoothScrollToPosition(adapterPosition)
            } catch (ignore: Exception) {
            }
        }
    }

    companion object {
        private const val UNSELECTED = -1
    }
}
