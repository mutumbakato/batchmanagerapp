package com.mutumbakato.batchmanager.ui;

import com.mutumbakato.batchmanager.data.models.Expense;
import com.mutumbakato.batchmanager.fragments.BaseView;
import com.mutumbakato.batchmanager.presenters.BasePresenter;

import java.util.List;
import java.util.Map;

public interface ExpensesListContract {

    interface View extends BaseView<Presenter> {

        void showCategorisedExpenses(List<String> categories, Map<String, List<Expense>> expenses);

        void showExpenseProgress(boolean isLoading);

        void showForm(String id);

        void showLoadingExpenseError(String message);

        void showNoExpenses();

        void showSuccessfullySavedMessage();

        void showConfirmDelete(Expense expense, int mills);

        void showDeleteUndoItem(Expense expense);

        void showTotalExpenses(String totalExpenses);

        void applyPermissions(String role);

        interface ListInteractionListener {
            void showForm(String is);

            void showTotal(String total);

            void showEmpty(boolean isVisible);

            void showProgress(boolean isLoading);

            void applyPermissions(String role);
        }

        interface ExpenseItemInteraction {
            void onDelete(Expense expense);

            void onEdit(Expense expense);
        }

    }

    interface Presenter extends BasePresenter {

        void loadExpense(boolean forceUpdate, boolean showProgress);

        void result(int requestCode, int resultCode);

        void editExpense(Expense expense);

        void deleteExpense(Expense expense);

        void delete(Expense expense);

        String getBatchId();
    }

}
