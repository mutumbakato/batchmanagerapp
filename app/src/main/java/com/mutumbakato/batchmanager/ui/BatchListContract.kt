package com.mutumbakato.batchmanager.ui

import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter


interface BatchListContract {

    interface View : BaseView<Presenter> {

        fun showFirstLaunch()

        fun setLoadingIndicator(active: Boolean)

        fun showBatches(batches: List<Batch>)

        fun showBatches(batches: Map<String, List<Batch>>)

        fun showAddBatch()

        fun showBatchDetailsUi(batchId: String)

        fun showLoadingBatchError(message: String)

        fun showNoBatches()

        fun showSuccessfullySavedMessage()

        fun showBatchDeletedMessage()

        fun showLoadingMore(isLoadingMore: Boolean)

        fun showCountBadge(count: Int)

    }

    interface Presenter : BasePresenter {

        fun result(requestCode: Int, resultCode: Int)

        fun loadBatches(forceUpdate: Boolean)

        fun loadMoreBatches()

        fun addNewBatch()

        fun openBatchDetails(requestedBatch: Batch)

        fun refreshBatches()

        fun setFarm(farmId: String)

    }
}
