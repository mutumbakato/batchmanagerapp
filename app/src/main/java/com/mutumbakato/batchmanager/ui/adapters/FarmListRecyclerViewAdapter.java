package com.mutumbakato.batchmanager.ui.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.models.Farm;
import com.mutumbakato.batchmanager.fragments.farm.FarmListFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * specified {@link OnListFragmentInteractionListener}.
 */
public class FarmListRecyclerViewAdapter extends RecyclerView.Adapter<FarmListRecyclerViewAdapter.ViewHolder> {

    private final OnListFragmentInteractionListener mListener;
    private List<Farm> mValues;

    public FarmListRecyclerViewAdapter(List<Farm> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.farmlist_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mNameView.setText(mValues.get(position).getName());
        holder.mView.setOnClickListener(v -> {
            if (null != mListener) {
                mListener.onFarmListFragmentInteraction(holder.mItem);
            }
        });
        holder.mFarmSettings.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onSettings(holder.mItem);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void updateData(List<Farm> farms) {
        this.mValues = farms;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameView;
        public final ImageButton mFarmSettings;
        public Farm mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameView = view.findViewById(R.id.item_number);
            mFarmSettings = view.findViewById(R.id.setting_farm);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
