package com.mutumbakato.batchmanager.ui.components

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.textfield.TextInputEditText
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.utils.Validator
import java.util.*
import kotlin.collections.ArrayList

private var batchId: String = ""
private var batches: List<Batch> = ArrayList()

class BatchEditText(context: Context, attrs: AttributeSet) : TextInputEditText(context, attrs),
        View.OnClickListener, ValidatorEditText, BatchDataSource.LoadBatchesCallback {

    private var mListener: ValidatorEditText.ErrorListener? = null
    private var fragmentManager: FragmentManager? = null
    private var onBatchSelected: (batch: Batch?) -> Unit = {}

    init {
        isClickable = true
        setOnClickListener(this)
        isFocusable = false
        setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_drop_down, 0)
        RepositoryUtils.getBatchRepo(context).getAllBatches(PreferenceUtils.farmId, this)
    }

    override fun onClick(view: View) {
        if (mListener != null) {
            mListener!!.onRemoveError()
        }
        val dialog = ExpenseCategoryDialog()
        dialog.setOnOptionItemClickListener(object : ExpenseCategoryDialog.OnOptionItemClicked {
            override fun onItem(batch: Batch) {
                setText(batch.name)
                batchId = batch.id
                onBatchSelected(batch)
                dialog.dismiss()
            }
        })

        if (fragmentManager != null) {
            dialog.show(fragmentManager!!, "")
            dialog.setOnDismissListener(DialogInterface.OnDismissListener {
                if (mListener != null && !Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
                    mListener!!.onShowError("Please select a Batch.")
                }
            })
        }
    }

    override fun isValid(): Boolean {
        return if (Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
            true
        } else {
            if (mListener != null) {
                mListener!!.onShowError("Invalid Category")
            }
            false
        }
    }

    override fun setOnErrorListener(listener: ValidatorEditText.ErrorListener) {
        mListener = listener
    }

    fun setOnBatchSelected(onSelected: (batch: Batch?) -> Unit) {
        onBatchSelected = onSelected
    }

    fun setFragmentManager(manager: FragmentManager) {
        fragmentManager = manager
    }

    fun setBatchId(id: String?) {
        //Check to avoid an infinite loop
        if (id !== batchId) {
            batchId = id ?: ""
            setCurrentBatch()
        }
    }

    fun getBatchId(): String {
        return batchId
    }

    override fun onBatchesLoaded(data: List<Batch>) {
        batches = data
        setCurrentBatch()
    }

    override fun onDataNotAvailable() {

    }

    private fun setCurrentBatch() {
        for (batch in batches) {
            if (batch.id == batchId) {
                setText(batch.name)
                onBatchSelected(batch)
            }
        }
    }

    class ExpenseCategoryDialog : DialogFragment() {

        private var adapter: OptionsAdapter? = null
        private var mListener: OnOptionItemClicked? = null
        private var dismissListener: DialogInterface.OnDismissListener? = null

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.options_list_view, container, false)
            val listView = view.findViewById<ListView>(R.id.options_list)
            listView.adapter = adapter
            listView.setOnItemClickListener { _, _, i, _ -> mListener!!.onItem(batches[i]) }
            dialog?.setTitle("Select a Batch")
            view.findViewById<View>(R.id.option_input_layout).visibility = View.GONE

            if (dismissListener != null) {
                dialog?.setOnDismissListener(dismissListener)
            }
            return view
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            adapter = OptionsAdapter(activity, getBatches(batches))
        }

        private fun getBatches(batches: List<Batch>?): Array<String> {
            Collections.sort(batches!!) { t, t1 -> t.name.compareTo(t1.name) }
            val c = ArrayList<String>()
            for (batch in batches) {
                c.add(batch.name)
            }
            return c.toTypedArray()
        }

        internal fun setOnOptionItemClickListener(listener: OnOptionItemClicked) {
            mListener = listener
        }

        internal fun setOnDismissListener(onDismissListener: DialogInterface.OnDismissListener) {
            dismissListener = onDismissListener
        }

        interface OnOptionItemClicked {
            fun onItem(batch: Batch)
        }
    }
}
