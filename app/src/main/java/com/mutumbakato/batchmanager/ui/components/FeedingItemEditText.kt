package com.mutumbakato.batchmanager.ui.components

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.utils.Validator

class FeedingItemEditText(context: Context, attrs: AttributeSet) : androidx.appcompat.widget.AppCompatEditText(context, attrs), View.OnClickListener, ValidatorEditText {

    private var mListener: ValidatorEditText.ErrorListener? = null
    private var fragmentManager: FragmentManager? = null

    init {
        isClickable = true
        setOnClickListener(this)
        isFocusable = false
        setCompoundDrawablesWithIntrinsicBounds(0, 0,
                R.drawable.ic_action_drop_down, 0)
    }

    override fun onClick(view: View) {
        if (mListener != null) {
            mListener!!.onRemoveError()
        }
        val dialog = ExpenseCategoryDialog()
        dialog.setOnOptionItemClickListener(object : ExpenseCategoryDialog.OnOptionItemClicked {
            override fun onItem(item: String) {
                if (item == "Others") {
                    text = null
                    isFocusable = true
                    isFocusableInTouchMode = true
                    val activity = context as Activity
                    KeyBoard.showSoftKeyboard(activity)
                    requestFocus()
                } else {
                    isFocusable = false
                    setText(item)
                }
                dialog.dismiss()
            }
        })
        dialog.setOnDismissListener(DialogInterface.OnDismissListener {
            if (!Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
                if (mListener != null) {
                    mListener!!.onShowError("Please select an item")
                }
            }
        })

        if (fragmentManager != null) {
            dialog.show(fragmentManager!!, "")
        }
    }

    override fun isValid(): Boolean {
        if (!Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
            if (mListener != null) {
                mListener!!.onShowError("Please select an item")
            }
            return false
        }
        return true
    }

    override fun setOnErrorListener(listener: ValidatorEditText.ErrorListener) {
        mListener = listener
    }

    fun setFragmentManager(manager: FragmentManager) {
        fragmentManager = manager
    }

    class ExpenseCategoryDialog : DialogFragment() {

        private var adapter: OptionsAdapter? = null
        private var mListener: OnOptionItemClicked? = null
        private val products = arrayOf("Feeds", "Water")
        private var onDismissListener: DialogInterface.OnDismissListener? = null

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.options_list_view, container, false)
            val listView = view.findViewById<ListView>(R.id.options_list)
            listView.adapter = adapter
            listView.setOnItemClickListener { _, _, i, _ ->
                if (mListener != null)
                    mListener!!.onItem(products[i])
            }

            dialog?.setTitle("Select Item")
            view.findViewById<View>(R.id.option_input_layout).visibility = View.GONE

            if (onDismissListener != null) {
                dialog?.setOnDismissListener(onDismissListener)
            }

            return view
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            adapter = OptionsAdapter(activity, products)
        }

        internal fun setOnOptionItemClickListener(listener: OnOptionItemClicked) {
            mListener = listener
        }

        internal fun setOnDismissListener(onDismissListener: DialogInterface.OnDismissListener) {
            this.onDismissListener = onDismissListener
        }

        interface OnOptionItemClicked {
            fun onItem(item: String)
        }
    }
}
