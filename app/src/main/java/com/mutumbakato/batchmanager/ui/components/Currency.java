package com.mutumbakato.batchmanager.ui.components;

/**
 * Created by cato on 7/22/17.
 */

public class Currency {
    public String name;
    public String symbol;
    public String cc;

    public Currency(String name, String symbol, String cc) {
        this.name = name;
        this.symbol = symbol;
        this.cc = cc;
    }

    public String toString() {
        return name + " - " + symbol;
    }
}
