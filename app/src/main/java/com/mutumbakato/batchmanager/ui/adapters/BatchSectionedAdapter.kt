package com.mutumbakato.batchmanager.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.fragments.batch.BatchListFragment
import com.mutumbakato.batchmanager.utils.DateUtils
import org.zakariya.stickyheaders.SectioningAdapter
import java.util.*

class BatchSectionedAdapter(private var mBatches: Map<String, List<Batch>>?,
                            private val mListener: BatchListFragment.OnBatchListInteractionListener) : SectioningAdapter() {

    private var mCategories: List<String> = ArrayList(mBatches!!.keys)

    override fun onCreateGhostHeaderViewHolder(parent: ViewGroup): GhostHeaderViewHolder {
        val ghostView = View(parent.context)
        ghostView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        return GhostHeaderViewHolder(ghostView)
    }

    override fun getNumberOfSections(): Int {
        return mCategories.size
    }

    override fun getNumberOfItemsInSection(sectionIndex: Int): Int {
        return (mBatches!![mCategories[sectionIndex]] ?: error("")).size
    }

    override fun doesSectionHaveHeader(sectionIndex: Int): Boolean {
        return true
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup, headerType: Int): SectioningAdapter.HeaderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.section_header_item, parent, false)
        return HeaderViewHolder(v)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup, itemType: Int): SectioningAdapter.ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.fragment_batch, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindHeaderViewHolder(viewHolder: SectioningAdapter.HeaderViewHolder?, sectionIndex: Int,
                                        headerType: Int) {
        val headerViewHolder = viewHolder as HeaderViewHolder?
        val section = mCategories[sectionIndex]
        headerViewHolder!!.titleTextView.text = if (section.equals("Closed", ignoreCase = true)) "Closed Batches" else "Active Batches ( " + mBatches!![section]!!.size + " )"
    }


    override fun onBindItemViewHolder(viewHolder: SectioningAdapter.ItemViewHolder?, sectionIndex: Int,
                                      itemIndex: Int, itemType: Int) {
        val itemViewHolder = viewHolder as ItemViewHolder?
        itemViewHolder!!.bind(mCategories[sectionIndex], itemIndex)
    }

    fun updateData(batches: Map<String, List<Batch>>) {
        mBatches = batches
        mCategories = ArrayList(batches.keys)
        notifyAllSectionsDataSetChanged()
    }

    inner class HeaderViewHolder internal constructor(itemView: View) : SectioningAdapter.HeaderViewHolder(itemView) {
        val titleTextView: TextView = itemView.findViewById(R.id.expenses_header_title)
    }

    inner class ItemViewHolder internal constructor(internal val mView: View) : SectioningAdapter.ItemViewHolder(mView), View.OnClickListener {

        private val mSupplierTextView: TextView = mView.findViewById(R.id.batch_textView_supplier)
        private val mNameTextView: TextView = mView.findViewById(R.id.batch_textView_date)
        internal val mDateTextView: TextView = mView.findViewById(R.id.batch_textView_quantity)
        private val mTypeImage: ImageView = mView.findViewById(R.id.chicken_type_icon)
        internal lateinit var mItem: Batch

        override fun toString(): String {
            return super.toString() + " '" + mNameTextView.text + "'"
        }

        @SuppressLint("ResourceType")
        fun bind(section: String, position: Int) {
            mItem = (mBatches!![section] ?: error(""))[position]
            mSupplierTextView.text = DateUtils.toAge(mItem.date!!)
            mDateTextView.text = mItem.supplier
            mNameTextView.text = mItem.name
            mView.setOnClickListener(this)
            mTypeImage.setImageResource(if (mItem.type == "Layers") R.drawable.ic_type_eggs else if (mItem.type == "Broilers") R.drawable.ic_type_broiler else R.drawable.ic_type_chicken)
        }

        override fun onClick(view: View) {
            mListener.onBatchListFragmentInteraction(mItem)
        }

    }
}
