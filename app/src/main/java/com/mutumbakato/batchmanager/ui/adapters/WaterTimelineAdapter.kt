package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Water
import com.mutumbakato.batchmanager.fragments.water.WaterFragment
import com.mutumbakato.batchmanager.ui.components.WeekPickerDialogFragment
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import kotlinx.android.synthetic.main.fragment_timeline_item.view.*
import net.cachapa.expandablelayout.ExpandableLayout
import org.zakariya.stickyheaders.SectioningAdapter
import java.util.*
import kotlin.collections.ArrayList

private const val UNSELECTED = -1

class WaterTimelineAdapter(
        private val dateOfBirth: String,
        private val listener: WaterFragment.OnWaterItemInteraction,
        private val mRecyclerView: RecyclerView,
        private val weekFragmentManager: FragmentManager, closeDate: String? = null) : SectioningAdapter() {

    private var mTimeline: Map<Int, List<Int>> = DateUtils.getTimeLine(dateOfBirth, closeDate)
    private var mCategories: List<Int>? = null
    private var mWaters: Map<Int, List<Water>> = HashMap()
    private var role: String = UserRoles.USER_GUEST
    private var selectedItem = UNSELECTED

    init {
        this.mCategories = ArrayList(mTimeline.keys).sortedWith(Comparator { t, t2 -> t.compareTo(t2) })
    }

    override fun onCreateGhostHeaderViewHolder(parent: ViewGroup): GhostHeaderViewHolder {
        val ghostView = View(parent.context)
        ghostView.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT)
        return GhostHeaderViewHolder(ghostView)
    }

    override fun getNumberOfSections(): Int {
        return mCategories!!.size
    }

    override fun getNumberOfItemsInSection(sectionIndex: Int): Int {
        return (mTimeline[mCategories!![sectionIndex]] ?: error("")).size
    }

    override fun doesSectionHaveHeader(sectionIndex: Int): Boolean {
        return true
    }

    override fun onCreateHeaderViewHolder(parent: ViewGroup?, headerType: Int): SectioningAdapter.HeaderViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        val v = inflater.inflate(R.layout.timeline_section_header, parent, false)
        return HeaderViewHolder(v)
    }

    override fun onCreateItemViewHolder(parent: ViewGroup?, itemType: Int): SectioningAdapter.ItemViewHolder {
        val inflater = LayoutInflater.from(parent!!.context)
        val v = inflater.inflate(R.layout.fragment_timeline_item, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindHeaderViewHolder(viewHolder: SectioningAdapter.HeaderViewHolder?, sectionIndex: Int, headerType: Int) {
        val headerViewHolder = viewHolder as HeaderViewHolder?
        headerViewHolder!!.titleTextView.text = headerViewHolder.titleTextView.context.getString(R.string.age_week, mCategories!![sectionIndex])
        headerViewHolder.totalTextView.text = DateUtils.dateFromAge(dateOfBirth, mCategories!![sectionIndex])
        headerViewHolder.titleTextView.setOnClickListener {
            WeekPickerDialogFragment.newInstance(sectionIndex, dateOfBirth).apply {
                setWeekSelectedListener(object : WeekPickerDialogFragment.OnListFragmentInteractionListener {
                    override fun onGoToWeek(week: Int?) {
                        goToWeek(week!!)
                        dismiss()
                    }
                })
                show(weekFragmentManager, "week_picker")
            }
        }
    }

    override fun onBindItemViewHolder(viewHolder: SectioningAdapter.ItemViewHolder?, sectionIndex: Int,
                                      itemIndex: Int, itemType: Int) {
        val itemViewHolder = viewHolder as ItemViewHolder?
        itemViewHolder!!.bind(mCategories!![sectionIndex], itemIndex)
    }

    fun updateData(waters: Map<Int, List<Water>>) {
        mWaters = waters
        notifyAllSectionsDataSetChanged()
    }

    fun goToWeek(week: Int = DateUtils.ageWeeksFromDate(dateOfBirth)) {
        val position = getWeekPosition(week)
        mRecyclerView.scrollToPosition(getAdapterPositionForSectionHeader(position))
    }

    fun applyPermissions(mRole: String) {
        role = mRole
        notifyDataSetChanged()
    }

    private fun getWeekPosition(week: Int): Int {
        var position = 0
        for (i in mCategories!!.indices) {
            if (mCategories!![i] == week)
                position = mCategories!![i]
        }
        return position
    }

    inner class HeaderViewHolder internal constructor(itemView: View) : SectioningAdapter.HeaderViewHolder(itemView) {
        val titleTextView: TextView = itemView.findViewById(R.id.week_age_text)
        val totalTextView: TextView = itemView.findViewById(R.id.week_header_date)
    }

    inner class ItemViewHolder internal constructor(internal val mView: View) : SectioningAdapter.ItemViewHolder(mView) {

        private val mWaterTextView: TextView = mView.findViewById(R.id.total_quantity_textView)
        private val mDayTextView: TextView = mView.findViewById(R.id.day_textView)
        private val mDateTextView: TextView = mView.findViewById(R.id.date_textView)
        private val mItemContainerView: View = mView.findViewById(R.id.section_item_container)
        private val mAverageView: TextView = mView.findViewById(R.id.weight_aim_text)
        private val expandablePaned: ExpandableLayout = mView.findViewById(R.id.details_expansion)
        private val errorMessage: TextView = mView.empty_message_text
        private val mNewEntry: ImageButton = mView.new_entry
        private val itemsList: RecyclerView = mView.items_list
        private val totalLayout: LinearLayout = mView.total_layout
        private val totalView: TextView = mView.timeline_item_total
        private val itemsLayout: LinearLayout = mView.timeline_items_layout

        internal var mItem: Int = 0

        override fun toString(): String {
            return super.toString() + " '" + mWaterTextView.text + "'"
        }

        fun bind(section: Int, position: Int) {

            mItem = (mTimeline[section] ?: error(""))[position]
            val ageInDays = DateUtils.ageDaysFromDate(dateOfBirth, DateUtils.today())
            val itemAge = (mCategories!![section] * 7) + mItem
            val date = DateUtils.rawDateFromAgeDays(dateOfBirth, itemAge)
            val mItems = mWaters[itemAge] ?: ArrayList()
            val avgWater = getAverage(mItems)
            val aim = 0f

            expandablePaned.apply {
                duration = 0
                isExpanded = selectedItem == adapterPosition
                duration = 300
            }

            mDayTextView.text = mView.context.getString(R.string.day_number_string, (itemAge + 1).toString())
            mWaterTextView.text = mView.context.getString(R.string.avg_ml, avgWater)
            mAverageView.text = mView.context.getString(R.string.aim_ml, aim)

            if (mItems.isNotEmpty()) {
                totalView.text = "${getTotalQuantity(mItems)} Ltr"
                itemsLayout.visibility = View.VISIBLE
                if (mItems.size > 1) {
                    totalLayout.visibility = View.VISIBLE
                } else {
                    totalLayout.visibility = View.GONE
                }
            } else {
                itemsLayout.visibility = View.GONE
            }

            mNewEntry.setOnClickListener {
                listener.onAdd(date)
            }

            errorMessage.visibility = if (mItems.isEmpty()) View.VISIBLE else View.GONE
            itemsList.apply {
                layoutManager = LinearLayoutManager(mView.context)
                adapter = WaterRecyclerViewAdapter(mItems, listener, itemsList).apply {
                    applyPermissions(role)
                }
            }

            if (itemAge > ageInDays) {
                mItemContainerView.setOnClickListener {
                    Toast.makeText(mItemContainerView.context, "This day has not reached", Toast.LENGTH_SHORT).show()
                }
            } else {
                mItemContainerView.setOnClickListener {
                    if (expandablePaned.isExpanded)
                        expandablePaned.collapse()
                    else
                        expandablePaned.expand()
                }
            }

            if (date == DateUtils.today()) {
                mDateTextView.text = DateUtils.getDateFromAge(dateOfBirth, mCategories!![section], mItem) + " - " + mView.context.getString(R.string.today)
                mDateTextView.setTextColor(ContextCompat.getColor(mDateTextView.context, R.color.colorOrange))
            } else {
                mDateTextView.text = DateUtils.getDateFromAge(dateOfBirth, mCategories!![section], mItem)
                mDateTextView.setTextColor(ContextCompat.getColor(mDateTextView.context, R.color.textGray))
            }

            mItemContainerView.setBackgroundResource(when (mItem) {
                0 -> R.drawable.section_top_background
                6 -> R.drawable.section_bottom_background
                else -> R.drawable.section_background
            })

            if (itemAge > ageInDays) {
                if (DateUtils.ageWeeksFromDate(dateOfBirth) == mCategories!![section])
                    mView.alpha = 0.7F
                else {
                    mView.alpha = getFade(mItem)
                }
            } else {
                mView.alpha = 1f
            }

            if (itemAge > ageInDays) {
                mItemContainerView.setOnClickListener {
                    Toast.makeText(mItemContainerView.context, "This day has not reached", Toast.LENGTH_SHORT).show()
                }
            } else {
                mItemContainerView.setOnClickListener {
                    val holder: ItemViewHolder? = mRecyclerView.findViewHolderForAdapterPosition(selectedItem) as ItemViewHolder?
                    holder?.apply {
                        collapse()
                    }
                    if (adapterPosition == selectedItem) {
                        selectedItem = UNSELECTED
                    } else {
                        expand()
                    }
                }
            }
        }

        private fun getTotalQuantity(mItems: List<Water>): Float? {
            var total = 0f
            for (item in mItems) {
                total += item.quantity
            }
            return total
        }

        private fun getAverage(mItems: List<Water>): Float {
            var average = 0f
            for (item in mItems) {
                average += item.average
            }
            return (average * 1000)
        }

        fun expand() {
            expandablePaned.expand()
            selectedItem = adapterPosition
        }

        fun collapse() {
            expandablePaned.collapse()
        }
    }

    private fun getFade(day: Int): Float {
        return when (day) {
            0 -> 0.55f
            1 -> 0.4f
            2 -> 0.3f
            3 -> 0.15f
            else -> 0.07f
        }
    }
}
