package com.mutumbakato.batchmanager.ui.components

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.GridLayoutManager
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.utils.DateUtils
import kotlinx.android.synthetic.main.fragment_weekpickerdialog_list.*
import kotlinx.android.synthetic.main.fragment_weekpickerdialog_list.view.*

class WeekPickerDialogFragment : DialogFragment() {

    private var initialWeek = 0
    private lateinit var dateOfBirth: String
    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FullScreenDialogStyle)
        arguments?.let {
            initialWeek = it.getInt(ARG_COLUMN_COUNT)
            dateOfBirth = it.getString(ARG_DOB)!!
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_weekpickerdialog_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(view.week_grid) {
            layoutManager = GridLayoutManager(context, 4)
            adapter = WeekPickerDialogAdapter(dateOfBirth, initialWeek, listener)
            smoothScrollToPosition(initialWeek)
        }
        cancel_button.setOnClickListener {
            dismiss()
        }
        this_week_button.setOnClickListener {
            listener!!.onGoToWeek(DateUtils.ageWeeksFromDate(dateOfBirth))
        }
    }

    fun setWeekSelectedListener(mListener: OnListFragmentInteractionListener) {
        listener = mListener
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnListFragmentInteractionListener {
        fun onGoToWeek(week: Int?)
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"
        const val ARG_DOB = "date_of_birth"

        @JvmStatic
        fun newInstance(initialWeek: Int, dateOfBirth: String) =
                WeekPickerDialogFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, initialWeek)
                        putString(ARG_DOB, dateOfBirth)
                    }
                }
    }
}
