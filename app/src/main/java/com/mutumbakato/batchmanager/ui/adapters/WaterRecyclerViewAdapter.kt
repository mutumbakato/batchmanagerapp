package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.models.Water
import com.mutumbakato.batchmanager.fragments.water.WaterFragment
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import net.cachapa.expandablelayout.ExpandableLayout

/**
 * [RecyclerView.Adapter] that can display a [Feeding] and makes a call to the
 */
class WaterRecyclerViewAdapter(private var mWater: List<Water>,
                               private val mListener: WaterFragment.OnWaterItemInteraction,
                               private val recyclerView: RecyclerView) : RecyclerView.Adapter<WaterRecyclerViewAdapter.ViewHolder>() {

    private var selectedItem = UNSELECTED
    private var role = UserRoles.USER_GUEST

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_feeding, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mWater!!.size
    }

    fun updateData(weights: List<Water>) {
        selectedItem = UNSELECTED
        mWater = weights
        notifyDataSetChanged()
    }

    fun applyPermissions(role: String) {
        this.role = role
        notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView), View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {

        private val mDateTextView: TextView = mView.findViewById(R.id.death_textView_date)
        private val mReasonTextView: TextView = mView.findViewById(R.id.death_textView_reason)
        private val mCountTextView: TextView = mView.findViewById(R.id.death_textView_count)
        private val expandableLayout: ExpandableLayout = mView.findViewById(R.id.death_details_expansion)
        private val mEdit: ImageButton = mView.findViewById(R.id.death_button_edit)
        private val mDelete: ImageButton = mView.findViewById(R.id.death_button_delete)
        private lateinit var mItem: Water

        override fun toString(): String {
            return super.toString() + " '" + mReasonTextView.text + "'"
        }

        override fun onClick(view: View) {
            when (view.id) {
                R.id.death_button_edit -> {
                    selectedItem = UNSELECTED
                    mListener.onEdit(mItem)
                    collapse()
                }
                R.id.death_button_delete -> {
                    mListener.onDelete(mWater[adapterPosition])
                    collapse()
                    selectedItem = UNSELECTED
                }
                else -> {
                    val holder = recyclerView.findViewHolderForAdapterPosition(selectedItem)
                            as ViewHolder?
                    holder?.collapse()
                    if (adapterPosition == selectedItem) {
                        selectedItem = UNSELECTED
                    } else {
                        expand()
                    }
                }
            }
        }

        fun bind(position: Int) {
            mItem = mWater[position]
            collapse()
            mDateTextView.text = DateUtils.parseDateToddMMyyyy(mItem.date)
            mReasonTextView.text = "Entry ${position + 1}"
            mCountTextView.text = String.format("%s %s", mItem.quantity.toString(), "Ltr")
            expandableLayout.setOnExpansionUpdateListener(this)
            mDelete.setOnClickListener(this)
            mEdit.setOnClickListener(this)
            mView.setOnClickListener(this)
            mEdit.visibility = if (UserRoles.canEdit(role)) View.VISIBLE else View.GONE
            mDelete.visibility = if (UserRoles.canDelete(role)) View.VISIBLE else View.GONE
        }

        private fun expand() {
            mView.isSelected = true
            expandableLayout.expand()
            selectedItem = adapterPosition
        }

        private fun collapse() {
            mView.isSelected = false
            expandableLayout.collapse()
        }

        override fun onExpansionUpdate(expansionFraction: Float, state: Int) {
            try {
                recyclerView.smoothScrollToPosition(adapterPosition)
            } catch (ignored: Exception) {
            }

        }
    }

    companion object {
        private const val UNSELECTED = -1
    }
}
