package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import com.mutumbakato.batchmanager.fragments.vaccination.ScheduleListFragment.OnListFragmentInteractionListener
import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [VaccinationSchedule] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 */
class ScheduleRecyclerViewAdapter(private var mValues: List<VaccinationSchedule>,
                                  private var mScheduleId: String?,
                                  private val mListener: OnListFragmentInteractionListener?) :
        RecyclerView.Adapter<ScheduleRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_schedule, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.mItem = mValues[position]
        holder.isCurrent = mScheduleId == mValues[position].id

        if (holder.isCurrent) {
            holder.mUserButton.text = "UnSelect"
            holder.mUserButton.setTextColor(ContextCompat.getColor(holder.mView.context, R.color.colorDirtyGreen))
            holder.mUserButton.setBackgroundResource(R.drawable.schedule_button_remove)
        } else {
            holder.mUserButton.text = "Select"
            holder.mUserButton.setTextColor(ContextCompat.getColor(holder.mView.context, R.color.colorWhite))
            holder.mUserButton.setBackgroundResource(R.drawable.schedule_button)
        }

        holder.mTitleView.text = mValues[position].title
        holder.mContentView.text = mValues[position].description
        holder.mView.setOnClickListener {
            mListener?.onListFragmentInteraction(holder.mItem!!)
        }

        holder.mView.setOnLongClickListener {
            mListener?.updateSchedule(holder.mItem!!)
            true
        }

        holder.mUserButton.setOnClickListener {
            if (!holder.isCurrent) {
                mScheduleId = holder.mItem!!.id
                mListener!!.onUseSchedule(holder.mItem!!)
            } else {
                mScheduleId = "0"
                mListener!!.onRemoveSchedule(holder.mItem!!.id)
            }
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return mValues.size
    }

    fun updateSchedules(schedule: List<VaccinationSchedule>) {
        Collections.sort(schedule) { t0, t1 -> t0.title.compareTo(t1.title) }
        mValues = schedule
        notifyDataSetChanged()
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mTitleView: TextView = mView.findViewById<View>(R.id.schedule_description) as TextView
        val mContentView: TextView = mView.findViewById<View>(R.id.schedule_content) as TextView
        val mUserButton: Button = mView.findViewById<View>(R.id.schedule_use_button) as Button
        var mItem: VaccinationSchedule? = null
        var isCurrent: Boolean = false

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
