package com.mutumbakato.batchmanager.ui.components

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ListView
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.textfield.TextInputEditText
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.ExpenseCategoriesDataSource
import com.mutumbakato.batchmanager.data.models.ExpenseCategory
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.ExpenseCategoriesRepository
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.utils.Validator
import java.util.*

class ExpenseCategoryEditText(context: Context, attrs: AttributeSet) : TextInputEditText(context, attrs), View.OnClickListener, ValidatorEditText {

    private var mListener: ValidatorEditText.ErrorListener? = null
    private var fragmentManager: FragmentManager? = null

    init {
        isClickable = true
        setOnClickListener(this)
        isFocusable = false
        setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_action_drop_down, 0)
    }

    override fun onClick(view: View) {
        if (mListener != null) {
            mListener!!.onRemoveError()
        }
        val dialog = ExpenseCategoryDialog()
        dialog.setOnOptionItemClickListener(object : ExpenseCategoryDialog.OnOptionItemClicked {
            override fun onItem(item: String) {
                setText(item)
                dialog.dismiss()
            }
        })

        if (fragmentManager != null) {
            dialog.show(fragmentManager!!, "")
            dialog.setOnDismissListener(DialogInterface.OnDismissListener {
                if (mListener != null && !Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
                    mListener!!.onShowError("Please select a category.")
                }
            })
        }
    }

    override fun isValid(): Boolean {
        return if (Validator.isValidWord(text!!.toString().trim { it <= ' ' })) {
            true
        } else {
            if (mListener != null) {
                mListener!!.onShowError("Invalid Category")
            }
            false
        }
    }

    override fun setOnErrorListener(listener: ValidatorEditText.ErrorListener) {
        mListener = listener
    }

    fun setFragmentManager(manager: FragmentManager) {
        fragmentManager = manager
    }

    class ExpenseCategoryDialog : DialogFragment(), ExpenseCategoriesDataSource.CategoriesLoadCallback {
        private var adapter: OptionsAdapter? = null
        private var mListener: OnOptionItemClicked? = null
        private var categories: List<ExpenseCategory>? = null
        private var mCategoryRepository: ExpenseCategoriesRepository? = null
        private var dismissListener: DialogInterface.OnDismissListener? = null
        private var newCategoryInput: EditText? = null

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                  savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.options_list_view, container, false)
            val listView = view.findViewById<ListView>(R.id.options_list)
            newCategoryInput = view.findViewById(R.id.new_category_input)
            listView.adapter = adapter

            listView.setOnItemClickListener { _, _, i, _ -> mListener!!.onItem(categories!![i].name) }
            listView.setOnItemLongClickListener { _, _, i, _ ->
                mCategoryRepository!!.deleteCategory(categories!![i].id)
                mCategoryRepository!!.getAllCategories(this@ExpenseCategoryDialog)
                true
            }

            view.findViewById<View>(R.id.expense_category_add_btn).setOnClickListener { saveCategory() }
            dialog?.setTitle("Select category")
            newCategoryInput!!.setOnEditorActionListener { _, i, _ ->
                if (i == EditorInfo.IME_ACTION_DONE) {
                    saveCategory()
                }
                false
            }
            newCategoryInput!!.imeOptions = EditorInfo.IME_ACTION_DONE
            newCategoryInput!!.setSingleLine(true)
            if (dismissListener != null) {
                dialog?.setOnDismissListener(dismissListener)
            }
            return view
        }

        private fun saveCategory() {
            val cat = newCategoryInput!!.text.toString().trim { it <= ' ' }
            if (TextUtils.isEmpty(cat)) {
                return
            }
            mCategoryRepository!!.saveCategory(ExpenseCategory(cat, 0, PreferenceUtils.farmId))
            newCategoryInput!!.text = null
            mCategoryRepository!!.getAllCategories(this@ExpenseCategoryDialog)
            KeyBoard.hideSoftKeyboard(activity!!)
            newCategoryInput!!.clearFocus()
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            adapter = OptionsAdapter(activity, arrayOfNulls(0))
            mCategoryRepository = RepositoryUtils.getExpenseCategoryRepo(context)
            mCategoryRepository!!.getAllCategories(this)
        }

        private fun getCategories(categories: List<ExpenseCategory>): Array<String> {
            Collections.sort(categories) { t, t1 -> t.name.compareTo(t1.name) }
            val c = ArrayList<String>()
            for (category in categories) {
                c.add(category.name)
            }
            return c.toTypedArray()
        }

        override fun onCategoriesLoaded(categories: List<ExpenseCategory>) {
            this.categories = categories
            adapter!!.addItems(getCategories(categories))
        }

        override fun onCategoriesNotAvailable() {

        }

        fun setOnOptionItemClickListener(listener: OnOptionItemClicked) {
            mListener = listener
        }

        fun setOnDismissListener(onDismissListener: DialogInterface.OnDismissListener) {
            dismissListener = onDismissListener
        }

        interface OnOptionItemClicked {
            fun onItem(item: String)
        }
    }

}
