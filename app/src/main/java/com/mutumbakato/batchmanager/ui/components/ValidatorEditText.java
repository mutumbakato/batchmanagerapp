package com.mutumbakato.batchmanager.ui.components;


public interface ValidatorEditText {

    boolean isValid();

    void setOnErrorListener(ErrorListener listener);

    interface ErrorListener {
        void onShowError(String error);

        void onRemoveError();
    }

}
