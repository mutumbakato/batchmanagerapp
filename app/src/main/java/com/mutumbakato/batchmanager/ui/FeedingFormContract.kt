package com.mutumbakato.batchmanager.ui


import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter

interface FeedingFormContract {

    interface View : BaseView<Presenter> {

        fun setDate(date: String)

        fun setCount(count: String)

        fun setComment(comment: String)

        fun setLoadingError()

        fun showDateError(error: String)

        fun showCountError(error: String)

        fun showCommentError(error: String)

        fun toggleExpansion(expand: Boolean)

        fun clearInputs()

        interface FormInteractionListener {
            fun onDoneEditing()

            fun onStartEditing()

            fun showProgress(isLoading: Boolean)
        }
    }

    interface Presenter : BasePresenter {

        val isNew: Boolean

        fun populate()

        fun save(date: String, count: Float, comment: String)

        fun startEditing(id: String?, date: String?)

        fun stopEditing()

    }

}
