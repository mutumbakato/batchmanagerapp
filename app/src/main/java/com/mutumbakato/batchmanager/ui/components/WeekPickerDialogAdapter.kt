package com.mutumbakato.batchmanager.ui.components

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.ui.components.WeekPickerDialogFragment.OnListFragmentInteractionListener
import com.mutumbakato.batchmanager.utils.DateUtils
import kotlinx.android.synthetic.main.fragment_weekpickerdialog.view.*

class WeekPickerDialogAdapter(
        val dateOfBirth: String,
        private val initialWeek: Int,
        private val mListener: OnListFragmentInteractionListener?, closeDate: String? = null)
    : RecyclerView.Adapter<WeekPickerDialogAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener
    private val mValues: List<Int> = ArrayList(DateUtils.getTimeLine(dateOfBirth, closeDate).keys).sortedWith(Comparator { t, t2 -> t.compareTo(t2) })

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Int
            mListener?.onGoToWeek(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_weekpickerdialog, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mWeekView.text = "W $item"
        if (item == initialWeek) {
            holder.mWeekView.setBackgroundResource(R.drawable.tag_background_primary)
            holder.mWeekView.setTextColor(Color.parseColor("#ffffff"))
        } else {
            holder.mWeekView.setBackgroundResource(R.drawable.week_button)
            holder.mWeekView.setTextColor(Color.parseColor("#757575"))
        }
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mWeekView: TextView = mView.item_number
        override fun toString(): String {
            return super.toString() + " '" + mWeekView.text + "'"
        }
    }
}
