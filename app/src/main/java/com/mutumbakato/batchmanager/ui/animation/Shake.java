package com.mutumbakato.batchmanager.ui.animation;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.mutumbakato.batchmanager.R;

/**
 * Created by cato on 11/18/17.
 */

public class Shake {
    public static Animation run(Context context) {
        return AnimationUtils.loadAnimation(context, R.anim.shake);
    }
}
