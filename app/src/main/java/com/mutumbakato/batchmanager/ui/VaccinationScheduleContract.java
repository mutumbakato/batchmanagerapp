package com.mutumbakato.batchmanager.ui;

import com.mutumbakato.batchmanager.data.models.VaccinationSchedule;
import com.mutumbakato.batchmanager.fragments.BaseView;
import com.mutumbakato.batchmanager.presenters.BasePresenter;

import java.util.List;

/**
 * Created by cato on 11/2/17.
 */

public interface VaccinationScheduleContract {

    interface View extends BaseView<Presenter> {

        void showSchedules(List<VaccinationSchedule> schedules);

        void showNoSchedules();

        void showScheduleError(String message);

        void showLoadingSchedules(boolean isLoading);

        void showAddSchedule();

    }

    interface Presenter extends BasePresenter {

        void saveSchedule(String title, String description);

        void updateSchedule(VaccinationSchedule schedule);

        void refresh();

        void useSchedule(VaccinationSchedule vaccinationSchedule);

        void removeSchedule(String id);

        void deleteSchedule(VaccinationSchedule schedule);

    }


}
