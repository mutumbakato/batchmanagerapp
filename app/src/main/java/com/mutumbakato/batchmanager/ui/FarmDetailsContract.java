package com.mutumbakato.batchmanager.ui;

import com.mutumbakato.batchmanager.data.models.Employee;
import com.mutumbakato.batchmanager.data.models.Farm;

import java.util.List;

public interface FarmDetailsContract {

    interface View {

        void setPresenter(Presenter presenter);

        void showFarm(Farm farm);

        void showName(String name);

        void showLocation(String location);

        void showContacts(String contacts);

        void showUnits(String units);

        void showCurrency(String currency);

        void showForm(String farmId);

        void sowAddWorker();

        boolean isActive();

        void shoWorkers(List<Employee> employees);

        void showError(String message);

        void showNoData();

        void showNoWorkers();

        void showProgress(boolean show);

        void showSuccessMessage(String message);

        void exits();
    }

    interface Presenter {

        void start();

        void refresh();

        void loadFarmData(boolean showLoading);

        void updateFarm(Farm farm);

        void deleteFarm();

        void editFarm();

    }

}
