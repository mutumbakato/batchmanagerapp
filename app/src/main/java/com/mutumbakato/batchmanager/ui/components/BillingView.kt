package com.mutumbakato.batchmanager.ui.components

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.utils.Billing

class BillingView constructor(context: Context, attributeSet: AttributeSet) : LinearLayout(context, attributeSet) {

    init {
        inflate(context, R.layout.billing_view, this)
        initBilling()
    }

    private fun initBilling() {
        if (Billing.isPremium()) {
            visibility = View.GONE
        }
    }

}