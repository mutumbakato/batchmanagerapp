package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.ui.ExpensesListContract
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import kotlinx.android.synthetic.main.fragment_expense_item.view.*

import net.cachapa.expandablelayout.ExpandableLayout

class ExpensesRecyclerAdapter(private var mExpenses: List<Expense>,
                              private val mListener: ExpensesListContract.View.ExpenseItemInteraction,
                              private val recyclerView: RecyclerView) : RecyclerView.Adapter<ExpensesRecyclerAdapter.ItemViewHolder>() {

    private var selectedItem = UNSELECTED
    private var role = UserRoles.USER_ADMIN

    fun updateData(expenses: List<Expense>) {
        mExpenses = expenses
        notifyDataSetChanged()
    }

    fun applyPermissions(role: String) {
        this.role = role
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.fragment_expense_item, parent, false)
        return ItemViewHolder(v)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int {
        return mExpenses.size
    }

    inner class ItemViewHolder internal constructor(private val mView: View) : RecyclerView.ViewHolder(mView), View.OnClickListener, ExpandableLayout.OnExpansionUpdateListener {

        private val mDescription: TextView = mView.findViewById(R.id.expense_textView_description)
        private val mAmount: TextView = mView.findViewById(R.id.expense_textView_amount)
        private val mDateTextView: TextView = mView.expense_textView_date
        private val mCategory: TextView = mView.findViewById(R.id.expense_textView_category)
        private val mEdit: ImageButton = mView.findViewById(R.id.expense_button_edit)
        private val mDelete: ImageButton = mView.findViewById(R.id.expense_button_delete)
        private val expandableLayout: ExpandableLayout = mView.findViewById(R.id.expense_details_expansion)
        private val divider: View = mView.findViewById(R.id.expenses_list_divider)
        private var mItem: Expense? = null

        override fun toString(): String {
            return super.toString() + " '" + mAmount.text + "'"
        }

        fun bind(position: Int) {
            collapse()
            mItem = mExpenses[position]
            mDescription.text = mItem!!.description
            mAmount.text = "${PreferenceUtils.currency} ${Currency.format(mItem!!.amount)}"
            mCategory.text = String.format("Category: %s", mItem!!.category)
            mDateTextView.text = DateUtils.parseDateToddMMyyyy(mItem!!.date)

            mView.setOnClickListener(this)
            mEdit.setOnClickListener(this)
            mDelete.setOnClickListener(this)

            mDelete.visibility = if (UserRoles.canDelete(role)) View.VISIBLE else View.GONE
            mEdit.visibility = if (UserRoles.canEdit(role)) View.VISIBLE else View.GONE

            if (position == mExpenses.size - 1) {
                divider.visibility = View.GONE
            } else {
                divider.visibility = View.VISIBLE
            }
        }

        fun expand() {
            mView.isSelected = true
            expandableLayout.expand()
            selectedItem = adapterPosition
        }

        internal fun collapse() {
            mView.isSelected = false
            expandableLayout.collapse()
        }

        override fun onClick(view: View) {
            when (view.id) {
                R.id.expense_button_edit -> {
                    selectedItem = UNSELECTED
                    mListener.onEdit(mItem)
                    collapse()
                }
                R.id.expense_button_delete -> {
                    mListener.onDelete(mItem)
                    selectedItem = UNSELECTED
                    collapse()
                }
                else -> {
                    try {
                        val holder =
                                recyclerView.findViewHolderForAdapterPosition(selectedItem) as ItemViewHolder?
                        holder?.collapse()
                        if (adapterPosition == selectedItem) {
                            selectedItem = UNSELECTED
                        } else {
                            expand()
                        }
                    } catch (e: ClassCastException) {
                        e.printStackTrace()
                    }
                }
            }
        }

        override fun onExpansionUpdate(expansionFraction: Float, state: Int) {
            try {
                recyclerView.smoothScrollToPosition(adapterPosition)
            } catch (ignore: Exception) {
            }
        }
    }

    companion object {
        private const val UNSELECTED = -1
    }
}
