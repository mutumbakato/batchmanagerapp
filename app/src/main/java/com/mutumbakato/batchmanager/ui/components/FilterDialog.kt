package com.mutumbakato.batchmanager.ui.components

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.utils.DateUtils
import kotlinx.android.synthetic.main.fragment_filter_dialog.*

class FilterDialog : DialogFragment() {

    private var title = "Filter"
    private var onApply: (filter: Filter) -> Unit = {}
    private var onClear: () -> Unit = {}
    private var filter = Filter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            title = it.getString(ARG_TITLE, "Filter")
            val filterString = it.getString(ARG_FILTER, ",,,,,").split(",")
            filter = Filter(
                    name = filterString[Filter.FILTER_NAME],
                    id = filterString[Filter.FILTER_ID],
                    from = filterString[Filter.FILTER_FROM],
                    to = filterString[Filter.FILTER_TO],
                    type = filterString[Filter.FILTER_TYPE],
                    rangeName = filterString[Filter.FILTER_RANGE_NAME])
        }
        setStyle(STYLE_NO_TITLE, R.style.FullScreenDialogStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_filter_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        toolbar.title = title
        toolbar.setNavigationOnClickListener {
            dismiss()
        }
        farm_check.isChecked = filter.type == "Farm"
        batch_check.isChecked = filter.type == "Batch"
        toggleBatchSelector(filter.type == "Batch")
        farm_check.setOnCheckedChangeListener { check, b ->
            batch_check.isChecked = !b
            if (b) {
                filter.type = check.text.toString()
                filter.name = check.text.toString()
                filter_batch_selector.text = null
            }
        }
        filter_batch_selector.setBatchId(filter.id)
        batch_check.setOnCheckedChangeListener { check, b ->
            farm_check.isChecked = !b
            toggleBatchSelector(b)
            if (b) {
                filter.type = check.text.toString()
            }
        }
        filter_by_month.setOnCheckedChangeListener { _, b ->
            label_select_period.visibility = if (b) View.GONE else View.VISIBLE
//          label_select_month.visibility = if (b) View.VISIBLE else View.GONE
        }
        filter_to_date.setText(DateUtils.monthAgo())
        filter_batch_selector.setFragmentManager(childFragmentManager)
        period_select_input.setFragmentManager(childFragmentManager)
        period_select_input.setText(filter.rangeName)
        period_select_input.setPeriodSelected { date, name ->
            if (date != "Custom") {
                filter.rangeName = name
                filter_from_date.setText(date)
                filter_to_date.setText(DateUtils.today())
                range_input_group.visibility = View.GONE
            } else {
                range_input_group.visibility = View.VISIBLE
                filter.rangeName = ""
            }
        }
        //        month_select_input.setFragmentManager(childFragmentManager)
        //        month_select_input.setOnMonthPicked {
        //            filter.rangeName = DateUtils.formatMonth(it)
        //            val range = DateUtils.monthToDateRange(it)
        //            filter_from_date.setText(range[0])
        //            filter_to_date.setText(range[1])
        //        }
        apply_filter_button.setOnClickListener {
            val id = if (filter.type == "Farm") PreferenceUtils.farmId else filter_batch_selector.getBatchId()
            val from = filter_from_date.text.toString().trim()
            val to = filter_to_date.text.toString()
            val name = if (filter.type == "Batch") filter_batch_selector.text.toString().trim() else filter.name
            filter = filter.copy(name = name, id = id, from = from, to = to)
            if (filter.rangeName.isEmpty()) {
                filter.rangeName = "$from - $to"
            }
            onApply(filter)
            dismiss()
        }
        reset_filter_button.setOnClickListener {
            onClear()
            dismiss()
        }
        applyFilter()
    }

    private fun toggleBatchSelector(isVisible: Boolean) {
        filter_batch_selector.visibility = if (isVisible) View.VISIBLE else View.GONE
//      filter_batch_title.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun applyFilter() {
        filter_from_date.setText(filter.from)
        filter_to_date.setText(filter.to)
    }

    fun setOnApply(apply: (filter: Filter) -> Unit) {
        onApply = apply
    }

    fun onClearFilter(clear: () -> Unit) {
        onClear = clear
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    companion object {

        private const val ARG_TITLE = "filter_title"
        private const val ARG_FILTER = "filter_filter"

        @JvmStatic
        fun newInstance(title: String, filter: String) =
                FilterDialog().apply {
                    arguments = Bundle().apply {
                        putString(ARG_TITLE, title)
                        putString(ARG_FILTER, filter)
                    }
                }
    }
}
