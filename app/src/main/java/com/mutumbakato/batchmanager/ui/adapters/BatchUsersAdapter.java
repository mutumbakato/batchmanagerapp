package com.mutumbakato.batchmanager.ui.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.models.Batch;
import com.mutumbakato.batchmanager.data.models.BatchUser;
import com.mutumbakato.batchmanager.fragments.batch.BatchUserFragment;
import com.mutumbakato.batchmanager.fragments.batch.BatchListFragment.OnBatchListInteractionListener;
import com.mutumbakato.batchmanager.ui.components.RoleTextView;
import com.mutumbakato.batchmanager.utils.UserRoles;

import java.util.Collections;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Batch} and makes a call to the
 * specified {@link OnBatchListInteractionListener}.
 */
public class BatchUsersAdapter extends RecyclerView.Adapter<BatchUsersAdapter.BatchViewHolder> {

    private final BatchUserFragment.BatchUserListInteractionListener mListener;
    private List<BatchUser> mBatchUsers;
    private String role = "";

    public BatchUsersAdapter(List<BatchUser> items, BatchUserFragment.BatchUserListInteractionListener listener) {
        mBatchUsers = items;
        mListener = listener;
    }

    @NonNull
    @Override
    public BatchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_batch_user, parent, false);
        return new BatchViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull final BatchViewHolder holder, int position) {

        holder.mItem = mBatchUsers.get(position);
        holder.mExtraTextView.setText(mBatchUsers.get(position).getExtra());
        holder.mNameTextView.setText(mBatchUsers.get(position).getName());
        holder.mRoleTextView.setText(mBatchUsers.get(position).getRole());

        if (holder.mItem.getRole().equals(UserRoles.USER_OWNER)) {
            holder.mView.setAlpha(0.6f);
        } else {
            holder.mView.setAlpha(1);
        }

        if (role.equals(UserRoles.USER_ADMIN) && !holder.mItem.getRole().equals(UserRoles.USER_OWNER)) {
            holder.mRoleTextView.setEnabled(true);
            holder.mRoleTextView.setOnItemSelectedListener(item -> {
                if (mListener != null)
                    mListener.onUpdate(holder.mItem, item);
            });
            holder.mView.setOnLongClickListener(view -> {
                if (mListener != null)
                    mListener.onRemove(holder.mItem);
                return true;
            });
        } else {
            holder.mRoleTextView.setEnabled(false);
        }
    }

    @Override
    public int getItemCount() {
        return mBatchUsers.size();
    }

    public void updateData(List<BatchUser> batches) {
        Collections.sort(batches, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        this.mBatchUsers = batches;
        notifyDataSetChanged();
    }

    public void applyPermissions(String role) {
        this.role = role;
        notifyDataSetChanged();
    }

    public class BatchViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final RoleTextView mRoleTextView;
        final TextView mNameTextView;
        final TextView mExtraTextView;
        BatchUser mItem;

        public BatchViewHolder(View view) {
            super(view);
            mView = view;
            mRoleTextView = view.findViewById(R.id.batch_user_role_text);
            mExtraTextView = view.findViewById(R.id.batch_user_extra_text);
            mNameTextView = view.findViewById(R.id.batch_user_name_text);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mExtraTextView.getText() + "'";
        }
    }
}
