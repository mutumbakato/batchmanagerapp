package com.mutumbakato.batchmanager.ui;


import com.mutumbakato.batchmanager.data.models.ExpenseCategory;
import com.mutumbakato.batchmanager.fragments.BaseView;
import com.mutumbakato.batchmanager.presenters.BasePresenter;

import java.util.List;

public interface ExpenseFormContract {

    interface View extends BaseView<Presenter> {

        void setCategories(List<ExpenseCategory> categories);

        void setDate(String date);

        void setCategory(String category);

        void setDescription(String description);

        void setAmount(String amount);

        void setDateError(String dateError);

        void setCategoryError(String categoryError);

        void setDescriptionError(String descriptionError);

        void setAmountError(String amountError);

        void clearInputs();

        void toggleExpansion(boolean expand);

        interface FormInteractionListener {
            void onDoneEditing();

            void onStartEditing();
        }

    }

    interface Presenter extends BasePresenter {

        void save(String date, String category, String description, float amount, String batchId);

        boolean isNew();

        void populate();

        void startEditing(String id);

        void stopEditing();

    }


}
