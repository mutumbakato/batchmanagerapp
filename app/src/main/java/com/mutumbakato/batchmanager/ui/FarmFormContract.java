package com.mutumbakato.batchmanager.ui;

public interface FarmFormContract {

    interface View {

        boolean isActive();

        void setPresenter(Presenter presenter);

        void showName(String name);

        void showLocation(String location);

        void showContacts(String email);

        void showUnits(String units);

        void showCurrency(String currency);

        void showDetails(String farmId);

        void showProgress(boolean show);

        void showError(String message);

        void clearInputs();

        void exit();

    }

    interface Presenter {

        void start();

        void saveFarm(String name, String location, String contact, String units, String currency);

        void loadData(boolean showProgress);

        boolean isEdit();

        void refresh();

        void deleteFarm();

    }
}
