package com.mutumbakato.batchmanager.ui.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.fragments.export.ExportComplete
import com.mutumbakato.batchmanager.fragments.export.SelectBatchFragment

class ExportViewPagerAdapter(fragmentActivity: FragmentActivity) : FragmentStateAdapter(fragmentActivity) {

    override fun getItemCount(): Int {
        return 2
    }

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> SelectBatchFragment.newInstance(PreferenceUtils.farmId)
//            1 -> ExportDataFragment.newInstance()
//            2 -> ExportConfigFragment.newInstance()
            1 -> ExportComplete.newInstance()
            else -> SelectBatchFragment.newInstance(PreferenceUtils.farmId)
        }
    }
}