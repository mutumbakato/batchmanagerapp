package com.mutumbakato.batchmanager.ui

import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter

/**
 * Created by cato on 8/8/17.
 */

interface BatchFromContract {

    interface View : BaseView<Presenter> {

        fun showEmptyBatchError()

        fun showBatchList()

        fun setDate(title: String)

        fun setName(name: String)

        fun setQuantity(quantity: String)

        fun setRate(rate: String)

        fun setSupplier(supplier: String)

        fun setType(type: String)

        fun setAge(age: String)

        fun createInitialExpense(batchId: String, date: String, quantity: Int, rate: Double)

    }

    interface Presenter : BasePresenter {

        fun isDataMissing(): Boolean

        fun saveBatch(date: String, name: String, supplier: String, quantity: Int, rate: Double, type: String, age: String)

        fun populateBatch()
    }
}
