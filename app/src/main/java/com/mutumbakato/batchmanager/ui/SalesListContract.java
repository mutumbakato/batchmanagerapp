package com.mutumbakato.batchmanager.ui;

import com.mutumbakato.batchmanager.data.models.Sale;
import com.mutumbakato.batchmanager.data.repository.SalesRepository;
import com.mutumbakato.batchmanager.fragments.BaseView;
import com.mutumbakato.batchmanager.presenters.BasePresenter;

import java.util.HashMap;
import java.util.List;

public interface SalesListContract {

    interface View extends BaseView<Presenter> {

        void showSales(List<String> categories, HashMap<String, List<Sale>> sales);

        void showSalesProgress(boolean isLoading);

        void showForm(String batchId);

        void showLoadingSalesError(String message);

        void showNoSales();

        void showSuccessfullySavedMessage();

        void showTotalSales(String total);

        void showConfirmDelete(Sale sale, int mills);

        void showDeleteUndoItem(Sale sale);

        void applyPermissions(String userAdmin);

        interface ListInteractionListener {
            void showForm(String is);

            void showTotal(String total);

            void showEmpty(boolean isVisible);

            void showProgress(boolean isLoading);

            void applyPermissions(String role);
        }

        interface SalesItemInteractionListener {
            void onEdit(Sale sale);

            void onDelete(Sale sale);
        }

    }

    interface Presenter extends BasePresenter {

        void result(int requestCode, int resultCode);

        void editSale(Sale sale);

        void deleteSale(Sale sale);

        void delete(Sale id);

        SalesRepository getRepository();

        String getBatchId();

    }

}
