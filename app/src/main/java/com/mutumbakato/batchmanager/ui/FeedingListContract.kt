package com.mutumbakato.batchmanager.ui

import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.fragments.BaseView
import com.mutumbakato.batchmanager.presenters.BasePresenter

interface FeedingListContract {

    interface View : BaseView<Presenter> {


        fun showFeedsProgress(active: Boolean)

        fun showFeeds(feedings: List<Feeding>)

        fun showSectionedFeeds(feeds: Map<Int, List<Feeding>>)

        fun showForm(batchId: String)

        fun showNoFeeds()

        fun showSuccessfullySavedMessage()

        fun showConfirmDelete(feeding: Feeding, mills: Int)

        fun showDeleteUndoItem(feeding: Feeding)

        fun showMortalityRate(count: String)

        fun showEdit(id: String)

        fun goToWeek()

        fun applyPermissions(role: String)

        interface ListInteractionListener {

            fun showForm(id: String?, date: String?)

            fun showTotal(total: String)

            fun showEmpty(isVisible: Boolean)

            fun showProgress(isLoading: Boolean)

            fun applyPermissions(role: String)

        }

        interface OnFeedsItemInteraction {

            fun onEdit(feeding: Feeding)

            fun onDelete(feeding: Feeding)

            fun onAdd(date: String)
        }

    }

    interface Presenter : BasePresenter {

        fun result(requestCode: Int, resultCode: Int)

        fun loadFeeds(forceUpdate: Boolean)

        fun deleteFeeds(feeding: Feeding)

        fun delete(feeding: Feeding)

        fun edit(feeding: Feeding)

        fun gotToWeek()

    }
}

