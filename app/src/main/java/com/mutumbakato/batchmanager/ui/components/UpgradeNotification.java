package com.mutumbakato.batchmanager.ui.components;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.activities.user.LoginActivity;
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;
import com.mutumbakato.batchmanager.utils.licence.LicenseUtils;
import com.mutumbakato.batchmanager.utils.licence.PaymentsActivity;

/**
 * Created by User on 1/13/2018.
 */

public class UpgradeNotification extends androidx.appcompat.widget.AppCompatTextView implements View.OnClickListener {

    public UpgradeNotification(Context context) {
        super(context);
        initView();
    }

    public UpgradeNotification(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public UpgradeNotification(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        setOnClickListener(this);
        setBackgroundColor(Color.parseColor("#ffbb33"));
        setPadding(16, 16, 16, 16);
        setMaxLines(2);
        setLinkTextColor(Color.parseColor("#2196F3"));

        if (LicenseUtils.isLicenceExpired()) {
            setText(getContext().getText(R.string.licence_expire));
            setVisibility(View.VISIBLE);
        } else if (PreferenceUtils.INSTANCE.isNotLoggedIn()) {
            setText(R.string.login_text);
            setVisibility(VISIBLE);
            setOnClickListener(view -> LoginActivity.Companion.start(getContext()));
        } else {
            setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        PaymentsActivity.start(getContext());
    }
}
