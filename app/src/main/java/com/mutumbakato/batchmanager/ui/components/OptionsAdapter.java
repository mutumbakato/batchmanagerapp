package com.mutumbakato.batchmanager.ui.components;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import static android.view.LayoutInflater.from;

public class OptionsAdapter extends BaseAdapter {

    private String[] values;
    private Context context;

    public OptionsAdapter(Context context, String[] values) {
        this.values = values;
        this.context = context;
    }

    @Override
    public int getCount() {
        return values.length;
    }

    @Override
    public Object getItem(int i) {
        return values[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public void addItems(String[] items) {
        this.values = items;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TextView itemView = (TextView) from(context).inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        itemView.setText(values[i]);
        return itemView;
    }
}
