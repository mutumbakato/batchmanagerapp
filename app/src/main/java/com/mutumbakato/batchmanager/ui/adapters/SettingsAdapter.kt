package com.mutumbakato.batchmanager.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.Features
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.fragments.farm.FarmFragment.OnListFragmentInteractionListener
import kotlinx.android.synthetic.main.fragment_farm.view.*

class SettingsAdapter(
        private val mValues: List<Features.FeatureItem>,
        private val mListener: OnListFragmentInteractionListener?)
    : RecyclerView.Adapter<SettingsAdapter.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as Features.FeatureItem
            mListener?.onListFragmentInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_settings, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.mContentView.text = item.name
        holder.mIcon.setImageResource(item.icon)
        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mContentView: TextView = mView.feature_name
        val mIcon: ImageView = mView.farm_feature_icon

        override fun toString(): String {
            return super.toString() + " '" + mContentView.text + "'"
        }
    }
}
