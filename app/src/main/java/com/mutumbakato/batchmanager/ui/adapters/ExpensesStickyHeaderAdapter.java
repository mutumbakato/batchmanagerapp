package com.mutumbakato.batchmanager.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.models.Expense;
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;
import com.mutumbakato.batchmanager.ui.ExpensesListContract;
import com.mutumbakato.batchmanager.utils.Currency;
import com.mutumbakato.batchmanager.utils.DateUtils;
import com.mutumbakato.batchmanager.utils.UserRoles;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.zakariya.stickyheaders.SectioningAdapter;

import java.util.List;
import java.util.Map;

public class ExpensesStickyHeaderAdapter extends SectioningAdapter {

    private static final int UNSELECTED = -1;
    private Map<String, List<Expense>> mExpenses;
    private List<String> mCategories;
    private ExpensesListContract.View.ExpenseItemInteraction mListener;
    private RecyclerView recyclerView;
    private int selectedItem = UNSELECTED;
    private String role = UserRoles.USER_ADMIN;

    public ExpensesStickyHeaderAdapter(Map<String, List<Expense>> expenses, List<String> mCategories,
                                       ExpensesListContract.View.ExpenseItemInteraction mListener, RecyclerView recyclerView) {
        this.mExpenses = expenses;
        this.mCategories = mCategories;
        this.mListener = mListener;
        this.recyclerView = recyclerView;
    }

    @Override
    public GhostHeaderViewHolder onCreateGhostHeaderViewHolder(ViewGroup parent) {
        final View ghostView = new View(parent.getContext());
        ghostView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));
        return new GhostHeaderViewHolder(ghostView);
    }

    @Override
    public int getNumberOfSections() {
        return mCategories.size();
    }

    @Override
    public int getNumberOfItemsInSection(int sectionIndex) {
        return mExpenses.get(mCategories.get(sectionIndex)).size();
    }

    @Override
    public boolean doesSectionHaveHeader(int sectionIndex) {
        return true;
    }

    @Override
    public SectioningAdapter.HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.section_header_item, parent, false);
        return new HeaderViewHolder(v);
    }

    @Override
    public SectioningAdapter.ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v = inflater.inflate(R.layout.fragment_expense_section_item, parent, false);
        return new ItemViewHolder(v);
    }

    @Override
    public void onBindHeaderViewHolder(SectioningAdapter.HeaderViewHolder viewHolder, int sectionIndex,
                                       int headerType) {
        HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
        headerViewHolder.titleTextView.setText(DateUtils.INSTANCE.parseDateToddMMyyyy(mCategories.get(sectionIndex)));
        headerViewHolder.totalTextView.setText(String.format("%s %s", PreferenceUtils.INSTANCE.getCurrency(), getSectionTotal(mCategories.get(sectionIndex))));
    }

    private String getSectionTotal(String s) {
        float total = 0;
        for (Expense exp : mExpenses.get(s))
            total += exp.getAmount();
        return Currency.INSTANCE.format(total);
    }

    @Override
    public void onBindItemViewHolder(SectioningAdapter.ItemViewHolder viewHolder, final int sectionIndex,
                                     int itemIndex, int itemType) {
        final ItemViewHolder itemViewHolder = (ItemViewHolder) viewHolder;
        itemViewHolder.bind(mCategories.get(sectionIndex), itemIndex);
    }

    public void updateData(Map<String, List<Expense>> expenses, List<String> categories) {
        mExpenses = expenses;
        mCategories = categories;
        notifyAllSectionsDataSetChanged();
    }

    public void applyPermissions(String role) {
        this.role = role;
        notifyAllSectionsDataSetChanged();
    }

    public class HeaderViewHolder extends SectioningAdapter.HeaderViewHolder {
        private TextView titleTextView;
        private TextView totalTextView;

        HeaderViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.expenses_header_title);
            totalTextView = itemView.findViewById(R.id.expenses_header_total);
        }
    }

    public class ItemViewHolder extends SectioningAdapter.ItemViewHolder implements View.OnClickListener,
            ExpandableLayout.OnExpansionUpdateListener {

        private final View mView;
        private final TextView mDescription;
        private final TextView mAmount;
        private final TextView mCategory;
        private final ImageButton mEdit, mDelete;
        private final ExpandableLayout expandableLayout;
        private final View divider;
        private Expense mItem;

        ItemViewHolder(View view) {
            super(view);
            mView = view;
            mDescription = view.findViewById(R.id.expense_textView_description);
            mAmount = view.findViewById(R.id.expense_textView_amount);
            mCategory = view.findViewById(R.id.expense_textView_category);
            expandableLayout = view.findViewById(R.id.expense_details_expansion);
            mEdit = view.findViewById(R.id.expense_button_edit);
            mDelete = view.findViewById(R.id.expense_button_delete);
            divider = view.findViewById(R.id.expenses_list_divider);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mAmount.getText() + "'";
        }

        public void bind(String section, int position) {
            collapse();
            mItem = mExpenses.get(section).get(position);
            mDescription.setText(mItem.getDescription());
            mAmount.setText(Currency.INSTANCE.format(mItem.getAmount()));
            mCategory.setText(String.format("Category: %s", mItem.getCategory()));
            mView.setOnClickListener(this);
            mEdit.setOnClickListener(this);
            mDelete.setOnClickListener(this);
            mDelete.setVisibility(UserRoles.INSTANCE.canDelete(role) ? View.VISIBLE : View.GONE);
            mEdit.setVisibility(UserRoles.INSTANCE.canEdit(role) ? View.VISIBLE : View.GONE);

            if (position == mExpenses.get(section).size() - 1) {
                divider.setVisibility(View.GONE);
            } else {
                divider.setVisibility(View.VISIBLE);
            }

        }

        public void expand() {
            mView.setSelected(true);
            expandableLayout.expand();
            selectedItem = getAdapterPosition();
        }

        void collapse() {
            mView.setSelected(false);
            expandableLayout.collapse();
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.expense_button_edit: {
                    selectedItem = UNSELECTED;
                    mListener.onEdit(mItem);
                    collapse();
                }
                break;
                case R.id.expense_button_delete: {
                    mListener.onDelete(mItem);
                    selectedItem = UNSELECTED;
                    collapse();
                }
                break;
                default: {
                    try {
                        ItemViewHolder holder = (ItemViewHolder) recyclerView.
                                findViewHolderForAdapterPosition(selectedItem);
                        if (holder != null) {
                            holder.collapse();
                        }
                        if (getAdapterPosition() == selectedItem) {
                            selectedItem = UNSELECTED;
                        } else {
                            expand();
                        }
                    } catch (ClassCastException e) {
                        e.printStackTrace();
                    }
                }
                break;
            }

        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            try {
                recyclerView.smoothScrollToPosition(getAdapterPosition());
            } catch (Exception ignore) {
            }
        }
    }
}
