package com.mutumbakato.batchmanager

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.mutumbakato.batchmanager.fragments.farm.FarmFragment
import com.mutumbakato.batchmanager.ui.adapters.SettingsAdapter
import kotlinx.android.synthetic.main.content_settings.*
import kotlinx.android.synthetic.main.toolbar.*

class SettingsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        with(settings_list) {
            layoutManager = LinearLayoutManager(context)
            adapter = SettingsAdapter(Features.SETTINGS_ITEMS, object : FarmFragment.OnListFragmentInteractionListener {
                override fun onListFragmentInteraction(item: Features.FeatureItem?) {

                }
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, SettingsActivity::class.java))
        }
    }
}
