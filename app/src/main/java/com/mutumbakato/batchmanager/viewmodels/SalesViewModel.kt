package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mutumbakato.batchmanager.data.models.DataResource
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.models.Status
import com.mutumbakato.batchmanager.data.repository.SalesLiveRepository

class SalesViewModel constructor(private val repository: SalesLiveRepository) : ViewModel() {

    val error = MutableLiveData<Status>()

    val farmId = MutableLiveData<String>()

    val batchId = MutableLiveData<String>()

    val saleId = MutableLiveData<String>()

    val status = MutableLiveData<Status>()

    val isLoading = MutableLiveData<Boolean>()

    val isEditing = MutableLiveData<Boolean>()

    val filter = MutableLiveData<Filter>()

    val mode = MutableLiveData<String>()

    init {
        isLoading.value = false
        isEditing.value = false
        filter.value = Filter()
        mode.value = "all"
    }

    fun setFarmId(farmId: String) {
        this.farmId.postValue(farmId)
        val f = filter.value?.copy(id = farmId)
        filter.postValue(f)
    }

    fun setBatchId(batchId: String) {
        this.batchId.postValue(batchId)
    }

    fun setSaleId(saleId: String?) {
        this.saleId.postValue(saleId)
    }

    private val salesData: LiveData<DataResource<List<Sale>>> = Transformations.map(farmId) {
        repository.listFarmSales(it)
    }

    private val salesFilterData: LiveData<DataResource<List<Sale>>> = Transformations.map(filter) {
        repository.listBatchSales(it.id)
    }

    private val currentSale: LiveData<DataResource<Sale>> = Transformations.map(saleId) {
        repository.getSaleById(it ?: "0")
    }

    val sale: LiveData<Sale> = Transformations.switchMap(currentSale) {
        it.data
    }

    val sales: LiveData<List<Sale>> = Transformations.switchMap(salesData) {
        it.data
    }

    val filteredSales: LiveData<List<Sale>> = Transformations.switchMap(salesFilterData) {
        it.data
    }

    fun applyFilter(f: Filter) {
        filter.value = f
        mode.value = "filter"
    }

    fun clearFilter() {
        filter.value = Filter()
        mode.value = "all"
    }

    fun createSale(sale: Sale) {
        repository.create(sale) {
            isEditing.postValue(false)
        }
    }

    fun updateSale(sale: Sale) {
        repository.update(sale) {
            isEditing.postValue(false)
        }
    }

    fun deleteSale(sale: Sale) {
        repository.deleteSales(sale)
    }

    fun stopEditing() {
        isEditing.postValue(false)
    }
}