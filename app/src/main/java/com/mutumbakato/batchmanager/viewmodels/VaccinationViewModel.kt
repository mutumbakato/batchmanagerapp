package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mutumbakato.batchmanager.data.repository.VaccinationRepository
import com.mutumbakato.batchmanager.data.repository.VaccinationScheduleRepository

class VaccinationViewModel constructor(val vaccinationRepository: VaccinationRepository,
                                       val vaccinationScheduleRepository: VaccinationScheduleRepository) : ViewModel() {

    private val farmId = MutableLiveData<String>()

    private val scheduleId = MutableLiveData<String>()

}