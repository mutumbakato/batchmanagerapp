package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreException
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils

class UserViewModel : ViewModel() {

    private val firebase = FirebaseFirestore.getInstance().collection("users")
    private var userId: String = PreferenceUtils.userId

    val user = MutableLiveData<User>()

    fun setUserId(id: String) {
        userId = id
        firebase.document(id).addSnapshotListener { snap: DocumentSnapshot?, _: FirebaseFirestoreException? ->
            val u = snap?.toObject(User::class.java)
            user.value = u
        }
    }

    fun saveUserName(userName: String, onUpdate: () -> Unit) {
        firebase.document(userId).update("name", userName).addOnSuccessListener {
            onUpdate()
        }
    }

}