package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.data.StatisticsDataSource
import com.mutumbakato.batchmanager.data.repository.*

object ViewModelFactory {

    class ExpensesViewModelFactory constructor(private val repo: ExpensesLiveRepository,
                                               private val batchRepository: BatchRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ExpensesViewModel(repo, batchRepository) as T
        }
    }

    class SalesViewModelFactory constructor(private val repo: SalesLiveRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return SalesViewModel(repo) as T
        }
    }

    class FeedsViewModelFactory constructor(private val repository: FeedsRepository,
                                            private val batchRepository: BatchRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return FeedsViewModel(repository, batchRepository) as T
        }
    }

    class BatchViewModelFactory constructor(private val statisticsDataSource: StatisticsDataSource) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return BatchViewModel(statisticsDataSource) as T
        }
    }

    class ExportViewModelFactory constructor(val repository: StatisticsDataSource) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return ExportViewModel(repository) as T
        }
    }

    class WeightViewModelFactory constructor(val repository: WeightRepository,
                                             private val batchRepository: BatchRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return WeightViewModel(repository, batchRepository) as T
        }
    }

    class WaterViewModelFactory constructor(val repository: WaterRepository,
                                            private val batchRepository: BatchRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return WaterViewModel(repository, batchRepository) as T
        }
    }
}