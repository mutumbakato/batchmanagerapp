package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.models.Weight
import com.mutumbakato.batchmanager.data.models.WeightResource
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.WeightRepository
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles

class WeightViewModel constructor(private val repo: WeightRepository, val batchRepo: BatchRepository) : ViewModel() {

    private var mDateOfBirth = DateUtils.today()

    private val weightId = MutableLiveData<String>()

    val batchId = MutableLiveData<String>()

    val mDate = MutableLiveData<String>()

    val userRole = MutableLiveData<String>()

    val goTo = MutableLiveData<String>()

    init {
        userRole.value = UserRoles.USER_GUEST
    }

    val isFormOpen: LiveData<Boolean> = Transformations.map(mDate) {
        it != null && it.isNotEmpty()
    }

    val weight: LiveData<Weight> = Transformations.switchMap(weightId) {
        repo.getWeightById(it ?: "0")
    }

    private val weightData: LiveData<WeightResource> = Transformations.map(batchId) {
        repo.listWeights(it)
    }

    private val weights: LiveData<List<Weight>> = Transformations.switchMap(weightData) {
        it.data
    }

    val sortedWeights: LiveData<Map<Int, List<Weight>>> = Transformations.map(weights) {
        getSortedWeight(it)
    }

    val error: LiveData<String> = Transformations.switchMap(weightData) {
        it.error
    }

    val isLoading: LiveData<Boolean> = Transformations.switchMap(weightData) {
        it.isLoading
    }

    fun setBatchId(id: String) {
        batchId.postValue(id)
        getUser(id)
    }

    fun setWeightId(id: String?) {
        weightId.postValue(id)
    }

    fun setDate(date: String?) {
        mDate.postValue(date)
    }

    fun save(weight: Weight) {
        if (weightId.value == null) {
            repo.createWeight(weight)
        } else {
            repo.updateWeight(weight)
        }
    }

    fun delete(weight: Weight) {
        repo.deleteWeight(weight)
    }

    fun setDateOfBirth(dob: String) {
        mDateOfBirth = dob
    }

    private fun getSortedWeight(weight: List<Weight>): HashMap<Int, List<Weight>> {
        val sortedWeight = LinkedHashMap<Int, List<Weight>>()
        val dates = getSections(weight)
        for (date in dates) {
            sortedWeight[date] = getSectionData(weight, date)
        }
        return sortedWeight
    }

    private fun getSections(weight: List<Weight>): List<Int> {
        val sections = ArrayList<Int>()
        for (expense in weight) {
            val day = DateUtils.ageDaysFromDate(mDateOfBirth, expense.date)
            if (!sections.contains(day))
                sections.add(day)
        }
        sections.sortWith(Comparator { o1, o2 -> o2!!.compareTo(o1!!) })
        return sections
    }

    private fun getSectionData(weight: List<Weight>, date: Int): List<Weight> {
        val exp = ArrayList<Weight>()
        for (expense in weight) {
            if (DateUtils.ageDaysFromDate(mDateOfBirth, expense.date) == date)
                exp.add(expense)
        }
        exp.sortWith(Comparator { o1, o2 -> o1.date.compareTo(o2.date) })
        return exp
    }

    private fun getUser(mBatchId: String) {
        batchRepo?.getBatch(mBatchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                if (batch.userId == PreferenceUtils.userId) {
                    userRole.value = UserRoles.USER_ADMIN
                } else
                    batchRepo.getBatchUser(mBatchId, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                        override fun onDataItemLoaded(data: BatchUser) {
                            userRole.value = data.role
                        }

                        override fun onDataItemNotAvailable() {

                        }

                        override fun onError(message: String) {

                        }
                    })
            }

            override fun onDataNotAvailable() {

            }
        })
    }

    fun closeForm() {
        setWeightId(null)
        setDate(null)
    }
}
