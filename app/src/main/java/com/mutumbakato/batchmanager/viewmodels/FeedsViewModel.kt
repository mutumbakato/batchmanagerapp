package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.*
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.FeedsRepository
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles

class FeedsViewModel constructor(private val repo: FeedsRepository,
                                 private val batchRepository: BatchRepository) : ViewModel() {

    private var mDateOfBirth = DateUtils.today()

    val feedsId = MutableLiveData<String>()

    val batchId = MutableLiveData<String>()

    val mDate = MutableLiveData<String>()

    val userRole = MutableLiveData<String>()

    val goTo = MutableLiveData<String>()

    init {
        userRole.value = UserRoles.USER_GUEST
    }

    val isFormOpen: LiveData<Boolean> = Transformations.map(mDate) {
        it != null && it.isNotEmpty()
    }

    val feed: LiveData<Feeds> = Transformations.switchMap(feedsId) {
        repo.getFeedsById(it ?: "0")
    }

    private val feedsData: LiveData<DataResource<List<Feeds>>> = Transformations.map(batchId) {
        repo.listFeeds(it)
    }

    val feeds: LiveData<List<Feeds>> = Transformations.switchMap(feedsData) {
        it.data
    }

    private val oldFeedsData: LiveData<DataResource<List<Feeding>>> = Transformations.map(batchId) {
        repo.listOldFeeds(it)
    }


    val oldFeeds: LiveData<List<Feeding>> = Transformations.switchMap(oldFeedsData) {
        it.data
    }

    val sortedFeeds: LiveData<Map<Int, List<Feeds>>> = Transformations.map(feeds) {
        getSortedFeeds(it)
    }

    val error: LiveData<Status> = Transformations.switchMap(feedsData) {
        it.status
    }

    val isLoading: LiveData<Boolean> = Transformations.switchMap(feedsData) {
        it.isLoading
    }

    fun setBatchId(id: String) {
        batchId.postValue(id)
        getUser(id)
    }

    fun setFeedsId(id: String?) {
        feedsId.postValue(id)
    }

    fun setDate(date: String?) {
        mDate.postValue(date)
    }

    fun save(feeds: Feeds) {
        if (batchId.value != null) {
            repo.createFeeds(feeds)
        } else {
            repo.updateFeeds(feeds)
        }
    }

    fun delete(feeds: Feeds) {
        repo.deleteFeeds(feeds)
    }

    fun setDateOfBirth(dob: String) {
        mDateOfBirth = dob
    }

    private fun getSortedFeeds(feeds: List<Feeds>): HashMap<Int, List<Feeds>> {
        val sortedFeeds = LinkedHashMap<Int, List<Feeds>>()
        val dates = getSections(feeds)
        for (date in dates) {
            sortedFeeds[date] = getSectionData(feeds, date)
        }
        return sortedFeeds
    }

    private fun getSections(feeds: List<Feeds>): List<Int> {
        val sections = ArrayList<Int>()
        for (expense in feeds) {
            val day = DateUtils.ageDaysFromDate(mDateOfBirth, expense.date)
            if (!sections.contains(day))
                sections.add(day)
        }
        sections.sortWith(Comparator { o1, o2 -> o2!!.compareTo(o1!!) })
        return sections
    }

    private fun getSectionData(feeds: List<Feeds>, date: Int): List<Feeds> {
        val exp = ArrayList<Feeds>()
        for (expense in feeds) {
            if (DateUtils.ageDaysFromDate(mDateOfBirth, expense.date) == date)
                exp.add(expense)
        }
        exp.sortWith(Comparator { o1, o2 -> o1.date.compareTo(o2.date) })
        return exp
    }

    private fun getUser(mBatchId: String) {
        batchRepository.getBatch(mBatchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                if (batch.userId == PreferenceUtils.userId) {
                    userRole.value = UserRoles.USER_ADMIN
                } else
                    batchRepository.getBatchUser(mBatchId, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                        override fun onDataItemLoaded(data: BatchUser) {
                            userRole.value = data.role
                        }

                        override fun onDataItemNotAvailable() {

                        }

                        override fun onError(message: String) {

                        }
                    })
            }

            override fun onDataNotAvailable() {}
        })
    }

    fun closeForm() {
        setFeedsId(null)
        setDate(null)
    }

}
