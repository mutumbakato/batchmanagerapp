package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.*
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.WaterRepository
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles

class WaterViewModel constructor(private val waterRepository: WaterRepository, val batchRepo: BatchRepository) : ViewModel() {

    private var mDateOfBirth = DateUtils.today()
    val waterId = MutableLiveData<String>()
    val batchId = MutableLiveData<String>()
    val mDate = MutableLiveData<String>()
    val userRole = MutableLiveData<String>()
    val gotTo = MutableLiveData<String>()

    val isFormOpen: LiveData<Boolean> = Transformations.map(mDate) {
        it != null && it.isNotEmpty()
    }

    val water: LiveData<Water> = Transformations.switchMap(waterId) {
        waterRepository.getWaterById(it ?: "0")
    }

    private val waterResource: LiveData<DataResource<List<Water>>> = Transformations.map(batchId) {
        waterRepository.listWaters(it)
    }

    val waters: LiveData<List<Water>> = Transformations.switchMap(waterResource) {
        it.data
    }

    val sortedWaters: LiveData<Map<Int, List<Water>>> = Transformations.map(waters) {
        getSortedWater(it)
    }

    val error: LiveData<Status> = Transformations.switchMap(waterResource) {
        it.status
    }

    val isLoading: LiveData<Boolean> = Transformations.switchMap(waterResource) {
        it.isLoading
    }

    fun setBatchId(id: String) {
        batchId.postValue(id)
        getUser(id)
    }

    fun setWaterId(id: String?) {
        waterId.postValue(id)
    }

    fun setDate(date: String?) {
        mDate.postValue(date)
    }

    fun save(water: Water) {
        if (waterId.value == null) {
            waterRepository.createWater(water)
        } else {
            waterRepository.updateWater(water)
        }
    }

    fun delete(water: Water) {
        waterRepository.deleteWater(water)
    }

    fun setDateOfBirth(dob: String) {
        mDateOfBirth = dob
    }

    private fun getSortedWater(water: List<Water>): HashMap<Int, List<Water>> {
        val sortedWater = LinkedHashMap<Int, List<Water>>()
        val dates = getSections(water)
        for (date in dates) {
            sortedWater[date] = getSectionData(water, date)
        }
        return sortedWater
    }

    private fun getSections(water: List<Water>): List<Int> {
        val sections = ArrayList<Int>()
        for (expense in water) {
            val day = DateUtils.ageDaysFromDate(mDateOfBirth, expense.date)
            if (!sections.contains(day))
                sections.add(day)
        }
        sections.sortWith(Comparator { o1, o2 -> o2!!.compareTo(o1!!) })
        return sections
    }

    private fun getSectionData(water: List<Water>, date: Int): List<Water> {
        val exp = ArrayList<Water>()
        for (expense in water) {
            if (DateUtils.ageDaysFromDate(mDateOfBirth, expense.date) == date)
                exp.add(expense)
        }
        exp.sortWith(Comparator { o1, o2 -> o1.date.compareTo(o2.date) })
        return exp
    }

    private fun getUser(mBatchId: String) {
        batchRepo.getBatch(mBatchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                if (batch.userId == PreferenceUtils.userId) {
                    userRole.value = UserRoles.USER_ADMIN
                } else
                    batchRepo.getBatchUser(mBatchId, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                        override fun onDataItemLoaded(data: BatchUser) {
                            userRole.value = data.role
                        }

                        override fun onDataItemNotAvailable() {

                        }

                        override fun onError(message: String) {

                        }
                    })
            }

            override fun onDataNotAvailable() {

            }
        })
    }

    fun closeForm() {
        setWaterId(null)
        setDate(null)
    }
}
