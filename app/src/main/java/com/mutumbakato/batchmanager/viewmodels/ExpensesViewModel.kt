package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mutumbakato.batchmanager.data.models.DataResource
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.models.Status
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.ExpensesLiveRepository

class ExpensesViewModel constructor(private val repository: ExpensesLiveRepository,
                                    private val batchRepository: BatchRepository) : ViewModel() {

    val error = MutableLiveData<Status>()

    val farmId = MutableLiveData<String>()

    val batchId = MutableLiveData<String>()

    val expenseId = MutableLiveData<String>()

    val status = MutableLiveData<Status>()

    val isLoading = MutableLiveData<Boolean>()

    val isEditing = MutableLiveData<Boolean>()

    val filter = MutableLiveData<Filter>()

    val mode = MutableLiveData<String>()

    init {
        isLoading.value = false
        isEditing.value = false
        filter.value = Filter()
        mode.value = "all"
    }

    fun setFarmId(farmId: String) {
        this.farmId.postValue(farmId)
        val f = filter.value?.copy(id = farmId)
        filter.postValue(f)
    }

    fun setBatchId(batchId: String) {
        this.batchId.postValue(batchId)
    }

    fun setExpenseId(expenseId: String?) {
        this.expenseId.postValue(expenseId)
    }

    private val expensesData: LiveData<DataResource<List<Expense>>> = Transformations.map(farmId) {
        repository.listFarmExpenses(it)
    }

    private val expensesFilterData: LiveData<DataResource<List<Expense>>> = Transformations.map(filter) {
        repository.listBatchExpenses(it.id)
    }

    private val currentExpense: LiveData<DataResource<Expense>> = Transformations.map(expenseId) {
        repository.getExpenseById(it ?: "0")
    }

    val expense: LiveData<Expense> = Transformations.switchMap(currentExpense) {
        it.data
    }

    val expenses: LiveData<List<Expense>> = Transformations.switchMap(expensesData) {
        it.data
    }

    val filteredExpenses: LiveData<List<Expense>> = Transformations.switchMap(expensesFilterData) {
        it.data
    }

    val months: LiveData<List<String>> = Transformations.switchMap(farmId) {
        repository.listExpensesMonths(it)
    }

    fun applyFilter(f: Filter) {
        filter.value = f
        mode.value = "filter"
    }

    fun clearFilter() {
        filter.value = Filter()
        mode.value = "all"
    }

    fun createExpense(expense: Expense) {
        repository.create(expense) {
            isEditing.postValue(false)
        }
    }

    fun updateExpense(expense: Expense) {
        repository.update(expense) {
            isEditing.postValue(false)
        }
    }

    fun deleteExpense(expense: Expense) {
        repository.deleteExpenses(expense)
    }

    fun stopEditing() {
        isEditing.postValue(false)
    }
}