package com.mutumbakato.batchmanager.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.mutumbakato.batchmanager.data.StatisticsDataSource
import com.mutumbakato.batchmanager.data.models.*
import com.mutumbakato.batchmanager.data.models.exports.Exports
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.Standards
import com.mutumbakato.batchmanager.utils.TransformationX
import kotlin.collections.set
import kotlin.math.pow
import kotlin.math.sqrt

class BatchViewModel constructor(private val statisticsDataSource: StatisticsDataSource) : ViewModel() {

    private val batchId = MutableLiveData<String>()
    private var dob: String = ""
    private var closingDate: String = DateUtils.today()
    private var doc = 0
    private var batchType = "Layers"
    private var exports: Exports? = null

    val days = MutableLiveData<List<Int>>()
    val weeks = MutableLiveData<List<Int>>()


    val isWeek = Transformations.map(days) {
        it.size > 45
    }

    val isLaying = Transformations.map(days) {
        it.size >= 7 * 20 && batchType != "Broilers"
    }

    fun setBatchId(id: String) {
        batchId.postValue(id)
    }

    val batch: LiveData<Batch> = Transformations.switchMap(batchId) {
        statisticsDataSource.getBatch(it)
    }

    private val stats: LiveData<Statistics> = TransformationX(batch) {
        val batch = it[0] as Batch
        batch.let {
            exports = Exports(batch, isLaying = false)
            dob = batch.date!!
            closingDate = batch.closeDate ?: DateUtils.today()
            doc = batch.quantity
            batchType = batch.type
            days.postValue(DateUtils.getDailyTimeline(dob, closingDate, batch.type))
            weeks.postValue(DateUtils.getWeeklyTimeline(dob, closingDate, batch.type))
        }
        statisticsDataSource.getStatistics(batch.id)
    }

    private val ageToday = Transformations.map(batch) {
        DateUtils.ageDaysFromDate(it.date!!)
    }

    val batchCount: LiveData<BatchCount> = Transformations.switchMap(stats) {
        it.batchCount
    }

    @Deprecated("Use batchCount instead")
    val count: LiveData<Int> = Transformations.map(batchCount) {
        it.remaining
    }

    val sold: LiveData<Int> = Transformations.switchMap(stats) {
        it.sold
    }

    val totalExpenditure: LiveData<Float> = Transformations.switchMap(stats) {
        it.totalExpenditure
    }

    val totalSales: LiveData<Float> = Transformations.switchMap(stats) {
        it.totalSales
    }

    val costPerBird: LiveData<Float> = Transformations.switchMap(stats) {
        it.costPerBird
    }

    val totalFeeds: LiveData<Float> = Transformations.switchMap(stats) {
        it.totalFeeds
    }

    val totalWater: LiveData<Float> = Transformations.switchMap(stats) {
        it.totalWater
    }

    val aveTotalFeeds = TransformationX(totalFeeds, count) {
        ((it[0] as Float?) ?: 0f) / ((it[1] as Int?) ?: 1) * 1000
    }

    val aveTotalWater = TransformationX(totalWater, count) {
        ((it[0] as Float?) ?: 0f) / ((it[1] as Int?) ?: 1) * 1000
    }

    val eggCount: LiveData<EggCount> = Transformations.switchMap(stats) {
        it.eggCount
    }

    val sales: LiveData<List<EntryItem>> = Transformations.switchMap(stats) {
        it.sales
    }

    val expenses: LiveData<List<EntryItem>> = Transformations.switchMap(stats) {
        it.expenses
    }

    private val eggs: LiveData<List<EntryItem>> = Transformations.switchMap(stats) {
        it.eggs
    }

    private val henDay = Transformations.switchMap(stats) { it.henDay }

    private val deaths: LiveData<List<EntryItem>> = Transformations.switchMap(stats) {
        it.deaths
    }

    private val feeding: LiveData<List<EntryItem>> = Transformations.switchMap(stats) {
        it.feeding
    }

    private val water: LiveData<List<EntryItem>> = Transformations.switchMap(stats) {
        it.water
    }

    private val weight = Transformations.switchMap(stats) {
        it.weight
    }

    val avgWeight: LiveData<Float> = Transformations.switchMap(stats) {
        it.avgWeight
    }

    val avgDailyGain = TransformationX(ageToday, avgWeight) {
        val age = (it[1] as Float?) ?: 0f
        val weight = (it[0] as Int?) ?: 0
        if (age > 0 && weight > 0) age / weight else 0f
    }

    val waterConsumption: LiveData<Float> = Transformations.switchMap(stats) {
        it.lastWaterTotal
    }

    val feedConversion = TransformationX(totalFeeds, avgWeight, batchCount) {
        val totalFeeds = (it[0] as Float?) ?: 0f
        val count = (it[2] as BatchCount?)
        val avgWeight = (it[1] as Float?) ?: 0f
        if (totalFeeds > 0 && avgWeight > 0 && count != null) totalFeeds / ((avgWeight / 1000) * (count.remaining)) else 0f
    }

    private val bodyWeightUniformityData = Transformations.switchMap(stats) {
        it.weightUniformityData
    }

    @Suppress("UNCHECKED_CAST")
    val weightUniformity = TransformationX(bodyWeightUniformityData) {
        getUniformity(it[0] as List<UniformityData>)
    }

    val feedsConsumption: LiveData<Float> = Transformations.switchMap(stats) {
        it.lastFeedsTotal
    }

    val eggsPercentage: LiveData<Float> = Transformations.switchMap(stats) {
        it.eggsPercentage
    }

    val dailyDeaths = Transformations.map(deaths) {
        val d = getDailyValues(it)
        exports?.sortedData?.set(Exports.DEATHS, d)
        return@map d
    }

    val dailyFeeds = Transformations.map(feeding) {
        val f = getDailyValues(it)
        exports?.sortedData?.set(Exports.FEEDS, f)
        return@map f
    }

    val dailyFeedsTarget = Transformations.map(days) { days ->
        val dailyValue: MutableMap<Int, Float> = hashMapOf()
        days.forEach {
            dailyValue[it] = Standards.getDailyFeedsTarget(it, batchType)
        }
        dailyValue
    }


    val dailyWater = TransformationX(water) {
        val w = getDailyValues(it[0] as List<EntryItem>)
        exports?.sortedData?.set(Exports.WATER, w)
        w
    }

    val dailyWeight = TransformationX(weight) {
        val ww = getDailyValues(it[0] as List<EntryItem>)
        exports?.sortedData?.set(Exports.WEIGHT, ww)
        ww
    }

    val dailyWeightTarget = Transformations.map(days) { days ->
        val dailyValue: MutableMap<Int, Float> = hashMapOf()
        days.forEach {
            dailyValue[it] = Standards.getDailyWeightTarget(it, batchType)
        }
        dailyValue
    }

    val dailyEggs = TransformationX(eggs) {
        val ee = getDailyValues(it[0] as List<EntryItem>)
        exports?.sortedData?.set(Exports.EGGS, ee)
        ee
    }

    val dailyHenDay = TransformationX(henDay) {
        val ee = getDailyValues(it[0] as List<EntryItem>)
        exports?.sortedData?.set(Exports.HEN_DAY, ee)
        ee
    }

    val dailyCumDeaths = Transformations.map(dailyDeaths) {
        val cd = getCumulativeValues(it)
        exports?.sortedData?.set(Exports.CUM_DEATHS, cd)
        return@map cd
    }

    val dailyWeightGain = Transformations.map(dailyWeight) {
        val wg = getWeightGain(it)
        exports?.sortedData?.set(Exports.WEIGHT_GAIN, wg)
        return@map wg
    }

    val dailyCumFeeds = Transformations.map(dailyFeeds) {
        val cf = getCumulativeValues(it)
        exports?.sortedData?.set(Exports.CUM_FEEDS, cf)
        return@map cf
    }

    val dailyCumEggs = Transformations.map(dailyEggs) {
        val ce = getCumulativeValues(it)
        exports?.sortedData?.set(Exports.CUM_EGGS, ce)
        return@map ce
    }

    val dailyCumWater = Transformations.map(dailyWater) {
        val cw = getCumulativeValues(it)
        exports?.sortedData?.set(Exports.CUM_WATER, cw)
        return@map cw
    }

    val weeklyFeeds = TransformationX(feeding) { getWeeklyAverageValues(it[0] as List<EntryItem>) }

    val weeklyWater = TransformationX(water) { getWeeklyAverageValues(it[0] as List<EntryItem>) }

    val weeklyWeight = TransformationX(weight) { getWeeklyWeight(it[0] as List<EntryItem>) }

    val weeklyDeaths = TransformationX(deaths) { getWeeklyTotalValues(it[0] as List<EntryItem>) }

    val weeklyEggs = TransformationX(eggs) { getWeeklyTotalValues(it[0] as List<EntryItem>) }

    val cumWeeklyFeeds = Transformations.map(weeklyFeeds) { getCumulativeValues(it) }

    val cumWeeklyWater = Transformations.map(weeklyWater) { getCumulativeValues(it) }

    val weeklyWeightGain = Transformations.map(weeklyWeight) { getWeightGain(it) }

    private fun getDailyValues(data: List<EntryItem>): Map<Int, Float> {
        val dailyValues: MutableMap<Int, Float> = hashMapOf()
        val timeline = DateUtils.getDailyTimeline(dob, closingDate, batchType)
        timeline.forEach { day ->
            val dd = data.filter { entryItem ->
                DateUtils.ageDaysFromDate(dob, entryItem.x) == day
            }
            dailyValues[day] = (if (dd.isEmpty()) 0f else dd[0].y)
        }
        return dailyValues
    }

    private fun getWeeklyAverageValues(data: List<EntryItem>): Map<Int, Float> {
        val weeklyValues: MutableMap<Int, Float> = hashMapOf()
        val timeline = DateUtils.getWeeklyTimeline(dob, closingDate, batchType)
        timeline.forEach { week ->
            var weekValue = 0f
            var count = 0
            data.forEachIndexed { _, entryItem ->
                if (DateUtils.ageWeeksFromDate(dob, entryItem.x) == week) {
                    weekValue += entryItem.y
                    count++
                } else {
                    weekValue += 0f
                }
            }
            weeklyValues[week] = if (weekValue > 0) (weekValue / count) else 0f
        }
        return weeklyValues
    }

    private fun getWeeklyTotalValues(data: List<EntryItem>): Map<Int, Float> {
        val weeklyValues: MutableMap<Int, Float> = hashMapOf()
        val timeline = DateUtils.getWeeklyTimeline(dob, closingDate, batchType)
        timeline.forEach { week ->
            var weekValue = 0f
            data.forEachIndexed { _, entryItem ->
                weekValue += if (DateUtils.ageWeeksFromDate(dob, entryItem.x) == week) {
                    entryItem.y
                } else {
                    0f
                }
            }
            weeklyValues[week] = weekValue
        }
        return weeklyValues
    }

    private fun getCumulativeValues(data: Map<Int, Float>): Map<Int, Float> {
        val cum: MutableMap<Int, Float> = HashMap()
        var lastValue = 0f
        data.forEach {
            cum[it.key] = it.value + lastValue
            lastValue += it.value
        }
        return cum
    }

    private fun getWeightGain(weightData: Map<Int, Float>): Map<Int, Float> {
        val gain: MutableMap<Int, Float> = hashMapOf()
        var lastWeight = 0f
        weightData.forEach {
            gain[it.key] = (it.value - lastWeight)
            lastWeight = it.value
        }
        return gain
    }

    private fun getWeeklyWeight(data: List<EntryItem>): Map<Int, Float> {
        val values = HashMap<Int, Float>()
        val timeline = DateUtils.getWeeklyTimeline(dob, closingDate, batchType)
        timeline.forEach { week ->
            val weekValue: Float
            val ww = arrayListOf<EntryItem>()
            data.forEachIndexed { _, entryItem ->
                if (DateUtils.ageWeeksFromDate(dob, entryItem.x) == week) {
                    ww.add(entryItem)
                }
            }
            weekValue = if (ww.isNotEmpty()) ww[ww.size - 1].y else 0f
            values[week] = weekValue
        }
        return values
    }


    private fun getUniformity(items: List<UniformityData>): Double {
        return if (items.isNotEmpty()) {
            val values = if (items.isNotEmpty()) items[0].data else ""
            val valuesArray = values.split(",").map { v -> v.toDouble() }
            if (valuesArray.isNotEmpty())
                calculateUniformity(valuesArray)
            else
                0.0
        } else {
            0.0
        }
    }

    private fun calculateUniformity(numArray: List<Double>): Double {

        var sum = 0.0

        var standardDeviation = 0.0

        for (num in numArray) {
            sum += num
        }

        val mean = sum / numArray.size

        for (num in numArray) {
            standardDeviation += (num - mean).pow(2.0)
        }

        val sd = sqrt(standardDeviation / numArray.size)

        return (1 - (sd / mean)) * 100
    }


}