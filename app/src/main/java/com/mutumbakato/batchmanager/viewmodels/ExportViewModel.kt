package com.mutumbakato.batchmanager.viewmodels

import android.os.Environment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.mutumbakato.batchmanager.data.StatisticsDataSource
import com.mutumbakato.batchmanager.data.models.exports.ExportConfig
import com.mutumbakato.batchmanager.data.models.exports.ExportData
import com.mutumbakato.batchmanager.utils.Excel
import com.mutumbakato.batchmanager.utils.Utils
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.io.File

class ExportViewModel constructor(val repository: StatisticsDataSource) : ViewModel() {

    private val isExporting = MutableLiveData<Boolean>()
    private val exportData = MutableLiveData<ExportData>()
    var exportFilePath = MutableLiveData<File>()
    val exportConfig = MutableLiveData<ExportConfig>()

    val exportRunning: LiveData<Boolean>
        get() = isExporting

    fun setExportConfig(data: ExportConfig?) {
        exportConfig.value = data
    }

    fun setBatchId(batchId: String) {
        exportConfig.value = exportConfig.value?.copy(batchId = batchId)
                ?: ExportConfig(batchId = batchId)
    }

    fun export(filePath: File, onFinish: () -> Unit, onError: (error: String) -> Unit) {
        exportFilePath.value = filePath
        isExporting.value = true
        repository.getExportData(exportConfig.value!!, {
            exportData.value = it
            doAsync {
                val file = writeToFile(it)
                uiThread {
                    isExporting.value = false
                    exportFilePath.value = file
                    if (file !== null) {
                        onFinish()
                    } else {
                        onError("Export failed!")
                    }
                }
            }
        }, onError)
    }

    private fun writeToFile(exportData: ExportData): File? {
        val file = File(Environment.getExternalStorageDirectory().path + "/${Utils.BASE_FILE_PATH}").apply {
            if (!exists()) {
                mkdir()
            }
        }
        return try {
            Excel("${file.path}/${exportConfig.value!!.batchName}.xls").write(exportData)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }
}
