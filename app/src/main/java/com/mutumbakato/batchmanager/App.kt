package com.mutumbakato.batchmanager

import android.content.ContextWrapper
import androidx.multidex.MultiDexApplication
import com.pixplicity.easyprefs.library.Prefs

/**
 * Created by cato on 11/12/17.
 */
class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()
    }

}
