package com.mutumbakato.batchmanager

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_privacy_policy.*
import kotlinx.android.synthetic.main.toolbar_flat.*
import java.util.*

class PrivacyPolicy : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)
        toolbar.title = "About"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        privacyPolicy.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://batchmanager-d0ce9.firebaseapp.com/privacy_policy.html"))
            startActivity(browserIntent)
        }
        try {
            version.text = String.format("Version %s", packageManager.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES).versionName)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        copyRight.text = String.format("Copyright @ %d Poultry Batch Manager", Calendar.getInstance().get(Calendar.YEAR))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, PrivacyPolicy::class.java)
            context.startActivity(starter)
        }
    }
}
