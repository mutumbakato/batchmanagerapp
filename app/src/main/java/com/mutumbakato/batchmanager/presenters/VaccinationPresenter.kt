package com.mutumbakato.batchmanager.presenters

import android.os.Handler
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationCheck
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import com.mutumbakato.batchmanager.data.repository.VaccinationRepository
import com.mutumbakato.batchmanager.data.repository.VaccinationScheduleRepository
import com.mutumbakato.batchmanager.ui.VaccinationContract
import java.util.*

class VaccinationPresenter(private var mScheduleId: String?,
                           private val mBatchId: String?,
                           private val mRepo: VaccinationRepository,
                           private val mScheduleRepo: VaccinationScheduleRepository,
                           private val mVaccinationListView: VaccinationContract.View) :
        VaccinationContract.Presenter,
        BaseDataSource.DataLoadCallback<Vaccination> {

    private val deleteHandler = Handler()
    private val deleteQueue: HashMap<String, Vaccination>

    init {
        mVaccinationListView.setPresenter(this)
        deleteQueue = LinkedHashMap()
    }

    override fun start() {
        loadData()
    }

    override fun loadVaccination(callback: BaseDataSource.DataLoadCallback<Vaccination>) {
        mVaccinationListView.showLoadingVaccinationsIndicator(true)
        if (mScheduleId != null) {
            mRepo.listVaccinations(mScheduleId!!, callback)
            mScheduleRepo.getSchedule(mScheduleId!!, "", object : BaseDataSource.DataItemCallback<VaccinationSchedule> {
                override fun onDataItemLoaded(data: VaccinationSchedule) {
                    if (mVaccinationListView.isActive) {
                        mVaccinationListView.showScheduleDetails(data.title)
                    }
                }

                override fun onDataItemNotAvailable() {}

                override fun onError(message: String) {}
            })
        } else {
            if (mVaccinationListView.isActive) {
                mVaccinationListView.showNoVaccinations()
            }
        }
    }

    override fun result(requestCode: Int, resultCode: Int) {}

    override fun editVaccination(vaccination: Vaccination) {
        if (mVaccinationListView.isActive) {
            mVaccinationListView.showForm(vaccination.id)
        }
    }

    override fun addVaccination() {
        if (mVaccinationListView.isActive) {
            mVaccinationListView.showForm(mScheduleId)
        }
    }

    override fun editSchedule() {}

    override fun removeSchedule(scheduleId: String) {
        mScheduleRepo.removeSchedule(mBatchId!!, scheduleId)
    }

    override fun setSchedule(scheduleId: String) {
        mScheduleId = scheduleId
    }

    override fun addToComplete(vaccination: Vaccination) {
        mRepo.addToComplete(mBatchId!!, vaccination)
    }

    override fun removeFromComplete(vaccination: Vaccination) {
        mRepo.removeFromComplete(mBatchId!!, vaccination)
    }

    override fun openVaccinationDetails(requestedVaccination: Vaccination) {
        if (mVaccinationListView.isActive) {
            mVaccinationListView.showVaccinationDetailsUi(requestedVaccination.id)
        }
    }

    override fun refreshVaccinations() {
        mRepo.refreshVaccinations()
        loadData()
    }

    override fun LoadMoreVaccinations() {

    }

    override fun saveVaccination(vaccination: Vaccination) {
        mRepo.createVaccination(vaccination)
    }

    override fun deleteVaccination(vaccination: Vaccination) {
        deleteQueue[vaccination.id] = vaccination
        mVaccinationListView.showUndoDelete(vaccination, DELETE_DELAY_TIME)
        deleteHandler.postDelayed({
            if (deleteQueue[vaccination.id] != null) {
                mRepo.deleteVaccinations(mScheduleId!!, vaccination.id)
                deleteQueue.remove(vaccination.id)
            }
        }, DELETE_DELAY_TIME.toLong())
    }

    override fun undoDelete(id: String) {
        mVaccinationListView.showDeleteUndoItem(deleteQueue[id])
        deleteQueue.remove(id)
    }

    override fun getBatchId(): String? {
        return mScheduleId
    }

    private fun loadData() {
        loadVaccination(this)
    }

    private fun getSortedVaccinations(vaccinations: List<Vaccination>): HashMap<Int, List<Vaccination>> {
        val sortedVaccinations = LinkedHashMap<Int, List<Vaccination>>()
        val dates = getSections(vaccinations)
        for (age in dates) {
            sortedVaccinations[age] = getSectionData(vaccinations, age)
        }
        return sortedVaccinations
    }

    private fun getSections(vaccinations: List<Vaccination>): List<Int> {
        val sections = ArrayList<Int>()
        for (vaccination in vaccinations) {
            if (!sections.contains(vaccination.age)) {
                sections.add(vaccination.age)
            }
        }
        sections.sortWith(Comparator { a1, a2 -> a1!! - a2!! })
        return sections
    }

    private fun getSectionData(vaccinations: List<Vaccination>, age: Int): List<Vaccination> {
        val exp = ArrayList<Vaccination>()
        for (vaccination in vaccinations) {
            if (vaccination.age == age) {
                exp.add(vaccination)
            }
        }
        exp.sortWith(Comparator { o1, o2 -> o1.infection.compareTo(o2.infection) })
        return exp
    }

    override fun onDataLoaded(data: List<Vaccination>) {
        if (mBatchId != null && mBatchId.isNotEmpty()) {
            mVaccinationListView.showLoadingVaccinationsIndicator(false)
            mRepo.getCheckList(mBatchId, object : BaseDataSource.DataLoadCallback<VaccinationCheck> {
                override fun onDataLoaded(data: List<VaccinationCheck>) {
                    if (mVaccinationListView.isActive) {
                        mVaccinationListView.showChecks(data)
                    }
                }

                override fun onEmptyData() {

                }

                override fun onError(message: String) {

                }
            })
        }

        if (mVaccinationListView.isActive) {
            mVaccinationListView.showLoadingVaccinationsIndicator(false)
            mVaccinationListView.showVaccinations(data)
            mVaccinationListView.showVaccinations(getSortedVaccinations(data))
        }

    }

    override fun onEmptyData() {
        if (mVaccinationListView.isActive) {
            mVaccinationListView.showLoadingVaccinationsIndicator(false)
            mVaccinationListView.showNoVaccinations()
        }
    }

    override fun onError(message: String) {
        if (mVaccinationListView.isActive) {
            mVaccinationListView.showLoadingVaccinationsIndicator(false)
            mVaccinationListView.showNoVaccinations()
        }
    }

    companion object {
        private const val DELETE_DELAY_TIME = 4000
    }

}
