package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.EggsDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.models.EggCount
import com.mutumbakato.batchmanager.data.models.Eggs
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.EggsRepository
import com.mutumbakato.batchmanager.ui.EggsListContract
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.UserRoles
import java.util.*

class EggsListPresenter(private val mBatchId: String, private val mEggsRepository: EggsRepository,
                        private val mBatchRepo: BatchRepository, private val mEggsListView: EggsListContract.View) : EggsListContract.Presenter, EggsDataSource.DataUpdatedListener {

    init {
        mEggsRepository.setDataChangedListener(this)
        mEggsListView.setPresenter(this)
    }

    override fun start() {
        loadEggs(false)
        getUser(mBatchId)
    }

    override fun result(requestCode: Int, resultCode: Int) {

    }

    override fun loadEggs(forceUpdate: Boolean) {
        loadEggs(forceUpdate, true)
    }

    override fun deleteEggs(eggs: Eggs) {
        mEggsListView.showConfirmDelete(eggs, DELETE_DELAY_TIME)
    }

    override fun delete(eggs: Eggs) {
        mEggsRepository.deleteDataItem(eggs, mBatchId)
    }

    override fun edit(eggs: Eggs) {
        mEggsListView.showEdit(eggs.id)
    }

    private fun loadEggs(forceUpdate: Boolean, showUILoadIndicator: Boolean) {

        if (showUILoadIndicator) {
            mEggsListView.setLoadingIndicator(true)
        }

        if (forceUpdate) {
            mEggsRepository.refreshData()
        }

        mEggsRepository.getAll(mBatchId, object : BaseDataSource.DataLoadCallback<Eggs> {
            override fun onDataLoaded(data: List<Eggs>) {
                if (!mEggsListView.isActive) {
                    return
                }
                mEggsListView.setLoadingIndicator(false)
                presentEggs(data)
            }

            override fun onEmptyData() {
                if (!mEggsListView.isActive)
                    return
                mEggsListView.setLoadingIndicator(false)
                mEggsListView.showNoEggs()
            }

            override fun onError(message: String) {
                mEggsListView.setLoadingIndicator(false)
            }
        })
    }

    private fun presentEggs(eggs: List<Eggs>) {
        Collections.sort(eggs) { eggs1, eggs2 -> eggs2.date.compareTo(eggs1.date) }
        if (eggs.isNotEmpty()) {
            mEggsListView.showEggs(eggs)
            mEggsRepository.getTotalEggs(mBatchId, object : EggsDataSource.OnEggsTotal {
                override fun onTotal(total: EggCount) {
                    val eggs12 = total.total + total.damage
                    val t = "${Currency.format(eggs12.toFloat())}"
                    mEggsListView.showTotalEggs(t)
                }
            })
        } else
            mEggsListView.showNoEggs()
    }

    override fun onDataUpdated() {
        loadEggs(forceUpdate = false, showUILoadIndicator = false)
    }

    private fun getUser(mBatchId: String) {
        mBatchRepo.getBatch(mBatchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                if (batch.userId == PreferenceUtils.userId) {
                    mEggsListView.applyPermissions(UserRoles.USER_ADMIN)
                } else
                    mBatchRepo.getBatchUser(mBatchId, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                        override fun onDataItemLoaded(data: BatchUser) {
                            if (mEggsListView.isActive) {
                                mEggsListView.applyPermissions(data.role)
                            }
                        }

                        override fun onDataItemNotAvailable() {

                        }

                        override fun onError(message: String) {

                        }
                    })
            }

            override fun onDataNotAvailable() {

            }
        })
    }

    companion object {

        private const val DELETE_DELAY_TIME = 4000
    }
}
