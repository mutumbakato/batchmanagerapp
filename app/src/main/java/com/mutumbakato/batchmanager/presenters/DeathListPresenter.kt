package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.DeathDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.models.Death
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.DeathRepository
import com.mutumbakato.batchmanager.ui.DeathListContract
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap
import kotlin.Comparator

class DeathListPresenter(private val mBatchId: String, private val mDateOfBirth: String,
                         private val mDeathRepository: DeathRepository, private val mBatchRepo: BatchRepository,
                         private val mDeathListView: DeathListContract.View) : DeathListContract.Presenter, DeathDataSource.DataUpdatedListener {

    init {
        mDeathRepository.setDataUpdateCallback(this)
        mDeathListView.setPresenter(this)
    }

    override fun start() {
        loadDeath(forceUpdate = false, showProgress = true)
    }

    override fun result(requestCode: Int, resultCode: Int) {

    }

    override fun goToWeek(week: Int) {
        if (mDeathListView.isActive) {
            mDeathListView.showWeek(week)
        }
    }

    override fun refreshDeath() {
        loadDeath(forceUpdate = true, showProgress = true)
    }

    override fun deleteDeath(death: Death) {
        mDeathListView.showConfirmDelete(death, DELETE_DELAY_TIME)
    }

    override fun delete(death: Death) {
        mDeathRepository.deleteDataItem(death, mBatchId)
    }

    override fun edit(death: Death) {
        mDeathListView.showEdit(death.id, death.date)
    }

    override fun toggleAdapter(type: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getBatchId(): String {
        return mBatchId
    }

    override fun loadDeath(forceUpdate: Boolean, showProgress: Boolean) {

        if (showProgress) {
            mDeathListView.showProgress(true)
        }

        if (forceUpdate) {
            mDeathRepository.refreshData()
        }

        mDeathRepository.getAll(mBatchId, object : BaseDataSource.DataLoadCallback<Death> {
            override fun onDataLoaded(data: List<Death>) {
                if (!mDeathListView.isActive) {
                    return
                }
                if (showProgress) {
                    mDeathListView.showProgress(false)
                }
                presentDeath(data)
                getUser(mBatchId)
            }

            override fun onEmptyData() {
                if (!mDeathListView.isActive)
                    return
                if (showProgress)
                    mDeathListView.showProgress(false)
                mDeathListView.showNoDeath()
            }

            override fun onError(message: String) {

            }
        })
    }

    private fun getUser(mBatchId: String) {
        mBatchRepo.getBatch(mBatchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                if (batch.userId == PreferenceUtils.userId) {
                    mDeathListView.applyPermissions(UserRoles.USER_ADMIN)
                } else
                    mBatchRepo.getBatchUser(mBatchId, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                        override fun onDataItemLoaded(data: BatchUser) {
                            if (mDeathListView.isActive) {
                                mDeathListView.applyPermissions(data.role)
                            }
                        }

                        override fun onDataItemNotAvailable() {

                        }

                        override fun onError(message: String) {

                        }
                    })
            }

            override fun onDataNotAvailable() {

            }
        })
    }

    private fun presentDeath(death: List<Death>) {
        if (death.isNotEmpty()) {
            mDeathListView.showDeaths(death)
            mDeathListView.showSectionedDeaths(getSortedDeaths(death))
            mDeathRepository.getMortalityRate(mBatchId, object : DeathDataSource.OnMortalityRate {
                override fun onMortalityLoaded(rate: Int) {
                    mDeathListView.showMortalityRate(if (rate < 10) "0$rate" else rate.toString())
                }
            })
        } else
            mDeathListView.showNoDeath()
    }

    override fun onDataUpdated() {
        loadDeath(forceUpdate = false, showProgress = false)
    }

    private fun getSortedDeaths(deaths: List<Death>): HashMap<Int, List<Death>> {
        val sortedDeaths = LinkedHashMap<Int, List<Death>>()
        val dates = getSections(deaths)
        for (date in dates) {
            sortedDeaths[date] = getSectionData(deaths, date)
        }
        return sortedDeaths
    }

    private fun getSections(deaths: List<Death>): List<Int> {
        val sections = ArrayList<Int>()
        for (expense in deaths) {
            val day = DateUtils.ageDaysFromDate(mDateOfBirth, expense.date)
            if (!sections.contains(day))
                sections.add(day)
        }
        sections.sortWith(Comparator { o1, o2 -> o2!!.compareTo(o1!!) })
        return sections
    }

    private fun getSectionData(deaths: List<Death>, date: Int): List<Death> {
        val exp = ArrayList<Death>()
        for (expense in deaths) {
            if (DateUtils.ageDaysFromDate(mDateOfBirth, expense.date) == date)
                exp.add(expense)
        }
        exp.sortWith(Comparator { o1, o2 -> o1.date.compareTo(o2.date) })
        return exp
    }

    companion object {
        private const val DELETE_DELAY_TIME = 4000
    }
}
