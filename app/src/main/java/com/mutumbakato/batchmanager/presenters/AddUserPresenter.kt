package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.UserRepository
import com.mutumbakato.batchmanager.ui.AddUserContract
import com.mutumbakato.batchmanager.utils.UserRoles
import java.util.*

class AddUserPresenter(private val mBatchId: String, private val ownerId: String, private val mRepository: BatchRepository, private val userRepository: UserRepository, private val mView: AddUserContract.View) : AddUserContract.Presenter, BaseDataSource.DataLoadCallback<BatchUser>, BaseDataSource.DataItemCallback<String> {
    private var users: List<BatchUser> = ArrayList()

    init {
        mView.setPresenter(this)
    }

    override fun addUser(user: String, role: String, type: String, extra: String) {
        if (type == BatchUser.BATCH_USER_TYPE_PBM_USER) {
            addUserByEmail(user, role)
        } else {
            addUserFromEmployees(user, role, extra)
        }
    }

    override fun addUserByEmail(email: String, role: String) {
        if (mView.isActive) {
            mView.showProgress(true)
        }
        userRepository.getUserByEmail(email, object : UserDataSource.UserCallBack {
            override fun onSuccess(user: User?) {
                val u = BatchUser(UUID.randomUUID().toString(), mBatchId, user!!.id,
                        user.name, role, user.email, PreferenceUtils.userId,
                        Calendar.getInstance().time.toString())
                mRepository.addUser(u, object : BaseDataSource.DataItemCallback<String> {
                    override fun onDataItemLoaded(data: String) {
                        if (mView.isActive) {
                            mView.showProgress(false)
                            mView.showSuccessMessage(user.name + " has been added!")
                            loadBatchUsers(mBatchId, false)
                        }
                    }

                    override fun onDataItemNotAvailable() {
                        if (mView.isActive) {
                            mView.showProgress(false)
                            mView.showError("User not found with email $email")
                        }
                    }

                    override fun onError(message: String) {
                        if (mView.isActive) {
                            mView.showProgress(false)
                            mView.showError(message)
                        }
                    }
                })
            }

            override fun onError(message: String) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showError(message)
                }
            }
        })
    }

    override fun addUserFromEmployees(employeeId: String, role: String, extra: String) {
        val user = BatchUser(UUID.randomUUID().toString(), mBatchId, employeeId, extra, role, "Employee", PreferenceUtils.userId, Calendar.getInstance().time.toString())
        mRepository.addUser(user, object : BaseDataSource.DataItemCallback<String> {
            override fun onDataItemLoaded(data: String) {
                if (mView.isActive) {
                    mView.showSuccessMessage("$extra has been added!")
                    loadBatchUsers(mBatchId, false)
                }
            }

            override fun onDataItemNotAvailable() {

            }

            override fun onError(message: String) {
                if (mView.isActive)
                    mView.showError(message)
            }
        })
    }

    override fun changeRole(user: BatchUser, role: String) {
        val u = BatchUser(user.id, user.batchId, user.userId, user.name, role, user.extra, user.addedBy, user.date)
        mRepository.updateBatchUser(u, object : BaseDataSource.DataItemCallback<BatchUser> {

            override fun onDataItemLoaded(data: BatchUser) {
                if (mView.isActive) {
                    mView.showSuccessMessage("Done!")
                    loadBatchUsers(mBatchId)
                }
            }

            override fun onDataItemNotAvailable() {

            }

            override fun onError(message: String) {
                if (mView.isActive)
                    mView.showError(message)
            }
        })
    }

    override fun removeUser(user: BatchUser) {
        mRepository.removeUser(user, this)
    }

    override fun leave(userId: String) {
        if (mView.isActive) {
            mView.showProgress(true)
        }
        mRepository.getBatchUser(mBatchId, userId, object : BaseDataSource.DataItemCallback<BatchUser> {
            override fun onDataItemLoaded(data: BatchUser) {
                mRepository.removeUser(data, this@AddUserPresenter)
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showBatches()
                }
            }

            override fun onDataItemNotAvailable() {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showError("User not found")
                }
            }

            override fun onError(message: String) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showError(message)
                }
            }
        })
    }

    override fun chooseSource() {
        if (mView.isActive) {
            mView.showSourceChooser()
        }
    }

    override fun addyEmail() {
        if (mView.isActive) {
            mView.showEmailDialog()
        }
    }

    override fun chooseFromEmployees() {

    }

    override fun loadBatchUsers(batchId: String) {
        if (mView.isActive) {
            mView.showProgress(true)
        }
        mRepository.getBatchUsers(batchId, this)
    }

    override fun applyPermissions(role: String) {
        if (mView.isActive) {
            mView.applyPermissions(role)
            if (ownerId == PreferenceUtils.userId)
                mView.hideLeaveButton()
        }
    }

    override fun start() {
        loadBatchUsers(mBatchId, true)
    }

    private fun loadBatchUsers(mBatchId: String, b: Boolean) {
        if (b && mView.isActive) {
            mView.showProgress(true)
        }
        loadBatchUsers(mBatchId)
    }

    override fun onDataLoaded(data: List<BatchUser>) {
        this.users = data
        if (mView.isActive) {
            mView.showProgress(false)
            mView.showBatchUsers(data)
        }
        checkForOwner()
    }

    private fun checkForOwner() {
        var owner: BatchUser? = null
        for (o in users) {
            if (o.userId == ownerId) {
                owner = o
            }
        }

        if (owner == null) {
            createOwner()
        }
    }

    private fun createOwner() {
        userRepository.getUserById(ownerId, object : UserDataSource.UserCallBack {
            override fun onSuccess(user: User?) {
                val owner = BatchUser(UUID.randomUUID().toString(), mBatchId, user!!.id,
                        user.name, UserRoles.USER_OWNER, user.email, PreferenceUtils.userId,
                        Calendar.getInstance().time.toString())
                mRepository.addUser(owner, object : BaseDataSource.DataItemCallback<String> {
                    override fun onDataItemLoaded(data: String) {
                        loadBatchUsers(mBatchId, false)
                    }

                    override fun onDataItemNotAvailable() {

                    }

                    override fun onError(message: String) {

                    }
                })

            }

            override fun onError(message: String) {

            }
        })
    }

    override fun onEmptyData() {
        if (mView.isActive) {
            mView.showProgress(false)
            mView.showNoUsers()
        }
    }


    override fun onError(message: String) {
        if (mView.isActive) {
            mView.showError(message)
        }
    }

    override fun onDataItemLoaded(data: String) {
        if (mView.isActive) {
            mView.showSuccessMessage("User has been removed!")
            loadBatchUsers(mBatchId)
        }
    }

    override fun onDataItemNotAvailable() {

    }

    fun getUsers(): List<String> {
        val users = ArrayList<String>()
        for (user in this.users) {
            users.add(user.userId)
        }
        return users
    }
}
