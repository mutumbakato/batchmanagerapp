package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.FarmDataSource
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.repository.FarmRepository
import com.mutumbakato.batchmanager.ui.FarmDetailsContract

class FarmDetailsPresenter(private val mFarmId: String, private val farmRepo: FarmRepository, private val mView: FarmDetailsContract.View) : FarmDetailsContract.Presenter, FarmDataSource.FarmCallback {
    private var mFarm: Farm? = null

    init {
        this.mView.setPresenter(this)
    }

    override fun start() {
        loadFarmData(true)
    }

    override fun refresh() {

    }

    override fun loadFarmData(showLoading: Boolean) {
        if (showLoading && mView.isActive) {
            mView.showProgress(true)
        }
        farmRepo.getFarm(mFarmId, this)
    }

    override fun updateFarm(farm: Farm) {
        farmRepo.updateFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                if (mView.isActive) {
                    showFarm(farm)
                    mView.showSuccessMessage("Saved")
                }

            }

            override fun onFail(message: String) {

            }
        })
    }

    override fun deleteFarm() {
        if (mView.isActive)
            mView.showProgress(true)
        farmRepo.deleteFarm(mFarm!!, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.exits()
                }
            }

            override fun onFail(message: String) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showError(message)
                }
            }
        })
    }

    override fun editFarm() {
        if (mView.isActive) {
            mView.showForm(mFarmId)
        }
    }

    override fun onLoad(farm: Farm) {
        showFarm(farm)
    }

    private fun showFarm(farm: Farm) {
        mFarm = farm
        if (mView.isActive) {
            mView.showFarm(farm)
            mView.showProgress(false)
            mView.showName(farm.name)
            mView.showLocation(farm.location)
            mView.showContacts(farm.contact)
            mView.showUnits(farm.units)
            mView.showCurrency(farm.currency)
        }
    }

    override fun onFail(message: String) {
        if (mView.isActive) {
            mView.showProgress(false)
            mView.showError(message)
        }
    }
}
