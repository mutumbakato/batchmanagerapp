package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.ExpenseCategoriesDataSource
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.models.ExpenseCategory
import com.mutumbakato.batchmanager.data.repository.ExpenseCategoriesRepository
import com.mutumbakato.batchmanager.data.repository.ExpenseRepository
import com.mutumbakato.batchmanager.data.utils.Data
import com.mutumbakato.batchmanager.ui.ExpenseFormContract

class ExpenseFormPresenter(private val mBatchId: String, private var mExpenseId: String?, private val mExpenseRepository: ExpenseRepository,
                           private val mCategoryRepository: ExpenseCategoriesRepository,
                           private val mExpenseFormView: ExpenseFormContract.View) : ExpenseFormContract.Presenter, BaseDataSource.DataItemCallback<Expense>, ExpenseCategoriesDataSource.CategoriesLoadCallback {
    private var mServerId: Long = 0

    init {
        mExpenseFormView.setPresenter(this)
    }

    override fun start() {
        loadCategories()
        if (!isNew) {
            this.populate()
        } else {
            if (mExpenseFormView.isActive)
                mExpenseFormView.clearInputs()
        }
    }

    override fun save(date: String, category: String, description: String, amount: Float, bacthId: String) {
        if (isNew) {
            createExpense(date, category, description, amount, bacthId)
            mExpenseFormView.clearInputs()
        } else {
            updateExpense(date, category, description, amount, mServerId, bacthId)
            mExpenseFormView.toggleExpansion(false)
        }
    }

    override fun isNew(): Boolean {
        return mExpenseId == null
    }

    override fun populate() {
        mExpenseRepository.getOne(mExpenseId!!, mBatchId, this)
    }

    override fun startEditing(id: String) {
        mExpenseId = id
        start()
        if (mExpenseFormView.isActive) {
            mExpenseFormView.toggleExpansion(true)
        }
    }

    override fun stopEditing() {
        if (mExpenseFormView.isActive) {
            mExpenseFormView.toggleExpansion(false)
        }
    }

    private fun loadCategories() {
        mCategoryRepository.getAllCategories(this)
    }

    override fun onCategoriesLoaded(categories: List<ExpenseCategory>) {
        if (mExpenseFormView.isActive)
            mExpenseFormView.setCategories(categories)
    }

    override fun onCategoriesNotAvailable() {

    }

    override fun onDataItemLoaded(data: Expense) {
        mServerId = data.serverId
        if (mExpenseFormView.isActive) {
            mExpenseFormView.setDate(data.date)
            mExpenseFormView.setDescription(data.description)
            mExpenseFormView.setAmount(data.amount.toString())
            mExpenseFormView.setCategory(data.category)
        }
    }

    override fun onDataItemNotAvailable() {

    }

    override fun onError(message: String) {

    }

    private fun createExpense(date: String, category: String, description: String, amount: Float, batchId: String) {
        val newExpense = Expense(mBatchId, date, category, description, amount)
        mExpenseRepository.saveData(newExpense)
    }

    private fun updateExpense(date: String, category: String, description: String, amount: Float, serverId: Long, batchId: String) {
        val updatedExpense = Expense(mExpenseId!!, mBatchId, date, category, description, amount, serverId, Data.STATUS_NORMAL, 0)
        mExpenseRepository.updateData(updatedExpense)
    }
}
