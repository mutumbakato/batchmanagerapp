package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.repository.VaccinationRepository
import com.mutumbakato.batchmanager.ui.VaccinationFromContract
import java.util.*

class VaccinationFromPresenter(private val mScheduleId: String, private val mVaccinationId: String?, private val mVaccinationRepository: VaccinationRepository, private val mVaccinationFormView: VaccinationFromContract.View) : VaccinationFromContract.Presenter, BaseDataSource.DataItemCallback<Vaccination> {

    private var mServerId: Long = 0

    private val isNewVaccination: Boolean
        get() = mVaccinationId == null

    init {
        mVaccinationFormView.setPresenter(this)
    }

    override fun start() {
        if (!isNewVaccination) {
            populateVaccination()
        }
    }

    override fun saveVaccination(age: String, thenEvery: String,
                                 infection: String, vaccine: String, method: String, recurrent: Boolean) {
        if (isNewVaccination) {
            createVaccination(mScheduleId, age, thenEvery, infection, vaccine, method, recurrent)
        } else {
            updateVaccination(mScheduleId, age, thenEvery, infection, vaccine, method, recurrent)
        }
    }

    override fun deleteVaccination(vaccinationId: String) {
        mVaccinationRepository.deleteVaccinations(mScheduleId, vaccinationId)
    }

    override fun populateVaccination() {
        if (isNewVaccination) {
            throw RuntimeException("populateVaccination() was called but vaccination is new.")
        }
        mVaccinationRepository.getVaccination(mScheduleId, mVaccinationId!!, this)
    }

    private fun createVaccination(scheduleId: String, age: String, thenEvery: String, infection: String, vaccine: String, method: String, recurrent: Boolean) {
        val newVaccination = Vaccination(UUID.randomUUID().toString(),
                scheduleId, Integer.parseInt(age), thenEvery, infection, vaccine, method,
                recurrent, 0, "normal", 0)
        mVaccinationRepository.createVaccination(newVaccination)
        mVaccinationFormView.showVaccinationList()
    }

    private fun updateVaccination(scheduleId: String, age: String, thenEvery: String, infection: String, vaccine: String, method: String, recurrent: Boolean) {
        if (isNewVaccination) {
            throw RuntimeException("updateVaccination() was called but task is new.")
        }

        val updateVaccination = Vaccination(mVaccinationId!!,
                scheduleId, Integer.parseInt(age), thenEvery, infection, vaccine, method,
                recurrent, mServerId, "normal", 0)

        mVaccinationRepository.updateVaccination(updateVaccination)
        mVaccinationFormView.showVaccinationList() // After an edit, go back to the list.
    }

    override fun onDataItemLoaded(vaccination: Vaccination) {
        mServerId = vaccination.serverId
        // The view may not be able to handle UI updates anymore
        if (mVaccinationFormView.isActive) {
            mVaccinationFormView.setVaccine(vaccination.vaccine)
            mVaccinationFormView.setAge(vaccination.age.toString())
            mVaccinationFormView.setDisease(vaccination.infection.toString())
            mVaccinationFormView.setMethod(vaccination.method.toString())
            mVaccinationFormView.setThenEvery(vaccination.thenEvery.toString())
        }
    }

    override fun onDataItemNotAvailable() {
        if (mVaccinationFormView.isActive)
            mVaccinationFormView.showEmptyVaccinationError()
    }

    override fun onError(message: String) {

    }
}
