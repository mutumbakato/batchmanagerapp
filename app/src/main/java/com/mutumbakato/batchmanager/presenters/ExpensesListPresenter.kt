package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.ExpenseDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.ExpenseRepository
import com.mutumbakato.batchmanager.ui.ExpensesListContract
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.UserRoles
import java.util.*


class ExpensesListPresenter(private val mBatchId: String, private val mRepo: ExpenseRepository, private val mBatchRepo: BatchRepository, private val mExpenseListView: ExpensesListContract.View) : ExpensesListContract.Presenter, ExpenseDataSource.DataUpdatedListener, BaseDataSource.DataLoadCallback<Expense> {

    init {
        mRepo.setOnDataUpdatedListener(this)
        mExpenseListView.setPresenter(this)
    }

    override fun start() {
        loadExpense(forceUpdate = false, showProgress = true)
        getUser(mBatchId)
    }

    override fun loadExpense(forceUpdate: Boolean, showProgress: Boolean) {

        if (forceUpdate) {
            mRepo.refreshData()
        }

        if (showProgress) {
            if (mExpenseListView.isActive) {
                mExpenseListView.showExpenseProgress(true)
            }
        }
        mRepo.getAll(mBatchId, this)
    }

    override fun result(requestCode: Int, resultCode: Int) {

    }

    override fun editExpense(expense: Expense) {
        if (mExpenseListView.isActive) {
            mExpenseListView.showForm(expense.id)
        }
    }

    override fun deleteExpense(expense: Expense) {
        mExpenseListView.showConfirmDelete(expense, DELETE_DELAY_TIME)
    }

    override fun delete(expense: Expense) {
        mRepo.deleteDataItem(expense, mBatchId)
    }

    override fun getBatchId(): String {
        return mBatchId
    }

    override fun onDataUpdated() {
        loadExpense(forceUpdate = false, showProgress = false)
    }

    override fun onDataLoaded(data: List<Expense>) {
        if (!mExpenseListView.isActive)
            return
        if (data.isNotEmpty()) {
            mExpenseListView.showExpenseProgress(false)
            mExpenseListView.showCategorisedExpenses(getSections(data), getSortedExpenses(data))
            mRepo.getTotalExpenses(mBatchId, object : ExpenseDataSource.ExpenseTotalCallback {
                override fun onTotal(total: Float) {
                    mExpenseListView.showTotalExpenses(Currency.format(total))
                }
            })
        } else {
            mExpenseListView.showNoExpenses()
        }
    }

    override fun onEmptyData() {
        if (mExpenseListView.isActive) {
            mExpenseListView.showExpenseProgress(false)
            mExpenseListView.showNoExpenses()
        }
    }

    override fun onError(message: String) {
        if (mExpenseListView.isActive) {
            mExpenseListView.showExpenseProgress(false)
            mExpenseListView.showLoadingExpenseError(message)
        }
    }

    private fun getSortedExpenses(expenses: List<Expense>): HashMap<String, List<Expense>> {
        val sortedExpenses = LinkedHashMap<String, List<Expense>>()
        val dates = getSections(expenses)
        for (date in dates) {
            sortedExpenses[date] = getSectionData(expenses, date)
        }
        return sortedExpenses
    }

    private fun getSections(expenses: List<Expense>): List<String> {
        val sections = ArrayList<String>()

        for (expense in expenses) {
            if (!sections.contains(expense.date))
                sections.add(expense.date)
        }
        sections.sortWith(Comparator { o1, o2 -> o2.compareTo(o1) })
        return sections
    }

    private fun getSectionData(expenses: List<Expense>, date: String): List<Expense> {
        val exp = ArrayList<Expense>()
        for (expense in expenses) {
            if (expense.date == date)
                exp.add(expense)
        }
        exp.sortWith(Comparator { o1, o2 -> o1.description.compareTo(o2.description) })
        return exp
    }

    private fun getUser(mBatchId: String) {
        mBatchRepo.getBatch(mBatchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                if (batch.userId == PreferenceUtils.userId) {
                    mExpenseListView.applyPermissions(UserRoles.USER_ADMIN)
                } else
                    mBatchRepo.getBatchUser(mBatchId, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                        override fun onDataItemLoaded(data: BatchUser) {
                            if (mExpenseListView.isActive) {
                                mExpenseListView.applyPermissions(data.role)
                            }
                        }

                        override fun onDataItemNotAvailable() {

                        }

                        override fun onError(message: String) {

                        }
                    })
            }

            override fun onDataNotAvailable() {}
        })
    }

    companion object {

        private const val DELETE_DELAY_TIME = 4000
    }
}
