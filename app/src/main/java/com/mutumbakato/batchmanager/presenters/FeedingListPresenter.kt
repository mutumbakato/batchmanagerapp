package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.FeedingDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.FeedingRepository
import com.mutumbakato.batchmanager.ui.FeedingListContract
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap
import kotlin.Comparator

class FeedingListPresenter(private val mBatchId: String, private val mDateOfBirth: String, private val mFeedingRepository: FeedingRepository, private val mBatchRepo: BatchRepository,
                           private val mFeedsListView: FeedingListContract.View) : FeedingListContract.Presenter, FeedingDataSource.DataUpdatedListener {

    init {
        mFeedingRepository.setDataUpdateCallback(this)
        mFeedsListView.setPresenter(this)
    }

    override fun start() {
        loadFeeds(false)
        getUser(mBatchId)
    }

    override fun result(requestCode: Int, resultCode: Int) {

    }

    override fun loadFeeds(forceUpdate: Boolean) {
        loadFeeds(forceUpdate, true)
    }

    override fun deleteFeeds(feeding: Feeding) {
        mFeedsListView.showConfirmDelete(feeding, DELETE_DELAY_TIME)
    }

    override fun delete(feeding: Feeding) {
        mFeedingRepository.deleteDataItem(feeding, mBatchId)
    }

    override fun edit(feeding: Feeding) {
        mFeedsListView.showEdit(feeding.id)
    }

    override fun gotToWeek() {
        if (mFeedsListView.isActive)
            mFeedsListView.goToWeek()
    }

    private fun loadFeeds(forceUpdate: Boolean, showUILoadIndicator: Boolean) {
        if (showUILoadIndicator) {
            mFeedsListView.showFeedsProgress(true)
        }
        if (forceUpdate) {
            mFeedingRepository.refreshData()
        }
        mFeedingRepository.getAll(mBatchId, object : BaseDataSource.DataLoadCallback<Feeding> {
            override fun onDataLoaded(data: List<Feeding>) {
                if (!mFeedsListView.isActive) {
                    return
                }
                if (showUILoadIndicator) {
                    mFeedsListView.showFeedsProgress(false)
                }
                presentFeeds(data)
            }

            override fun onEmptyData() {
                if (!mFeedsListView.isActive)
                    return
                if (showUILoadIndicator)
                    mFeedsListView.showFeedsProgress(false)
                mFeedsListView.showNoFeeds()
            }

            override fun onError(message: String) {
                if (showUILoadIndicator)
                    mFeedsListView.showFeedsProgress(false)
            }
        })
    }

    private fun presentFeeds(feeding: List<Feeding>) {
        if (feeding.isNotEmpty()) {
            mFeedsListView.showFeeds(feeding)
            mFeedsListView.showSectionedFeeds(getSortedFeeds(feeding))
            mFeedingRepository.getMortalityRate(mBatchId, object : FeedingDataSource.OnMortalityRate {
                override fun onMortalityLoaded(rate: Int) {
                    mFeedsListView.showMortalityRate(rate.toString())
                }
            })
        } else
            mFeedsListView.showNoFeeds()
    }

    override fun onDataUpdated() {
        loadFeeds(forceUpdate = false, showUILoadIndicator = false)
    }

    private fun getUser(mBatchId: String) {
        mBatchRepo.getBatch(mBatchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                if (batch.userId == PreferenceUtils.userId) {
                    mFeedsListView.applyPermissions(UserRoles.USER_ADMIN)
                } else
                    mBatchRepo.getBatchUser(mBatchId, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                        override fun onDataItemLoaded(data: BatchUser) {
                            if (mFeedsListView.isActive) {
                                mFeedsListView.applyPermissions(data.role)
                            }
                        }

                        override fun onDataItemNotAvailable() {

                        }

                        override fun onError(message: String) {

                        }
                    })
            }

            override fun onDataNotAvailable() {

            }
        })
    }

    private fun getSortedFeeds(feeds: List<Feeding>): HashMap<Int, List<Feeding>> {
        val sortedFeeds = LinkedHashMap<Int, List<Feeding>>()
        val dates = getSections(feeds)
        for (date in dates) {
            sortedFeeds[date] = getSectionData(feeds, date)
        }
        return sortedFeeds
    }

    private fun getSections(feeds: List<Feeding>): List<Int> {
        val sections = ArrayList<Int>()
        for (expense in feeds) {
            val day = DateUtils.ageDaysFromDate(mDateOfBirth, expense.date)
            if (!sections.contains(day))
                sections.add(day)
        }
        sections.sortWith(Comparator { o1, o2 -> o2!!.compareTo(o1!!) })
        return sections
    }

    private fun getSectionData(feeds: List<Feeding>, date: Int): List<Feeding> {
        val exp = ArrayList<Feeding>()
        for (expense in feeds) {
            if (DateUtils.ageDaysFromDate(mDateOfBirth, expense.date) == date)
                exp.add(expense)
        }
        exp.sortWith(Comparator { o1, o2 -> o1.date.compareTo(o2.date) })
        return exp
    }


    companion object {

        private const val DELETE_DELAY_TIME = 4000
    }


}
