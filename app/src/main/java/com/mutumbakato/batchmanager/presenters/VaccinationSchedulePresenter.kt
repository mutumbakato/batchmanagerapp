package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.VaccinationScheduleDataSource
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.VaccinationScheduleRepository
import com.mutumbakato.batchmanager.ui.VaccinationScheduleContract
import com.mutumbakato.batchmanager.utils.Utils
import java.util.*

/**
 * Created by cato on 11/2/17.
 */
class VaccinationSchedulePresenter(private val mBatchId: String,
                                   private val mRepo: VaccinationScheduleRepository,
                                   private val mView: VaccinationScheduleContract.View) :
        VaccinationScheduleContract.Presenter,
        BaseDataSource.DataLoadCallback<VaccinationSchedule>,
        VaccinationScheduleDataSource.DataUpdatedListener {

    init {
        mView.setPresenter(this)
        mRepo.setDataUpdateCallback(this)
    }

    override fun saveSchedule(title: String, description: String) {
        val schedule = VaccinationSchedule(UUID.randomUUID().toString(),
                PreferenceUtils.farmId, title, description, "private",
                0, Utils.DEFAULT_STATUS, 0, PreferenceUtils.farmId)
        mRepo.saveSchedule(schedule)
    }

    override fun updateSchedule(schedule: VaccinationSchedule) {
        mRepo.updateSchedule(schedule)
    }

    override fun refresh() {
        mRepo.refreshData()
        loadSchedule(true)
    }

    override fun useSchedule(vaccinationSchedule: VaccinationSchedule) {
        mRepo.useSchedule(mBatchId, vaccinationSchedule)
    }

    override fun removeSchedule(id: String) {
        mRepo.removeSchedule(mBatchId, id)
    }

    override fun deleteSchedule(schedule: VaccinationSchedule) {
        mRepo.deleteSchedule(schedule.id, PreferenceUtils.farmId)
        refresh()
    }

    override fun start() {
        loadSchedule(true)
    }

    private fun loadSchedule(showLoading: Boolean) {
        if (showLoading && mView.isActive) {
            mView.showLoadingSchedules(true)
        }
        mRepo.getAllSchedules(PreferenceUtils.farmId, this)
    }

    override fun onDataLoaded(data: List<VaccinationSchedule>) {
        if (mView.isActive) {
            mView.showLoadingSchedules(false)
            mView.showSchedules(data)
        }
    }

    override fun onEmptyData() {
        if (mView.isActive) {
            mView.showNoSchedules()
            mView.showLoadingSchedules(false)
        }
    }

    override fun onError(message: String) {
        if (mView.isActive) {
            mView.showScheduleError(message)
            mView.showLoadingSchedules(false)
        }
    }

    override fun onDataUpdated() {
        loadSchedule(false)
    }
}
