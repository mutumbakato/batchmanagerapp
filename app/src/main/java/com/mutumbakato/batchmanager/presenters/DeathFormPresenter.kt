package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.models.Death
import com.mutumbakato.batchmanager.data.repository.DeathRepository
import com.mutumbakato.batchmanager.data.utils.Data
import com.mutumbakato.batchmanager.ui.DeathFormContract

class DeathFormPresenter(private var mId: String?, private val mBatchId: String, private val mRepository: DeathRepository, private val mView: DeathFormContract.View) : DeathFormContract.Presenter, BaseDataSource.DataItemCallback<Death> {

    private var mServerId: Long = 0

    override val isNew: Boolean
        get() = mId == null

    init {
        mView.setPresenter(this)
    }

    override fun populate() {
        mRepository.getOne(mId!!, mBatchId, this)
    }

    override fun save(date: String, count: Int, comment: String) {
        if (isNew) {
            createDeath(date, count, comment)
            stopEditing()
        } else {
            updateDeath(date, count, comment)
            stopEditing()
        }
    }

    override fun startEditing(id: String?, date: String) {
        mId = id
        start()
        if (mView.isActive) {
            mView.setDate(date)
            mView.toggleExpansion(true)
        }
    }

    override fun stopEditing() {
        if (mView.isActive) {
            mView.toggleExpansion(false)
        }
    }

    override fun start() {
        if (!isNew) {
            populate()
        } else {
            if (mView.isActive) {
                mView.clearInputs()
            }
        }
    }

    private fun updateDeath(date: String, count: Int, comment: String) {
        val death = Death(mId!!, mBatchId, date, count, comment, mServerId, Data.STATUS_NORMAL, 0)
        mRepository.updateData(death)
    }

    private fun createDeath(date: String, count: Int, comment: String) {
        val death = Death(mBatchId, date, count, comment)
        mRepository.saveData(death)
    }

    override fun onDataItemLoaded(data: Death) {
        mServerId = data.serverId
        if (mView.isActive) {
            mView.setDate(data.date)
            mView.setCount(data.count.toString())
            mView.setComment(data.comment)
        }
    }

    override fun onDataItemNotAvailable() {

    }

    override fun onError(message: String) {

    }
}
