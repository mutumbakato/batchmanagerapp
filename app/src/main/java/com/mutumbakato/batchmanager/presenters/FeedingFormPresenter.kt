package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.repository.FeedingRepository
import com.mutumbakato.batchmanager.data.utils.Data
import com.mutumbakato.batchmanager.ui.FeedingFormContract

class FeedingFormPresenter(private var mId: String?,
                           private val mBatchId: String,
                           private val mRepository: FeedingRepository,
                           private val mView: FeedingFormContract.View) : FeedingFormContract.Presenter, BaseDataSource.DataItemCallback<Feeding> {

    private var mServerId: Long = 0
    private var mDate: String? = null

    override val isNew: Boolean
        get() = mId == null

    init {
        mView.setPresenter(this)
    }

    override fun populate() {
        mRepository.getOne(mId!!, mBatchId, this)
    }

    override fun save(date: String, count: Float, comment: String) {
        if (isNew) {
            createFeeds(date, count, comment)
            stopEditing()
        } else {
            updateFeeds(date, count, comment)
            stopEditing()
        }
    }

    override fun startEditing(id: String?, date: String?) {
        mId = id
        mDate = date
        start()
        if (mView.isActive) {
            mView.toggleExpansion(true)
        }
    }

    override fun stopEditing() {
        if (mView.isActive) {
            mView.toggleExpansion(false)
        }
    }

    override fun start() {
        if (!isNew) {
            populate()
        } else {
            if (mView.isActive) {
                mView.clearInputs()
                mView.setDate(mDate!!)
            }
        }
    }

    private fun updateFeeds(date: String, count: Float, comment: String) {
        val feeding = Feeding(mId!!, mBatchId, date, count, comment, mServerId, Data.STATUS_NORMAL, 0)
        mRepository.updateData(feeding)
    }

    private fun createFeeds(date: String, count: Float, comment: String) {
        val feeding = Feeding(mBatchId, date, count, comment)
        mRepository.saveData(feeding)
    }

    override fun onDataItemLoaded(data: Feeding) {
        mServerId = data.serverId
        if (mView.isActive) {
            mView.setDate(data.date)
            mView.setCount(data.quantity.toString())
            mView.setComment(data.item)
        }
    }

    override fun onDataItemNotAvailable() {

    }

    override fun onError(message: String) {

    }
}
