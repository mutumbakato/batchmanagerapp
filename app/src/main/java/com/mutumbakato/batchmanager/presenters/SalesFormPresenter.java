package com.mutumbakato.batchmanager.presenters;

import com.mutumbakato.batchmanager.data.local.SalesLocalSource;
import com.mutumbakato.batchmanager.data.models.Sale;
import com.mutumbakato.batchmanager.data.repository.SalesRepository;
import com.mutumbakato.batchmanager.data.utils.Data;
import com.mutumbakato.batchmanager.ui.SalesFormContract;

public class SalesFormPresenter implements SalesFormContract.Presenter, SalesLocalSource.DataItemCallback<Sale> {

    private String mId;
    private String mBatchId;
    private long mServerId = 0;
    private SalesRepository mRepository;
    private SalesFormContract.View mView;

    public SalesFormPresenter(String id, String batch, SalesRepository repo, SalesFormContract.View view) {
        mId = id;
        mBatchId = batch;
        mRepository = repo;
        mView = view;
        mView.setPresenter(this);
    }

    @Override
    public void save(String date, String item, float rate, int quantity) {
        if (isNew()) {
            createSale(date, item, rate, quantity);
            mView.clearInputs();
        } else {
            updateSale(date, item, rate, quantity);
            stopEditing();
        }
    }

    @Override
    public boolean isNew() {
        return mId == null;
    }

    @Override
    public void populate() {
        mRepository.getOne(mId, mBatchId, this);
    }

    @Override
    public void startEditing(String id) {
        mId = id;
        start();
        if (mView.isActive()) {
            mView.toggleExpantion(true);
        }
    }

    @Override
    public void stopEditing() {
        if (mView.isActive()) {
            mView.toggleExpantion(false);
        }
    }

    @Override
    public void start() {
        if (!isNew()) {
            populate();
        } else {
            mView.clearInputs();
        }
    }

    @Override
    public void onDataItemLoaded(Sale data) {
        mServerId = data.getServerId();
        if (mView.isActive()) {
            mView.setDate(data.getDate());
            mView.setItem(data.getItem());
            mView.setRate(String.valueOf(data.getRate()));
            mView.setQuantity(String.valueOf(data.getQuantity()));
        }
    }

    @Override
    public void onDataItemNotAvailable() {
        if (mView.isActive()) {
            mView.showError();
        }
    }

    @Override
    public void onError(String message) {

    }

    private void createSale(String date, String item, float rate, int quantity) {
        Sale newSale = new Sale(mBatchId, date, item, rate, quantity, "Unknown");
        mRepository.saveData(newSale);
    }

    private void updateSale(String date, String item, float rate, int quantity) {
        Sale saleUpdate = new Sale(mId, mBatchId, date, item, rate, quantity, "Unknown",
                mServerId, Data.STATUS_NORMAL, 0);
        mRepository.updateData(saleUpdate);
    }
}
