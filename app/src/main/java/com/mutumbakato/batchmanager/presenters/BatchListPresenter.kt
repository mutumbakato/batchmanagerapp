package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.ui.BatchListContract
import com.mutumbakato.batchmanager.utils.DateUtils
import java.util.ArrayList
import java.util.HashMap
import java.util.LinkedHashMap
import kotlin.Comparator

class BatchListPresenter(private var mFarmId: String?,
                         private val isArchived: Boolean,
                         private val mBatchRepository: BatchRepository,
                         private val mBatchListView: BatchListContract.View) : BatchListContract.Presenter,
        BatchDataSource.DataUpdatedListener,
        BatchDataSource.LoadBatchesCallback {

    init {
        mBatchListView.setPresenter(this)
        mBatchRepository.setDataUpdatedListener(this)
    }

    override fun start() {
        loadBatches(false)
    }

    override fun result(requestCode: Int, resultCode: Int) {
        mBatchRepository.refreshBatches()
    }

    override fun loadBatches(forceUpdate: Boolean) {
        loadBatches(forceUpdate, true)
    }

    override fun loadMoreBatches() {

    }

    override fun addNewBatch() {
        mBatchListView.showAddBatch()
    }

    override fun openBatchDetails(requestedBatch: Batch) {
        mBatchListView.showBatchDetailsUi(requestedBatch.id)
    }

    override fun refreshBatches() {
        loadBatches(true)
    }

    override fun setFarm(farmId: String) {
        mFarmId = farmId
        start()
    }

    /**
     * @param forceUpdate   Pass in true to refresh the data in the [BatchDataSource]
     * @param showLoadingUI Pass in true to display a loading icon in the UI
     */
    private fun loadBatches(forceUpdate: Boolean, showLoadingUI: Boolean) {
        if (showLoadingUI) {
            mBatchListView.setLoadingIndicator(true)
        }

        if (forceUpdate) {
            mBatchRepository.refreshBatches()
        }

        // App is busy until further notice
        if (!isArchived) {
            mBatchRepository.getAllBatches(mFarmId!!, this)
        } else {
            mBatchRepository.getArchivedBatches(mFarmId!!, this)
        }
    }

    private fun processBatches(batchesToShow: List<Batch>) {
        if (batchesToShow.isEmpty()) {
            mBatchListView.showNoBatches()
        } else {
            // Show the list of batches
            mBatchListView.showBatches(batchesToShow)
            mBatchListView.showBatches(getSortedBatches(batchesToShow))
            mBatchListView.showCountBadge(batchesToShow.size)
        }
    }

    override fun onDataUpdated() {
        loadBatches(forceUpdate = false, showLoadingUI = false)
    }

    private fun getSortedBatches(batches: List<Batch>): HashMap<String, List<Batch>> {
        val sortedBatches = LinkedHashMap<String, List<Batch>>()
        val statuses = getSections(batches)
        for (status in statuses) {
            sortedBatches[status] = getSectionData(batches, status)
        }
        return sortedBatches
    }

    private fun getSections(batches: List<Batch>): List<String> {
        val sections = ArrayList<String>()
        for (batch in batches) {
            if (!sections.contains(batch.status))
                sections.add(batch.status)
        }
        sections.sortWith(Comparator { o1, o2 -> o2.compareTo(o1) })
        return sections
    }

    private fun getSectionData(batches: List<Batch>, status: String): List<Batch> {
        val bts = ArrayList<Batch>()
        for (batch in batches) {
            if (batch.status == status)
                bts.add(batch)
        }
        bts.sortWith(Comparator { o1, o2 -> o2.date!!.compareTo(o1.date!!) })
        return bts
    }

    override fun onBatchesLoaded(batches: List<Batch>) {
        // The view may not be able to handle UI updates anymore
        if (!mBatchListView.isActive) {
            return
        }
        mBatchListView.setLoadingIndicator(false)
        processBatches(batches)
    }

    override fun onDataNotAvailable() {
        // The view may not be able to handle UI updates anymore
        if (!mBatchListView.isActive) {
            return
        }
        mBatchListView.showNoBatches()
    }
}
