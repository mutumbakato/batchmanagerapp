package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.ui.BatchDetailsContract
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles

class BatchDetailsPresenter(private var mBatchId: String?, private val mRepo: BatchRepository, private val mView: BatchDetailsContract.View) : BatchDetailsContract.Presenter, BatchDataSource.GetBatchCallback {
    private var mBatch: Batch? = null
    private var mUser: BatchUser? = null

    init {
        mView.setPresenter(this)
    }

    override fun closeBatch(archive: Boolean, users: List<String>) {
        mBatch!!.status = if (archive) "closed" else "normal"
        mBatch!!.users = users
        mBatch!!.closeDate = if (archive) DateUtils.today() else null
        mRepo.updateBatch(mBatch!!)
    }

    override fun deleteBatch(id: String) {
        mRepo.deleteBatch(id)
    }

    override fun start() {
        mRepo.getBatch(mBatchId!!, this)
    }

    private fun getUser() {
        if (mBatch!!.userId == PreferenceUtils.userId) {
            if (mView.isActive)
                mView.applyPermissions(UserRoles.USER_ADMIN)
        } else {
            mRepo.getBatchUser(mBatchId!!, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                override fun onDataItemLoaded(data: BatchUser) {
                    mUser = data
                    if (mView.isActive) {
                        mView.applyPermissions(mUser!!.role)
                    }
                }

                override fun onDataItemNotAvailable() {

                }

                override fun onError(message: String) {

                }
            })
        }
    }

    override fun start(batchId: String) {
        mBatchId = batchId
        mRepo.getBatch(mBatchId!!, this)
    }

    private fun presentBatch(batch: Batch) {
        mBatch = batch
        if (mView.isActive) {
            mView.showDetails(batch)
            mView.showName(batch.name)
            mView.showAge(DateUtils.toAge(batch.date!!))
            mView.showDate(DateUtils.parseDateToddMMyyyy(batch.date!!))
            mView.showQuantity(Currency.format(batch.quantity.toFloat()))
            mView.showRate(PreferenceUtils.currency + " " + Currency.format(batch.rate.toFloat()))
            mView.showSupplier(batch.supplier!!)
            mView.showBreed(batch.type)
        }
    }

    override fun addUser() {
        mView.showAddUser(mBatchId!!, mBatch!!.name)
    }

    private fun showNotFound() {
        if (mView.isActive)
            mView.showNotFound()
    }

    override fun onBatchLoaded(batch: Batch) {
        mBatch = batch
        presentBatch(batch)
        getUser()
    }

    override fun onDataNotAvailable() {
        showNotFound()
    }
}
