package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.FarmDataSource
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.FarmRepository
import com.mutumbakato.batchmanager.ui.FarmFormContract

class FarmFormPresenter(private val farmId: String?, private val farmRepo: FarmRepository, private val mView: FarmFormContract.View) : FarmFormContract.Presenter, FarmDataSource.FarmCallback {
    private var mFarm: Farm? = null

    init {
        this.mView.setPresenter(this)
    }

    override fun start() {
        if (isEdit)
            loadData(true)
    }

    override fun saveFarm(name: String, location: String, contact: String, units: String, currency: String) {
        if (isEdit) {
            updateFarm(name, location, contact, units, currency)
        } else {
            createFarm(name, location, contact, units, currency)
        }
    }

    override fun loadData(showProgress: Boolean) {
        if (showProgress && mView.isActive) {
            mView.showProgress(showProgress)
        }
        farmRepo.getFarm(farmId!!, this)

    }

    override fun isEdit(): Boolean {
        return farmId != null
    }

    override fun refresh() {
        farmRepo.refresh()
    }

    override fun deleteFarm() {
        if (mView.isActive)
            mView.showProgress(true)
        farmRepo.deleteFarm(mFarm!!, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.exit()
                }
            }

            override fun onFail(message: String) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showError(message)
                }
            }
        })
    }

    override fun onLoad(farm: Farm) {
        mFarm = farm
        if (mView.isActive) {
            mView.showProgress(false)
            mView.showName(farm.name)
            mView.showCurrency(farm.currency)
            mView.showContacts(farm.contact)
            mView.showUnits(farm.units)
            mView.showLocation(farm.location)
        }
    }

    override fun onFail(message: String) {
        if (mView.isActive) {
            mView.showProgress(false)
            mView.showError(message)
        }
    }

    private fun updateFarm(name: String, location: String, contact: String, units: String, currency: String) {

        if (mView.isActive)
            mView.showProgress(true)

        val farm = Farm(farmId!!, mFarm!!.userId, name, location, contact, units, currency)

        farmRepo.updateFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {

                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.exit()
                }
            }

            override fun onFail(message: String) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showError(message)
                }
            }
        })
    }

    private fun createFarm(name: String, location: String, contact: String, units: String, currency: String) {
        val farm = Farm(PreferenceUtils.userId, name, location, contact, units, currency)
        farmRepo.createFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showDetails(farm.id)
                }
            }

            override fun onFail(message: String) {
                if (mView.isActive) {
                    mView.showProgress(false)
                    mView.showError(message)
                }
            }
        })
    }
}
