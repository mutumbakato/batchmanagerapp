package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.ui.BatchFromContract
import java.util.*

class BatchFromPresenter(private var mBatchId: String?, private val mBatchRepository: BatchRepository,
                         private val mBatchFormView: BatchFromContract.View, private var isDataMissing: Boolean) : BatchFromContract.Presenter, BatchDataSource.GetBatchCallback {
    private var mServerId: Long = 0
    private var mStatus: String? = null
    private var vaccinationSchedule: String? = null
    private var mBatch: Batch? = null
    private var users: List<String> = ArrayList()


    private val isNewBatch: Boolean
        get() = mBatchId == null

    init {
        mBatchFormView.setPresenter(this)
    }

    override fun start() {
        if (!isNewBatch && isDataMissing) {
            populateBatch()
            loadUsers()
        }

    }

    override fun saveBatch(date: String, name: String, supplier: String,
                           quantity: Int, rate: Double, type: String, age: String) {
        if (isNewBatch)
            createBatch(date, name, supplier, quantity, rate, type, age)
        else
            updateBatch(date, name, supplier, quantity, rate, type, age)
    }

    override fun populateBatch() {
        if (isNewBatch) {
            throw RuntimeException("populateBatch() was called but task is new.")
        }
        mBatchRepository.getBatch(mBatchId!!, this)
    }

    override fun isDataMissing(): Boolean {
        return false
    }

    override fun onBatchLoaded(batch: Batch) {
        mBatch = batch
        mServerId = batch.serverId
        mStatus = batch.status
        mBatchId = batch.id
        vaccinationSchedule = batch.vaccinationSchedule

        // The view may not be able to handle UI updates anymore
        if (mBatchFormView.isActive) {
            mBatchFormView.setDate(batch.date!!)
            mBatchFormView.setName(batch.name)
            mBatchFormView.setSupplier(batch.supplier!!)
            mBatchFormView.setQuantity(batch.quantity.toString())
            mBatchFormView.setRate(batch.rate.toString())
            mBatchFormView.setType(batch.type)
            mBatchFormView.setAge(batch.age)
        }
        isDataMissing = false
    }

    override fun onDataNotAvailable() {
        if (mBatchFormView.isActive)
            mBatchFormView.showEmptyBatchError()
    }

    private fun createBatch(date: String, name: String, supplier: String,
                            quantity: Int, rate: Double, type: String, age: String) {
        val newBatch = Batch(date, name, supplier, quantity, rate, type, age, PreferenceUtils.userId, PreferenceUtils.farmId)
        mBatchRepository.saveBatch(newBatch, object : BatchDataSource.CreateBatchCallback {
            override fun onBatchCreated(batchId: String) {
                mBatchFormView.createInitialExpense(batchId, date, quantity, rate)
            }
        })
        mBatchFormView.showBatchList()
    }

    private fun updateBatch(date: String, name: String, supplier: String, quantity: Int, rate: Double,
                            type: String, age: String) {
        if (isNewBatch) {
            throw RuntimeException("updateBatch() was called but task is new.")
        }
        val updateBatch = Batch(mBatchId!!, mServerId, date, name, supplier, quantity, rate,
                type, age, vaccinationSchedule, mStatus, 0, mBatch!!.userId, mBatch!!.farmId)
        updateBatch.users = users
        mBatchRepository.updateBatch(updateBatch)
        mBatchFormView.showBatchList()
    }

    private fun loadUsers() {
        mBatchRepository.getBatchUsers(mBatchId!!, object : BaseDataSource.DataLoadCallback<BatchUser> {
            override fun onDataLoaded(data: List<BatchUser>) {
                val users = ArrayList<String>()
                for (user in data) {
                    users.add(user.userId)
                }
                this@BatchFromPresenter.users = users
            }

            override fun onEmptyData() {}

            override fun onError(message: String) {}
        })
    }

}
