package com.mutumbakato.batchmanager.presenters

import android.util.Log

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.models.Eggs
import com.mutumbakato.batchmanager.data.repository.EggsRepository
import com.mutumbakato.batchmanager.data.utils.Data
import com.mutumbakato.batchmanager.ui.EggsFormContract

class EggsFormPresenter(private var mId: String?, private val mBatchId: String, private var mCount: Int, private val mEggsRepository: EggsRepository, private val mEggsFormView: EggsFormContract.View?) : EggsFormContract.Presenter, BaseDataSource.DataItemCallback<Eggs> {
    private var mServerId: Long = 0

    override val isNew: Boolean
        get() = mId == null

    init {
        mEggsFormView!!.setPresenter(this)
    }

    override fun save(date: String, treys: Int, eggs: Int, damage: Int) {
        if (isNew) {
            create(date, treys, eggs, damage)
            mEggsFormView!!.clearInputs()
        } else {
            update(date, treys, eggs, damage)
            mEggsFormView!!.toggleExpansion(false)
        }

    }

    private fun create(date: String, treys: Int, eggs: Int, damage: Int) {
        val newEggs = Eggs(mBatchId, date, treys, eggs, damage, mCount)
        mEggsRepository.saveData(newEggs)
    }

    private fun update(date: String, treys: Int, eggs: Int, damage: Int) {
        val newEggs = Eggs(mId, mBatchId, date, treys, eggs, damage, mCount, mServerId, Data.STATUS_NORMAL, 0)
        mEggsRepository.updateData(newEggs)
    }

    override fun populate() {
        mEggsRepository.getOne(mId!!, mBatchId, this)
    }

    override fun startEditing(id: String?) {
        mId = id
        start()
        if (mEggsFormView!!.isActive) {
            mEggsFormView.toggleExpansion(true)
        }
    }

    override fun stopEditing() {
        if (mEggsFormView!!.isActive) {
            mEggsFormView.toggleExpansion(false)
        }
    }

    override fun start() {
        if (!isNew) {
            populate()
        } else {
            mEggsFormView?.clearInputs()
        }
    }

    override fun onDataItemLoaded(data: Eggs) {
        mServerId = data.serverId
        mCount = data.count
        Log.d("Eggs form", "onDataItemLoaded: $mCount")
        if (mEggsFormView!!.isActive) {
            mEggsFormView.showDate(data.date)
            mEggsFormView.showTreys(data.treys.toString())
            mEggsFormView.showEggs(data.remainderEggs.toString())
            mEggsFormView.showDamage(data.damage.toString())
        }
    }

    override fun onDataItemNotAvailable() {

    }

    override fun onError(message: String) {

    }
}
