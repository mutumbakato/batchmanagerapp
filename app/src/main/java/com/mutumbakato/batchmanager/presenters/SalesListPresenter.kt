package com.mutumbakato.batchmanager.presenters

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.SalesDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.BatchRepository
import com.mutumbakato.batchmanager.data.repository.SalesRepository
import com.mutumbakato.batchmanager.ui.SalesListContract
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.UserRoles
import java.util.*


class SalesListPresenter(private val mBatchId: String, private val mRepository: SalesRepository, private val mBatchRepo: BatchRepository, private val mView: SalesListContract.View) : SalesListContract.Presenter, SalesDataSource.DataUpdatedListener {

    init {
        mView.setPresenter(this)
        mRepository.setDataUpdatedListener(this)
    }

    override fun start() {
        loadData(false, true)
        getUser(mBatchId)
    }


    override fun result(requestCode: Int, resultCode: Int) {

    }

    override fun editSale(sale: Sale) {
        mView.showForm(sale.id)
    }


    override fun deleteSale(sale: Sale) {
        mView.showConfirmDelete(sale, DELETE_DELAY_TIME)
    }

    override fun delete(sale: Sale) {
        mRepository.deleteDataItem(sale, mBatchId)
    }

    override fun getRepository(): SalesRepository {
        return mRepository
    }

    override fun getBatchId(): String {
        return mBatchId
    }

    private fun loadData(forceRefresh: Boolean, showLoading: Boolean) {

        if (showLoading) {
            mView.showSalesProgress(true)
        }

        if (forceRefresh) {
            mRepository.refreshData()
        }

        mRepository.getAll(mBatchId, object : BaseDataSource.DataLoadCallback<Sale> {
            override fun onDataLoaded(data: List<Sale>) {
                if (mView.isActive) {
                    mView.showSalesProgress(false)
                    mView.showSales(getSections(data), getSortedSales(data))
                    mRepository.getTotalSales(mBatchId, object : SalesDataSource.OnTotalSales {
                        override fun onTotal(total: Float) {
                            mView.showTotalSales(Currency.format(total))
                        }
                    })
                }
            }

            override fun onEmptyData() {
                if (mView.isActive) {
                    mView.showNoSales()
                    mView.showSalesProgress(false)
                }
            }

            override fun onError(message: String) {
                if (mView.isActive) {
                    mView.showLoadingSalesError(message)
                    mView.showSalesProgress(false)
                }
            }
        })

    }

    override fun onDataUpdated() {
        loadData(false, false)
    }

    private fun getSortedSales(sales: List<Sale>): HashMap<String, List<Sale>> {
        val sortedSales = LinkedHashMap<String, List<Sale>>()
        val dates = getSections(sales)
        for (date in dates) {
            sortedSales[date] = getSectionData(sales, date)
        }
        return sortedSales
    }

    private fun getSections(sales: List<Sale>): List<String> {
        val sections = ArrayList<String>()

        for (sale in sales) {
            if (!sections.contains(sale.date))
                sections.add(sale.date)
        }

        Collections.sort(sections) { o1, o2 -> o2.compareTo(o1) }
        return sections
    }

    private fun getSectionData(sales: List<Sale>, date: String): List<Sale> {
        val sls = ArrayList<Sale>()
        for (sale in sales) {
            if (sale.date == date)
                sls.add(sale)
        }
        Collections.sort(sls) { t, t1 -> t1.id.compareTo(t.id) }
        return sls
    }

    private fun getUser(mBatchId: String) {
        mBatchRepo.getBatch(mBatchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                if (batch == null)
                    return
                if (batch.userId == PreferenceUtils.userId) {
                    mView.applyPermissions(UserRoles.USER_ADMIN)
                } else
                    mBatchRepo.getBatchUser(mBatchId, PreferenceUtils.userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                        override fun onDataItemLoaded(data: BatchUser) {
                            if (mView.isActive) {
                                mView.applyPermissions(data.role)
                            }
                        }

                        override fun onDataItemNotAvailable() {

                        }

                        override fun onError(message: String) {

                        }
                    })
            }

            override fun onDataNotAvailable() {

            }
        })
    }

    companion object {

        private val DELETE_DELAY_TIME = 4000
    }


}
