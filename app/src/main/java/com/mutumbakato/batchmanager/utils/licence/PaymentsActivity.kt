package com.mutumbakato.batchmanager.utils.licence

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.activities.batch.BatchListActivity
import com.mutumbakato.batchmanager.activities.user.LoginActivity
import com.mutumbakato.batchmanager.data.UserDataSource.UserCallBack
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils.userToken
import com.mutumbakato.batchmanager.data.utils.ApiUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.toolbar.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by cato on 12/3/17.
 */
class PaymentsActivity : BaseActivity() {

    private var api: PaymentsEndPoints? = null
    private var progressDialog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        api = ApiUtils.getEndPoints(PaymentsEndPoints::class.java) as PaymentsEndPoints
        progressDialog = ProgressDialog(this)
        voucher_input!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                voucher_label.error = null
            }
        })
        why_pay.setOnClickListener {
            whyPay()
        }
        use_free.setOnClickListener {
            userForFree()
        }
        button_submit_voucher.setOnClickListener {
            activate()
        }
    }

    private fun whyPay() {
        WhyPayActivity.start(this)
    }

    private fun userForFree() {
        if (Prefs.getString(PreferenceUtils.LICENCE, "") != "active") {
            Prefs.edit().putString(PreferenceUtils.LICENCE, PreferenceUtils.LIMITED).apply()
        }
        BatchListActivity.start(this)
        finish()
    }

    private fun activate() {
        val voucher = voucher_input.text.toString().trim { it <= ' ' }
        if (voucher.isEmpty()) {
            voucher_label.error = "Please enter a valid Key."
        } else {
            progressDialog!!.setMessage("Activating licence...")
            progressDialog!!.show()
            api!!.activateLicence(userToken, voucher).enqueue(object : Callback<String> {
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    progressDialog!!.dismiss()

                    Log.d(TAG, "onResponse: " + response.body())
                    val snackbar = Snackbar.make(voucher_input, "", Snackbar.LENGTH_INDEFINITE)
                    snackbar.setAction("GOT IT") { snackbar.dismiss() }

                    try {
                        val jsonObject = JSONObject(response.body())
                        if (!jsonObject.getBoolean("error")) {
                            handleVoucher(jsonObject.getJSONObject("data"))
                        } else {
                            snackbar.setText(jsonObject.getString("message")).show()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        snackbar.setText("Failed to activate licence please try again later.").show()
                    }
                }

                override fun onFailure(call: Call<String>, t: Throwable) {
                    progressDialog!!.dismiss()
                    Snackbar.make(voucher_input,
                            "We are having trouble reaching the server.",
                            Snackbar.LENGTH_LONG).show()
                }
            })
        }
    }

    @Throws(JSONException::class)
    private fun handleVoucher(data: JSONObject) {
        val voucher = data.getString("licence")
        Prefs.putString(PreferenceUtils.LICENCE, voucher)
        BatchListActivity.start(this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_licence, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        } else if (item.itemId == R.id.action_logout) {
            logout()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        val userRepository = RepositoryUtils.getUserRepo(this)
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Logout")
        builder.setMessage("Are you sure you want to logout? All your data will be removed from this device!")
        builder.setPositiveButton("Continue") { dialogInterface, i ->
            progressDialog!!.setMessage("Logging out...")
            progressDialog!!.show()
            userRepository.logOut(object : UserCallBack {
                override fun onSuccess(user: User?) {
                    progressDialog!!.dismiss()
                    Prefs.edit().clear().apply()
                    RepositoryUtils.getBatchRepo(this@PaymentsActivity).clearMemoryCache(false)
                    LoginActivity.start(this@PaymentsActivity)
                    finish()
                }

                override fun onError(message: String) {
                    progressDialog!!.dismiss()
                }
            })
        }
        builder.setNegativeButton("Cancel") { dialogInterface, i -> dialogInterface.dismiss() }
        builder.create().show()
    }

    companion object {
        private val TAG = PaymentsActivity::class.java.simpleName
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, PaymentsActivity::class.java)
            context.startActivity(starter)
        }
    }
}