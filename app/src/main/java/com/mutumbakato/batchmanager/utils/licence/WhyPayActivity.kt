package com.mutumbakato.batchmanager.utils.licence

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import kotlinx.android.synthetic.main.toolbar.*

class WhyPayActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_why_pay)
        toolbar.title = getString(R.string.why_pay)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, WhyPayActivity::class.java)
            context.startActivity(starter)
        }
    }
}