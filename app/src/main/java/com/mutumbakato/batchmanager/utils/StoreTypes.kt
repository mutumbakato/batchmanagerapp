package com.mutumbakato.batchmanager.utils

object StoreTypes {
    const val TYPE_FEEDS = "feeds_store"
    const val TYPE_EGGS = "eggs_store"
    const val TYPE_MEDICS = "medics_store"
}