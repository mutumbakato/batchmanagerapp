package com.mutumbakato.batchmanager.utils

/**
 * Created by cato on 11/4/17.
 */

object Utils {
    const val DEFAULT_STATUS = "normal"
    const val WHATSAPP_LINK = "https://chat.whatsapp.com/8mLviGLrb41IiH3GjSioXE"
    const val BASE_FILE_PATH = "Batch Manager"
}
