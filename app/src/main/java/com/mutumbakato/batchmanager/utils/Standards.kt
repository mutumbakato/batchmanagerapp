package com.mutumbakato.batchmanager.utils

object Standards {

    private val bWeight = intArrayOf(42, 63, 74, 90, 109, 134, 163, 193, 228, 269, 313, 362, 414, 469, 528, 589, 654, 722, 792, 865, 941, 1018, 1098, 1180, 1264, 1349, 1436, 1525, 1615, 1706, 1798, 1892, 1986, 2081, 2177, 2273, 2369, 2466, 2563, 2661, 2758, 2855, 2952, 3049, 3145, 3240, 3335, 3430, 3524, 3617, 3707, 3797, 3885, 3973, 4059, 4144, 4227, 4309, 4389, 4466, 4542, 4616, 4688, 4759)
    private val bGain = intArrayOf(0, 0, 0, 0, 0, 0, 0, 30, 36, 41, 44, 48, 52, 55, 59, 62, 65, 68, 70, 73, 75, 78, 80, 82, 84, 85, 87, 89, 90, 91, 92, 93, 94, 95, 96, 96, 97, 97, 97, 97, 97, 97, 97, 97, 96, 95, 95, 95, 94, 93, 91, 90, 88, 87, 86, 85, 83, 82, 80, 77, 76, 74, 73, 70)
    private val bFeeds = intArrayOf(0, 0, 0, 0, 0, 0, 0, 0, 37, 43, 50, 57, 64, 72, 74, 78, 85, 91, 103, 110, 114, 118, 123, 128, 133, 137, 144, 150, 156, 160, 164, 167, 170, 174, 177, 179, 182, 186, 190, 193, 197, 203, 208, 213, 218, 224, 228, 231, 236, 241, 243, 244, 245, 247, 247, 246, 245, 243, 241, 239, 237, 234, 232, 228)
    val bCumFeeds = intArrayOf(0, 0, 0, 0, 0, 0, 0, 145, 182, 225, 275, 331, 395, 467, 541, 619, 704, 795, 898, 1007, 1121, 1239, 1362, 1489, 1622, 1759, 1903, 2054, 2209, 2369, 2533, 2700, 2870, 3043, 3220, 3399, 3581, 3767, 3958, 4151, 4348, 4552, 4760, 4973, 5191, 5414, 5642, 5873, 6109, 6349, 6592, 6835, 7080, 7326, 7573, 7819, 8063, 8306, 8547, 8786, 9022, 9256, 9488, 9716)

    val lbWeight = intArrayOf(0, 0, 0, 0)
    val llWeight = intArrayOf(0, 0, 0, 0)
    val eggWeight = intArrayOf(0, 0, 0, 0)

    fun getDailyWeightTarget(day: Int, type: String): Float {
        return if (type == "Broilers") getTargetBWeight(day) else 0f
    }

    fun getDailyFeedsTarget(day: Int, type: String): Float {
        return if (type == "Broilers") getBFeedsTarget(day) else 0f
    }

    private fun getTargetBWeight(day: Int): Float {
        return if (day >= 0 && day < bWeight.size) bWeight[day].toFloat() else 0f
    }

    private fun getBGainTarget(day: Int): Float {
        return if (day > 0 && day < bGain.size) bGain[day].toFloat() else 0f
    }

    private fun getBFeedsTarget(day: Int): Float {
        return if (day >= 0 && day < bFeeds.size) bFeeds[day].toFloat() else 0f
    }

}