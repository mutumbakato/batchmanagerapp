package com.mutumbakato.batchmanager.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * CombinedLiveData is a helper class to combine results from multiple LiveData sources.
 * @param liveDatas Variable number of LiveData arguments.
 * @param combine   Function reference that will be used to combine all LiveData data.
 * @param R         The type of data returned after combining all LiveData data.
 * Usage:
 * CombinedLiveData<SomeType>(
 *     getLiveData1(),
 *     getLiveData2(),
 *     ... ,
 *     getLiveDataN()
 * ) { datas: List<Any?> ->
 *     // Use datas[0], datas[1], ..., datas[N] to return a SomeType value
 * }
 * */
class TransformationX<R>(vararg liveData: LiveData<*>, private val combine: suspend (data: List<Any?>) -> R) : MediatorLiveData<R>() {

    private val data: MutableList<Any?> = MutableList(liveData.size) { null }

    init {
        for (i in liveData.indices) {
            super.addSource(liveData[i]) {
                data[i] = it
                GlobalScope.launch(context = Dispatchers.Unconfined) {
                    postValue(combine(data))
                }
            }
        }
    }
}