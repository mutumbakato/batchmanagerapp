package com.mutumbakato.batchmanager.utils

import com.mutumbakato.batchmanager.data.models.exports.ExportData
import com.mutumbakato.batchmanager.data.models.exports.Exports
import jxl.CellView
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.*
import jxl.format.Alignment
import jxl.format.Border
import jxl.format.BorderLineStyle
import jxl.format.Colour
import jxl.format.VerticalAlignment
import jxl.write.*
import jxl.write.Number
import jxl.write.biff.RowsExceededException
import java.io.File
import java.io.IOException
import java.util.*

class ExportExcel constructor(val path: String) {

    // Lets create a times font
    private val arial10pt = WritableFont(WritableFont.ARIAL, 11)
    private var arialBold: WritableCellFormat? = null
    private var arial: WritableCellFormat? = null
    private var workbook: WritableWorkbook
    private var file: File

    init {
        val wbSettings = WorkbookSettings()
        wbSettings.locale = Locale("en", "EN")
        file = File(path)
        workbook = Workbook.createWorkbook(file, wbSettings)
    }

    @Throws(IOException::class, WriteException::class)
    fun write(data: Exports): File? {
        //Cell formatting
        formattingConfig()
        createRecords(data)
        workbook.write()
        workbook.close()
        return file
    }

    private fun createRecords(data: Exports) {
        if (data.batch.type == "Layers") {
            createLayerRecords(data)
        } else {
            createBroilerRecords(data)
        }
    }

    private fun createBroilerRecords(data: Exports) {

        var sheetIndex = 0
        var column = 0
        workbook.createSheet("Records", sheetIndex)
        val sheet = workbook.getSheet(sheetIndex)

        //Following columns -> add records
        column = addDates(sheet, data, column)
        column = addMortality(sheet, data, column)
        column = addWeight(sheet, data, column)
        column = addFeeds(sheet, data, column)
        addWater(sheet, data, column)

//        if (data.expenses.isNotEmpty()) {
//            workbook.createSheet("Expenses", ++sheetIndex)
//            createExpenses(workbook.getSheet(sheetIndex), data)
//        }
//
//        if (data.sales.isNotEmpty()) {
//            workbook.createSheet("Sales", ++sheetIndex)
//            createExpenses(workbook.getSheet(sheetIndex), data)
//        }
    }

    private fun createLayerRecords(data: Exports) {
        var sheetIndex = -1
        if (data.isLaying) {
            //Create layers
            workbook.createSheet("Laying", ++sheetIndex)
            createLaying(workbook.getSheet(sheetIndex), data)
        }
        //create rearing records
        workbook.createSheet("Rearing", ++sheetIndex)
        createRearing(workbook.getSheet(sheetIndex), data)

//        if (data.expenses.isNotEmpty()) {
//            workbook.createSheet("Expenses", ++sheetIndex)
//            createExpenses(workbook.getSheet(sheetIndex), data)
//        }
//
//        if (data.sales.isNotEmpty()) {
//            workbook.createSheet("Sales", ++sheetIndex)
//            createSales(workbook.getSheet(sheetIndex), data)
//        }
    }

    private fun createRearing(sheet: WritableSheet, data: Exports) {
        var column = 0
        column = addDates(sheet, data, column)
        column = addMortality(sheet, data, column)
        column = addFeeds(sheet, data, column)
        column = addWater(sheet, data, column)
        addWeight(sheet, data, column)
    }

    private fun createLaying(sheet: WritableSheet, data: Exports) {
        var column = 0
        column = addDates(sheet, data, column)
        column = addMortality(sheet, data, column)
        column = addEggs(sheet, data, column)
        column = addFeeds(sheet, data, column)
        column = addWater(sheet, data, column)
        addWeight(sheet, data, column)
    }

    private fun addDates(sheet: WritableSheet, data: Exports, column: Int): Int {
        //Create dates and days labels
        val dates = data.days
        addCaption(sheet, column, 0, "Date")
        addCaption(sheet, column + 1, 0, "Days")

        for (i in dates) {
            val row = i.key + 2
            // First column -> add date
            addRecordLabel(sheet, column, row, i.value)
            // Second column -> add days
            addRecordLabel(sheet, column + 1, row, "Day ${i.key + 1}")
        }
        return column + 2
    }

    private fun addMortality(sheet: WritableSheet, data: Exports, column: Int): Int {

        val deaths = data.sortedData[Exports.DEATHS] ?: hashMapOf()
        var cumDeaths = 0
        var rate: Int

        addCaption(sheet, column, 0, "Mortality")
        sheet.mergeCells(column, 0, column + 1, 0)
        addCaption(sheet, column, 1, "count")
        addCaption(sheet, column + 1, 1, "cum")
        addCaption(sheet, column + 2, 1, "rate(%)")

        deaths.forEach {
            val row = it.key + 2
            val count = it.value.toInt()
            cumDeaths += it.value.toInt()
            rate = ((cumDeaths / data.batch.quantity) * 100)
            addRecordNumber(sheet, column, row, count.toDouble())
            addRecordNumber(sheet, column + 1, row, cumDeaths.toDouble())
            addRecordNumber(sheet, column + 2, row, rate.toDouble())
        }
        return column + 3
    }

    private fun addWeight(sheet: WritableSheet, data: Exports, column: Int): Int {
        val weights = data.sortedData[Exports.WEIGHT] ?: hashMapOf()
        addCaption(sheet, column, 0, "Body Weight")
        sheet.mergeCells(column, 0, column + 1, 0)
        addCaption(sheet, column, 1, "avg (gm)")
        addCaption(sheet, column + 1, 1, "gain (gm)")
        var prevWeight = 0f
        weights.forEach {
            val row = it.key + 2
            val avg = it.value
            val gain = avg - prevWeight
            prevWeight = avg
            addRecordNumber(sheet, column, row, avg.toInt().toDouble())
            addRecordNumber(sheet, column + 1, row, gain.toInt().toDouble())
        }
        return column + 2
    }

    private fun addFeeds(sheet: WritableSheet, data: Exports, column: Int): Int {
        val feeds = data.sortedData[Exports.FEEDS] ?: hashMapOf()
        var cumFeeds = 0f
        addCaption(sheet, column, 0, "Feeds")
        sheet.mergeCells(column, 0, column + 2, 0)
        addCaption(sheet, column, 1, "total(kg)")
        addCaption(sheet, column + 1, 1, "gm/bird")
        addCaption(sheet, column + 2, 1, "cum")

        feeds.forEach {
            val row = it.key + 2
            val feedsPerBird = it.value
            cumFeeds += feedsPerBird
            addRecordNumber(sheet, column, row, 0.0)
            addRecordNumber(sheet, column + 1, row, feedsPerBird.toInt().toDouble())
            addRecordNumber(sheet, column + 2, row, cumFeeds.toDouble())
        }
        return column + 3
    }

    private fun addWater(sheet: WritableSheet, data: Exports, column: Int): Int {
        val waterIntake = data.sortedData[Exports.WATER] ?: hashMapOf()
        var cumWater = 0f

        addCaption(sheet, column, 0, "Water")
        sheet.mergeCells(column, 0, column + 2, 0)
        addCaption(sheet, column, 1, "total(ltr)")
        addCaption(sheet, column + 1, 1, "ml/bird")
        addCaption(sheet, column + 2, 1, "cum")

        waterIntake.forEach {
            val row = it.key + 2
            val water = it.value
            cumWater += water
            addRecordNumber(sheet, column, row, 0.0)
            addRecordNumber(sheet, column + 1, row, water.toDouble())
            addRecordNumber(sheet, column + 2, row, cumWater.toDouble())
        }
        return column + 3
    }

    private fun addEggs(sheet: WritableSheet, data: Exports, column: Int): Int {
        val eggs = data.sortedData[Exports.EGGS] ?: hashMapOf()
        addCaption(sheet, column, 0, "Eggs")
        sheet.mergeCells(column, 0, column + 1, 0)
        addCaption(sheet, column, 1, "good")
        addCaption(sheet, column + 1, 1, "damage")

        eggs.forEach {
            val row = it.key + 2
            val good = it.value
            addRecordNumber(sheet, column, row, good.toDouble())
            addRecordNumber(sheet, column + 1, row, 0.0)
        }
        return column + 2
    }

    private fun createSales(sheet: WritableSheet, data: ExportData) {

        addCaption(sheet, 0, 0, "Date")
        addCaption(sheet, 1, 0, "Item")
        addCaption(sheet, 2, 0, "Quantity")
        addCaption(sheet, 3, 0, "Unit cost")
        addCaption(sheet, 4, 0, "Total")

        data.sales.forEachIndexed { i, sale ->
            addLabel(sheet, 0, i + 1, sale.date)
            addLabel(sheet, 1, i + 1, sale.item)
            addNumber(sheet, 2, i + 1, sale.quantity.toDouble())
            addNumber(sheet, 3, i + 1, sale.rate.toDouble())
            addNumber(sheet, 4, i + 1, sale.totalAmount.toDouble())
        }
    }

    private fun createExpenses(sheet: WritableSheet, data: ExportData) {

        addCaption(sheet, 0, 0, "Date")
        addCaption(sheet, 1, 0, "Category")
        addCaption(sheet, 2, 0, "Description")
        addCaption(sheet, 3, 0, "Amount")

        data.expenses.forEachIndexed { i, expense ->
            addLabel(sheet, 0, i + 1, expense.date)
            addLabel(sheet, 1, i + 1, expense.category)
            addLabel(sheet, 2, i + 1, expense.description)
            addNumber(sheet, 3, i + 1, expense.amount.toDouble())
        }
    }

    @Throws(RowsExceededException::class, WriteException::class)
    private fun addCaption(sheet: WritableSheet, column: Int, row: Int, s: String) {
        val label = Label(column, row, s, arialBold)
        sheet.addCell(label)
    }

    @Throws(WriteException::class, RowsExceededException::class)
    private fun addNumber(sheet: WritableSheet, column: Int, row: Int, value: Double) {
        val number = Number(column, row, value, arial)
        sheet.addCell(number)
    }

    @Throws(WriteException::class, RowsExceededException::class)
    private fun addRecordNumber(sheet: WritableSheet, column: Int, row: Int, value: Double) {
        val number = Number(column, row, value, if (isWeek(row)) WritableCellFormat().apply {
            setBackground(Colour.VERY_LIGHT_YELLOW)
            setFont(arial10pt)
            setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_50)
            wrap = true
        } else arial)
        sheet.addCell(number)
    }

    @Throws(WriteException::class, RowsExceededException::class)
    private fun addLabel(sheet: WritableSheet, column: Int, row: Int, s: String) {
        val label = Label(column, row, s, arial)
        sheet.addCell(label)
    }

    @Throws(WriteException::class, RowsExceededException::class)
    private fun addRecordLabel(sheet: WritableSheet, column: Int, row: Int, s: String) {
        val label = Label(column, row, s, if (isWeek(row)) WritableCellFormat().apply {
            setBackground(Colour.VERY_LIGHT_YELLOW)
            setFont(arial10pt)
            setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_50)
            wrap = true
        } else arial)
        //val label = Label(column, row, s, arial)
        sheet.addCell(label)
    }

    private fun formattingConfig() {
        // Define the cell format
        arial = WritableCellFormat(arial10pt).apply {
            // Lets automatically wrap the cells
            setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_50)
            wrap = true
            alignment = Alignment.GENERAL
            verticalAlignment = VerticalAlignment.CENTRE
        }

        // create create a bold font
        val times10ptBold = WritableFont(
                WritableFont.ARIAL, 11, WritableFont.BOLD, false,
                UnderlineStyle.NO_UNDERLINE)

        arialBold = WritableCellFormat(times10ptBold).apply {
            alignment = Alignment.JUSTIFY
            verticalAlignment = VerticalAlignment.CENTRE
            setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_50)
            // Lets automatically wrap the cells
            wrap = true
        }

        val cv = CellView()
        cv.format = arial
        cv.format = arialBold
        cv.isAutosize = true
    }

    private fun isWeek(row: Int): Boolean {
        return row != 0 && ((row - 1) % 7 == 0)
    }
}