package com.mutumbakato.batchmanager.utils

import android.text.TextUtils

object Validator {

    fun isValidNumber(str: String?): Boolean {
        var str = str
        if (str != null)
            str = str.replace("[,.]".toRegex(), "")
        return str != null && TextUtils.isDigitsOnly(str) && !TextUtils.isEmpty(str)
    }

    fun isValidWord(str: String?): Boolean {
        return str != null && !TextUtils.isEmpty(str)
    }

    fun isEmailValid(str: String): Boolean {
        return str.contains("@")
    }

}
