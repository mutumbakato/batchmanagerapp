package com.mutumbakato.batchmanager.utils

import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

object DateUtils {

    private val months = arrayOf("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")

    var FORMAT_YYYY_MM_DD = "yyyy-MM-dd"

    fun parseDateToddMMyyyy(time: String): String {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val date: Date
        var str = ""

        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return str
    }

    fun parseDateToddmmyyyy(time: String): String? {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "dd/MM/yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val date: Date
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun parseDateToddMMyyyyTT(time: String): String? {

        val inputPattern = "yyyy-MM-dd HH:mm"
        val inputPattern2 = "yyyy-MM-dd hh:mm a"
        val outputPattern = "dd MMM yyyy hh:mm a"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val inputFormat2 = SimpleDateFormat(inputPattern2, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val date: Date
        val str: String?

        date = try {
            inputFormat.parse(time)
        } catch (e: ParseException) {
            inputFormat2.parse(time)
        }

        str = outputFormat.format(date)
        return str
    }

    fun cameAfter(time: String): Boolean {
        val inputPattern = "yyyy-MM-dd hh:mm a"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        val lastDate: Date

        try {
            date = inputFormat.parse(time)
            lastDate = inputFormat.parse(PreferenceUtils.lastLogTime)
            return date.after(lastDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return false
    }

    fun parseDateToyyyymmdd(time: String): String? {

        val outputPattern = "yyyy-MM-dd"
        val inputPattern = "dd MMM yyyy"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val date: Date
        var str: String? = null

        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun toAge(d: String): String {

        val inputPattern = "yyyy-MM-dd"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        val today = Calendar.getInstance().time

        return try {
            date = inputFormat.parse(d) as Date
            val days: Int
            val diff = today.time - date.time
            val diffDays = diff / (24 * 60 * 60 * 1000)
            days = diffDays.toInt() + 1
            val weeks = days / 7
            val dys = if (days >= 7) days % 7 else if (days == 0) 1 else days
            weeks.toString() + (if (weeks == 1) " Week" else " Weeks") + if (dys > 0) " : " + dys + if (dys == 1) " Day" else " Days" else ""
        } catch (e: ParseException) {
            e.printStackTrace()
            d
        }
    }

    fun toWeeks(d: String): Int {
        return if (getDays(d) > 0) {
            getDays(d) / 7
        } else 0
    }

    /**
     * @param d date in yyy-MM-dd format
     * @return int number of days passed since that date.
     */
    fun getDays(d: String): Int {
        val inputPattern = "yyyy-MM-dd"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        val today = Calendar.getInstance().time
        return try {
            date = inputFormat.parse(d)
            val days: Int
            val diff = today.time - date.time
            val diffDays = diff / (24 * 60 * 60 * 1000)
            days = diffDays.toInt()
            days
        } catch (e: ParseException) {
            e.printStackTrace()
            0
        }
    }

    fun ageDaysFromDate(birth: String, d: String = today()): Int {
        val inputPattern = "yyyy-MM-dd"
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        val bDate: Date
        return try {
            date = inputFormat.parse(d)
            bDate = inputFormat.parse(birth)
            val days: Int
            val diff = date.time - bDate.time
            val diffDays = diff / (24 * 60 * 60 * 1000)
            days = diffDays.toInt()
            days
        } catch (e: ParseException) {
            e.printStackTrace()
            0
        }
    }

    fun ageWeeksFromDate(birth: String, d: String = today()): Int {
        val days = ageDaysFromDate(birth, d)
        return if (days > 0) {
            days / 7
        } else 0
    }

    private fun dateFromAgeDays(birth: String, ageInDays: Int): String {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "dd MMM yyyy"
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        try {
            date = inputFormat.parse(birth)
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(Calendar.DAY_OF_YEAR, ageInDays)
            return outputFormat.format(calendar.time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ageInDays.toString()
    }

    fun dateFromAge(birth: String, ageInWeeks: Int): String {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "dd MMM yyyy"
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        try {
            date = inputFormat.parse(birth)
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(Calendar.WEEK_OF_YEAR, ageInWeeks)
            return outputFormat.format(calendar.time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ageInWeeks.toString()
    }

    fun rowDateFromAgeWeeks(birth: String, ageInWeeks: Int): String {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "yyyy-MM-dd"
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        try {
            date = inputFormat.parse(birth)
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(Calendar.WEEK_OF_YEAR, ageInWeeks)
            return outputFormat.format(calendar.time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ageInWeeks.toString()
    }

    fun rawDateFromAgeDays(birth: String, ageInDays: Int): String {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "yyyy-MM-dd"
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        try {
            date = inputFormat.parse(birth)
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(Calendar.DAY_OF_YEAR, ageInDays)
            return outputFormat.format(calendar.time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ageInDays.toString()
    }

    fun simpleDateFromAgeDays(birth: String, ageInDays: Int): String {
        val inputPattern = "yyyy-MM-dd"
        val outputPattern = "dd-MM-yyyy"
        val outputFormat = SimpleDateFormat(outputPattern, Locale.ENGLISH)
        val inputFormat = SimpleDateFormat(inputPattern, Locale.ENGLISH)
        val date: Date
        try {
            date = inputFormat.parse(birth)
            val calendar = Calendar.getInstance()
            calendar.time = date
            calendar.add(Calendar.DAY_OF_YEAR, ageInDays)
            return outputFormat.format(calendar.time)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return ageInDays.toString()
    }

    fun today(): String {
        val formatPattern = "yyyy-MM-dd"
        val date = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat(formatPattern, Locale.ENGLISH)
        return dateFormat.format(date)
    }

    fun monthAgo(months: Int = 1): String {
        val formatPattern = "yyyy-MM-dd"
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MONTH, -months)
        val date = calendar.time
        val dateFormat = SimpleDateFormat(formatPattern, Locale.ENGLISH)
        return dateFormat.format(date)
    }

    fun weekAgo(weeks: Int = 1): String {
        val formatPattern = "yyyy-MM-dd"
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.WEEK_OF_MONTH, -weeks)
        val date = calendar.time
        val dateFormat = SimpleDateFormat(formatPattern, Locale.ENGLISH)
        return dateFormat.format(date)
    }

    fun thisMonth(): String {
        val formatPattern = "yyyy-MM"
        val date = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat(formatPattern, Locale.ENGLISH)
        return dateFormat.format(date)
    }

    fun getDateFromAge(birthDate: String, weeks: Int, days: Int): String {
        val dd = weeks * 7 + days
        return dateFromAgeDays(birthDate, dd)
    }

    fun getRowDateFromAge(birthDate: String, weeks: Int, days: Int): String {
        val dd = weeks * 7 + days
        return rawDateFromAgeDays(birthDate, dd)
    }

    fun getTimeLine(dateOfBirth: String, closeDate: String?, batchType: String = "Layers"): Map<Int, List<Int>> {

        var closeWeek = if (batchType === "Layers") 150 else 9
        if (closeDate != null) {
            closeWeek = ageWeeksFromDate(dateOfBirth, closeDate)
        }

        val timeline: HashMap<Int, List<Int>> = hashMapOf()
        val weeks = ageWeeksFromDate(dateOfBirth) + 2

        for (i in 0..weeks) {
            if (i <= closeWeek) {
                if (i <= ageWeeksFromDate(dateOfBirth)) {
                    timeline[i] = arrayListOf(0, 1, 2, 3, 4, 5, 6)
                } else {
                    timeline[i] = arrayListOf(0, 1, 2, 3, 4)
                    break
                }
            } else
                break
        }
        return timeline
    }

    fun getDailyTimeline(dateOfBirth: String, closeDate: String?, batchType: String): List<Int> {
        val days: MutableList<Int> = arrayListOf()
        val timeline = getTimeLine(dateOfBirth, closeDate, batchType)
        for (i in 0..(timeline.size * 7)) {
            days.add(i)
        }
        return days
    }

    fun getWeeklyTimeline(dateOfBirth: String, closeDate: String?, batchType: String = "Layers"): List<Int> {
        return ArrayList(getTimeLine(dateOfBirth, closeDate, batchType).keys)
    }

    fun now(): String {
        val date = Calendar.getInstance().time
        val dateFormat = SimpleDateFormat("yyyy-mm-dd kk:mm:ss", Locale.getDefault())
        return dateFormat.format(date)
    }

    fun formatMonth(date: String): String {
        val year = date.split("-")[0]
        val month = months[date.split("-")[1].toInt()]
        return "$month $year"
    }

    fun monthToDateRange(date: String): Array<String> {
        val from = "$date-31"
        val to = "$date-01"
        return arrayOf(from, to)
    }
}
