package com.mutumbakato.batchmanager.utils

object UserRoles {

    const val USER_OWNER = "Owner"
    const val USER_ADMIN = "Admin"
    const val USER_WORKER = "Worker"
    const val USER_GUEST = "Guest"

    fun canCreate(role: String): Boolean {
        return role == USER_ADMIN || role == USER_WORKER
    }

    fun canEdit(role: String): Boolean {
        return role == USER_ADMIN
    }

    fun canDelete(role: String): Boolean {
        return role == USER_ADMIN
    }

    fun canRead(role: String): Boolean {
        return true
    }

}
