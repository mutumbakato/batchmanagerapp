package com.mutumbakato.batchmanager.utils

import com.mutumbakato.batchmanager.data.models.exports.ExportData
import jxl.CellView
import jxl.Workbook
import jxl.WorkbookSettings
import jxl.format.*
import jxl.format.Alignment
import jxl.format.Border
import jxl.format.BorderLineStyle
import jxl.format.Colour
import jxl.format.VerticalAlignment
import jxl.write.*
import jxl.write.Number
import jxl.write.biff.RowsExceededException
import java.io.File
import java.io.IOException
import java.util.*

class Excel constructor(val path: String) {

    // Lets create a times font
    private val arial10pt = WritableFont(WritableFont.ARIAL, 11)
    private var arialBold: WritableCellFormat? = null
    private var arial: WritableCellFormat? = null
    private var workbook: WritableWorkbook
    private var file: File

    init {
        val wbSettings = WorkbookSettings()
        wbSettings.locale = Locale("en", "EN")
        file = File(path)
        workbook = Workbook.createWorkbook(file, wbSettings)
    }

    @Throws(IOException::class, WriteException::class)
    fun write(data: ExportData): File? {
        //Cell formatting
        formattingConfig()
        createRecords(data)
        workbook.write()
        workbook.close()
        return file
    }

    private fun createRecords(data: ExportData) {
        if (data.batch?.type == "Layers") {
            createLayerRecords(data)
        } else {
            createBroilerRecords(data)
        }
    }

    private fun createBroilerRecords(data: ExportData) {

        var sheetIndex = 0
        var column = 0
        workbook.createSheet("Records", sheetIndex)
        val sheet = workbook.getSheet(sheetIndex)

        //Following columns -> add records
        column = addWeeks(sheet, data, column)
        column = addDates(sheet, data, column)
        column = addMortality(sheet, data, column)
        column = addWeight(sheet, data, column)
        column = addFeeds(sheet, data, column)
        addWater(sheet, data, column)

        if (data.expenses.isNotEmpty()) {
            workbook.createSheet("Expenses", ++sheetIndex)
            createExpenses(workbook.getSheet(sheetIndex), data)
        }

        if (data.sales.isNotEmpty()) {
            workbook.createSheet("Sales", ++sheetIndex)
            createExpenses(workbook.getSheet(sheetIndex), data)
        }
    }

    private fun createLayerRecords(data: ExportData) {
        var sheetIndex = -1
        if (data.isLaying) {
            //Create layers
            workbook.createSheet("Laying", ++sheetIndex)
            createLaying(workbook.getSheet(sheetIndex), data)
        }
        //create rearing records
        workbook.createSheet("Rearing", ++sheetIndex)
        createRearing(workbook.getSheet(sheetIndex), data)
        if (data.expenses.isNotEmpty()) {
            workbook.createSheet("Expenses", ++sheetIndex)
            createExpenses(workbook.getSheet(sheetIndex), data)
        }

        if (data.sales.isNotEmpty()) {
            workbook.createSheet("Sales", ++sheetIndex)
            createSales(workbook.getSheet(sheetIndex), data)
        }
    }

    private fun createRearing(sheet: WritableSheet, data: ExportData) {
        var column = 0
        column = addWeeks(sheet, data, column, 0, data.layingDay)
        column = addDates(sheet, data, column, 0, data.layingDay)
        column = addMortality(sheet, data, column, 0, data.layingDay)
        column = addFeeds(sheet, data, column, 0, data.layingDay)
        column = addWater(sheet, data, column, 0, data.layingDay)
        addWeight(sheet, data, column)
    }

    private fun createLaying(sheet: WritableSheet, data: ExportData) {
        var column = 0
        column = addWeeks(sheet, data, column, data.layingDay, data.lastDay)
        column = addDates(sheet, data, column, data.layingDay, data.lastDay)
        column = addMortality(sheet, data, column, data.layingDay, data.lastDay)
        column = addEggs(sheet, data, column, data.layingDay, data.lastDay)
        column = addFeeds(sheet, data, column, data.layingDay, data.lastDay)
        column = addWater(sheet, data, column, data.layingDay, data.lastDay)
        addWeight(sheet, data, column, data.layingDay, data.lastDay)
    }

    private fun addDates(sheet: WritableSheet, data: ExportData, column: Int, startDay: Int = 0, lastDay: Int = data.lastDay): Int {
        //Create dates and days labels
        addCaption(sheet, column, 0, "Days")
        addCaption(sheet, column + 1, 0, "Date")
        addCaption(sheet, column, 1, " ")
        addCaption(sheet, column + 1, 1, " ")
        for (i in startDay..lastDay) {
            val date = DateUtils.simpleDateFromAgeDays(data.batch?.date ?: "", i)
            val row = (i - startDay) + 2
            // First column -> add date
            addRecordLabel(sheet, column, row, "Day ${i + 1}")
            addRecordLabel(sheet, column + 1, row, date)
        }
        return column + 2
    }

    private fun addWeeks(sheet: WritableSheet, data: ExportData, column: Int, startDay: Int = 0, lastDay: Int = data.lastDay): Int {

        //Create dates and days labels
        addCaption(sheet, column, 0, "Weeks")
        addCaption(sheet, column, 1, " ")

        for (day in startDay..lastDay) {
            val row = (day - startDay) + 2
            val week = day / 7
            // First column -> add date
            if (day % 7 == 0) {
                sheet.mergeCells(column, row, column, row + 6)
                addRecordLabel(sheet, column, row, "Week $week")
            }
            // Second column -> add days
        }
        return column + 1
    }

    private fun addMortality(sheet: WritableSheet, data: ExportData, column: Int, startDay: Int = 0, lastDay: Int = data.lastDay): Int {

        var cumMortality = 0
        var rate = 0
        addCaption(sheet, column, 0, "Mortality")
        sheet.mergeCells(column, 0, column + 2, 0)
        addCaption(sheet, column, 1, "count")
        addCaption(sheet, column + 1, 1, "cum")
        addCaption(sheet, column + 2, 1, "rate(%)")

        for (i in startDay..lastDay) {
            val row = (i - startDay) + 2
            val rawDate = DateUtils.rawDateFromAgeDays(data.batch?.date ?: "", i)
            val results = data.deaths.filter {
                it.date == rawDate
            }

            if (results.isNotEmpty()) {
                val deathExport = results[0]
                cumMortality += deathExport.count
                rate = ((cumMortality.toFloat() / data.batch!!.quantity.toFloat()) * 100f).toInt()
                addRecordNumber(sheet, column, row, deathExport.count.toDouble())
                addRecordNumber(sheet, column + 1, row, cumMortality.toDouble())
                addRecordNumber(sheet, column + 2, row, rate.toDouble())
            } else {
                addRecordNumber(sheet, column, row, 0.toDouble())
                addRecordNumber(sheet, column + 1, row, cumMortality.toDouble())
                addRecordNumber(sheet, column + 2, row, rate.toDouble())
            }
        }
        return column + 3
    }

    private fun addWeight(sheet: WritableSheet, data: ExportData, column: Int, startDay: Int = 0, lastDay: Int = data.lastDay): Int {
        addCaption(sheet, column, 0, "Body Weight")
        sheet.mergeCells(column, 0, column + 1, 0)
        addCaption(sheet, column, 1, "avg (gm)")
        addCaption(sheet, column + 1, 1, "gain (gm)")

        var prevWeight = 0f
        for (i in startDay..lastDay) {
            val row = (i - startDay) + 2
            val rawDate = DateUtils.rawDateFromAgeDays(data.batch?.date ?: "", i)
            val results = data.weight.filter {
                it.date == rawDate
            }
            if (results.isNotEmpty()) {
                val weightExport = results[0]
                val gain = weightExport.avg - prevWeight
                prevWeight = weightExport.avg
                addRecordNumber(sheet, column, row, weightExport.avg.toInt().toDouble())
                addRecordNumber(sheet, column + 1, row, gain.toInt().toDouble())
            } else {
                addRecordLabel(sheet, column, row, "")
                addRecordLabel(sheet, column + 1, row, "")
            }
        }
        return column + 2
    }

    private fun addFeeds(sheet: WritableSheet, data: ExportData, column: Int, startDay: Int = 0, lastDay: Int = data.lastDay): Int {
        var cumFeeds = 0
        addCaption(sheet, column, 0, "Feeds")
        sheet.mergeCells(column, 0, column + 2, 0)
        addCaption(sheet, column, 1, "total(kg)")
        addCaption(sheet, column + 1, 1, "gm/bird")
        addCaption(sheet, column + 2, 1, "cum (gm)")

        for (i in startDay..lastDay) {
            val row = (i - startDay) + 2
            val rawDate = DateUtils.rawDateFromAgeDays(data.batch?.date ?: "", i)
            val results = data.feeds.filter {
                it.date == rawDate
            }
            if (results.isNotEmpty()) {
                val feedsExport = results[0]
                cumFeeds += feedsExport.gpb.toInt()
                addRecordNumber(sheet, column, row, feedsExport.total.toInt().toDouble())
                addRecordNumber(sheet, column + 1, row, feedsExport.gpb.toInt().toDouble())
                addRecordNumber(sheet, column + 2, row, cumFeeds.toDouble())
            } else {
                addRecordLabel(sheet, column, row, "")
                addRecordLabel(sheet, column + 1, row, "")
                addRecordLabel(sheet, column + 2, row, "")
            }
        }
        return column + 3
    }

    private fun addWater(sheet: WritableSheet, data: ExportData, column: Int, startDay: Int = 0, lastDay: Int = data.lastDay): Int {
        var cumWater = 0
        addCaption(sheet, column, 0, "Water")
        sheet.mergeCells(column, 0, column + 2, 0)
        addCaption(sheet, column, 1, "total(ltr)")
        addCaption(sheet, column + 1, 1, "ml/bird")
        addCaption(sheet, column + 2, 1, "cum")

        for (i in startDay..lastDay) {
            val row = (i - startDay) + 2
            val rawDate = DateUtils.rawDateFromAgeDays(data.batch?.date ?: "", i)
            val results = data.water.filter {
                it.date == rawDate
            }

            if (results.isNotEmpty()) {
                val it = results[0]
                val mlpb = ((it.total / it.count) * 1000).toInt()
                cumWater += mlpb
                addRecordNumber(sheet, column, row, it.total.toInt().toDouble())
                addRecordNumber(sheet, column + 1, row, mlpb.toDouble())
                addRecordNumber(sheet, column + 2, row, cumWater.toDouble())
            } else {
                addRecordLabel(sheet, column, row, "")
                addRecordLabel(sheet, column + 1, row, "")
                addRecordLabel(sheet, column + 2, row, "")
            }
        }
        return column + 3
    }

    private fun addEggs(sheet: WritableSheet, data: ExportData, column: Int, startDay: Int = 0, lastDay: Int = data.lastDay): Int {

        addCaption(sheet, column, 0, "Eggs")
        sheet.mergeCells(column, 0, column + 1, 0)
        addCaption(sheet, column, 1, "good")
        addCaption(sheet, column + 1, 1, "damage")

        for (i in startDay..lastDay) {
            val row = (i - startDay) + 2
            val rawDate = DateUtils.rawDateFromAgeDays(data.batch?.date ?: "", i)
            val results = data.eggs.filter {
                it.date == rawDate
            }
            if (results.isNotEmpty()) {
                val it = results[0]
                addRecordNumber(sheet, column, row, it.eggs.toDouble())
                addRecordNumber(sheet, column + 1, row, it.damage.toDouble())
            } else {
                addRecordLabel(sheet, column, row, "")
                addRecordLabel(sheet, column + 1, row, "")
            }
        }
        return column + 2
    }

    private fun addFWRatio(sheet: WritableSheet, data: ExportData, column: Int): Int {
        //Food to water ratio
        return column
    }

    private fun addPullets(sheet: WritableSheet, data: ExportData, column: Int): Int {
        //Number of remaining pullets for rearing
        return column
    }

    private fun createSales(sheet: WritableSheet, data: ExportData) {

        addCaption(sheet, 0, 0, "Date")
        addCaption(sheet, 1, 0, "Item")
        addCaption(sheet, 2, 0, "Quantity")
        addCaption(sheet, 3, 0, "Unit cost")
        addCaption(sheet, 4, 0, "Total")

        data.sales.forEachIndexed { i, sale ->
            addLabel(sheet, 0, i + 1, sale.date)
            addLabel(sheet, 1, i + 1, sale.item)
            addNumber(sheet, 2, i + 1, sale.quantity.toDouble())
            addNumber(sheet, 3, i + 1, sale.rate.toDouble())
            addNumber(sheet, 4, i + 1, sale.totalAmount.toDouble())
        }
    }

    private fun createExpenses(sheet: WritableSheet, data: ExportData) {

        addCaption(sheet, 0, 0, "Date")
        addCaption(sheet, 1, 0, "Category")
        addCaption(sheet, 2, 0, "Description")
        addCaption(sheet, 3, 0, "Amount")

        data.expenses.forEachIndexed { i, expense ->
            addLabel(sheet, 0, i + 1, expense.date)
            addLabel(sheet, 1, i + 1, expense.category)
            addLabel(sheet, 2, i + 1, expense.description)
            addNumber(sheet, 3, i + 1, expense.amount.toDouble())
        }
    }

    @Throws(RowsExceededException::class, WriteException::class)
    private fun addCaption(sheet: WritableSheet, column: Int, row: Int, s: String) {
        val label = Label(column, row, s, arialBold)
        sheet.addCell(label)
    }

    @Throws(WriteException::class, RowsExceededException::class)
    private fun addNumber(sheet: WritableSheet, column: Int, row: Int, value: Double) {
        val number = Number(column, row, value, arial)
        sheet.addCell(number)
    }

    @Throws(WriteException::class, RowsExceededException::class)
    private fun addRecordNumber(sheet: WritableSheet, column: Int, row: Int, value: Double) {
        val number = Number(column, row, value, if (isWeek(row)) WritableCellFormat().apply {
            setBackground(Colour.VERY_LIGHT_YELLOW)
            setFont(arial10pt)
            setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_50)
            wrap = true
        } else arial)
        sheet.addCell(number)
    }

    @Throws(WriteException::class, RowsExceededException::class)
    private fun addLabel(sheet: WritableSheet, column: Int, row: Int, s: String) {
        val label = Label(column, row, s, arial)
        sheet.addCell(label)
    }

    @Throws(WriteException::class, RowsExceededException::class)
    private fun addRecordLabel(sheet: WritableSheet, column: Int, row: Int, s: String) {
        val label = Label(column, row, s, if (isWeek(row)) WritableCellFormat().apply {
            setBackground(Colour.VERY_LIGHT_YELLOW)
            setFont(arial10pt)
            setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_50)
            wrap = true
        } else arial)
        //val label = Label(column, row, s, arial)
        sheet.addCell(label)
    }

    private fun formattingConfig() {
        // Define the cell format
        arial = WritableCellFormat(arial10pt).apply {
            // Lets automatically wrap the cells
            setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_50)
            wrap = true
            alignment = Alignment.GENERAL
            verticalAlignment = VerticalAlignment.CENTRE
        }

        // create create a bold font
        val times10ptBold = WritableFont(
                WritableFont.ARIAL, 11, WritableFont.BOLD, false,
                UnderlineStyle.NO_UNDERLINE)

        arialBold = WritableCellFormat(times10ptBold).apply {
            alignment = Alignment.JUSTIFY
            verticalAlignment = VerticalAlignment.CENTRE
            setBorder(Border.ALL, BorderLineStyle.THIN, Colour.GRAY_50)
            // Lets automatically wrap the cells
            wrap = true
        }

        val cv = CellView()
        cv.format = arial
        cv.format = arialBold
        cv.isAutosize = true
    }

    private fun isWeek(row: Int): Boolean {
        return row != 0 && ((row - 1) % 7 == 0)
    }
}