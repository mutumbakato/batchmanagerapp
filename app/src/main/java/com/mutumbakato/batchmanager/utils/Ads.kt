package com.mutumbakato.batchmanager.utils

import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds

object Ads {

    fun initAds(adView: AdView) {
        MobileAds.initialize(adView.context)
        val adRequest = AdRequest.Builder()
                .addTestDevice("C4C29F2663C1BA978D359C292424E8D3")
                .addKeyword("poultry")
                .addKeyword("farm")
                .addKeyword("Agriculture")
                .addKeyword("chicken")
                .addKeyword("broilers")
                .addKeyword("layer chicken")
                .addKeyword("day old chicks")
                .addKeyword("Farm equipment")
                .addKeyword("livestock")
                .addKeyword("turkey")
                .addKeyword("goats")
                .addKeyword("pigs")
                .addKeyword("veterinary")
                .addKeyword("sheep")
                .addKeyword("ducks")
                .addKeyword("eggs")
                .addKeyword("heating system")
                .addKeyword("incubator")
                .addKeyword("brooder")
                .addKeyword("poultry feeds")
                .addKeyword("Poultry vaccination")
                .build()
        adView.loadAd(adRequest)
    }
}
