package com.mutumbakato.batchmanager.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.mutumbakato.batchmanager.activities.vaccination.VaccinationActivity
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationCheck
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.VaccinationRepository
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.ui.notification.NotificationUtils
import com.pixplicity.easyprefs.library.Prefs
import java.text.SimpleDateFormat
import java.util.*

class ReminderBroadcastReceiver : BroadcastReceiver(), BatchDataSource.LoadBatchesCallback {

    private var vaccinationRepo: VaccinationRepository? = null
    private var mContext: Context? = null

    override fun onReceive(context: Context, intent: Intent) {
        val lastCheck = Calendar.getInstance().time.day.toLong()
        val isChecked = Prefs.getLong(PreferenceUtils.NOTIFICATION_LOCK, 0) == lastCheck
        if (!isChecked && intent.action != null && intent.action == Intent.ACTION_USER_PRESENT && PreferenceUtils.farmId.isNotEmpty()) {
            val batchRepository = RepositoryUtils.getBatchRepo(context)
            vaccinationRepo = RepositoryUtils.getVaccinationRepo(context)
            mContext = context
            batchRepository.getAllBatches(PreferenceUtils.farmId, this)
            Prefs.edit().putLong(PreferenceUtils.NOTIFICATION_LOCK, Calendar.getInstance().time.day.toLong()).apply()
        }
    }

    override fun onBatchesLoaded(batches: List<Batch>) {
        walkThrough(batches)
    }

    override fun onDataNotAvailable() {}

    private fun walkThrough(batches: List<Batch>?) {
        if (batches == null)
            return
        for (batch in batches) {
            val vSchedule = batch.vaccinationSchedule
            if (vSchedule != null) {
                vaccinationRepo!!.listVaccinations(vSchedule, object : BaseDataSource.DataLoadCallback<Vaccination> {
                    override fun onDataLoaded(data: List<Vaccination>) {
                        for (vaccination in data) {
                            val age = vaccination.age
                            assert(batch.date != null)
                            val vaccinationDay = DateUtils.rowDateFromAgeWeeks(batch.date!!, age)
                            val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                            try {
                                val vaccinationDate = dateFormat.parse(vaccinationDay)
                                val today = Calendar.getInstance().time
                                val day = (1000 * 60 * 60 * 24).toLong()
                                val diff = vaccinationDate.time - today.time
                                val vaccinationDayHasNotPassed = diff >= 0 || diff > -day
                                if (vaccinationDayHasNotPassed) {
                                    if (diff <= day) {
                                        checkIfNotChecked(batch.id, vaccination, batch)
                                    }
                                }
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                        }
                    }

                    override fun onEmptyData() {

                    }

                    override fun onError(message: String) {

                    }
                })
            }
        }
    }

    private fun checkIfNotChecked(batchId: String, vaccination: Vaccination, batch: Batch) {
        vaccinationRepo!!.getCheckList(batchId, object : BaseDataSource.DataLoadCallback<VaccinationCheck> {
            override fun onDataLoaded(data: List<VaccinationCheck>) {
                var isChecked = false
                for (check in data) {
                    if (check.vaccinationId == vaccination.id)
                        isChecked = true
                }
                if (!isChecked) {
                    showNotification(vaccination, batch)
                }
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        })

    }

    private fun showNotification(vaccination: Vaccination, batch: Batch) {
        val intent = Intent(mContext, VaccinationActivity::class.java)
        intent.putExtra(VaccinationActivity.SCHEDULE_ID_EXTRA, vaccination.scheduleId)
        intent.putExtra(VaccinationActivity.EXTRA_DOB, batch.date)
        val notificationUtils = NotificationUtils(mContext!!)
        notificationUtils.showNotificationMessage("Batch Manager", batch.name + " " +
                vaccination.infection + " Vaccination" + " is on " + DateUtils.dateFromAge(batch.date!!, vaccination.age),
                Calendar.getInstance().timeInMillis.toString() + "", intent)
    }

}

