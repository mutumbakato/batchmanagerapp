package com.mutumbakato.batchmanager.utils.licence;

import android.content.Context;

import androidx.appcompat.app.AlertDialog;

import com.mutumbakato.batchmanager.R;
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;
import com.mutumbakato.batchmanager.utils.DateUtils;
import com.pixplicity.easyprefs.library.Prefs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by User on 1/11/2018.
 */

public class LicenseUtils {

    private static String UPDATE_DATE = "2018-01-16";

    public static boolean isLicenceExpired() {
//        String joinDate = "";
//        try {
//            if (isBeforeTheUpdate(joinDate)) {
//                joinDate = UPDATE_DATE;
//            }
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//      return (DateUtils.getDays(joinDate) > 30 && !Prefs.getString(PreferenceUtils.LICENCE, "").equals("active"));
        return false;
    }

    public static boolean isLicenceActive() {
        return Prefs.getString(PreferenceUtils.LICENCE, "none").equals("active");
    }

    private static boolean isBeforeTheUpdate(String createDate) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(DateUtils.INSTANCE.getFORMAT_YYYY_MM_DD(), Locale.ENGLISH);
        Date created = format.parse(createDate);
        Date UpdateDate = format.parse(UPDATE_DATE);
        return created.getTime() < UpdateDate.getTime();
    }

    public static void showUpgradeDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Upgrade");
        builder.setMessage(context.getText(R.string.upgrade));
        builder.setNegativeButton("Cancel", (dialogInterface, i) -> dialogInterface.dismiss());

        builder.setPositiveButton("Upgrade", (dialogInterface, i) -> {
            dialogInterface.dismiss();
            PaymentsActivity.start(context);
        });
        builder.create().show();
    }

    public static int daysLeft() {
        String joinDate = "";
        try {
            if (isBeforeTheUpdate(joinDate)) {
                joinDate = UPDATE_DATE;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return (30 - DateUtils.INSTANCE.getDays(joinDate));
    }

}
