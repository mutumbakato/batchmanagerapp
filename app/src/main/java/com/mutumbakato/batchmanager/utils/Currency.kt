package com.mutumbakato.batchmanager.utils

import android.text.TextUtils
import java.text.DecimalFormat
import java.util.*

object Currency {
    fun format(value: Float): String {
//        if (value > 1000000 || value < -1000000) {
//            return try {
//                val newValue = value / 1000000
//                String.format(Locale.ENGLISH, "%.2fM", newValue)
//            } catch (e: Exception) {
//                "0"
//            }
//        }
        return DecimalFormat("#,###.#").format(value.toDouble())
    }

    fun shorten(value: Float): String {
        if (value > 10000 || value < -10000) {
            return try {
                val newValue = value / 1000000
                String.format(Locale.ENGLISH, "%.2fM", newValue)
            } catch (e: Exception) {
                "0"
            }
        }
        return DecimalFormat("#,###.#").format(value.toDouble())
    }

    fun cleanNumbers(str: String): String {
        var str = str
        str = str.replace("[^\\d.]".toRegex(), "")
        return if (str.isEmpty()) "0" else str
    }
}
