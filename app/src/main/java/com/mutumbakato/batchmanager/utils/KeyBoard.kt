package com.mutumbakato.batchmanager.utils


import android.app.Activity
import android.view.inputmethod.InputMethodManager

object KeyBoard {
    fun hideSoftKeyboard(activity: Activity) {
        try {
            val inputMethodManager = activity.getSystemService(
                    Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(
                    activity.currentFocus!!.windowToken, 0)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }

    fun showSoftKeyboard(activity: Activity) {
        try {
            val inputMethodManager = activity.getSystemService(
                    Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInputFromInputMethod(
                    activity.currentFocus!!.windowToken, 0)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

    }
}
