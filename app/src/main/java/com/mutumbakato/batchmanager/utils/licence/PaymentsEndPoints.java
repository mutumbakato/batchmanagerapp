package com.mutumbakato.batchmanager.utils.licence;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by cato on 10/21/17.
 */

public interface PaymentsEndPoints {
    @FormUrlEncoded
    @POST("licence/activate")
    Call<String> activateLicence(@Header("Authorization") String token,
                                 @Field("key") String voucher);
}
