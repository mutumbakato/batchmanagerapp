package com.mutumbakato.batchmanager

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mutumbakato.batchmanager.activities.batch.BatchListActivity
import com.mutumbakato.batchmanager.activities.user.LoginActivity
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.FarmDataSource
import com.mutumbakato.batchmanager.data.firestore.FarmFireStore
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.Death
import com.mutumbakato.batchmanager.data.models.Eggs
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.sync.Sync
import com.mutumbakato.batchmanager.data.sync.SyncListener
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.services.Alarm
import kotlinx.android.synthetic.main.activity_setup.*

class SetupActivity : AppCompatActivity() {

    private var farmFireStore: FarmFireStore? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setup)

        if (PreferenceUtils.isNotLoggedIn) {
            LoginActivity.start(this)
            finish()
        }

        farmFireStore = FarmFireStore()
        findFarms()
        //Set an alarm for record input
        Alarm.setAlarm(this)
    }

    /**
     * Looks for farms in firebase owned by the logged in user
     */
    private fun findFarms() {
        showMessage(getString(R.string.process_finding_farm))
        val userId = PreferenceUtils.userId
        farmFireStore!!.observeFarms(object : FarmDataSource.FarmListCallback {
            override fun onLoad(farms: List<Farm>) {
                if (farms.isNotEmpty()) {
                    PreferenceUtils.setFarm(farms[0])
                    allGood()
                } else {
                    createFarm()
                }
            }

            override fun onFail(message: String) {
                showMessage(message)
            }
        }, userId)
    }

    private fun createFarm() {
        showMessage(getString(R.string.process_create_farm))
        val userId = PreferenceUtils.userId
        val farm = Farm(userId, "My Farm")
        farmFireStore!!.createFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                showMessage(getString(R.string.create_farm_complete))
                PreferenceUtils.farmId = farm.id
                synchronize()
            }

            override fun onFail(message: String) {
                showMessage(message)
            }
        })
    }

    private fun synchronize() {
        showMessage(getString(R.string.process_sync))
        Sync.getInstance().setListener(object : SyncListener {
            override fun onSuccess() {
                showMessage(getString(R.string.sync_complete))
                allGood()
            }

            override fun onFail() {
                showMessage("Synchronization Failed")
            }
        }).syncAll(this)
    }

    private fun allGood() {
        RepositoryUtils.getFarmRepo(this).getFarms(PreferenceUtils.userId, object : FarmDataSource.FarmListCallback {
            override fun onLoad(farms: List<Farm>) {

            }

            override fun onFail(message: String) {

            }
        })
        RepositoryUtils.getBatchRepo(this).getAllBatches(PreferenceUtils.farmId,
                object : BatchDataSource.LoadBatchesCallback {
                    override fun onBatchesLoaded(batches: List<Batch>) {
                        for (batch in batches) {
                            RepositoryUtils.getEggsRepo(this@SetupActivity).getAll(batch.id, object : BaseDataSource.DataLoadCallback<Eggs> {
                                override fun onDataLoaded(data: List<Eggs>) {

                                }

                                override fun onEmptyData() {

                                }

                                override fun onError(message: String) {

                                }
                            })
                            RepositoryUtils.getDeathRepo(this@SetupActivity).getAll(batch.id, object : BaseDataSource.DataLoadCallback<Death> {
                                override fun onDataLoaded(data: List<Death>) {

                                }

                                override fun onEmptyData() {

                                }

                                override fun onError(message: String) {

                                }
                            })
                            RepositoryUtils.getExpensesLiveRepo(this@SetupActivity).listFarmExpenses(batch.farmId)
                            RepositoryUtils.getSalesLiveRepo(this@SetupActivity).listFarmSales(batch.farmId)
                            RepositoryUtils.getFeedsRepo(this@SetupActivity).listFeeds(batch.id)
                            RepositoryUtils.getWeightRepository(this@SetupActivity).listWeights(batch.id)
                            RepositoryUtils.getWaterRepository(this@SetupActivity).listWaters(batch.id)
                        }
                    }

                    override fun onDataNotAvailable() {}
                })
        BatchListActivity.start(this)
        finish()
    }

    private fun showMessage(message: String) {
        status_message!!.text = message
    }

    companion object {

        fun start(context: Context) {
            val starter = Intent(context, SetupActivity::class.java)
            context.startActivity(starter)
        }
    }
}
