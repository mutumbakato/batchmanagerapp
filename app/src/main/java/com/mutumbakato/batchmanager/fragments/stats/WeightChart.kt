package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.YAxis
import com.mutumbakato.batchmanager.R

class WeightChart : BaseChartFragment() {

    private var targetWeight: Map<Int, Float> = mapOf()
    private var weight: Map<Int, Float> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartTitle = getString(R.string.body_weight)
        chatType = "weight"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.weeklyWeight.observe(viewLifecycleOwner, Observer {
            it?.let { w ->
                if (isWeek) {
                    weight = w
                    updateChartData(getWeightData(weight), getTargetData(targetWeight))
                }
            }
        })

        viewModel.dailyWeightTarget.observe(viewLifecycleOwner, Observer {
            it?.let { t ->
                if (!isWeek) {
                    targetWeight = t
                    updateChartData(getWeightData(weight), getTargetData(targetWeight))
                }
            }
        })

        viewModel.dailyWeight.observe(viewLifecycleOwner, Observer {
            it?.let { w ->
                if (!isWeek) {
                    weight = w
                    updateChartData(getWeightData(weight), getTargetData(targetWeight))
                }
            }
        })
    }

    private fun getTargetData(data: Map<Int, Float>): EntryObject {
        return EntryObject(
                data,
                getString(R.string.target_gm_bird),
                "",
                ContextCompat.getColor(context!!, R.color.colorStandard),
                YAxis.AxisDependency.LEFT,
                true)
    }

    private fun getWeightData(data: Map<Int, Float>): EntryObject {
        return EntryObject(
                data,
                getString(R.string.body_weight_gm),
                "",
                ContextCompat.getColor(context!!, R.color.colorGreenDark))
    }

    companion object {
        @JvmStatic
        fun newInstance() = WeightChart()
    }
}
