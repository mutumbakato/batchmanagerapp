package com.mutumbakato.batchmanager.fragments.water

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.WaterActivity
import com.mutumbakato.batchmanager.data.models.Water
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.ui.adapters.WaterRecyclerViewAdapter
import com.mutumbakato.batchmanager.ui.adapters.WaterTimelineAdapter
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import com.mutumbakato.batchmanager.viewmodels.WaterViewModel
import kotlinx.android.synthetic.main.water_fragment.*
import org.zakariya.stickyheaders.StickyHeaderLayoutManager

class WaterFragment : Fragment() {

    companion object {
        fun newInstance() = WaterFragment()
    }

    private lateinit var dateOfBirth: String
    private var closeDate: String? = null
    private lateinit var batchId: String
    private lateinit var viewModel: WaterViewModel
    private lateinit var waterSectionedAdapter: WaterTimelineAdapter
    private lateinit var waterAdapter: WaterRecyclerViewAdapter

    private var onWaterItemInteraction = object : OnWaterItemInteraction {
        override fun onAdd(date: String) {
            viewModel.setDate(date)
        }

        override fun onEdit(item: Water) {
            viewModel.setWaterId(item.id)
        }

        override fun onDelete(item: Water) {
            Snackbar.make(waterList, "Do you want to delete Water ${item.quantity}", Snackbar.LENGTH_LONG).run {
                setAction("Delete") {
                    viewModel.delete(item)
                }
                show()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dateOfBirth = activity!!.intent.getStringExtra(WaterActivity.DOB_EXTRA) ?: DateUtils.today()
        batchId = activity!!.intent.getStringExtra(WaterActivity.BATCH_ID_EXTRA) ?: "0"
        closeDate = activity!!.intent.getStringExtra(WaterActivity.CLOSE_DATE_EXTRA)
        viewModel = ViewModelProvider(activity!!)[WaterViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.water_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        waterSectionedAdapter = WaterTimelineAdapter(dateOfBirth, onWaterItemInteraction, waterList, childFragmentManager, closeDate)
        waterAdapter = WaterRecyclerViewAdapter(ArrayList(), onWaterItemInteraction, waterList)
        val stickyHeaderLayoutManager = StickyHeaderLayoutManager()

        waterList.run {
            layoutManager = stickyHeaderLayoutManager
            adapter = waterSectionedAdapter
        }

        //Go to currentWeek
        waterSectionedAdapter.goToWeek()
        viewModel.sortedWaters.observe(viewLifecycleOwner, Observer {
            waterSectionedAdapter.updateData(it)
        })

        viewModel.waters.observe(viewLifecycleOwner, Observer {
            waterAdapter.updateData(it)
        })

        viewModel.userRole.observe(viewLifecycleOwner, Observer {
            waterSectionedAdapter.applyPermissions(it)
        })

        viewModel.gotTo.observe(viewLifecycleOwner, Observer {
            waterSectionedAdapter.goToWeek()
        })
    }

    interface OnWaterItemInteraction {
        fun onAdd(date: String)
        fun onEdit(item: Water)
        fun onDelete(item: Water)
    }
}
