package com.mutumbakato.batchmanager.fragments.stores

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Store
import kotlinx.android.synthetic.main.fragment_stores_list.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [StoresFragment.OnListFragmentInteractionListener] interface.
 */
class StoresFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_stores_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set the adapter
        with(store_list) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = StoresRecyclerViewAdapter(arrayListOf(), object : OnListFragmentInteractionListener {
                override fun onListFragmentInteraction(item: Store?) {
                }
            })
        }
    }


    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Store?)
    }

}
