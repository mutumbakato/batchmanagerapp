package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.YAxis
import com.mutumbakato.batchmanager.R

class EggsChart : BaseChartFragment() {

    private var eggs: Map<Int, Float> = mapOf()
    private var henDay: Map<Int, Float> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartTitle = getString(R.string.egg_production)
        enableRightAxis = true
        chatType = "eggs"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.dailyEggs.observe(viewLifecycleOwner, Observer {
            it?.let { e ->
                if (!isWeek) {
                    eggs = e
                    updateChartData(getEggsEntryObject(eggs), getHDEntryObject(henDay))
                }
            }
        })

        viewModel.dailyHenDay.observe(viewLifecycleOwner, Observer {
            it?.let { hd ->
                if (!isWeek) {
                    henDay = hd
                    updateChartData(getEggsEntryObject(eggs), getHDEntryObject(henDay))
                }
            }
        })

        viewModel.weeklyEggs.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    eggs = it
                    updateChartData(getEggsEntryObject(eggs), getHDEntryObject(henDay))
                }
            }
        })
    }

    private fun getEggsEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.title_eggs),
                getString(R.string.title_eggs),
                ContextCompat.getColor(context!!, R.color.colorEggs),
                YAxis.AxisDependency.LEFT)
    }

    private fun getHDEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.hen_day),
                getString(R.string.hen_day_percentage),
                ContextCompat.getColor(context!!, R.color.colorPurpleDark),
                YAxis.AxisDependency.RIGHT)
    }

    companion object {
        @JvmStatic
        fun newInstance() = EggsChart()
    }
}
