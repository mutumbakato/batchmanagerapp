package com.mutumbakato.batchmanager.fragments.batch

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.mutumbakato.batchmanager.Features
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.adapters.BatchFeaturesAdapter
import com.mutumbakato.batchmanager.ui.components.GridDividerDecoration
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.feature_content.*

class BatchHomeFragment : BaseFragment() {

    private var mListener: OnFeaturesFragmentInteractionListener? = null
    private lateinit var featuresAdapter: BatchFeaturesAdapter
    private lateinit var viewModel: BatchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!, ViewModelFactory.BatchViewModelFactory(
                RepositoryUtils.getStatisticsDataSource(context!!)))[BatchViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_features, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.batch.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                featuresAdapter = BatchFeaturesAdapter(Features.ITEMS, mListener, it.quantity, it.type)
                featuresAdapter.setBatch(it)
                with(features_list) {
                    layoutManager = GridLayoutManager(context, 2)
                    adapter = featuresAdapter
                    addItemDecoration(GridDividerDecoration(context))
                    setHasFixedSize(true)
                    isNestedScrollingEnabled = true
                }
            }
        })

        viewModel.count.observe(viewLifecycleOwner, Observer {
            featuresAdapter.setRemaining(it ?: 0)
        })

        viewModel.eggsPercentage.observe(viewLifecycleOwner, Observer {
            featuresAdapter.setEggsCount(it ?: 0f)
        })

        viewModel.waterConsumption.observe(viewLifecycleOwner, Observer {
            featuresAdapter.setWater(it ?: 0f)
        })

        viewModel.feedsConsumption.observe(viewLifecycleOwner, Observer {
            featuresAdapter.setFeeds(it ?: 0f)
        })

        viewModel.avgWeight.observe(viewLifecycleOwner, Observer {
            featuresAdapter.setWeight(it ?: 0f)
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFeaturesFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnFeaturesFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    interface OnFeaturesFragmentInteractionListener {

        fun onFeaturesFragmentInteraction(item: Features.FeatureItem)
    }

    companion object {
        fun newInstance(): BatchHomeFragment {
            return BatchHomeFragment()
        }
    }
}
