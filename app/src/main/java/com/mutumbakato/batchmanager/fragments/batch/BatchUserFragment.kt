package com.mutumbakato.batchmanager.fragments.batch

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.batch.BatchListActivity
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.AddUserContract
import com.mutumbakato.batchmanager.ui.adapters.BatchUsersAdapter
import com.mutumbakato.batchmanager.utils.UserRoles
import kotlinx.android.synthetic.main.fragment_add_user.*
import kotlinx.android.synthetic.main.message_view.*
import java.util.*

/**
 * A placeholder fragment containing a simple view.
 */
class BatchUserFragment : BaseFragment(), AddUserContract.View {

    private var mPresenter: AddUserContract.Presenter? = null
    private var adapter: BatchUsersAdapter? = null
    private var mListener: BatchUserListInteractionListener? = null

    override val isActive: Boolean
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = BatchUsersAdapter(ArrayList(), mListener)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        batch_user_list.adapter = adapter
        leave_batch_button.setOnClickListener {
            leaveBatch()
        }
    }

    private fun leaveBatch() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle(activity!!.getString(R.string.leave_batch_title))
                .setMessage(activity!!.getString(R.string.leave_batch_message))
                .setPositiveButton(activity!!.getString(R.string.leave_confirm)) { dialogInterface, i ->
                    mPresenter!!.leave(PreferenceUtils.userId)
                    dialogInterface.dismiss()
                }
                .setNegativeButton(activity!!.getString(R.string.cancel)) { dialogInterface, i -> dialogInterface.dismiss() }
                .create().show()
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    override fun showBatchUsers(users: List<BatchUser>) {
        adapter!!.updateData(users)
        batch_user_list.visibility = View.VISIBLE
        message_view.visibility = View.GONE
        user_count_text.text = String.format(getString(R.string.people), users.size)
        mListener!!.showHasUsers(true)
    }

    override fun showEmailDialog() {
        showInput()
    }

    override fun showRoleDialog(user: String, role: String, type: String, extra: String) {
        val roles = BatchUser.ROLES
        val builder = AlertDialog.Builder(context!!)
        builder.setSingleChoiceItems(roles, indexOf(role, roles)) { dialog, which ->
            mPresenter!!.addUser(user, roles[which], type, extra)
            dialog.dismiss()
        }.create().show()
    }

    private fun indexOf(role: String, roles: Array<String>): Int {
        for (i in roles.indices) {
            if (roles[i] == role)
                return i
        }
        return 0
    }

    override fun showSourceChooser() {
        val sources = arrayOf("Add with email", "Choose From Employees")
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Choose source").setItems(sources) { dialog, which ->
            when (which) {
                0 -> {
                    mPresenter!!.addyEmail()
                }
                1 -> {
                    mPresenter!!.chooseFromEmployees()
                }
            }
            dialog.dismiss()
        }.create().show()
    }

    override fun showProgress(isLoading: Boolean) {
        mListener!!.showProgress(isLoading)
    }

    override fun showError(message: String) {
        Snackbar.make(batch_user_list, message, Snackbar.LENGTH_LONG).show()
    }

    override fun showSuccessMessage(s: String) {
        Toast.makeText(context, s, Toast.LENGTH_LONG).show()
    }

    override fun showNoUsers() {
        mListener!!.showHasUsers(false)
    }

    override fun applyPermissions(role: String) {
        adapter!!.applyPermissions(role)
    }

    override fun showBatches() {
        BatchListActivity.start(context!!)
    }

    override fun hideLeaveButton() {
        leave_batch_button.visibility = View.GONE
    }

    override fun setPresenter(presenter: AddUserContract.Presenter) {
        mPresenter = presenter
    }

    private fun showInput() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Enter a registered user email")
        // Set up the input
        val view = layoutInflater.inflate(R.layout.dialog_input_view, null)
        val input = view.findViewById<EditText>(R.id.dialog_input)
        input.inputType = InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS
        builder.setView(view)
        // Set up the buttons
        builder.setPositiveButton("Add") { dialog, _ ->
            val text = input.text.toString().trim { it <= ' ' }
            if (!TextUtils.isEmpty(text)) {
                showRoleDialog(text, UserRoles.USER_GUEST, BatchUser.BATCH_USER_TYPE_PBM_USER, "")
                dialog.cancel()
            } else if (TextUtils.isEmpty(text)) {
                input.error = "Field must not be empty!"
            } else {
                dialog.cancel()
            }
        }
        builder.setNegativeButton("Cancel") { dialog, _ -> dialog.cancel() }
        builder.show()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BatchUserListInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context should implement BatchUserListInteractionListener")
        }
    }

    interface BatchUserListInteractionListener {

        fun onItemClick(user: BatchUser)

        fun onUpdate(user: BatchUser, role: String)

        fun onRemove(user: BatchUser)

        fun showProgress(isShow: Boolean)

        fun showHasUsers(hasUsers: Boolean)
    }
}
