package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.YAxis
import com.mutumbakato.batchmanager.R

class FeedsChart : BaseChartFragment() {

    private var feeds: Map<Int, Float> = mapOf()
    private var target: Map<Int, Float> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartTitle = getString(R.string.feeds_intake)
        chatType = "feeds"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dailyFeeds.observe(viewLifecycleOwner, Observer {
            it?.let { f ->
                if (!isWeek) {
                    feeds = f
                    updateChartData(getFeedsData(feeds), getTargetData(target))
                }
            }
        })

        viewModel.dailyFeedsTarget.observe(viewLifecycleOwner, Observer {
            it?.let { w ->
                if (!isWeek) {
                    target = w
                    updateChartData(getFeedsData(feeds), getTargetData(target))
                }
            }
        })

        viewModel.weeklyFeeds.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    feeds = it
                    updateChartData(getFeedsData(feeds), getTargetData(target))
                }
            }
        })
    }


    private fun getFeedsData(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.feeds),
                getString(R.string.feeds_gm_bird),
                ContextCompat.getColor(context!!, R.color.colorYellowDark))
    }

    private fun getTargetData(data: Map<Int, Float>): EntryObject {
        return EntryObject(
                data,
                "Target (gm/bird)",
                "",
                ContextCompat.getColor(context!!, R.color.colorStandard),
                YAxis.AxisDependency.LEFT, true)
    }

    companion object {
        @JvmStatic
        fun newInstance() = FeedsChart()
    }
}
