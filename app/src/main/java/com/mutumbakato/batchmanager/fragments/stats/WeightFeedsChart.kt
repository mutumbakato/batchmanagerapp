package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.YAxis
import com.mutumbakato.batchmanager.R

class WeightFeedsChart : BaseChartFragment() {

    private var feeds: Map<Int, Float> = mapOf()
    private var weight: Map<Int, Float> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartTitle = getString(R.string.body_weight_feeds)
        enableRightAxis = true
        chatType = "feeds-weight"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dailyFeeds.observe(viewLifecycleOwner, Observer {
            it?.let { f ->
                if (!isWeek) {
                    feeds = f
                    updateChartData(getFeedsEntryObject(feeds), getWeightEntryObject(weight))
                }
            }
        })

        viewModel.dailyWeight.observe(viewLifecycleOwner, Observer {
            it?.let { w ->
                if (!isWeek) {
                    weight = w
                    updateChartData(getFeedsEntryObject(feeds), getWeightEntryObject(weight))
                }
            }
        })

        viewModel.weeklyFeeds.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    feeds = it
                    updateChartData(getFeedsEntryObject(feeds), getWeightEntryObject(weight))
                }
            }
        })

        viewModel.weeklyWeight.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    weight = it
                    updateChartData(getFeedsEntryObject(feeds), getWeightEntryObject(weight))
                }
            }
        })
    }

    private fun getWeightEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.body_weight), getString(R.string.body_weight_gm),
                ContextCompat.getColor(context!!, R.color.colorGreenDark), YAxis.AxisDependency.RIGHT)
    }

    private fun getFeedsEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.feeds_intake), getString(R.string.feeds_gm_bird),
                ContextCompat.getColor(context!!, R.color.colorYellowDark), YAxis.AxisDependency.LEFT)
    }

    companion object {
        @JvmStatic
        fun newInstance() = WeightFeedsChart()
    }
}
