package com.mutumbakato.batchmanager.fragments.vaccination

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.vaccination.VaccinationActivity
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.ui.VaccinationScheduleContract
import com.mutumbakato.batchmanager.ui.adapters.ScheduleRecyclerViewAdapter
import com.mutumbakato.batchmanager.ui.components.ScheduleInputDialog
import com.mutumbakato.batchmanager.utils.KeyBoard
import kotlinx.android.synthetic.main.fragment_filter_dialog.*
import kotlinx.android.synthetic.main.fragment_schedule_dialog.*
import kotlinx.android.synthetic.main.fragment_schedule_list.vaccination_list
import java.util.*

class ScheduleDialog : DialogFragment(), VaccinationScheduleContract.View {

    private var mListener: ScheduleListFragment.OnListFragmentInteractionListener? = null
    private var mPresenter: VaccinationScheduleContract.Presenter? = null
    private var scheduleAdapter: ScheduleRecyclerViewAdapter? = null

    override val isActive: Boolean
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)
        val mScheduleId = activity!!.intent.getStringExtra(VaccinationActivity.SCHEDULE_ID_EXTRA)
        scheduleAdapter = ScheduleRecyclerViewAdapter(ArrayList(), mScheduleId,
                object : ScheduleListFragment.OnListFragmentInteractionListener {

                    override fun onListFragmentInteraction(item: VaccinationSchedule) {
                        mListener!!.onListFragmentInteraction(item)
                        mListener!!.onUseSchedule(item)
                        mPresenter!!.useSchedule(item)
                        dismiss()
                    }

                    override fun onUseSchedule(schedule: VaccinationSchedule) {
                        mPresenter!!.useSchedule(schedule)
                        mListener!!.onUseSchedule(schedule)
                    }

                    override fun onRemoveSchedule(schedule: String) {
                        mPresenter!!.removeSchedule(schedule)
                        mListener!!.onRemoveSchedule(schedule)
                    }

                    override fun updateSchedule(schedule: VaccinationSchedule) {
                        val inputDialog = ScheduleInputDialog()
                        inputDialog.edit(schedule.title, schedule.description)
                        inputDialog.setListener(object : ScheduleInputDialog.OnSubmitSchedule {
                            override fun submit(title: String, description: String) {
                                mPresenter!!.updateSchedule(VaccinationSchedule(schedule.id,
                                        schedule.userId, title,
                                        description,
                                        schedule.publicity,
                                        schedule.serverId,
                                        schedule.status, 0,
                                        PreferenceUtils.farmId))
                                inputDialog.dismiss()
                            }

                            override fun delete() {
                                mPresenter!!.deleteSchedule(schedule)
                                inputDialog.dismiss()
                            }
                        })
                        inputDialog.show(activity!!.supportFragmentManager, "")
                    }

                    override fun showProgress(isLoading: Boolean) {}
                })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_schedule_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Set the scheduleAdapter
        val mScheduleInput = view.findViewById<EditText>(R.id.schedule_input)
        mScheduleInput.clearFocus()
        mScheduleInput.setOnEditorActionListener { textView, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                val s = textView.text.toString().trim { it <= ' ' }
                if (s.isNotEmpty()) {
                    mPresenter!!.saveSchedule(s, "")
                    mScheduleInput.text = null
                    KeyBoard.hideSoftKeyboard(activity!!)
                }
            }
            false
        }
        vaccination_list.layoutManager = LinearLayoutManager(context)
        vaccination_list.adapter = scheduleAdapter
        toolbar.setNavigationIcon(R.drawable.ic_action_close)
        toolbar.title = "Select Schedule"
        toolbar.setNavigationOnClickListener {
            dismiss()
        }
        add_schedule_fab.setOnClickListener {
            val scheduleInputDialog = ScheduleInputDialog()
            scheduleInputDialog.show(activity!!.supportFragmentManager, "Schedule Input")
            scheduleInputDialog.setListener(object : ScheduleInputDialog.OnSubmitSchedule {
                override fun submit(title: String, description: String) {
                    mPresenter!!.saveSchedule(title, description)
                    scheduleInputDialog.dismiss()
                }

                override fun delete() {}
            })
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is ScheduleListFragment.OnListFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.start()
    }

    override fun showSchedules(schedules: List<VaccinationSchedule>) {
        scheduleAdapter!!.updateSchedules(schedules)
    }

    override fun showNoSchedules() {
        Snackbar.make(vaccination_list!!, "No Schedules", Snackbar.LENGTH_LONG).show()
    }

    override fun showScheduleError(message: String) {
        Snackbar.make(vaccination_list!!, message, Snackbar.LENGTH_LONG).show()
    }

    override fun setPresenter(presenter: VaccinationScheduleContract.Presenter) {
        mPresenter = presenter
    }

    override fun showLoadingSchedules(isLoading: Boolean) {
        mListener!!.showProgress(isLoading)
    }

    override fun showAddSchedule() {
        val scheduleInputDialog = ScheduleInputDialog()
        scheduleInputDialog.show(activity!!.supportFragmentManager, "Schedule Inout")
        scheduleInputDialog.setListener(object : ScheduleInputDialog.OnSubmitSchedule {
            override fun submit(title: String, description: String) {
                mPresenter!!.saveSchedule(title, description)
                scheduleInputDialog.dismiss()
            }

            override fun delete() {}
        })
    }

    fun refresh() {
        mPresenter!!.refresh()
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    companion object {
        fun newInstance(): ScheduleDialog {
            val fragment = ScheduleDialog()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
