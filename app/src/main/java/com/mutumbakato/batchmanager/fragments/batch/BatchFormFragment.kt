package com.mutumbakato.batchmanager.fragments.batch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.BatchFromContract
import com.mutumbakato.batchmanager.ui.components.ValidatorEditText
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.Validator
import com.mutumbakato.batchmanager.utils.licence.PaymentsActivity
import kotlinx.android.synthetic.main.fragment_batch_form.*


class BatchFormFragment : BaseFragment(), BatchFromContract.View {

    private var mPresenter: BatchFromContract.Presenter? = null
    private var mName: String? = null
    private var mSupplier: String? = null
    private var mDate: String? = null
    private var mType: String? = null
    private var mRate: String? = null
    private var mQuantity: String? = null
    private var mAge: String? = null

    override val isActive: Boolean
        get() = isAdded
    //        else if (Integer.parseInt(mQuantity) > 1000 && !Prefs.getString(PreferenceUtils.LICENCE, "").equals("active")) {
    //            showLicenceDialog();
    //            valid = false;
    //        }

    private val isFormValid: Boolean
        get() {
            mName = batch_input_name.text.toString().trim { it <= ' ' }
            mSupplier = batch_input_supplier.text.toString().trim { it <= ' ' }
            mType = batch_input_type.text!!.toString().trim { it <= ' ' }
            mDate = batch_input_date.text!!.toString().trim { it <= ' ' }
            mRate = batch_input_rate.text.toString().trim { it <= ' ' }
            mQuantity = batch_input_quantity.text.toString().trim { it <= ' ' }
            mAge = batch_input_age.text.toString().trim { it <= ' ' }
            var valid = true

            if (!Validator.isValidWord(mName)) {
                batch_label_name.error = "Please add a batch name."
                valid = false
            }

            if (!Validator.isValidWord(mSupplier)) {
                mSupplier = "Unknown Supplier"
            }

            if (!batch_input_type.isValid) {
                batch_label_type.error = "Please select the breed of chicken."
                valid = false
            }

            if (!Validator.isValidNumber(mQuantity)) {
                batch_label_quantity.error = "Please add the number of birds received."
                valid = false
            }

            if (!Validator.isValidNumber(mAge)) {
                mAge = "1"
            }

            if (!batch_input_date.isValid) {
                valid = false
            }
            if (mRate!!.isEmpty() || java.lang.Float.parseFloat(mRate!!) == 0f) {
                valid = false
                batch_label_rate.error = "Invalid amount"
            }
            return valid
        }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_batch_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareValidation()
        batch_save_btn.setOnClickListener {
            save()
        }
        batch_cancel_btn.setOnClickListener {
            close()
        }
    }

    private fun prepareValidation() {
        batch_input_date.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                batch_label_date.error = error
            }

            override fun onRemoveError() {
                batch_label_date.error = null
            }
        })
        batch_input_type.setFragmentManager(activity!!.supportFragmentManager)
        batch_input_type.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                batch_label_type.error = error
            }

            override fun onRemoveError() {
                batch_label_type.error = null
            }
        })

    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    internal fun save() {
        if (isFormValid) {
            mPresenter!!.saveBatch(mDate!!, mName!!,
                    mSupplier!!,
                    Integer.parseInt(mQuantity!!),
                    java.lang.Double.parseDouble(Currency.cleanNumbers(mRate!!)),
                    mType!!, mAge!!)
        }
    }

    private fun close() {
        activity!!.finish()
    }

    override fun showEmptyBatchError() {
        Snackbar.make(batch_input_date, "Not found", Snackbar.LENGTH_LONG).show()
    }

    override fun showBatchList() {
        Toast.makeText(context, "Saved", Toast.LENGTH_LONG).show()
        activity!!.finish()
    }

    override fun setDate(date: String) {
        batch_input_date.setText(date)
    }

    override fun setName(name: String) {
        batch_input_name.setText(name)
    }

    override fun setQuantity(quantity: String) {
        batch_input_quantity.setText(quantity)
    }

    override fun setRate(rate: String) {
        batch_input_rate.setText(rate)
    }

    override fun setSupplier(supplier: String) {
        batch_input_supplier.setText(supplier)
    }

    override fun setType(type: String) {
        batch_input_type.setText(type)
    }

    override fun setAge(age: String) {
        batch_input_age.setText(age)
    }

    override fun createInitialExpense(batchId: String, date: String, quantity: Int, rate: Double) {
        //        Expense expense = new Expense(batchId, date, "Initial Expenditure", "Chicks", (float) (quantity * rate));
        //        RepositoryUtils.getExpensesRepo(getContext()).saveData(expense);
    }

    override fun setPresenter(presenter: BatchFromContract.Presenter) {
        mPresenter = presenter
    }

    private fun showLicenceDialog() {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Upgrade")
        builder.setMessage(getText(R.string.upgrade))
        builder.setNegativeButton("Cancel") { dialogInterface, i -> dialogInterface.dismiss() }
        builder.setPositiveButton("Upgrade") { dialogInterface, i ->
            dialogInterface.dismiss()
            PaymentsActivity.start(context!!)
        }
        builder.create().show()
    }

    companion object {

        fun newInstance(): BatchFormFragment {
            val fragment = BatchFormFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
