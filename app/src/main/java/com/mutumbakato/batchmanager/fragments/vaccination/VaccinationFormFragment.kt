package com.mutumbakato.batchmanager.fragments.vaccination

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.VaccinationFromContract
import com.mutumbakato.batchmanager.utils.Validator
import kotlinx.android.synthetic.main.fragment_vaccination_form.*

class VaccinationFormFragment : BaseFragment(), VaccinationFromContract.View {

    private var mPresenter: VaccinationFromContract.Presenter? = null
    private var mThenEvery: String? = null
    private var mMethod: String? = null
    private var mInfection: String? = null
    private var mVaccine: String? = null
    private var mAge: String? = null
    private var mIsRecurrent = false

    override val isActive: Boolean
        get() = isAdded

    private val isFormValid: Boolean
        get() {
            mVaccine = vaccination_input_vaccine.text!!.toString().trim { it <= ' ' }
            mAge = vaccination_input_age.text!!.toString().trim { it <= ' ' }
            mMethod = vaccination_input_method.text!!.toString().trim { it <= ' ' }
            mInfection = vaccination_input_diease.text!!.toString().trim { it <= ' ' }
            mThenEvery = vaccination_input_then.text!!.toString().trim { it <= ' ' }

            var valid = true
            if (!Validator.isValidWord(mInfection)) {
                vaccination_label_disease.error = "Please add a disease."
                valid = false
            }

            if (!Validator.isValidNumber(mAge)) {
                vaccination_label_age.error = "Please add the age of vaccination"
                valid = false
            }

            if (!Validator.isValidWord(mMethod)) {
                vaccination_label_method.error = "Please add a vaccination method."
                valid = false
            }

            if (!Validator.isValidNumber(mThenEvery)) {
                mThenEvery = ""
                mIsRecurrent = false
            } else {
                mIsRecurrent = true
            }

            if (!Validator.isValidWord(mVaccine)) {
                mVaccine = "none"
            }
            return valid
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vaccination_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        is_recurrent_input.setOnCheckedChangeListener { _, b -> check(b) }
        vaccination_save_btn.setOnClickListener {
            save()
        }
        vaccination_cancel_btn.setOnClickListener {
            close()
        }
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    internal fun save() {
        if (isFormValid) {
            mPresenter!!.saveVaccination(mAge, mThenEvery, mInfection, mVaccine, mMethod, mIsRecurrent)
            activity!!.finish()
        }
    }

    internal fun close() {
        activity!!.finish()
    }

    internal fun check(isChecked: Boolean) {
        recurrent_inputs.visibility = if (isChecked) View.VISIBLE else View.GONE
    }

    override fun showEmptyVaccinationError() {

    }

    override fun showVaccinationList() {

    }

    override fun setVaccine(name: String) {
        vaccination_input_vaccine.setText(name)
    }

    override fun setDisease(quantity: String) {
        vaccination_input_diease.setText(quantity)
    }

    override fun setMethod(rate: String) {
        vaccination_input_method.setText(rate)
    }

    override fun setAge(age: String) {
        vaccination_input_age.setText(age)
    }

    override fun setFrom(from: String) {

    }

    override fun setThenEvery(every: String) {

    }

    override fun setDescription(description: String) {

    }

    override fun setPresenter(presenter: VaccinationFromContract.Presenter) {
        mPresenter = presenter
    }

    companion object {
        fun newInstance(): VaccinationFormFragment {
            val fragment = VaccinationFormFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
