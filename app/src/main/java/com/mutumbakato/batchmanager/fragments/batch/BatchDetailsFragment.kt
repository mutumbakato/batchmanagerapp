package com.mutumbakato.batchmanager.fragments.batch

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.batch.AddUserActivity.Companion.start
import com.mutumbakato.batchmanager.activities.batch.BatchFormActivity.Companion.start
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.BatchDetailsContract
import kotlinx.android.synthetic.main.fragment_batch_details.*

class BatchDetailsFragment : BaseFragment(), BatchDetailsContract.View {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private var mPresenter: BatchDetailsContract.Presenter? = null
    private var mListener: OnBatchDetailsFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? { // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_batch_details, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is OnBatchDetailsFragmentInteractionListener) {
            context
        } else {
            throw RuntimeException(context.toString()
                    + " must implement OnBatchDetailsFragmentInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun showDetails(batch: Batch) {
        mListener!!.onBatchDetailsInteraction(batch)
    }

    override fun showName(name: String) {
        batch_name_textView.text = name
        batch_name.text = name
    }

    override fun showDate(date: String) {
        batch_date_textView.text = date
    }

    override fun showQuantity(quantity: String) {
        batch_quantity_textView.text = quantity
    }

    override fun showSupplier(supplier: String) {
        batch_supplier_textView.text = supplier
    }

    override fun showRate(rate: String) {
        batch_rate_textView.text = rate
    }

    override fun showBreed(breed: String) {
        batch_breed_textView.text = breed
    }

    override fun showAge(age: String) {}
    override fun showEditBatch(batchId: String) {
        start(context!!, batchId)
    }

    override fun showLoadingBatch(b: Boolean) {}
    override fun showNotFound() {
        //mDetailsTextView.setText("Batch not found");
    }

    override fun showErrorLoading(message: String) {}
    override fun applyPermissions(role: String) {
        mListener!!.onApplyPermissions(role)
    }

    override fun showAddUser(mBatchId: String, name: String) {
        start(context!!, mBatchId, name)
    }

    override fun setPresenter(presenter: BatchDetailsContract.Presenter) {
        mPresenter = presenter
    }

    override val isActive: Boolean
        get() = isAdded

    interface OnBatchDetailsFragmentInteractionListener {
        fun onBatchDetailsInteraction(batch: Batch?)
        fun onApplyPermissions(role: String?)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment BatchDetailsFragment.
         */
        fun newInstance(): BatchDetailsFragment {
            return BatchDetailsFragment()
        }
    }
}