package com.mutumbakato.batchmanager.fragments.batch

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.batch.BatchFormActivity
import com.mutumbakato.batchmanager.activities.batch.BatchHomeActivity
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.fragments.batch.BatchListFragment.OnBatchListInteractionListener
import com.mutumbakato.batchmanager.ui.BatchListContract
import com.mutumbakato.batchmanager.ui.adapters.BatchListAdapter
import com.mutumbakato.batchmanager.ui.adapters.BatchSectionedAdapter
import kotlinx.android.synthetic.main.fragment_batch_list.*
import kotlinx.android.synthetic.main.message_view.*
import java.util.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the [OnBatchListInteractionListener]
 * interface.
 */
class BatchListFragment : BaseFragment(), BatchListContract.View {

    private var mListener: OnBatchListInteractionListener? = null
    private var adapter: BatchListAdapter? = null
    private var mAdapter: BatchSectionedAdapter? = null
    private var mPresenter: BatchListContract.Presenter? = null

    override val isActive: Boolean
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = BatchListAdapter(ArrayList(0), mListener)
        mAdapter = BatchSectionedAdapter(LinkedHashMap(), mListener!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_batch_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        batch_list.layoutManager = LinearLayoutManager(context)
        batch_list.adapter = mAdapter
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnBatchListInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnBatchListInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        if (mPresenter != null)
            mPresenter!!.start()
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun showFirstLaunch() {
    }

    override fun setLoadingIndicator(active: Boolean) {
        mListener!!.showProgress(active)
    }

    override fun showBatches(batches: List<Batch>) {
        message_view.visibility = View.GONE
        adapter!!.updateData(batches)
    }

    override fun showBatches(batches: Map<String, List<Batch>>) {
        mAdapter!!.updateData(batches)
        mListener!!.showAd()
    }

    override fun showAddBatch() {
        BatchFormActivity.start(context!!, null)
    }

    override fun showBatchDetailsUi(batchId: String) {
        BatchHomeActivity.start(context!!, batchId)
    }

    override fun showLoadingBatchError(message: String) {
        mListener!!.showProgress(false)
        Snackbar.make(batch_list, message, Snackbar.LENGTH_LONG).show()
        message_view.visibility = View.VISIBLE
        message_image.setImageResource(R.drawable.chicken_house)
        message_title.text = message
        message_description.setText(R.string.retry)
    }

    override fun showNoBatches() {
        mListener!!.showProgress(false)
        message_view.visibility = View.VISIBLE
        message_image.setImageResource(R.drawable.chicken_house)
        message_title.setText(R.string.no_batch)
        message_description.setText(R.string.no_batch_found)
    }

    override fun showSuccessfullySavedMessage() {

    }

    override fun showBatchDeletedMessage() {

    }

    override fun showLoadingMore(isLoadingMore: Boolean) {

    }

    override fun showCountBadge(count: Int) {

    }

    override fun setPresenter(presenter: BatchListContract.Presenter) {
        mPresenter = presenter
    }

    interface OnBatchListInteractionListener {
        fun onBatchListFragmentInteraction(batch: Batch)

        fun showProgress(isLoading: Boolean)

        fun showAd()
    }

    companion object {
        fun newInstance(): BatchListFragment {
            val fragment = BatchListFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
