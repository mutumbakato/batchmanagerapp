package com.mutumbakato.batchmanager.fragments.weight

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.WeightActivity
import com.mutumbakato.batchmanager.data.models.Weight
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.viewmodels.WeightViewModel
import kotlinx.android.synthetic.main.fragment_weight_form.*
import kotlinx.android.synthetic.main.fragment_weight_form.view.*
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

class WeightFormFragment : BaseFragment() {

    private lateinit var viewModel: WeightViewModel
    private var mDate: String? = null
    private var batchId: String? = null
    private var weightId: String? = null
    private var mWeight: String? = null
    private var batchCount: Int = 0
    private var panel: ExpandableLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(activity!!).get(WeightViewModel::class.java)
        batchCount = activity!!.intent.getIntExtra(WeightActivity.BATCH_COUNT_EXTRA, 0)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_weight_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        weight_label_count.markRequired()
        weight_label_date.markRequired()
        panel = view.weight_form_panel
        viewModel.isFormOpen.observe(viewLifecycleOwner, Observer {
            panel?.isExpanded = it
            if (!it)
                clearFields()
        })
        viewModel.mDate.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                mDate = it
                weight_input_date.setText(it)
            }
        })
        viewModel.batchId.observe(viewLifecycleOwner, Observer {
            batchId = it ?: "0"
        })
        viewModel.weight.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                weightId = it.id
                batchCount = it.batchCount
                weight_input_date.setText(it.date)
                weight_input_count.setText("${it.weight * 1000}")
            }
        })
        weight_save.setOnClickListener {
            validateAndSave()
        }
        weight_discard.setOnClickListener {
            clearFields()
        }
    }

    private fun validateAndSave() {
        mDate = weight_input_date.text.toString().trim()
        mWeight = weight_input_count.text.toString().trim()

        if (mDate == null || mDate?.isEmpty()!!) {
            weight_label_date.error = "Date is required"
            return
        }

        if (mWeight == null || mWeight!!.isEmpty() || mWeight!!.toFloat() <= 0f) {
            weight_label_count.error = "Add valid weight"
            return
        }
        val kgs = mWeight!!.toFloat() / 1000
        val weight = Weight(weightId
                ?: UUID.randomUUID().toString(), batchId!!, kgs
                , mDate!!, batchCount)
        viewModel.save(weight)
        clearFields()
    }

    private fun clearFields() {
        KeyBoard.hideSoftKeyboard(activity!!)
        weight_input_date.text = null
        weight_input_count.text = null
        viewModel.setDate(null)
        viewModel.setWeightId(null)
        panel!!.collapse()
    }

}
