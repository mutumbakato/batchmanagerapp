package com.mutumbakato.batchmanager.fragments.feeds

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.feeds.FeedingActivity
import com.mutumbakato.batchmanager.data.models.Feeds
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.components.ValidatorEditText
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.utils.Validator
import com.mutumbakato.batchmanager.viewmodels.FeedsViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_feeding_form.*
import kotlinx.android.synthetic.main.fragment_feeding_form.view.*
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

class FeedingFormFragment : BaseFragment() {

    private var panel: ExpandableLayout? = null
    private var feeds: Feeds? = null
    private var mBatchId: String? = null
    private var mBatchCount: Int = 0
    private lateinit var viewModel: FeedsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!,
                ViewModelFactory.FeedsViewModelFactory(RepositoryUtils.getFeedsRepo(context!!),
                        RepositoryUtils.getBatchRepo(context!!))).get(FeedsViewModel::class.java)
        mBatchCount = activity!!.intent.getIntExtra(FeedingActivity.BATCH_COUNT, 0)
    }

    private val isFormValid: Boolean
        get() {
            val date = feeds_input_date!!.text!!.toString().trim { it <= ' ' }
            val quantity = feeds_input_count!!.text!!.toString().trim { it <= ' ' }
            var item = feeds_input_item!!.text!!.toString().trim { it <= ' ' }
            var valid = true

            if (!feeds_input_date!!.isValid) {
                valid = false
            }

            if (!Validator.isValidNumber(quantity)) {
                feeds_label_count!!.error = "Please add quantity"
                valid = false
            }

            if (!Validator.isValidWord(item)) {
                item = "Unknown"
            }

            if (valid) {
                feeds = if (feeds == null) {
                    Feeds(UUID.randomUUID().toString(), mBatchId!!, date, quantity.toFloat(), item, mBatchCount, null, DateUtils.today(), DateUtils.today())
                } else {
                    feeds!!.copy(feedType = item, quantity = quantity.toFloat(), date = date)
                }
            }
            return valid
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_feeding_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        panel = view.feeds_form_panel
        feeds_label_date.markRequired()
        feeds_label_count.markRequired()
        prepareValidation()
        feeds_save.setOnClickListener {
            save()
        }
        feeds_discard.setOnClickListener {
            discard()
        }
        viewModel.mDate.observe(viewLifecycleOwner, Observer {
            feeds_input_date.setText(it)
        })
        viewModel.isFormOpen.observe(viewLifecycleOwner, Observer {
            toggleExpansion(it)
        })
        viewModel.feed.observe(viewLifecycleOwner, Observer {
            if (it != null)
                showFeeds(it)
        })
        viewModel.batchId.observe(viewLifecycleOwner, Observer {
            mBatchId = it
        })
        viewModel.feedsId.observe(viewLifecycleOwner, Observer {
            toggleExpansion(it != null)
        })
    }

    private fun prepareValidation() {
        feeds_input_date!!.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                feeds_label_date!!.error = error
            }

            override fun onRemoveError() {
                feeds_label_date!!.error = null
            }
        })
        feeds_input_count!!.setOnFocusChangeListener { _, b ->
            if (b) {
                feeds_label_count!!.error = null
            }
        }
        feeds_input_item!!.setFragmentManager(activity!!.supportFragmentManager)
    }

    private fun save() {
        if (isFormValid) {
            viewModel.save(feeds!!)
            clearInputs()
        }
    }

    private fun discard() {
        viewModel.setDate(null)
        clearInputs()
    }

    private fun showFeeds(feeds: Feeds) {
        this.feeds = feeds
        feeds_input_item.setText(feeds.feedType)
        feeds_input_date.setText(feeds.date)
        feeds_input_count.setText(feeds.quantity.toString())
    }

    fun setDate(date: String) {
        feeds_input_date!!.setText(date)
    }

    fun toggleExpansion(expand: Boolean) {
        if (expand) {
            panel!!.expand()
        } else {
            panel!!.collapse()
        }
    }

    fun clearInputs() {
        feeds = null
        KeyBoard.hideSoftKeyboard(activity!!)
        feeds_input_count!!.text = null
        feeds_input_item!!.text = null
        feeds_input_date!!.text = null
        feeds_label_date!!.error = null
        feeds_label_count!!.error = null
        viewModel.setDate(null)
        viewModel.setFeedsId(null)
    }

}
