package com.mutumbakato.batchmanager.fragments.stats

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.FullscreenStatsActivity
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_chart.*
import org.jetbrains.anko.collections.forEachWithIndex

open class BaseChartFragment : Fragment() {

    protected lateinit var viewModel: BatchViewModel
    private var lineData = LineData()
    private val gridLines = true
    protected var isWeek = false
    protected var chartTitle = ""
    protected var enableRightAxis = false
    private var isFullScreen = false
    protected var batchId = "0"
    protected var chatType = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!,
                ViewModelFactory.BatchViewModelFactory(RepositoryUtils.getStatisticsDataSource(context!!)))[BatchViewModel::class.java]
        isFullScreen = activity is FullscreenStatsActivity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_chart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chart_title.text = chartTitle
        full_screen.setImageResource(
                if (isFullScreen) R.drawable.ic_action_full_screen_exit else R.drawable.ic_action_full_screen)
        full_screen.setOnClickListener {
            if (isFullScreen) {
                activity?.finish()
            } else {
                openFullScreen()
            }
        }
        viewModel.batch.observe(viewLifecycleOwner, Observer {
            it?.let {
                batchId = it.id
            }
        })
        viewModel.isWeek.observe(viewLifecycleOwner, Observer {
            isWeek = it
        })
        viewModel.days.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!isWeek) {
                    initChart(it)
                }
            }
        })
        viewModel.weeks.observe(viewLifecycleOwner, Observer {
            if (isWeek) {
                initChart(it)
            }
        })
    }

    private fun initChart(days: List<Int>) {
        val dates = ArrayList<String>()
        //prepare dates for formatting
        days.forEach {
            dates.add("${if (isWeek) "Week" else "Day"} ${it + 1}")
        }
        line_chart.legend.apply {
            textColor = ContextCompat.getColor(context!!, R.color.colorTextNormal)
        }
        line_chart.description = Description().apply {
            this.text = ""
        }
        line_chart.xAxis.apply {
            position = XAxis.XAxisPosition.BOTTOM
            setDrawGridLines(gridLines)
            mAxisRange = 5f
            setValueFormatter { value, _ -> dates[value.toInt()] }
            textColor = ContextCompat.getColor(context!!, R.color.colorTextNormal)
        }
        line_chart.axisLeft.apply {
            setDrawGridLines(gridLines)
            setValueFormatter { value, _ -> String.format("%.0f", value) }
            textColor = ContextCompat.getColor(context!!, R.color.colorTextNormal)
        }
        line_chart.axisRight.apply {
            setDrawAxisLine(enableRightAxis)
            setDrawLabels(enableRightAxis)
            setDrawGridLines(gridLines && enableRightAxis)
            setValueFormatter { value, _ -> String.format("%.0f", value) }
            textColor = ContextCompat.getColor(context!!, R.color.colorTextNormal)
        }
        line_chart.data = lineData
        line_chart.invalidate()
    }

    protected fun updateChartData(vararg data: EntryObject) {
        val entryList = ArrayList<List<Entry>>()
        data.forEach {
            val entries: MutableList<Entry> = arrayListOf()
            it.data.forEach { ff ->
                if (ff.value > 0f) {
                    entries.add(Entry(ff.key.toFloat(), ff.value))
                }
            }
            entryList.add(entries)
        }
        lineData.clearValues()
        entryList.forEachWithIndex { index, it ->
            if (it.isNotEmpty()) {
                val properties = data[index]
                lineData.addDataSet(getDataSet(it, properties.name, properties.label, properties.color, properties.axis, properties.isDotted))
            }
        }
        lineData.notifyDataChanged()
        line_chart.notifyDataSetChanged()
        line_chart.invalidate()
    }

    private fun getDataSet(entries: List<Entry>, name: String, label: String, color: Int, axis: YAxis.AxisDependency, isDotted: Boolean = false): LineDataSet {

        val weightDataSet = LineDataSet(entries, name)
        weightDataSet.color = color
        weightDataSet.mode = LineDataSet.Mode.LINEAR
        weightDataSet.setDrawValues(false)
        weightDataSet.setDrawCircles(!isDotted)
        weightDataSet.setDrawCircleHole(false)
        weightDataSet.setCircleColor(color)
        weightDataSet.axisDependency = axis

        if (isDotted) {
            weightDataSet.enableDashedLine(10f, 5f, 0f)
        }

        if (axis == YAxis.AxisDependency.LEFT) {
            left_label.text = label
        } else {
            right_label.text = label
        }

        return weightDataSet
    }

    private fun openFullScreen() {
        if (chatType.isNotEmpty()) {
            FullscreenStatsActivity.start(context!!, batchId, chatType)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = BaseChartFragment()
    }

    data class EntryObject(val data: Map<Int, Float>,
                           val name: String,
                           val label: String,
                           val color: Int,
                           val axis: YAxis.AxisDependency = YAxis.AxisDependency.LEFT,
                           val isDotted: Boolean = false)
}
