package com.mutumbakato.batchmanager.fragments.batch

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.ActivityLogDataSource
import com.mutumbakato.batchmanager.data.models.ActivityLog
import com.mutumbakato.batchmanager.data.utils.Logger
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.EndlessRecyclerViewScrollListener
import com.mutumbakato.batchmanager.ui.adapters.ActivityLogAdapter
import kotlinx.android.synthetic.main.message_view.*
import java.util.*

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class ActivityLogFragment : BaseFragment() {

    internal lateinit var progressBar: ProgressBar
    private var adapter: ActivityLogAdapter? = null
    private var mBatchId: String? = null
    private var lastDate = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = ActivityLogAdapter(ArrayList(), 0)
        if (arguments != null) {
            mBatchId = arguments!!.getString(ARG_FARM_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_activitylog_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        progressBar = view.findViewById(R.id.activity_loading_progress)
        // Set the adapter
        val recyclerView = view.run { findViewById<RecyclerView>(R.id.list) }
        val linearLayoutManager = LinearLayoutManager(view.context)

        recyclerView.run {
            layoutManager = linearLayoutManager
            this.adapter = this@ActivityLogFragment.adapter
        }

        val scrollListener = object : EndlessRecyclerViewScrollListener(linearLayoutManager) {

            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                if (this@ActivityLogFragment.lastDate != adapter!!.lastDate) {
                    lastDate = adapter!!.lastDate
                    progressBar.visibility = View.VISIBLE
                    Logger.loadMore(mBatchId!!, adapter!!.lastDate, object : ActivityLogDataSource.LogsCallBack {
                        override fun onLoad(logs: List<ActivityLog>, newCount: Int) {
                            adapter!!.topUp(logs)
                            progressBar.visibility = View.GONE
                        }

                        override fun onError(message: String) {
                            progressBar.visibility = View.GONE
                        }
                    })
                } else {
                    Toast.makeText(context, "End of Logs", Toast.LENGTH_LONG).show()
                }
            }
        }
        recyclerView.addOnScrollListener(scrollListener)
    }

    override fun onResume() {
        super.onResume()
        Logger.getLogs(mBatchId!!, object : ActivityLogDataSource.LogsCallBack {
            override fun onLoad(logs: List<ActivityLog>, newCount: Int) {
                if (logs.isNotEmpty())
                    showLogs(logs)
                else
                    showEmpty()
            }

            override fun onError(message: String) {}
        })
    }

    private fun showLogs(logs: List<ActivityLog>) {
        if (isAdded) {
            adapter!!.addItems(logs, 0)
            message_view.visibility = View.GONE
        }
    }

    private fun showEmpty() {
        if (isAdded) {
            message_view.visibility = View.VISIBLE
            message_image.setImageResource(R.drawable.calendar)
            message_title.setText(R.string.no_activities)
            message_description.setText(R.string.no_activities_message)
        }
    }

    companion object {
        private const val ARG_FARM_ID = "farm_id"
        fun newInstance(farmId: String): ActivityLogFragment {
            val fragment = ActivityLogFragment()
            val args = Bundle()
            args.putString(ARG_FARM_ID, farmId)
            fragment.arguments = args
            return fragment
        }
    }
}
