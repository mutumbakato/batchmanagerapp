package com.mutumbakato.batchmanager.fragments.feeds

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.models.Feeds
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.adapters.FeedingOldRecyclerViewAdapter
import com.mutumbakato.batchmanager.viewmodels.FeedsViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_feeding_list.*

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class FeedingListFragmentOld : BaseFragment() {

    private lateinit var feedsAdapter: FeedingOldRecyclerViewAdapter
    private lateinit var viewModel: FeedsViewModel
    private val listener = object : OnFeedsItemInteraction {

        override fun onEdit(feeds: Feeding) {
            viewModel.setFeedsId(feeds.id)
            viewModel.setDate(feeds.date)
        }

        override fun onDelete(feeds: Feeding) {
//        showConfirmDelete(feeds)
        }

        override fun onAdd(date: String) {
            viewModel.setDate(date)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!,
                ViewModelFactory.FeedsViewModelFactory(RepositoryUtils.getFeedsRepo(context!!),
                        RepositoryUtils.getBatchRepo(context!!))).get(FeedsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_feeding_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        viewModel.userRole.observe(viewLifecycleOwner, Observer {
            applyPermissions(it)
        })
        viewModel.oldFeeds.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                feedsAdapter.updateData(it)
            }
        })
    }

    private fun initRecyclerView() {
        feedsAdapter = FeedingOldRecyclerViewAdapter(ArrayList(), listener, feeds_list)
        feeds_list.layoutManager = LinearLayoutManager(context)
        feeds_list.adapter = feedsAdapter
    }

    fun showForm(batchId: String) {
        viewModel.setBatchId(batchId)
    }

    fun showConfirmDelete(feeds: Feeds) {
        val snack = Snackbar.make(feeds_list, "Do you want to remove this record?", Snackbar.LENGTH_LONG)
        snack.setAction("Delete") {
            viewModel.delete(feeds)
            snack.dismiss()
        }
        snack.show()
    }

    fun applyPermissions(role: String) {
        feedsAdapter.applyPermissions(role)
    }

    interface OnFeedsItemInteraction {

        fun onEdit(feeds: Feeding)

        fun onDelete(feeds: Feeding)

        fun onAdd(date: String)
    }

    companion object {
        fun newInstance(): FeedingListFragmentOld {
            return FeedingListFragmentOld()
        }
    }
}
