package com.mutumbakato.batchmanager.fragments.vaccination

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.vaccination.VaccinationActivity
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.VaccinationScheduleContract
import com.mutumbakato.batchmanager.ui.adapters.ScheduleRecyclerViewAdapter
import com.mutumbakato.batchmanager.ui.components.ScheduleInputDialog
import kotlinx.android.synthetic.main.fragment_schedule_list.*
import kotlinx.android.synthetic.main.message_view.*
import java.util.*

class ScheduleListFragment : BaseFragment(), VaccinationScheduleContract.View {

    private var mListener: OnListFragmentInteractionListener? = null
    private var mPresenter: VaccinationScheduleContract.Presenter? = null
    private var scheduleAdapter: ScheduleRecyclerViewAdapter? = null

    override val isActive: Boolean
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val mScheduleId = activity!!.intent.getStringExtra(VaccinationActivity.SCHEDULE_ID_EXTRA)
        scheduleAdapter = ScheduleRecyclerViewAdapter(ArrayList(), mScheduleId, object : OnListFragmentInteractionListener {

            override fun onListFragmentInteraction(item: VaccinationSchedule) {
                mListener!!.onListFragmentInteraction(item)
            }

            override fun onUseSchedule(schedule: VaccinationSchedule) {
                mPresenter!!.useSchedule(schedule)
                mListener!!.onUseSchedule(schedule)
            }

            override fun onRemoveSchedule(schedule: String) {
                mPresenter!!.removeSchedule(schedule)
                mListener!!.onRemoveSchedule(schedule)
            }

            override fun updateSchedule(schedule: VaccinationSchedule) {
                val inputDialog = ScheduleInputDialog()
                inputDialog.edit(schedule.title, schedule.description)
                inputDialog.setListener(object : ScheduleInputDialog.OnSubmitSchedule {
                    override fun submit(title: String, description: String) {
                        mPresenter!!.updateSchedule(VaccinationSchedule(schedule.id,
                                schedule.userId, title, description,
                                schedule.publicity, schedule.serverId,
                                schedule.status, 0, PreferenceUtils.farmId))
                        inputDialog.dismiss()
                    }

                    override fun delete() {
                        mPresenter!!.deleteSchedule(schedule)
                        inputDialog.dismiss()
                    }
                })
                inputDialog.show(activity!!.supportFragmentManager, "")
            }

            override fun showProgress(isLoading: Boolean) {}
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_schedule_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vaccination_list.layoutManager = LinearLayoutManager(context)
        vaccination_list.adapter = scheduleAdapter
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    override fun showSchedules(schedules: List<VaccinationSchedule>) {
        scheduleAdapter!!.updateSchedules(schedules)
        message_view.visibility = View.GONE
    }

    override fun showNoSchedules() {
        message_view.visibility = View.VISIBLE
        message_image.setImageResource(R.drawable.syringe)
        message_title.setText(R.string.no_vaccination_schedules)
        message_description.setText(R.string.no_schedules_message)
    }

    override fun showScheduleError(message: String) {
        Snackbar.make(vaccination_list!!, message, Snackbar.LENGTH_LONG).show()
    }

    override fun setPresenter(presenter: VaccinationScheduleContract.Presenter) {
        mPresenter = presenter
    }

    override fun showLoadingSchedules(isLoading: Boolean) {
        mListener!!.showProgress(isLoading)
    }

    override fun showAddSchedule() {
        val scheduleInputDialog = ScheduleInputDialog()
        scheduleInputDialog.show(activity!!.supportFragmentManager, "Schedule Inout")
        scheduleInputDialog.setListener(object : ScheduleInputDialog.OnSubmitSchedule {
            override fun submit(title: String, description: String) {
                mPresenter!!.saveSchedule(title, description)
                scheduleInputDialog.dismiss()
            }

            override fun delete() {}
        })
    }

    fun refresh() {
        mPresenter!!.refresh()
    }

    interface OnListFragmentInteractionListener {

        fun onListFragmentInteraction(item: VaccinationSchedule)

        fun onUseSchedule(schedule: VaccinationSchedule)

        fun onRemoveSchedule(schedule: String)

        fun updateSchedule(schedule: VaccinationSchedule)

        fun showProgress(isLoading: Boolean)
    }

    companion object {
        fun newInstance(): ScheduleListFragment {
            val fragment = ScheduleListFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
