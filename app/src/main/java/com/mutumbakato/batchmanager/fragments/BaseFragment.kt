package com.mutumbakato.batchmanager.fragments

import androidx.fragment.app.Fragment
import com.google.android.material.textfield.TextInputLayout


open class BaseFragment : Fragment() {

    fun TextInputLayout.markRequired() {
        hint = "$hint *"
    }

}
