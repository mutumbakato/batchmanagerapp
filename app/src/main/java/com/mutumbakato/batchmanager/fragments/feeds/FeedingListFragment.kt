package com.mutumbakato.batchmanager.fragments.feeds

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.feeds.FeedingActivity
import com.mutumbakato.batchmanager.data.models.Feeds
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.adapters.FeedingRecyclerViewAdapter
import com.mutumbakato.batchmanager.ui.adapters.FeedingTimelineAdapter
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.viewmodels.FeedsViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_feeding_list.*
import org.zakariya.stickyheaders.StickyHeaderLayoutManager

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class FeedingListFragment : BaseFragment() {

    private lateinit var timelineAdapter: FeedingTimelineAdapter
    private lateinit var feedsAdapter: FeedingRecyclerViewAdapter
    private lateinit var viewModel: FeedsViewModel
    private var mDateOfBirth: String = ""
    private var mCloseDate: String? = null
    private lateinit var batchType: String

    private val listener = object : OnFeedsItemInteraction {

        override fun onEdit(feeds: Feeds) {
            viewModel.setFeedsId(feeds.id)
            viewModel.setDate(feeds.date)
        }

        override fun onDelete(feeds: Feeds) {
            showConfirmDelete(feeds)
        }

        override fun onAdd(date: String) {
            viewModel.setDate(date)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!).get(FeedsViewModel::class.java)
        mDateOfBirth = activity!!.intent.getStringExtra(FeedingActivity.DATE_OF_BIRTH)
                ?: DateUtils.today()
        mCloseDate = activity!!.intent.getStringExtra(FeedingActivity.CLOSE_DATE)
        batchType = activity!!.intent.getStringExtra(FeedingActivity.BATCH_TYPE) ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_feeding_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        viewModel.sortedFeeds.observe(viewLifecycleOwner, Observer {
            showSectionedFeeds(it)
        })
        viewModel.userRole.observe(viewLifecycleOwner, Observer {
            applyPermissions(it)
        })
        viewModel.feeds.observe(viewLifecycleOwner, Observer {
            if (it.isNotEmpty()) {
                feedsAdapter.updateData(it)
            }
        })
        viewModel.goTo.observe(viewLifecycleOwner, Observer {
            timelineAdapter.goToWeek()
        })
    }

    private fun initRecyclerView() {
        feedsAdapter = FeedingRecyclerViewAdapter(ArrayList(), listener, feeds_list)
        timelineAdapter = FeedingTimelineAdapter(mDateOfBirth, listener, feeds_list, childFragmentManager, mCloseDate, batchType)
        feeds_list.layoutManager = StickyHeaderLayoutManager()
        feeds_list.adapter = timelineAdapter
        timelineAdapter.goToWeek()
    }

    fun goToWeek() {
        timelineAdapter.goToWeek()
    }

    private fun showSectionedFeeds(feeds: Map<Int, List<Feeds>>) {
        timelineAdapter.updateData(feeds)
    }

    fun showForm(batchId: String) {
        viewModel.setBatchId(batchId)
    }

    fun showConfirmDelete(feeds: Feeds) {
        val snack = Snackbar.make(feeds_list, "Do you want to remove this record?", Snackbar.LENGTH_LONG)
        snack.setAction("Delete") {
            viewModel.delete(feeds)
            snack.dismiss()
        }
        snack.show()
    }

    fun applyPermissions(role: String) {
        timelineAdapter.applyPermissions(role)
    }

    interface OnFeedsItemInteraction {

        fun onEdit(feeds: Feeds)

        fun onDelete(feeds: Feeds)

        fun onAdd(date: String)
    }

    companion object {
        fun newInstance(): FeedingListFragment {
            return FeedingListFragment()
        }
    }
}
