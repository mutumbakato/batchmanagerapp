package com.mutumbakato.batchmanager.fragments.export

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mutumbakato.batchmanager.BuildConfig
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.viewmodels.ExportViewModel
import kotlinx.android.synthetic.main.fragment_export_complete.*
import java.io.File

class ExportComplete : Fragment() {

    private lateinit var viewModel: ExportViewModel
    private var exportPath: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!).get(ExportViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_export_complete, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        export_finish_button.setOnClickListener {
            exportPath?.let {
                openExportFile(it)
            }
        }
        export_share_button.setOnClickListener {
            exportPath?.let {
                shareFile(it)
            }
        }
        viewModel.exportFilePath.observe(viewLifecycleOwner, Observer {
            exportPath = it
        })
    }

    private fun openExportFile(excelFile: File) {
        val intent = Intent(Intent.ACTION_VIEW)
        val uri: Uri = FileProvider.getUriForFile(
                context!!,
                BuildConfig.APPLICATION_ID + ".provider",
                excelFile
        )
        intent.setDataAndType(uri, "application/vnd.ms-excel")
        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
        try {
            context!!.startActivity(Intent.createChooser(intent, "Open with..."))
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.microsoft.office.excel")))
            } catch (e: ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.microsoft.office.excel")))
            }
        }
    }

    private fun shareFile(excelFile: File) {
        val sendIntent = Intent()
        val uri: Uri = FileProvider.getUriForFile(
                context!!,
                BuildConfig.APPLICATION_ID + ".provider",
                excelFile
        )
        sendIntent.action = Intent.ACTION_VIEW
        sendIntent.setDataAndType(uri, "application/vnd.ms-excel")
        sendIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment ExportComplete.
         */
        @JvmStatic
        fun newInstance() = ExportComplete()
    }
}
