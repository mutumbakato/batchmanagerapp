package com.mutumbakato.batchmanager.fragments.export

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.farm.ExportActivity
import kotlinx.android.synthetic.main.export_fragment.*

class ExportFragment : Fragment() {

    companion object {
        fun newInstance(batchId: String) = ExportFragment().apply {
            arguments = Bundle().apply {
                putString("batch_id", batchId)
            }
        }
    }

    private var batchId: String = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            batchId = it.getString("batch_id", "0")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.export_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        export_batch_button.setOnClickListener {
            ExportActivity.starts(it.context, batchId)
        }
    }
}
