package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.YAxis
import com.mutumbakato.batchmanager.R

class DeathsChart : BaseChartFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartTitle = getString(R.string.deaths)
        chatType = "deaths"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dailyDeaths.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!isWeek) {
                    updateChartData(getEntryObject(it))
                }
            }
        })
        viewModel.weeklyDeaths.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    updateChartData(getEntryObject(it))
                }
            }
        })
    }


    private fun getEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.deaths),
                getString(R.string.deaths_curled_birds),
                ContextCompat.getColor(context!!, R.color.colorPurple),
                YAxis.AxisDependency.LEFT)
    }

    companion object {
        @JvmStatic
        fun newInstance() = DeathsChart()
    }
}
