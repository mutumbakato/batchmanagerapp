package com.mutumbakato.batchmanager.fragments.eggs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Eggs
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.EggsListContract
import com.mutumbakato.batchmanager.ui.EggsListContract.View.EggsItemInteractionListener
import com.mutumbakato.batchmanager.ui.adapters.EggsRecyclerViewAdapter
import com.mutumbakato.batchmanager.utils.DateUtils.parseDateToddMMyyyy
import kotlinx.android.synthetic.main.fragment_eggs_list.*
import java.util.*

class EggsListFragment
    : BaseFragment(), EggsListContract.View, EggsItemInteractionListener {

    private var adapter: EggsRecyclerViewAdapter? = null
    private var mPresenter: EggsListContract.Presenter? = null
    private var mListener: EggsListContract.View.ListInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_eggs_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initRecyclerView() {
        adapter = EggsRecyclerViewAdapter(ArrayList(0), eggs_list, this)
        eggs_list.layoutManager = LinearLayoutManager(context)
        eggs_list.adapter = adapter
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is EggsListContract.View.ListInteractionListener) {
            context
        } else {
            throw RuntimeException(context.toString()
                    + " must implement EggsListInteractionListener")
        }
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    override fun setLoadingIndicator(active: Boolean) {
        mListener!!.showProgress(active)
    }

    override fun showEggs(eggs: List<Eggs>) {
        adapter!!.updateData(eggs)
        mListener!!.showEmpty(false)
    }

    override fun showNoEggs() {
        mListener!!.showEmpty(true)
    }

    override fun showSuccessfullySavedMessage() {}
    override fun showConfirmDelete(eggs: Eggs, mills: Int) {
        Snackbar.make(eggs_list, "Delete eggs for " + parseDateToddMMyyyy(eggs.date), mills).apply {
            setAction("Delete") { mPresenter!!.delete(eggs) }
            show()
        }
    }

    override fun showEdit(id: String) {
        mListener!!.showForm(id)
    }

    override fun showTotalEggs(total: String) {
        mListener!!.showTotal(total)
    }

    override fun applyPermissions(role: String) {
        adapter!!.applyPermissions(role)
        mListener!!.applyPermissions(role)
    }

    override fun setPresenter(presenter: EggsListContract.Presenter) {
        mPresenter = presenter
    }

    override val isActive: Boolean
        get() = isAdded

    override fun onDelete(eggs: Eggs, position: Int) {
        mPresenter!!.deleteEggs(eggs)
    }

    override fun onEdit(eggs: Eggs) {
        mPresenter!!.edit(eggs)
    }

    companion object {
        fun newInstance(): EggsListFragment {
            return EggsListFragment()
        }
    }
}