package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.components.Description
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchCount
import com.mutumbakato.batchmanager.data.models.EntryItem
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_statistics.*

class StatisticsFragment : BaseFragment() {

    private lateinit var batch: Batch
    private lateinit var viewModel: BatchViewModel
    private var mDate = DateUtils.today()
    private var sales: List<EntryItem> = ArrayList()
    private var expenses: List<EntryItem> = ArrayList()
    private var feeds: List<EntryItem> = ArrayList()
    private var water: List<EntryItem> = ArrayList()
    private var sold: Int = 0
    private var totalSale = 0f
    private var totalExpenditure = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!,
                ViewModelFactory.BatchViewModelFactory(RepositoryUtils.getStatisticsDataSource(context!!)))[BatchViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_statistics, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.batch.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                batch = it
                mDate = it.date!!
                if (it.type == "Layers") {
                    eggs_stats_card.visibility = View.VISIBLE
                }
            }
        })
        viewModel.totalSales.observe(viewLifecycleOwner, Observer {
            totalSale = it ?: 0f
            textView_total_sales.text = "${PreferenceUtils.currency} ${Currency.format(totalSale)}"
            showProfit(totalSale, totalExpenditure)
        })
        viewModel.totalExpenditure.observe(viewLifecycleOwner, Observer {
            totalExpenditure = it ?: 0f
            textView_total_expenses.text = "${PreferenceUtils.currency} ${Currency.format(totalExpenditure)}"
            showProfit(totalSale, totalExpenditure)
        })
        viewModel.batchCount.observe(viewLifecycleOwner, Observer {
            it?.let {
                showMortality(it)
            }
        })
        viewModel.totalFeeds.observe(viewLifecycleOwner, Observer {
            total_feeds.text = String.format("%s Kg", Currency.format(it ?: 0f))
        })
        viewModel.aveTotalFeeds.observe(viewLifecycleOwner, Observer {
            it?.let {
                avg_total_feeds.text = String.format("%.0f gm/bird", it)
            }
        })

        viewModel.aveTotalWater.observe(viewLifecycleOwner, Observer {
            it?.let {
                avg_total_water.text = String.format("%.0f ml/bird", it)
            }
        })

        viewModel.sold.observe(viewLifecycleOwner, Observer {
            sold = it ?: 0
            textView_sold.text = sold.toString()
            showRemaining(batch.quantity, sold)
        })

        viewModel.costPerBird.observe(viewLifecycleOwner, Observer {
            cost_per_bird.text = String.format("%s %s", PreferenceUtils.currency, Currency.format(it
                    ?: 0f))
        })

        viewModel.eggCount.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                total_eggs_good.text = String.format("%s", Currency.format((it.total).toFloat()))
                total_eggs_bad.text = String.format("%s", Currency.format((it.damage).toFloat()))
            } else {
                total_eggs_good.text = "0"
                total_eggs_bad.text = "0"
            }
        })

        viewModel.eggsPercentage.observe(viewLifecycleOwner, Observer {
            it?.let {
                laying_percentage.text = "${String.format("%.1f", it)}%"
            }
        })

        viewModel.avgWeight.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                avg_weight.text = Currency.format(it) + " gm"
            } else {
                avg_weight.text = String.format("%.0f gm", 0f)
            }
        })

        viewModel.feedConversion.observe(viewLifecycleOwner, Observer {
            it?.let {
                feed_conversion.text = String.format("%.1f", it)
            }
        })

        viewModel.weightUniformity.observe(viewLifecycleOwner, Observer {
            it?.let {
                weight_uniformity.text = String.format("%.1f", it) + "%"
            }
        })

        viewModel.avgDailyGain.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                avg_daily_gain.text = String.format("%.1f gm", it)
            } else {
                avg_daily_gain.text = "--"
            }
        })

        viewModel.eggsPercentage.observe(viewLifecycleOwner, Observer {
            it?.let {
                laying_percentage_text.text = String.format("%.1f", it) + "%"
            }
        })

        viewModel.isLaying.observe(viewLifecycleOwner, Observer {
            laying_percentage_summary.visibility = if (it) View.VISIBLE else View.GONE
            average_daily_gain.visibility = if (!it) View.VISIBLE else View.GONE
        })
    }

    private fun showMortality(count: BatchCount) {

        textView_deaths.text = count.dead.toString()
        textView_live.text = count.remaining.toString()
        textView_mortality_rate.text = "${String.format("%.1f", count.mortalityRate)}%"

        val entries: MutableList<PieEntry> = ArrayList()
        entries.add(PieEntry(count.remaining.toFloat(), "Alive"))
        entries.add(PieEntry(count.dead.toFloat(), "Dead & Curled"))

        val set = PieDataSet(entries, "").apply {
            this.colors = arrayListOf(ContextCompat.getColor(context!!, R.color.colorOrange),
                    ContextCompat.getColor(context!!, R.color.colorPurple))
            this.valueTextColor = ContextCompat.getColor(context!!, R.color.colorTextNormal)
        }

        val data = PieData(set)

        chart_mortality.centerTextRadiusPercent = 5f
        chart_mortality.data = data
        chart_mortality.description = Description().apply {
            this.text = " "
        }

        chart_mortality.legend.textColor = ContextCompat.getColor(context!!, R.color.colorTextNormal)
        chart_mortality.invalidate() // refresh
    }

    private fun showRemaining(count: Int, sold: Int) {
        textView_remaining_after_sale.text = (count - sold).toString()
    }

    private fun showProfit(sales: Float, expenses: Float) {
        textView_total_profits.text = "${PreferenceUtils.currency} ${Currency.format(sales - expenses)}"
    }

    companion object {

        private const val ARG_BATCH_ID = "batch_id"

        fun newInstance(batchId: String): StatisticsFragment {
            val fragment = StatisticsFragment()
            val args = Bundle()
            args.putString(ARG_BATCH_ID, batchId)
            fragment.arguments = args
            return fragment
        }
    }
}
