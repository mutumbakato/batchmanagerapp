package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.YAxis
import com.mutumbakato.batchmanager.R

class WaterChart : BaseChartFragment() {

    private var water: Map<Int, Float> = mapOf()
    private var target: Map<Int, Float> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartTitle = getString(R.string.water_intake)
        chatType = "water"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dailyWater.observe(viewLifecycleOwner, Observer {
            it?.let { f ->
                if (!isWeek) {
                    water = f
                    updateChartData(getWaterData(water), getTargetData(target))
                }
            }
        })

//        viewModel.dailyFeedsTarget.observe(viewLifecycleOwner, Observer {
//            it?.let { w ->
//                if (!isWeek) {
//                    target = w
//                    updateChartData(getFeedsData(water), getTargetData(target))
//                }
//            }
//        })

        viewModel.weeklyWater.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    water = it
                    updateChartData(getWaterData(water), getTargetData(target))
                }
            }
        })
    }


    private fun getWaterData(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.water),
                getString(R.string.water_ml_bird),
                ContextCompat.getColor(context!!, R.color.colorBlue))
    }

    private fun getTargetData(data: Map<Int, Float>): EntryObject {
        return EntryObject(
                data,
                "Target (gm/bird)",
                "",
                ContextCompat.getColor(context!!, R.color.colorStandard),
                YAxis.AxisDependency.LEFT, true)
    }

    companion object {
        @JvmStatic
        fun newInstance() = WaterChart()
    }
}
