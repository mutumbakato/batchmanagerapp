package com.mutumbakato.batchmanager.fragments.farm

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.farm.FarmFormActivity
import com.mutumbakato.batchmanager.data.FarmDataSource
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.FarmRepository
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.ui.adapters.FarmListRecyclerViewAdapter
import kotlinx.android.synthetic.main.fragment_farm_list.*
import java.util.*

class FarmListFragment : DialogFragment(), FarmDataSource.FarmListCallback {

    private var adapter: FarmListRecyclerViewAdapter? = null
    private var mListener: OnListFragmentInteractionListener? = null
    private var farmRepository: FarmRepository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.FullScreenDialogStyle)
        farmRepository = RepositoryUtils.getFarmRepo(context)
        adapter = FarmListRecyclerViewAdapter(ArrayList(), mListener)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_farm_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter

        close_farms_list.setOnClickListener { dismiss() }
        add_farms_button.setOnClickListener {
            FarmFormActivity.start(context!!, null)
        }
    }

    override fun onResume() {
        super.onResume()
        farmRepository!!.getFarms(PreferenceUtils.userId, this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onLoad(farms: List<Farm>) {
        try {
            adapter!!.updateData(farms)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onFail(message: String) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    interface OnListFragmentInteractionListener {

        fun onFarmListFragmentInteraction(item: Farm)

        fun onSettings(item: Farm)
    }

    companion object {

        fun newInstance(): FarmListFragment {
            val fragment = FarmListFragment()
            val args = Bundle()
            fragment.arguments = args
            return fragment
        }
    }
}
