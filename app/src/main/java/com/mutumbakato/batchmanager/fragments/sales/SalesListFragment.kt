package com.mutumbakato.batchmanager.fragments.sales

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.SalesListContract
import com.mutumbakato.batchmanager.ui.adapters.SalesRecyclerAdapter
import com.mutumbakato.batchmanager.ui.adapters.SalesStickyHeaderAdapter
import com.mutumbakato.batchmanager.viewmodels.SalesViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_sales_list.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap

/**
 * A fragment representing a list of Items.
 */
class SalesListFragment : BaseFragment(), SalesListContract.View.SalesItemInteractionListener {

    private var mAdapter: SalesStickyHeaderAdapter? = null
    private var salesAdapter: SalesRecyclerAdapter? = null
    private val mSales = LinkedHashMap<String, List<Sale>>()
    private val categories = ArrayList<String>()
    private lateinit var viewModel: SalesViewModel
    private var mode = "all"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!, ViewModelFactory.SalesViewModelFactory(RepositoryUtils.getSalesLiveRepo(context!!))).get(SalesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sales_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        viewModel.sales.observe(viewLifecycleOwner, Observer {
            if (mode == "all")
                salesAdapter?.updateData(it)
        })
        viewModel.filteredSales.observe(viewLifecycleOwner, Observer {
            if (mode == "filter")
                salesAdapter?.updateData(it)
        })
        viewModel.mode.observe(viewLifecycleOwner, Observer {
            mode = it
        })
    }

    private fun initRecyclerView() {
        mAdapter = SalesStickyHeaderAdapter(mSales, categories, this, sales_list)
        salesAdapter = SalesRecyclerAdapter(ArrayList(), this, sales_list)
        sales_list.adapter = salesAdapter
        sales_list.layoutManager = LinearLayoutManager(context)
    }

    private fun showSales(categories: List<String>, sales: HashMap<String, List<Sale>>) {
        mAdapter!!.updateData(sales, categories)
    }

    override fun onDelete(sale: Sale?) {
        showConfirmDelete(sale!!)
    }

    fun showConfirmDelete(sale: Sale, mills: Int = 0) {
        Snackbar.make(sales_list!!, "Do you want to remove " + sale.quantity + " " + sale.item + "?", mills).apply {
            setAction("Delete") {
                viewModel.deleteSale(sale)
                dismiss()
            }
            show()
        }
    }

    fun applyPermissions(role: String) {
        mAdapter!!.applyPermissions(role)
    }

    override fun onEdit(sale: Sale) {
        viewModel.setSaleId(sale.id)
        viewModel.isEditing.postValue(true)
    }

    private fun getSortedSales(sales: List<Sale>): HashMap<String, List<Sale>> {
        val sortedSales = java.util.LinkedHashMap<String, List<Sale>>()
        val dates = getSections(sales)
        for (date in dates) {
            sortedSales[date] = getSectionData(sales, date)
        }
        return sortedSales
    }

    private fun getSections(sales: List<Sale>): List<String> {
        val sections = java.util.ArrayList<String>()

        for (sale in sales) {
            if (!sections.contains(sale.date))
                sections.add(sale.date)
        }

        sections.sortWith(Comparator { o1, o2 -> o2.compareTo(o1) })
        return sections
    }

    private fun getSectionData(sales: List<Sale>, date: String): List<Sale> {
        val sls = java.util.ArrayList<Sale>()
        for (sale in sales) {
            if (sale.date == date)
                sls.add(sale)
        }
        sls.sortWith(Comparator { t, t1 -> t1.id.compareTo(t.id) })
        return sls
    }
}
