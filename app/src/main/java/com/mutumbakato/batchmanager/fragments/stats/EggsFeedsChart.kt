package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.YAxis
import com.mutumbakato.batchmanager.R

class EggsFeedsChart : BaseChartFragment() {

    private var feeds: Map<Int, Float> = mapOf()
    private var eggs: Map<Int, Float> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartTitle = getString(R.string.eggs_feeds)
        enableRightAxis = true
        chatType = "eggs-feeds"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dailyFeeds.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!isWeek) {
                    feeds = it
                    updateChartData(getEggsEntryObject(eggs), getFeedsEntryObject(feeds))
                }
            }
        })
        viewModel.dailyEggs.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (!isWeek) {
                    eggs = it
                    updateChartData(getEggsEntryObject(eggs), getFeedsEntryObject(feeds))
                }
            }
        })
        viewModel.weeklyFeeds.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    feeds = it
                    updateChartData(getEggsEntryObject(eggs), getFeedsEntryObject(feeds))
                }
            }
        })
        viewModel.weeklyEggs.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    eggs = it
                    updateChartData(getEggsEntryObject(eggs), getFeedsEntryObject(feeds))
                }
            }
        })
    }

    private fun getEggsEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.title_eggs),
                getString(R.string.title_eggs),
                ContextCompat.getColor(context!!, R.color.colorEggs),
                YAxis.AxisDependency.LEFT)
    }

    private fun getFeedsEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.feeds_intake),
                getString(R.string.feeds_gm_bird),
                ContextCompat.getColor(context!!, R.color.colorYellowDark), YAxis.AxisDependency.RIGHT)
    }

    companion object {
        @JvmStatic
        fun newInstance() = EggsFeedsChart()
    }
}
