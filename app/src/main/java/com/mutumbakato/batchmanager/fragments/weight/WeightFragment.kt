package com.mutumbakato.batchmanager.fragments.weight

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.WeightActivity
import com.mutumbakato.batchmanager.data.models.Weight
import com.mutumbakato.batchmanager.ui.adapters.WeightTimelineAdapter
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.viewmodels.WeightViewModel
import kotlinx.android.synthetic.main.weight_fragment.*
import org.zakariya.stickyheaders.StickyHeaderLayoutManager

class WeightFragment : Fragment() {

    companion object {
        fun newInstance() = WeightFragment()
    }

    private lateinit var dateOfBirth: String
    private var closeDate: String? = null
    private lateinit var batchId: String
    private lateinit var batchType: String
    private lateinit var viewModel: WeightViewModel
    private lateinit var weightSectionedAdapter: WeightTimelineAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!).get(WeightViewModel::class.java)
        dateOfBirth = activity!!.intent.getStringExtra(WeightActivity.DOB_EXTRA)?: DateUtils.today()
        batchId = activity!!.intent.getStringExtra(WeightActivity.BATCH_ID_EXTRA) ?: "0"
        closeDate = activity!!.intent.getStringExtra(WeightActivity.CLOSE_DATE_EXTRA)
        batchType = activity!!.intent.getStringExtra(WeightActivity.BATCH_TYPE) ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.weight_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        weightSectionedAdapter = WeightTimelineAdapter(dateOfBirth, object : OnWeightItemInteraction {

            override fun onAdd(date: String) {
                viewModel.setDate(date)
            }

            override fun onEdit(item: Weight) {
                viewModel.setWeightId(item.id)
                viewModel.setDate(item.date)
            }

            override fun onDelete(item: Weight) {
                Snackbar.make(weightRecyclerView, "Do you want to delete Weight ${item.weight}", Snackbar.LENGTH_LONG).run {
                    setAction("Delete") {
                        viewModel.delete(item)
                    }
                    show()
                }
            }
        }, weightRecyclerView, childFragmentManager, closeDate, batchType)

        val stickyHeaderLayoutManager = StickyHeaderLayoutManager()
        weightRecyclerView.run {
            layoutManager = stickyHeaderLayoutManager
            adapter = weightSectionedAdapter
        }

        //Go to current week
        weightSectionedAdapter.goToWeek()
        viewModel.sortedWeights.observe(viewLifecycleOwner, Observer {
            weightSectionedAdapter.updateData(it)
        })

        viewModel.userRole.observe(viewLifecycleOwner, Observer {
            weightSectionedAdapter.applyPermissions(it)
        })

        viewModel.goTo.observe(viewLifecycleOwner, Observer {
            weightSectionedAdapter.goToWeek()
        })
    }

    interface OnWeightItemInteraction {
        fun onAdd(date: String)
        fun onEdit(item: Weight)
        fun onDelete(item: Weight)
    }
}
