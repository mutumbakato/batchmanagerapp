package com.mutumbakato.batchmanager.fragments.vaccination

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.vaccination.VaccinationScheduleDetailsActivity
import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationCheck
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.VaccinationContract
import com.mutumbakato.batchmanager.ui.adapters.VaccinationRecyclerViewAdapter
import kotlinx.android.synthetic.main.fragment_vaccination_list.*
import kotlinx.android.synthetic.main.message_view.*
import java.util.*

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class VaccinationScheduleDetailsFragment : BaseFragment(), VaccinationContract.View,
        VaccinationContract.View.VaccinationItemInteraction {

    private var mAdapter: VaccinationRecyclerViewAdapter? = null
    private var mPresenter: VaccinationContract.Presenter? = null
    private var mListener: VaccinationContract.View.ListInteractionListener? = null
    private var mScheduleId: String? = null

    override val isActive: Boolean
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mScheduleId = activity!!.intent.getStringExtra(VaccinationScheduleDetailsActivity.EXTRA_SCHEDULE_ID)
        mAdapter = VaccinationRecyclerViewAdapter(ArrayList(), mListener)
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_schedule_vaccination_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        schedule_description_text.setOnClickListener { }
        initRecyclerView()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is VaccinationContract.View.ListInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun showVaccinations(vaccinations: Map<Int, List<Vaccination>>) {}

    override fun showLoadingVaccinationsIndicator(isLoading: Boolean) {
        if (progress != null) {
            progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
        }
    }

    override fun showVaccinations(vaccinations: List<Vaccination>) {
        mAdapter!!.updateData(vaccinations)
        message_view.visibility = View.GONE
        mListener!!.hideFab("")
    }

    override fun showForm(id: String) {
        mListener!!.showForm(id)
    }

    override fun showVaccinationDetailsUi(batchId: String) {}

    override fun showLoadingVaccinationError(message: String) {
        message_view.visibility = View.VISIBLE
        message_title.setText(R.string.vaccination_error_text)
        message_description.text = message
    }

    override fun showNoVaccinations() {
        message_view.visibility = View.VISIBLE
        message_title.setText(R.string.no_vaccination_found)
        message_image.setImageResource(R.drawable.no_vaccination)
        message_description.setText(R.string.tap_to_add_vaccination_item)
    }

    override fun showSuccessfullySavedMessage() {}

    override fun showUndoDelete(vaccination: Vaccination, mills: Int) {
        Snackbar.make(vaccination_list, "You have deleted " + vaccination.vaccine + ".", mills).apply {
            setAction("Undo") {
                mPresenter!!.undoDelete(vaccination.id)
                dismiss()
            }
            show()
        }
    }

    override fun showDeleteUndoItem(vaccination: Vaccination) {}

    override fun showTotalVaccinations(totalVaccinations: String) {
        mListener!!.hideFab(totalVaccinations)
    }

    override fun showScheduleDetails(description: String) {
        schedule_description_text.text = description
    }

    override fun showChecks(checks: List<VaccinationCheck>) {
    }

    override fun setPresenter(presenter: VaccinationContract.Presenter) {
        mPresenter = presenter
    }

    override fun onDelete(vaccination: Vaccination) {
        mPresenter!!.deleteVaccination(vaccination)
    }

    override fun onEdit(vaccination: Vaccination) {
        mPresenter!!.editVaccination(vaccination)
    }

    private fun initRecyclerView() {
        vaccination_list.adapter = mAdapter
        vaccination_list.layoutManager = LinearLayoutManager(context)
    }

    companion object {
        fun newInstance(): VaccinationScheduleDetailsFragment {
            return VaccinationScheduleDetailsFragment()
        }
    }
}
