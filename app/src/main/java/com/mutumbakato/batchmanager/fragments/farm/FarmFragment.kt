package com.mutumbakato.batchmanager.fragments.farm

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.mutumbakato.batchmanager.Features
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.farm.FarmDetailsActivity
import com.mutumbakato.batchmanager.data.FarmDataSource
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.ui.adapters.FarmFeaturesAdapter
import kotlinx.android.synthetic.main.fragment_farm_features_list.*

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [FarmFragment.OnListFragmentInteractionListener] interface.
 */
class FarmFragment : Fragment() {

    private var columnCount = 1

    private var listener: OnListFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_farm_features_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(farm_features_list) {
            layoutManager = LinearLayoutManager(context)
            adapter = FarmFeaturesAdapter(Features.FARM_ITEMS, listener)
        }
        farm_card.setOnClickListener {
            FarmDetailsActivity.start(context!!, PreferenceUtils.farmId)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onResume() {
        super.onResume()
        getFarm()
    }

    private fun getFarm() {
        val farmId = PreferenceUtils.farmId
        RepositoryUtils.getFarmRepo(context).getFarm(farmId, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                farm_name_title.text = farm.name
            }

            override fun onFail(message: String) {
                farm_name_title.text = message
            }
        })
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Features.FeatureItem?)
    }

    companion object {
        const val ARG_COLUMN_COUNT = "column-count"
        @JvmStatic
        fun newInstance(columnCount: Int) =
                FarmFragment().apply {
                    arguments = Bundle().apply {
                        putInt(ARG_COLUMN_COUNT, columnCount)
                    }
                }
    }
}
