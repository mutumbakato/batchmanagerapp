package com.mutumbakato.batchmanager.fragments.water

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.WaterActivity
import com.mutumbakato.batchmanager.data.models.Water
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.viewmodels.WaterViewModel
import kotlinx.android.synthetic.main.fragment_water_form.*
import kotlinx.android.synthetic.main.fragment_water_form.view.*
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

class WaterFormFragment : BaseFragment() {

    private lateinit var viewModel: WaterViewModel
    private var batchCount: Int = 0
    private var panel: ExpandableLayout? = null
    private var mWater: Water? = null
    private var mBatchId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!).get(WaterViewModel::class.java)
        batchCount = activity!!.intent.getIntExtra(WaterActivity.BATCH_COUNT_EXTRA, 0)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_water_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        water_label_count.markRequired()
        water_label_date.markRequired()
        panel = view.water_form_panel

        viewModel.mDate.observe(viewLifecycleOwner, Observer {
            water_input_date.setText(it)
            panel?.isExpanded = it != null
        })

        viewModel.waterId.observe(viewLifecycleOwner, Observer {
            panel?.isExpanded = it != null
        })

        viewModel.batchId.observe(viewLifecycleOwner, Observer {
            mBatchId = it
        })

        viewModel.water.observe(viewLifecycleOwner, Observer {
            if (it != null) {
                mWater = it
                batchCount = it.batchCount
                water_input_date.setText(it.date)
                water_input_quantity.setText("${it.quantity}")
            }
        })

        water_save.setOnClickListener {
            validateAndSave()
        }

        water_discard.setOnClickListener {
            viewModel.setDate(null)
            viewModel.setWaterId(null)
        }
    }

    private fun validateAndSave() {

        val date = water_input_date.text.toString().trim()
        val quantity = water_input_quantity.text.toString().trim()

        if (date.isEmpty()) {
            water_label_date.error = "Date is required"
            return
        }

        if (quantity.isEmpty() || quantity.toFloat() <= 0f) {
            water_label_count.error = "Add valid water"
            return
        }

        val water = Water(mWater?.id
                ?: UUID.randomUUID().toString(), mBatchId, date, quantity.toFloat(), batchCount)

        viewModel.save(water)
        clearFields()
    }

    private fun clearFields() {
        KeyBoard.hideSoftKeyboard(activity!!)
        water_input_date.text = null
        water_input_quantity.text = null
        viewModel.setDate(null)
        viewModel.setWaterId(null)
    }

}
