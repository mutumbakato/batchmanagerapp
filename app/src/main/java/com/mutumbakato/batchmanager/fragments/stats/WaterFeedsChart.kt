package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import com.github.mikephil.charting.components.YAxis
import com.mutumbakato.batchmanager.R

class WaterFeedsChart : BaseChartFragment() {

    private var feeds: Map<Int, Float> = mapOf()
    private var water: Map<Int, Float> = mapOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chartTitle = getString(R.string.water_feeds_intake)
        enableRightAxis = true
        chatType = "feeds-water"
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.dailyFeeds.observe(viewLifecycleOwner, Observer {
            it?.let { f ->
                if (!isWeek) {
                    feeds = f
                    updateChartData(getWaterEntryObject(water), getFeedsEntryObject(feeds))
                }
            }
        })
        viewModel.dailyWater.observe(viewLifecycleOwner, Observer {
            it?.let { w ->
                if (!isWeek) {
                    water = w
                    updateChartData(getWaterEntryObject(water), getFeedsEntryObject(feeds))
                }
            }
        })
        viewModel.weeklyFeeds.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    feeds = it
                    updateChartData(getWaterEntryObject(water), getFeedsEntryObject(feeds))
                }
            }
        })
        viewModel.weeklyWater.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (isWeek) {
                    water = it
                    updateChartData(getWaterEntryObject(water), getFeedsEntryObject(feeds))
                }
            }
        })
    }

    private fun getWaterEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.water_intake),
                getString(R.string.water_ml_bird),
                ContextCompat.getColor(context!!, R.color.colorBlueDark))
    }

    private fun getFeedsEntryObject(data: Map<Int, Float>): EntryObject {
        return EntryObject(data,
                getString(R.string.feeds_intake),
                getString(R.string.feeds_gm_bird),
                ContextCompat.getColor(context!!, R.color.colorYellowDark), YAxis.AxisDependency.RIGHT)
    }

    companion object {
        @JvmStatic
        fun newInstance() = WaterFeedsChart()
    }
}
