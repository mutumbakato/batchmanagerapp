package com.mutumbakato.batchmanager.fragments.deaths

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.DeathFormContract
import com.mutumbakato.batchmanager.ui.components.ValidatorEditText
import com.mutumbakato.batchmanager.utils.Validator
import kotlinx.android.synthetic.main.fragment_death_form.*
import kotlinx.android.synthetic.main.fragment_death_form.view.*
import net.cachapa.expandablelayout.ExpandableLayout

class DeathFormFragment : BaseFragment(), DeathFormContract.View {

    private var mPresenter: DeathFormContract.Presenter? = null
    private var mListener: DeathFormContract.View.FormInteractionListener? = null
    private var mDate: String? = null
    private var mCount: String? = null
    private var mReason: String? = null
    private var panel: ExpandableLayout? = null

    override val isActive: Boolean
        get() = isAdded

    private val isFormValid: Boolean
        get() {
            mDate = death_input_date.text!!.toString().trim { it <= ' ' }
            mCount = death_input_count.text!!.toString().trim { it <= ' ' }
            mReason = death_input_reason.text!!.toString().trim { it <= ' ' }
            var valid = true
            if (!death_input_date.isValid) {
                valid = false
            }
            if (!Validator.isValidNumber(mCount)) {
                death_label_count.error = "Please add how many died."
                valid = false
            }
            if (!Validator.isValidWord(mReason)) {
                mReason = "Unknown"
            }
            return valid
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_death_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        death_label_count.markRequired()
        death_label_date.markRequired()
        prepareValidation()
        panel = view.death_form_panel
        death_save.setOnClickListener {
            save()
        }
        death_discard.setOnClickListener {
            discard()
        }
    }

    private fun prepareValidation() {
        death_input_date.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                death_label_date.error = error
            }

            override fun onRemoveError() {
                death_label_date.error = null
            }
        })
        death_input_count.setOnFocusChangeListener { _, b ->
            if (b) {
                death_label_count.error = null
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DeathFormContract.View.FormInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ExpensesFormInteractionListener")
        }
    }

    private fun save() {
        if (isFormValid) {
            mPresenter!!.save(mDate!!, Integer.parseInt(mCount!!), mReason!!)
        }
    }

    private fun discard() {
        mPresenter!!.stopEditing()
    }

    override fun setDate(date: String) {
        death_input_date.setText(date)
    }

    override fun setCount(count: String) {
        death_input_count.setText(count)
    }

    override fun setComment(comment: String) {
        death_input_reason.setText(comment)
    }

    override fun setLoadingError() {

    }

    override fun showDateError(error: String) {

    }

    override fun showCountError(error: String) {

    }

    override fun showCommentError(error: String) {

    }

    override fun toggleExpansion(expand: Boolean) {
        if (expand) {
            panel!!.expand()
            mListener!!.onStartEditing()
        } else {
            panel!!.collapse()
            mListener!!.onDoneEditing()
        }
    }

    override fun clearInputs() {
        death_input_count.text = null
        death_input_reason.text = null
        death_input_date.text = null
        death_input_count.error = null
    }

    override fun setPresenter(presenter: DeathFormContract.Presenter) {
        mPresenter = presenter
    }
}
