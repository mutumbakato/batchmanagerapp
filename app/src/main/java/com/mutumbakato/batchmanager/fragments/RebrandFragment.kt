package com.mutumbakato.batchmanager.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.method.LinkMovementMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import kotlinx.android.synthetic.main.fragment_rebrand.*

/**
 * A simple [Fragment] subclass.
 * Use the [RebrandFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class RebrandFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NO_TITLE, R.style.FullScreenRebrandStyle)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rebrand, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_poultry_link.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW).apply {
                data = Uri.parse(
                        "https://play.google.com/store/apps/details?id=com.poultrylink.app")
                setPackage("com.android.vending")
            }
            startActivity(intent)
            dismiss()
        }
        button_close_rebrand.setOnClickListener {
            dismiss()
        }
        button_poultry_link_later.setOnClickListener {
            PreferenceUtils.isRebrand = true
            dismiss()
        }
        rebrand_link_text.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun onStart() {
        super.onStart()
        val dialog = dialog
        if (dialog != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            dialog.window!!.setLayout(width, height)
        }
    }

    companion object {

        @JvmStatic
        fun newInstance() =
                RebrandFragment()
    }
}