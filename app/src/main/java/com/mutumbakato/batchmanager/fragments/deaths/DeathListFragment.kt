package com.mutumbakato.batchmanager.fragments.deaths

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.DeathActivity
import com.mutumbakato.batchmanager.data.models.Death
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.DeathListContract
import com.mutumbakato.batchmanager.ui.adapters.DeathRecyclerViewAdapter
import com.mutumbakato.batchmanager.ui.adapters.DeathTimelineAdapter
import kotlinx.android.synthetic.main.fragment_death_list.*
import org.zakariya.stickyheaders.StickyHeaderLayoutManager
import java.util.*

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */
class DeathListFragment : BaseFragment(), DeathListContract.View, DeathListContract.View.OnDeathItemInteraction {

    private var adapter: DeathRecyclerViewAdapter? = null
    private var timelineAdapter: DeathTimelineAdapter? = null
    private var mPresenter: DeathListContract.Presenter? = null
    private var mListener: DeathListContract.View.ListInteractionListener? = null

    private lateinit var mDateOfBirth: String
    private var mCloseDate: String? = null

    override val isActive: Boolean
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mDateOfBirth = activity!!.intent.getStringExtra(DeathActivity.DATE_OF_BIRTH)
        mCloseDate = activity!!.intent.getStringExtra(DeathActivity.CLOSE_DATE)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is DeathListContract.View.ListInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement DeathListInteractionListener")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_death_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
    }

    private fun initRecyclerView() {
        adapter = DeathRecyclerViewAdapter(ArrayList(0), this, death_list)
        timelineAdapter = DeathTimelineAdapter(mDateOfBirth, this, death_list!!, activity!!.supportFragmentManager, mCloseDate)
        death_list!!.layoutManager = StickyHeaderLayoutManager()
        death_list!!.adapter = timelineAdapter
        timelineAdapter!!.goToWeek()
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    override fun showProgress(active: Boolean) {
        mListener!!.showProgress(active)
    }

    override fun showDeaths(deaths: List<Death>) {
        adapter!!.updateData(deaths)
        mListener!!.showEmpty(false)
        mListener!!.showProgress(false)
    }

    override fun showSectionedDeaths(deaths: Map<Int, List<Death>>) {
        timelineAdapter!!.updateData(deaths)
        mListener!!.showEmpty(false)
        mListener!!.showProgress(false)
    }

    override fun showLoadingDeathError(message: String) {
        mListener!!.showProgress(false)
    }

    override fun showNoDeath() {
        //Do not show empty because the timeline is displayed
        mListener!!.showEmpty(false)
        mListener!!.showProgress(false)
    }

    override fun showSuccessfullySavedMessage() {

    }

    override fun showWeek(week: Int) {
        timelineAdapter!!.goToWeek(week)
    }

    override fun showConfirmDelete(death: Death, mills: Int) {
        val snack = Snackbar.make(death_list!!, "Do you want to remove this record?", mills)
        snack.setAction("Delete") {
            mPresenter!!.delete(death)
            snack.dismiss()
        }
        snack.show()
    }

    override fun showDeleteUndoItem(death: Death) {

    }

    override fun showMortalityRate(count: String) {
        mListener!!.showTotal(count)
    }

    override fun showEdit(id: String, date: String) {
        mListener!!.showForm(id, date)
    }

    override fun applyPermissions(role: String) {
        adapter!!.applyPermissions(role)
        timelineAdapter!!.applyPermissions(role)
        mListener!!.applyPermissions(role)
    }

    override fun toggleAdapter() {
        death_list.adapter = adapter
    }

    override fun setPresenter(presenter: DeathListContract.Presenter) {
        mPresenter = presenter
    }

    override fun onAdd(date: String) {
        mListener!!.add(date)
    }

    override fun onEdit(death: Death) {
        mPresenter!!.edit(death)
    }

    override fun onDelete(death: Death) {
        mPresenter!!.deleteDeath(death)
    }

    companion object {

        fun newInstance(): DeathListFragment {
            return DeathListFragment()
        }
    }


}
