package com.mutumbakato.batchmanager.fragments.farm

import android.app.ProgressDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.farm.FarmDetailsActivity.Companion.start
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.FarmFormContract
import kotlinx.android.synthetic.main.fragment_farm_form.*

/**
 * A placeholder fragment containing a simple view.
 */
class FarmFormFragment : BaseFragment(), FarmFormContract.View {

    private var mName: String? = null
    private var mLocation: String? = null
    private var mContacts: String? = null
    private var mUnits: String? = null
    private var mCurrency: String? = null
    private var vPresenter: FarmFormContract.Presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Please wait...")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_farm_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        farm_name.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {
                farm_name_label.error = null
            }

            override fun afterTextChanged(editable: Editable) {}
        })
        farm_save_button.setOnClickListener {
            saveFarm()
        }
    }

    fun saveFarm() {
        if (validate()) {
            vPresenter!!.saveFarm(mName, mLocation, mContacts, mUnits, mCurrency)
        }
    }

    override fun onResume() {
        super.onResume()
        vPresenter!!.start()
    }

    override fun isActive(): Boolean {
        return isAdded
    }

    override fun setPresenter(presenter: FarmFormContract.Presenter) {
        vPresenter = presenter
    }

    override fun showName(name: String) {
        farm_name.setText(name)
    }

    override fun showLocation(location: String) {
        farm_location.setText(location)
    }

    override fun showContacts(contact: String) {
        farm_contact.setText(contact)
    }

    override fun showUnits(units: String) {
        farm_units.setText(units)
    }

    override fun showCurrency(currency: String) {
        farm_currency.setText(currency)
    }

    override fun showDetails(farmId: String) {
        start(context!!, farmId)
        exit()
    }

    /**
     * @param show Boolean show progress dialog
     */
    override fun showProgress(show: Boolean) { //        if (show) {
//            if (!progressDialog.isShowing())
//                progressDialog.show();
//        } else if (progressDialog.isShowing()) {
//            progressDialog.dismiss();
//        }
    }

    /**
     * @param message Error message
     */
    override fun showError(message: String) {
        Snackbar.make(farm_name, message, Snackbar.LENGTH_LONG).show()
    }

    override fun clearInputs() {
        farm_currency.text = null
        farm_units.text = null
        farm_contact.text = null
        farm_location.text = null
        farm_name.text = null
    }

    override fun exit() {
        Toast.makeText(context, "Saved", Toast.LENGTH_LONG).show()
        activity!!.finish()
    }

    private fun validate(): Boolean {
        mName = farm_name.text.toString().trim { it <= ' ' }
        mLocation = farm_location.text.toString().trim { it <= ' ' }
        mContacts = farm_contact.text.toString().trim { it <= ' ' }
        mUnits = farm_units.text.toString().trim { it <= ' ' }
        mCurrency = farm_currency.text.toString().trim { it <= ' ' }
        if (mName!!.isEmpty()) {
            farm_name_label.error = "Farm name can not be empty"
            return false
        }
        if (mCurrency!!.isEmpty()) {
            mCurrency = "UGX"
        }
        if (mUnits!!.isEmpty()) {
            mUnits = "Kg"
        }
        return true
    }
}