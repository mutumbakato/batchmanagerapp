package com.mutumbakato.batchmanager.fragments.vaccination

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.vaccination.VaccinationActivity
import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationCheck
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.presenters.VaccinationSchedulePresenter
import com.mutumbakato.batchmanager.ui.VaccinationContract
import com.mutumbakato.batchmanager.ui.adapters.VaccinationSectionedAdapter
import kotlinx.android.synthetic.main.fragment_vaccination_list.*
import kotlinx.android.synthetic.main.message_view.*
import org.zakariya.stickyheaders.StickyHeaderLayoutManager
import java.util.*

/**
 * Mandatory empty constructor for the fragment manager to instantiate the
 * fragment (e.g. upon screen orientation changes).
 */

class VaccinationFragment : BaseFragment(), VaccinationContract.View,
        VaccinationContract.View.VaccinationItemInteraction {

    private var adapter: VaccinationSectionedAdapter? = null
    private var mPresenter: VaccinationContract.Presenter? = null
    private var mListener: VaccinationContract.View.ListInteractionListener? = null
    private var dateOfBirth: String? = null
    private var mBatchName: String? = null
    private lateinit var mBatchId: String
    private lateinit var scheduleDialog: ScheduleDialog

    override val isActive: Boolean
        get() = isAdded

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dateOfBirth = activity!!.intent.getStringExtra(VaccinationActivity.EXTRA_DOB)
        mBatchName = activity!!.intent.getStringExtra(VaccinationActivity.BATCH_NAME_EXTRA)
        mBatchId = activity!!.intent.getStringExtra(VaccinationActivity.BATCH_ID_EXTRA)
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_vaccination_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initScheduleList()
        schedule_description_text.setOnClickListener {
            scheduleDialog.show(childFragmentManager, "schedule_dialog")
        }
        change_schedule_button.setOnClickListener {
            //VaccinationScheduleActivity.start(context!!, dateOfBirth!!, mBatchName!!, "ScheduleId")
            scheduleDialog.show(childFragmentManager, "schedule_dialog")
        }
    }

    private fun initScheduleList() {
        scheduleDialog = ScheduleDialog.newInstance()
        VaccinationSchedulePresenter(mBatchId, RepositoryUtils.getScheduleRepo(context), scheduleDialog)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is VaccinationContract.View.ListInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement ListInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun showVaccinations(vaccinations: Map<Int, List<Vaccination>>) {
        adapter!!.updateData(vaccinations)
    }

    override fun showLoadingVaccinationsIndicator(isLoading: Boolean) {
        if (progress != null)
            progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun showVaccinations(vaccinations: List<Vaccination>) {
        message_view.visibility = View.GONE
        mListener!!.hideFab("")
    }

    override fun showForm(id: String) {
        mListener!!.showForm(id)
    }

    override fun showVaccinationDetailsUi(batchId: String) {}

    override fun showLoadingVaccinationError(message: String) {
        message_view.visibility = View.VISIBLE
        message_title.setText(R.string.vaccination_error_text)
        message_description.text = message
    }

    override fun showNoVaccinations() {
        message_view.visibility = View.VISIBLE
        message_title.setText(R.string.no_vaccination_found)
        message_image.setImageResource(R.drawable.no_vaccination)
        message_description.setText(R.string.tap_to_add_vaccination)
    }

    override fun showSuccessfullySavedMessage() {}

    override fun showUndoDelete(vaccination: Vaccination, mills: Int) {
        Snackbar.make(vaccination_list, "You have deleted " + vaccination.vaccine + ".", mills).apply {
            setAction("Undo") {
                mPresenter!!.undoDelete(vaccination.id)
                dismiss()
            }
            show()
        }
    }

    override fun showDeleteUndoItem(vaccination: Vaccination) {}

    override fun showTotalVaccinations(totalVaccinations: String) {
        mListener!!.hideFab(totalVaccinations)
    }

    override fun showScheduleDetails(description: String) {
        schedule_description_text.text = description
    }

    override fun showChecks(checks: List<VaccinationCheck>) {
        adapter!!.addChecks(checks)
    }

    override fun setPresenter(presenter: VaccinationContract.Presenter) {
        mPresenter = presenter
    }

    override fun onDelete(vaccination: Vaccination) {
        mPresenter!!.deleteVaccination(vaccination)
    }

    override fun onEdit(vaccination: Vaccination) {
        mPresenter!!.editVaccination(vaccination)
    }

    private fun initRecyclerView() {
        adapter = VaccinationSectionedAdapter(LinkedHashMap(),
                dateOfBirth, ArrayList(), { isChecked, vaccination ->
            if (isChecked) {
                mPresenter!!.addToComplete(vaccination)
            } else {
                mPresenter!!.removeFromComplete(vaccination)
            }
        }, vaccination_list)
        val stickyHeaderLayoutManager = StickyHeaderLayoutManager()
        vaccination_list.layoutManager = stickyHeaderLayoutManager
        vaccination_list.adapter = adapter
    }

    companion object {
        fun newInstance(): VaccinationFragment {
            return VaccinationFragment()
        }
    }
}
