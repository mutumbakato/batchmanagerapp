package com.mutumbakato.batchmanager.fragments.expenses

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.components.ValidatorEditText
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.Validator
import com.mutumbakato.batchmanager.viewmodels.ExpensesViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_expenses_form.*
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

class ExpensesFormFragment : BaseFragment() {

    private lateinit var viewModel: ExpensesViewModel
    private var mPanel: ExpandableLayout? = null
    private var mExpense: Expense? = null
    private var farmId: String? = null
    private var expenseId: String? = null
    private var mBatchId: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!, ViewModelFactory
                .ExpensesViewModelFactory(RepositoryUtils.getExpensesLiveRepo(context),
                        RepositoryUtils.getBatchRepo(context))).get(ExpensesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_expenses_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPanel = view.findViewById(R.id.expenses_form_panel)
        prepareValidation()
        expense_discard.setOnClickListener {
            discardExpense()
        }
        expense_save.setOnClickListener {
            saveExpense()
        }
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.expenseId.observe(activity!!, Observer {
            expenseId = it
        })
        viewModel.farmId.observe(activity!!, Observer {
            farmId = it
        })
        viewModel.isEditing.observe(activity!!, Observer {
            mPanel!!.isExpanded = it
            if (!it) {
                clearInputs()
            } else {
                expense_input_batch.setBatchId(mBatchId)
            }
        })
        viewModel.expense.observe(activity!!, Observer {
            if (it != null) {
                setExpense(it)
            }
        })
        viewModel.filter.observe(this, Observer {
            it?.let {
                val type = it.type
                val id = it.id
                if (type == "Batch") {
                    mBatchId = id
                }
            }
        })
    }

    private fun prepareValidation() {
        expense_input_batch.setFragmentManager(activity!!.supportFragmentManager)
        expense_input_category!!.setFragmentManager(activity!!.supportFragmentManager)
        expense_input_amount.setOnEditorActionListener { _, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                saveExpense()
                true
            } else
                false
        }
        expense_input_date!!.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                expense_label_date.error = error
            }

            override fun onRemoveError() {
                expense_label_date.error = null
            }
        })

        expense_input_category!!.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                expense_label_category.error = error
            }

            override fun onRemoveError() {
                expense_label_category.error = null
            }
        })

        expense_input_amount.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                expense_label_amount.error = error
            }

            override fun onRemoveError() {
                expense_label_amount.error = null
            }
        })

        expense_input_description!!.setOnFocusChangeListener { _, b ->
            if (b) {
                expense_label_description.error = null
            }
        }
    }

    private fun isFormValid(): Boolean {

        var valid = true
        val mCategory = expense_input_category!!.text!!.toString().trim { it <= ' ' }
        val mDate = expense_input_date!!.text!!.toString().trim { it <= ' ' }
        val mAmount = Currency.cleanNumbers(expense_input_amount!!.text!!.toString().trim { it <= ' ' })
        val mDescription = expense_input_description!!.text!!.toString().trim { it <= ' ' }
        mBatchId = expense_input_batch.getBatchId()

        if (farmId == null) {
            farmId = PreferenceUtils.farmId
        }

        if (!Validator.isValidWord(mDate)) {
            expense_label_date.error = "Please select a date."
            valid = false
        }

        if (!Validator.isValidWord(mCategory)) {
            expense_label_category.error = "Please select a category."
            valid = false
        }

        if (!Validator.isValidWord(mDescription)) {
            expense_label_description.error = "Please specify what you spent on."
            valid = false
        }

        if (!expense_input_amount!!.isValid) {
            valid = false
        }

        mExpense = Expense(expenseId
                ?: UUID.randomUUID().toString(), mBatchId, farmId, mDate, mCategory, mDescription, mAmount.toFloat(), 0,
                mExpense?.status ?: "normal", 0)

        return valid
    }

    private fun saveExpense() {
        if (isFormValid()) {
            if (expenseId == null)
                viewModel.createExpense(mExpense!!)
            else {
                viewModel.updateExpense(mExpense!!)
            }
        }
    }

    private fun discardExpense() {
        viewModel.stopEditing()
    }

    private fun setExpense(expense: Expense) {
        mExpense = expense
        expense_input_date!!.setText(expense.date)
        expense_input_category!!.setText(expense.category)
        expense_input_description!!.setText(expense.description)
        expense_input_amount.setText(Currency.format(expense.amount))
        expense_input_batch.setBatchId(expense.batchId)
    }

    fun clearInputs() {
        expense_input_description.text = null
        expense_input_amount.text = null
        expense_input_date.text = null
        expense_input_category.text = null
        expense_label_date.error = null
        expense_label_category.error = null
        expense_label_description.error = null
        expense_label_amount.error = null
        expense_input_batch.text = null
        expense_input_batch.setBatchId(null)
        viewModel.setExpenseId(null)
    }
}
