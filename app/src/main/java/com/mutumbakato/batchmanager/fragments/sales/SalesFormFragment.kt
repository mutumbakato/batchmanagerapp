package com.mutumbakato.batchmanager.fragments.sales

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.components.ValidatorEditText
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.Validator
import com.mutumbakato.batchmanager.viewmodels.SalesViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_sales_form.*
import net.cachapa.expandablelayout.ExpandableLayout
import java.util.*

class SalesFormFragment : BaseFragment() {

    private var mPanel: ExpandableLayout? = null
    private var mFarmId: String? = null
    private var mSaleId: String? = null
    private var mSale: Sale? = null
    private var mBatchId: String = ""
    private lateinit var viewModel: SalesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!,
                ViewModelFactory.SalesViewModelFactory(RepositoryUtils.getSalesLiveRepo(context!!))).get(SalesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sales_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPanel = view.findViewById(R.id.sales_form_panel)
        prepareValidation()
        sales_save.setOnClickListener {
            save()
        }
        sales_discard.setOnClickListener {
            cancel()
        }
        viewModel.farmId.observe(viewLifecycleOwner, Observer {
            mFarmId = it
        })
        viewModel.saleId.observe(viewLifecycleOwner, Observer {
            mSaleId = it
        })
        viewModel.sale.observe(viewLifecycleOwner, Observer {
            mSale = it
            if (it != null) {
                setSale(it)
            }
        })

        viewModel.isEditing.observe(viewLifecycleOwner, Observer {
            mPanel!!.isExpanded = it
            if (!it) {
                clearInputs()
            } else {
                sales_input_batch.setBatchId(mBatchId)
            }
        })

        viewModel.filter.observe(viewLifecycleOwner, Observer {
            viewModel.filter.observe(viewLifecycleOwner, Observer {
                it?.let {
                    val type = it.type
                    val id = it.id
                    if (type == "Batch") {
                        mBatchId = id
                    }
                }
            })
        })
    }

    private fun prepareValidation() {
        sales_input_rate.setOnEditorActionListener { _, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                save()
                true
            } else
                false
        }
        sales_input_item.setFragmentManager(childFragmentManager)
        sales_input_batch.setFragmentManager(childFragmentManager)
        sales_input_item.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                sales_label_item.error = error
            }

            override fun onRemoveError() {
                sales_label_item.error = null
            }
        })
        sales_input_date!!.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                sales_label_date.error = error
            }

            override fun onRemoveError() {
                sales_label_date.error = null
            }
        })
        sales_input_rate.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                if (sales_label_rate != null)
                    sales_label_rate.error = error
            }

            override fun onRemoveError() {
                if (sales_label_rate != null)
                    sales_label_rate.error = null
            }
        })
        sales_input_quantity.setOnFocusChangeListener { _, b ->
            if (b) {
                sales_label_quantity.error = null
            }
        }
    }

    internal fun save() {
        if (isFormValid()) {
            if (mSaleId == null)
                viewModel.createSale(mSale!!)
            else
                viewModel.updateSale(mSale!!)
        }
    }

    internal fun cancel() {
        viewModel.stopEditing()
    }

    private fun setSale(sale: Sale) {
        sales_input_date!!.setText(sale.date)
        sales_input_item.setText(sale.item)
        sales_input_rate.setText(sale.rate.toString())
        sales_input_quantity.setText(sale.quantity.toString())
        sales_input_batch.setBatchId(sale.batchId)
    }

    private fun clearInputs() {

        sales_input_rate.text = null
        sales_input_quantity.text = null
        sales_input_item.text = null
        sales_input_item.isFocusable = false
        sales_input_date!!.text = null
        sales_input_batch.text = null
        sales_input_batch.setBatchId(null)

        sales_label_date.error = null
        sales_label_item.error = null
        sales_label_quantity.error = null
        sales_label_rate.error = null
        viewModel.setSaleId(null)
    }

    private fun isFormValid(): Boolean {

        val mDate = sales_input_date.text!!.toString().trim { it <= ' ' }
        val mItem = sales_input_item.text!!.toString().trim { it <= ' ' }
        var mQuantity = sales_input_quantity.text!!.toString().trim { it <= ' ' }
        val mRate = Currency.cleanNumbers(sales_input_rate!!.text!!.toString().trim { it <= ' ' })
        val mBatchId = sales_input_batch.getBatchId()
        val mStoreId = ""
        var valid = true

        mQuantity = if (mQuantity.isEmpty()) "0" else mQuantity

        if (!sales_input_date.isValid) {
            valid = false
        }

        if (!sales_input_item.isValid) {
            valid = false
        }

        if (!Validator.isValidNumber(mQuantity)) {
            valid = false
            sales_label_quantity.error = "Quantity must be at least 1."
        }

        if (!sales_input_rate!!.isValid) {
            valid = false
        }

        mSale = Sale(mSaleId
                ?: UUID.randomUUID().toString(), mBatchId, mFarmId, mStoreId, mDate, mItem, mRate.toFloat(), mQuantity.toInt(), "", 0, "", 0)
        return valid
    }
}
