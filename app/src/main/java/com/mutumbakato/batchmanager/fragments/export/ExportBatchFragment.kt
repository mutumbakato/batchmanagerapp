package com.mutumbakato.batchmanager.fragments.export

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.exports.ExportConfig
import com.mutumbakato.batchmanager.viewmodels.ExportViewModel
import kotlinx.android.synthetic.main.fragment_select_batch.*

private const val ARG_FARM_ID = "farm_id"

class SelectBatchFragment : Fragment() {

    private lateinit var viewModel: ExportViewModel
    private var exportConfig: ExportConfig? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!).get(ExportViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_select_batch, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        select_batch_input.text = null
        select_batch_input.setFragmentManager(activity!!.supportFragmentManager)
        select_batch_input.setOnBatchSelected {
            viewModel.setExportConfig(exportConfig?.apply {
                batchName = it?.name ?: "Batch"
                batchId = it?.id ?: "0"
            })
        }
        viewModel.exportConfig.observe(viewLifecycleOwner, Observer {
            it?.let {
                exportConfig = it
                select_batch_input.setBatchId(it.batchId)
            }
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @param farmId Parameter 1.
         * @return A new instance of fragment SelectBatchFragment.
         */
        @JvmStatic
        fun newInstance(farmId: String) =
                SelectBatchFragment().apply {
                    arguments = Bundle().apply {
                        putString(ARG_FARM_ID, farmId)
                    }
                }
    }
}
