package com.mutumbakato.batchmanager.fragments.eggs

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.EggsFormContract
import com.mutumbakato.batchmanager.ui.components.ValidatorEditText
import com.mutumbakato.batchmanager.utils.Validator
import kotlinx.android.synthetic.main.fragment_eggs_form.*
import net.cachapa.expandablelayout.ExpandableLayout

class EggsFormFragment : BaseFragment(), EggsFormContract.View {

    private var mPanel: ExpandableLayout? = null
    private var mPresenter: EggsFormContract.Presenter? = null
    private var mListener: EggsFormContract.View.FormInteractionListener? = null
    private var mDate: String? = null
    private var mTreys: String? = null
    private var mEggs: String? = null
    private var mDamage: String? = null

    override val isActive: Boolean
        get() = isAdded

    private val isFormValid: Boolean
        get() {
            mDate = eggs_input_date.text!!.toString().trim { it <= ' ' }
            mTreys = eggs_input_treys.text!!.toString().trim { it <= ' ' }
            mEggs = eggs_input_eggs.text!!.toString().trim { it <= ' ' }
            mDamage = eggs_input_damage.text!!.toString().trim { it <= ' ' }
            var valid = true

            if (!eggs_input_date.isValid) {
                valid = false
                eggs_label_date.error = "Please add a date"
            }

            if (!Validator.isValidNumber(mTreys)) {
                valid = false
                eggs_label_treys.error = "Invalid number of treys."
            }
            if (!Validator.isValidNumber(mEggs)) {
                mEggs = "0"
            }
            if (!Validator.isValidNumber(mDamage)) {
                mDamage = "0"
            }
            return valid
        }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_eggs_form, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareValidation()
        eggs_label_treys.hint = ("Number of full trays (" + PreferenceUtils.eggsInATray
                + " eggs per tray, change in farm settings)")
        eggs_button_save.setOnClickListener {
            save()
        }
        eggs_button_done.setOnClickListener {
            done()
        }
        mPanel = view.findViewById(R.id.eggs_form_panel)
    }

    private fun prepareValidation() {
        eggs_input_date.setOnErrorListener(object : ValidatorEditText.ErrorListener {
            override fun onShowError(error: String) {
                eggs_label_date.error = error
            }

            override fun onRemoveError() {
                eggs_label_date.error = null
            }
        })

        eggs_input_treys.setOnFocusChangeListener { _, b ->
            if (b) {
                eggs_label_treys.error = null
            }
        }

        eggs_input_damage.setOnEditorActionListener { _, i, _ ->
            if (i == EditorInfo.IME_ACTION_DONE) {
                save()
                true
            }
            false
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is EggsFormContract.View.FormInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement EggsListInteractionListener")
        }
    }

    private fun save() {
        if (isFormValid) {
            mPresenter!!.save(mDate!!,
                    Integer.parseInt(mTreys!!),
                    Integer.parseInt(mEggs!!),
                    Integer.parseInt(mDamage!!))
        }
    }

    private fun done() {
        mPresenter!!.stopEditing()
        mListener!!.onDoneEditing()
    }

    override fun showDate(date: String) {
        eggs_input_date.setText(date)
    }

    override fun showTreys(treys: String) {
        eggs_input_treys.setText(treys)
    }

    override fun showEggs(eggs: String) {
        eggs_input_eggs.setText(eggs)
    }

    override fun showDamage(damage: String) {
        eggs_input_damage.setText(damage)
    }

    override fun clearInputs() {
        eggs_input_date.text = null
        eggs_input_treys.text = null
        eggs_input_eggs.text = null
        eggs_input_damage.text = null
        eggs_label_date.error = null
        eggs_label_treys.error = null
    }

    override fun showEggError() {
        Snackbar.make(eggs_input_damage, "Error getting details", Snackbar.LENGTH_LONG).show()
    }

    override fun toggleExpansion(expand: Boolean) {
        if (expand) {
            mPanel!!.expand()
            mListener!!.onStartEditing()
        } else {
            mListener!!.onDoneEditing()
            mPanel!!.collapse()
        }

    }

    override fun setPresenter(presenter: EggsFormContract.Presenter) {
        mPresenter = presenter
    }
}
