package com.mutumbakato.batchmanager.fragments.expenses

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.BaseFragment
import com.mutumbakato.batchmanager.ui.ExpensesListContract
import com.mutumbakato.batchmanager.ui.adapters.ExpensesRecyclerAdapter
import com.mutumbakato.batchmanager.ui.adapters.ExpensesStickyHeaderAdapter
import com.mutumbakato.batchmanager.viewmodels.ExpensesViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_expenses_list.*
import java.util.HashMap
import kotlin.Comparator
import kotlin.collections.ArrayList
import kotlin.collections.LinkedHashMap
import kotlin.collections.set

class ExpensesListFragment : BaseFragment(), ExpensesListContract.View.ExpenseItemInteraction {

    private lateinit var viewModel: ExpensesViewModel
    private var mAdapter: ExpensesStickyHeaderAdapter? = null
    private var expensesAdapter: ExpensesRecyclerAdapter? = null
    private val mSortedExpenses = LinkedHashMap<String, List<Expense>>()
    private val categories = ArrayList<String>()
    private var mode = "all"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!, ViewModelFactory
                .ExpensesViewModelFactory(RepositoryUtils.getExpensesLiveRepo(context),
                        RepositoryUtils.getBatchRepo(context))).get(ExpensesViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_expenses_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        viewModel.expenses.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (mode == "all") {
                    expensesAdapter?.updateData(it)
                }
            }
        })
        viewModel.filteredExpenses.observe(viewLifecycleOwner, Observer {
            it?.let {
                if (mode == "filter") {
                    expensesAdapter?.updateData(it)
                }
            }
        })
        viewModel.mode.observe(viewLifecycleOwner, Observer {
            mode = it
        })
    }

    private fun showCategorisedExpenses(expenses: List<Expense>) {
        val sorted = getSortedExpenses(expenses)
        mAdapter!!.updateData(sorted, ArrayList(sorted!!.keys))
    }

    fun showConfirmDelete(expense: Expense) {
        Snackbar.make(expenses_list, "Do you want to remove " + expense.description + "?", Snackbar.LENGTH_LONG).apply {
            setAction("Delete") {
                viewModel.deleteExpense(expense)
                dismiss()
            }
            show()
        }
    }

    fun applyPermissions(role: String) {
        if (mAdapter != null) {
            mAdapter!!.applyPermissions(role)
        }
    }

    override fun onDelete(expense: Expense) {
        showConfirmDelete(expense)
    }

    override fun onEdit(expense: Expense) {
        viewModel.setExpenseId(expense.id)
        viewModel.isEditing.postValue(true)
    }

    private fun initRecyclerView() {
        mAdapter = ExpensesStickyHeaderAdapter(mSortedExpenses, categories, this, expenses_list)
        expensesAdapter = ExpensesRecyclerAdapter(ArrayList(), this, expenses_list)
        expenses_list.adapter = expensesAdapter
        expenses_list.layoutManager = LinearLayoutManager(context)
    }

    private fun getSortedExpenses(expenses: List<Expense>?): HashMap<String, List<Expense>>? {
        if (expenses == null)
            return null
        val sortedExpenses = LinkedHashMap<String, List<Expense>>()
        val dates = getSections(expenses)
        for (date in dates) {
            sortedExpenses[date] = getSectionData(expenses, date)
        }
        return sortedExpenses
    }

    private fun getSections(expenses: List<Expense>): List<String> {
        val sections = ArrayList<String>()
        for (expense in expenses) {
            if (!sections.contains(expense.date))
                sections.add(expense.date)
        }
        sections.sortWith(Comparator { o1, o2 -> o2.compareTo(o1) })
        return sections
    }

    private fun getSectionData(expenses: List<Expense>, date: String): List<Expense> {
        val exp = ArrayList<Expense>()
        for (expense in expenses) {
            if (expense.date == date)
                exp.add(expense)
        }
        exp.sortWith(Comparator { o1, o2 -> o1.description.compareTo(o2.description) })
        return exp
    }

    companion object {
        fun newInstance(): ExpensesListFragment {
            return ExpensesListFragment()
        }
    }
}
