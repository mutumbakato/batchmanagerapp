package com.mutumbakato.batchmanager.fragments.stats

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import kotlinx.android.synthetic.main.fragment_stats_details.*

/**
 * A simple [Fragment] subclass.
 * Use the [StatsDetailsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
open class StatsDetailsFragment : Fragment() {

    protected lateinit var viewModel: BatchViewModel
    private var type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(activity!!)[BatchViewModel::class.java]
        arguments?.let {
            type = it.getString("type", "")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_stats_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (type) {
            "feeds" -> {
                feeds_stats.visibility = View.VISIBLE
                viewModel.totalFeeds.observe(viewLifecycleOwner, Observer {
                    stats_total_feeds.setText(String.format("%.1f Kg", it))
                })
                viewModel.aveTotalFeeds.observe(viewLifecycleOwner, Observer {
                    stats_avg_feeds.setText(String.format("%.0f gm", it))
                })
                viewModel.feedConversion.observe(viewLifecycleOwner, Observer {
                    it?.let {
                        stats_fcr.setText(String.format("%.1f", it))
                    }
                })
            }
            "weight" -> {
                weight_stats.visibility = View.VISIBLE
                viewModel.avgWeight.observe(viewLifecycleOwner, Observer {
                    stats_avg_weight.setText(String.format("%.0f gm", it))
                })
                viewModel.weightUniformity.observe(viewLifecycleOwner, Observer {
                    stats_weight_uniformity.setText("${String.format("%.1f", it)}%")
                })
                viewModel.avgDailyGain.observe(viewLifecycleOwner, Observer {
                    stats_avg_daily_gain.setText(String.format("%.0f gm", it))
                })
            }
            "deaths" -> {
                death_stats.visibility = View.VISIBLE
                viewModel.batchCount.observe(viewLifecycleOwner, Observer {
                    stats_total_deaths.setText("${it.dead}")
                    stats_birds_alive.setText("${it.remaining}")
                    stats_mortality_rate.setText("${String.format("%.1f", it.mortalityRate)}%")
                })
            }
            "water" -> {
                water_stats.visibility = View.VISIBLE
                viewModel.totalWater.observe(viewLifecycleOwner, Observer {
                    stats_total_water.setText(String.format("%.1f Ltr", it))
                })
                viewModel.aveTotalWater.observe(viewLifecycleOwner, Observer {
                    stats_avg_water.setText(String.format("%.0f ml/bird", it))
                })
                stats_feed_water_ratio.setText("--")
            }
            "eggs" -> {
                eggs_stats.visibility = View.VISIBLE
                viewModel.eggCount.observe(viewLifecycleOwner, Observer {
                    stats_total_eggs.setText("${it.total + it.damage}")
                    stats_good_eggs.setText("${it.total}")
                    stats_damaged_eggs.setText("${it.damage}")
                })
                viewModel.eggsPercentage.observe(viewLifecycleOwner, Observer {
                    stats_laying_percentage.setText("$it%")
                })
            }
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         * @return A new instance of fragment FeedsStatsDetails.
         */
        @JvmStatic
        fun newInstance(type: String) =
                StatsDetailsFragment().apply {
                    arguments = Bundle().apply {
                        putString("type", type)
                    }
                }

    }
}
