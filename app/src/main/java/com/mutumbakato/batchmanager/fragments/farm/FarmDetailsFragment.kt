package com.mutumbakato.batchmanager.fragments.farm

import android.os.Bundle
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.farm.FarmFormActivity
import com.mutumbakato.batchmanager.data.models.Employee
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.ui.FarmDetailsContract
import kotlinx.android.synthetic.main.fragment_farm_details.*

/**
 * A placeholder fragment containing a simple view.
 */
class FarmDetailsFragment : Fragment(), FarmDetailsContract.View {

    private var mPresenter: FarmDetailsContract.Presenter? = null
    private var farm: Farm? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_farm_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        farm_name_view.setTitle("Enter new name")
        farm_name_view.setTextSavedListener { value ->
            farm = Farm(farm!!.id, farm!!.userId, value, farm!!.location, farm!!.contact, farm!!.units, farm!!.currency)
            mPresenter!!.updateFarm(farm)
        }

        farm_location_view.setTitle("Enter new location")
        farm_location_view.setTextSavedListener { value ->
            farm = Farm(farm!!.id, farm!!.userId, farm!!.name, value, farm!!.contact, farm!!.units, farm!!.currency)
            mPresenter!!.updateFarm(farm)
        }

        farm_contact_view.setTitle("Enter farm contacts")
        farm_contact_view.setTextSavedListener { value ->
            farm = Farm(farm!!.id, farm!!.userId, farm!!.name, farm!!.location, value, farm!!.units, farm!!.currency)
            mPresenter!!.updateFarm(farm)
        }

        farm_units_view.setTitle("Enter measurement units")
        farm_units_view.setTextSavedListener { value ->
            farm = Farm(farm!!.id, farm!!.userId, farm!!.name, farm!!.location, farm!!.contact, value, farm!!.currency)
            mPresenter!!.updateFarm(farm)
        }

        farm_currency_view.setFragmentManager(activity!!.supportFragmentManager)
        farm_currency_view.setOnOptionItemClicked { item ->
            farm = Farm(farm!!.id, farm!!.userId, farm!!.name, farm!!.location, farm!!.contact, farm!!.units, item)
            mPresenter!!.updateFarm(farm)
        }

        farm_water_units_view.setTitle("Enter new water units")
        farm_water_units_view.setTextSavedListener {
            farm?.waterUnits = it
            mPresenter!!.updateFarm(farm)
        }

        farm_eggsInTray_view.setTitle("Enter number  eggs in a tray")
        farm_eggsInTray_view.setCustomInputType(InputType.TYPE_CLASS_NUMBER)
        farm_eggsInTray_view.setTextSavedListener {
            farm!!.eggsInTray = it.toInt()
            mPresenter!!.updateFarm(farm)
        }

        farm_name_view_label.setOnClickListener {
            editName()
        }

        farm_location_view_label.setOnClickListener {
            editLocation()
        }

        farm_contact_view_label.setOnClickListener {
            editContacts()
        }
        farm_currency_view_label.setOnClickListener {
            editCurrency()
        }
        farm_units_view_label.setOnClickListener {
            editUnits()
        }
        farm_eggsintray_label.setOnClickListener {
            editEggsInTray()
        }
        farm_water_units_label.setOnClickListener {
            editWaterUnits()
        }
    }

    private fun editName() {
        farm_name_view.showInput()
    }

    private fun editLocation() {
        farm_location_view.showInput()
    }

    private fun editContacts() {
        farm_contact_view.showInput()
    }

    private fun editCurrency() {
        farm_currency_view.showOptions()
    }

    private fun editUnits() {
        farm_units_view.showInput()
    }

    private fun editWaterUnits() {
        farm_water_units_view.showInput()
    }

    private fun editEggsInTray() {
        farm_eggsInTray_view.showInput()
    }

    override fun onResume() {
        super.onResume()
        mPresenter!!.start()
    }

    override fun setPresenter(presenter: FarmDetailsContract.Presenter) {
        this.mPresenter = presenter
    }

    override fun showFarm(farm: Farm) {
        this.farm = farm
        farm_water_units_view.text = farm.waterUnits
        farm_eggsInTray_view.text = farm.eggsInTray.toString()
    }

    override fun showName(name: String) {
        farm_name_view.text = name
    }

    override fun showLocation(location: String) {
        farm_location_view.text = location
    }

    override fun showContacts(contacts: String) {
        farm_contact_view.text = contacts
    }

    override fun showUnits(units: String) {
        farm_units_view.text = units
    }

    override fun showCurrency(currency: String) {
        farm_currency_view.setText(currency)
    }

    override fun showForm(farmId: String) {
        FarmFormActivity.start(context!!, farmId)
    }

    override fun sowAddWorker() {

    }

    override fun isActive(): Boolean {
        return isAdded
    }

    override fun shoWorkers(employees: List<Employee>) {

    }

    override fun showError(message: String) {
        Snackbar.make(farm_name_view, message, Snackbar.LENGTH_LONG).show()
    }

    override fun showNoData() {

    }

    override fun showNoWorkers() {

    }

    override fun showProgress(show: Boolean) {

    }

    override fun showSuccessMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show()
    }

    override fun exits() {
        activity!!.finish()
    }
}
