package com.mutumbakato.batchmanager

import java.util.*
import kotlin.collections.ArrayList

object Features {

    val ITEMS: ArrayList<FeatureItem> = ArrayList()

    val FARM_ITEMS: ArrayList<FeatureItem> = ArrayList()

    val SETTINGS_ITEMS: ArrayList<FeatureItem> = ArrayList()

    private val ITEM_MAP: MutableMap<String, FeatureItem> = HashMap()

    private val FEATURES = arrayOf("Mortality", "Feeding", "Water", "Body Weight", "Vaccination", "Eggs")

    private val FEATURE_ICONS = arrayOf(
            R.drawable.cardiogram,
            R.drawable.cereal,
            R.drawable.water,
            R.drawable.weight_scale,
            R.drawable.ic_injection,
            R.drawable.nest,
            R.drawable.invoice,
            R.drawable.coin)

    private val SETTINGS = arrayOf("Account", "Farm", "Contact Us", "share", "About", "Log out")

    private val SETTINGS_ICONS = arrayOf(
            R.drawable.ic_action_account,
            R.drawable.ic_action_farm,
            R.drawable.ic_action_whatsapp,
            R.drawable.ic_action_share,
            R.drawable.ic_action_info,
            R.drawable.ic_action_logout)

    private val FARM_FEATURES = arrayOf("Expenses", "Sales", "Vaccination")

    private val FARM_FEATURES_ICONS = arrayOf(
            R.drawable.coin,
            R.drawable.invoice,
            R.drawable.syringe,
            R.drawable.share,
            R.drawable.settings,
            R.drawable.value)

    init {

        for (i in FEATURES.indices) {
            addItem(createFeature(i))
        }

        for (i in FARM_FEATURES.indices) {
            FARM_ITEMS.add(FeatureItem(FARM_FEATURES[i], "", FARM_FEATURES_ICONS[i]))
        }

        for (i in SETTINGS.indices) {
            SETTINGS_ITEMS.add(FeatureItem(SETTINGS[i], "", SETTINGS_ICONS[i]))
        }
    }

    private fun addItem(item: FeatureItem) {
        ITEMS.add(item)
        ITEM_MAP[item.name] = item
    }

    private fun createFeature(position: Int): FeatureItem {
        return FeatureItem(FEATURES[position], "", FEATURE_ICONS[position])
    }

    class FeatureItem(val name: String, val content: String, val icon: Int) {
        override fun toString(): String {
            return content
        }
    }
}
