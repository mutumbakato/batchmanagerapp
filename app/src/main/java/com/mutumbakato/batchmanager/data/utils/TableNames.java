package com.mutumbakato.batchmanager.data.utils;

/**
 * Created by cato on 26/05/18.
 */

public class TableNames {
    public static final String TABLE_NAME_WORKERS = "workers";
    public static final String TABLE_NAME_BATCHES = "batches";
    public static final String TABLE_NAME_EGGS = "eggs";
    public static final String TABLE_NAME_VACCINATION = "vaccination";
    public static final String TABLE_NAME_FEEDS = "feeds";
    public static final String TABLE_NAME_SALES = "sales";
    public static final String TABLE_NAME__EXPENSES = "expenses";
    public static final String TABLE_NAME_SCHEDULES = "schedules";
    public static final String TABLE_NAME_MORTALITY = "mortality";
    public static final String TABLE_NAME_EXPENSE_CATEGORIES = "expense_categories";
    public static final String TABLE_NAME_CHECKS = "vaccination_checks";

}
