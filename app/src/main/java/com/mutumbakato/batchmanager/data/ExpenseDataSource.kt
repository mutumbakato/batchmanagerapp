package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Expense

interface ExpenseDataSource : BaseDataSource<Expense> {


    fun getTotalExpenses(batchId: String, callback: ExpenseTotalCallback)

    interface ExpenseTotalCallback {
        fun onTotal(total: Float)
    }

    interface DataUpdatedListener {
        fun onDataUpdated()
    }

}
