package com.mutumbakato.batchmanager.data.utils;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class ApiUtils {

    private static final String BASE_URL = "http://app.poultrybatchmanager.com/api/";
//  public static final String BASE_URL = "http://192.168.43.159:9000/";

    public static Object getEndPoints(Class cls) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(builder.build())
                .build();
        return retrofit.create(cls);
    }
}
