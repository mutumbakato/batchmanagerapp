package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Store

interface StoreDataSource {
    fun create(store: Store, onFinish: (() -> Unit)? = null, onError: ((error: String) -> Unit)? = null)
}