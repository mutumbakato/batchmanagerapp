package com.mutumbakato.batchmanager.data.preferences

import com.google.firebase.auth.FirebaseAuth
import com.mutumbakato.batchmanager.data.models.Farm
import com.pixplicity.easyprefs.library.Prefs
import java.util.*

object PreferenceUtils {

    const val CURRENT_BATCH = "current_batch"
    const val USER = "user"
    const val USER_ID = "user_id"
    const val USER_TOKEN = "token"
    const val LICENCE = "licence"
    const val USER_EMAIL = "email"
    const val LIMITED = "limited"
    const val NOTIFICATION_LOCK = "notification_lock"

    private const val FARM_ID = "farm_id"
    private const val CURRENCY = "default_currency"
    private const val WEIGHT_UNITS = "measuring_units"
    private const val LAST_LOG_TIME = "last_log_time"
    private const val WATER_UNIT = "water_units"
    private const val EGGS_IN_A_TRAY = "eggs_in_a_tray"

    val user: String?
        get() = FirebaseAuth.getInstance().currentUser!!.displayName

    val userId: String
        get() = Prefs.getString(USER_ID, "")

    val isNotLoggedIn: Boolean
        get() = FirebaseAuth.getInstance().currentUser == null

    val userToken: String
        get() = Prefs.getString(USER_TOKEN, "0")

    var farmId: String
        get() = Prefs.getString(FARM_ID, "")
        set(farmId) = Prefs.putString(FARM_ID, farmId)

    var currency: String
        get() = Prefs.getString(CURRENCY, "UGX")
        set(currency) = Prefs.putString(CURRENCY, currency)

    var lastLogTime: String
        get() {
            val calendar = Calendar.getInstance()
            val time = String.format(Locale.UK, " %1\$tY-%1\$tm-%1\$td %1\$tI:%1\$tM %1\$Tp", calendar)
            return Prefs.getString(LAST_LOG_TIME, time)
        }
        set(time) = Prefs.putString(LAST_LOG_TIME, time)

    val eggsInATray: Int
        get() = Prefs.getInt(EGGS_IN_A_TRAY, 30)

    val waterUnits: String
        get() = Prefs.getString(WATER_UNIT, "ltr")

    const val weightUnits: String = "g"
    //get() = Prefs.getString(WEIGHT_UNITS, "kg")

    var isRebrand: Boolean
        get() = Prefs.getBoolean("rebrand", false)
        set(value) = Prefs.putBoolean("rebrand", true)

    fun setFarm(farm: Farm) {
        Prefs.putString(FARM_ID, farm.id)
        Prefs.putString(CURRENCY, farm.currency)
        Prefs.putString(WEIGHT_UNITS, farm.units)
        Prefs.putInt(EGGS_IN_A_TRAY, if (farm.eggsInTray > 0) farm.eggsInTray else 30)
        Prefs.putString(WATER_UNIT, farm.waterUnits)
    }

}
