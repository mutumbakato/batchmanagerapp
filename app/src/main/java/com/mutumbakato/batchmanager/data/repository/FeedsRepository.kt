package com.mutumbakato.batchmanager.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mutumbakato.batchmanager.data.FeedsDataSource
import com.mutumbakato.batchmanager.data.local.FeedsLocalDataSource
import com.mutumbakato.batchmanager.data.models.DataResource
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.models.Feeds
import com.mutumbakato.batchmanager.data.models.Status
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.Logger
import com.mutumbakato.batchmanager.utils.DateUtils

class FeedsRepository constructor(private val localDataSource: FeedsLocalDataSource,
                                  private val remoteDataSource: FeedsDataSource) {

    private val isLoading = MutableLiveData<Boolean>()
    private val error: LiveData<Status> = MutableLiveData()
    private var isOnline: Boolean = false

    fun createFeeds(feeds: Feeds) {
        Logger.log(feeds.batchId, Logger.ACTION_ADDED, " ${feeds.quantity} ${PreferenceUtils.weightUnits} of feeds")
        isLoading.postValue(true)
        localDataSource.insert(feeds) {
            isLoading.postValue(false)
        }
        remoteDataSource.createFeeds(feeds) {
            isLoading.postValue(false)
        }
    }

    fun listFeeds(batchId: String): DataResource<List<Feeds>> {
        if (!isOnline) {
            listFromRemote(batchId)
            isOnline = true
        }
        val feeds = localDataSource.listFeeds(batchId)
        return DataResource(feeds, error, isLoading)
    }

    fun listOldFeeds(batchId: String): DataResource<List<Feeding>> {
        listOldFeedsFromRemote(batchId)
        val oldFeeds = localDataSource.listOldFeeds(batchId)
        return DataResource(oldFeeds, error, isLoading)
    }

    private fun listOldFeedsFromRemote(batchId: String) {
        remoteDataSource.listOldFeeds(batchId) {
            localDataSource.insertAllOldFeeds(it)
        }
    }

    fun getFeedsById(id: String): LiveData<Feeds> {
        return localDataSource.getFeedsById(id)
    }

    fun updateFeeds(feeds: Feeds) {
        Logger.log(feeds.batchId, Logger.ACTION_UPDATED, "feeds for ${DateUtils.parseDateToddMMyyyy(feeds.date)}")
        isLoading.postValue(true)
        localDataSource.update(feeds) {
            isLoading.postValue(false)
        }
        remoteDataSource.updateFeeds(feeds) {
        }
    }

    fun deleteFeeds(feeds: Feeds) {
        Logger.log(feeds.batchId, Logger.ACTION_DELETED, "feeds for ${DateUtils.parseDateToddMMyyyy(feeds.date)}")
        isLoading.postValue(true)
        localDataSource.deleteFeeds(feeds) {
            isLoading.postValue(false)
        }
        remoteDataSource.deleteFeeds(feeds) {
            isLoading.postValue(false)
        }
    }

    private fun listFromRemote(batchId: String) {
        isLoading.postValue(true)
        remoteDataSource.listFeeds(batchId) {
            localDataSource.deleteAllFeeds(batchId)
            localDataSource.insertAll(it) {
                isLoading.postValue(false)
            }
        }
    }

    companion object {

        @Volatile
        private var INSTANCE: FeedsRepository? = null

        fun getInstance(localDataSource: FeedsLocalDataSource, remoteDataSource: FeedsDataSource): FeedsRepository {
            if (INSTANCE == null)
                INSTANCE = FeedsRepository(localDataSource, remoteDataSource)
            return INSTANCE!!
        }
    }
}