package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

import com.mutumbakato.batchmanager.data.models.ExpenseCategory


@Dao
interface ExpenseCategoriesDao {
    /**
     * Select all category from the expense category table.
     *
     * @return all expense_categories.
     */
    @get:Query("SELECT * FROM expense_categories WHERE status != 'trash'  ORDER BY name ASC")
    val categories: List<ExpenseCategory>

    /**
     * Select all expense_categories from the expense_categories table.
     *
     * @return all expense_categories.
     */
    @get:Query("SELECT * FROM expense_categories WHERE last_modified != 0 AND server_id != 0 AND status !='trash'")
    val local: List<ExpenseCategory>

    /**
     * Select new expense_categories
     *
     * @return expense_categories that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM expense_categories WHERE server_id = 0")
    val new: List<ExpenseCategory>

    /**
     * Select locally updated expense_categories since the last synchronisation
     *
     * @return updated expense_categories  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM expense_categories WHERE server_id != 0 AND last_modified = 0")
    val updated: List<ExpenseCategory>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM expense_categories ORDER BY server_id DESC LIMIT 1")
    val lastServerId: Long

    /**
     * Select expense_categories deleted from the locally
     *
     * @return server ids of expense_categories where status = trash ;
     */
    @get:Query("SELECT server_id FROM expense_categories WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * Select a category by name.
     *
     * @param categoryId the expense category name.
     * @return the expense category with categoryId.
     */
    @Query("SELECT * FROM expense_categories WHERE _id = :categoryId AND status != 'trash' ")
    fun getExpenseById(categoryId: String): ExpenseCategory

    /**
     * Insert a expense category in the database. If the expense already exists, replace it.
     *
     * @param category the expense category to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCategory(category: ExpenseCategory): Long

    /**
     * Update a expense.
     *
     * @param category expenseCategory to be updated
     * @return the number of expense_categories updated. This should always be 1.
     */
    @Update
    fun updateExpense(category: ExpenseCategory): Int

    /**
     * Delete a expense category by name.
     *
     * @return the number of expense categories deleted. This should always be 1.
     */
    @Query("DELETE FROM expense_categories WHERE _id = :categoryId")
    fun deleteCategoryById(categoryId: String): Int

    /**
     * Delete all expense_categories.
     */
    @Query("DELETE FROM expense_categories")
    fun deleteCategories()

    /**
     * @return number of items deleted
     */
    @Query("DELETE FROM expense_categories WHERE status = 'trash'")
    fun clearTrash(): Int

}
