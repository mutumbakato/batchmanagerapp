package com.mutumbakato.batchmanager.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.models.Feeds

@Dao
interface FeedsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(feeds: Feeds)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(feeds: List<Feeds>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllOldFeeds(feeds: List<Feeding>)

    @Update
    fun update(feeds: Feeds)

    @Query("SELECT * FROM feeds WHERE batch_id = :batchId ")
    fun listFeeds(batchId: String): LiveData<List<Feeds>>

    @Query("SELECT * FROM feeding WHERE batch_id = :batchId")
    fun listOldFeeds(batchId: String): LiveData<List<Feeding>>

    @Query("SELECT * FROM feeds WHERE _id = :id")
    fun getFeedsById(id: String): LiveData<Feeds>

    @Delete
    fun deleteFeeds(feeds: Feeds)

    @Query("DELETE FROM feeds WHERE batch_id = :batchId")
    fun deleteAllFeeds(batchId: String)

}