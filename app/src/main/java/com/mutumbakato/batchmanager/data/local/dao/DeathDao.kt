package com.mutumbakato.batchmanager.data.local.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

import com.mutumbakato.batchmanager.data.models.Death

@Dao
interface DeathDao {

    /**
     * Select all deaths from the death table.
     *
     * @return all deaths.
     */
    @get:Query("SELECT * FROM death WHERE last_modified != 0 AND server_id != 0 AND status != 'trash'")
    val local: List<Death>

    /**
     * Select new death
     *
     * @return deaths that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM death WHERE server_id = 0")
    val new: List<Death>

    /**
     * Select locally updated deaths since the last synchronisation
     *
     * @return updated death  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM death WHERE server_id != 0 AND last_modified = 0 AND status != 'trash'")
    val updated: List<Death>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM death ORDER BY server_id DESC LIMIT 1")
    val lastServerId: Long

    /**
     * Select deaths deleted from the locally
     *
     * @return server ids of deaths where status = trash ;
     */
    @get:Query("SELECT server_id FROM death WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * Select all death from the death table.
     *
     * @return all death.
     */
    @Query("SELECT * FROM death WHERE batch_id = :batchId AND status != 'trash'")
    fun getDeath(batchId: String): List<Death>

    /**
     * Select a death by name.
     *
     * @param deathId the death name.
     * @return the death with deathId.
     */
    @Query("SELECT * FROM death WHERE _id = :deathId")
    fun getDeathById(deathId: String): Death

    /**
     * Insert a death in the database. If the death already exists, replace it.
     *
     * @param death the death to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDeath(death: Death): Long

    /**
     * Update a death.
     *
     * @param death death to be updated
     * @return the number of death updated. This should always be 1.
     */
    @Update
    fun updateDeath(death: Death): Int

    /**
     * Delete a death by name.
     */
    @Query("UPDATE death SET status = 'trash' WHERE _id = :deathId")
    fun deleteDeathById(deathId: String)

    /**
     * Delete all death.
     */
    @Query("DELETE FROM death WHERE batch_id = :batchId")
    fun deleteDeath(batchId: String)

    /**
     * Permanently deleteCategory all trashed entries
     */
    @Query("DELETE FROM death WHERE status = 'trash'")
    fun clearTrash()

    @Query("SELECT SUM(count) AS rate FROM death WHERE batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun mortalityRate(batchId: String): Int

}

