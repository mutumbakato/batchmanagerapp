package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

import com.mutumbakato.batchmanager.data.models.VaccinationSchedule

@Dao
interface VaccinationScheduleDao {

    /**
     * Select all vaccination_schedules from the vaccination_schedule table.
     *
     * @return all vaccination_schedules.
     */
    @get:Query("SELECT * FROM vaccination_schedule WHERE last_modified != 0 AND server_id != 0 AND status != 'trash'")
    val local: List<VaccinationSchedule>

    /**
     * Select new vaccination_schedule
     *
     * @return vaccination_schedules that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM vaccination_schedule WHERE server_id = 0")
    val new: List<VaccinationSchedule>

    /**
     * Select locally updated vaccination_schedules since the last synchronisation
     *
     * @return updated vaccination_schedule  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM vaccination_schedule WHERE server_id != 0 AND last_modified = 0 AND status != 'trash'")
    val updated: List<VaccinationSchedule>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM vaccination_schedule ORDER BY server_id DESC LIMIT 1")
    val lastServerId: Long

    /**
     * Select vaccination_schedules deleted from the locally
     *
     * @return server ids of vaccination_schedules where status = trash ;
     */
    @get:Query("SELECT server_id FROM vaccination_schedule WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * @return list of Vaccination schedule objects
     */
    @Query("SELECT * FROM vaccination_schedule WHERE status != 'trash' ")
    fun listSchedules(): List<VaccinationSchedule>

    /**
     * Select a vaccination by name.
     *
     * @param scheduleId the vaccination name.
     * @return the vaccination with vaccinationId.
     */
    @Query("SELECT * FROM vaccination_schedule WHERE _id = :scheduleId AND status != 'trash' ")
    fun getSchedulesById(scheduleId: String): VaccinationSchedule

    /**
     * Insert a schedule in the database. If the vaccination already exists, replace it.
     *
     * @param schedule the vaccination  schedule to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSchedule(schedule: VaccinationSchedule): Long

    /**
     * Update a vaccination schedule.
     *
     * @param schedule vaccination to be updated
     * @return the number of vaccination  schedules updated. This should always be 1.
     */
    @Update
    fun updateSchedule(schedule: VaccinationSchedule): Int

    /**
     * @param batchId    id of the batch
     * @param scheduleId id of the schedule to attach
     */
    @Query("UPDATE batches SET schedule = :scheduleId, last_modified = 0 WHERE _id = :batchId")
    fun useSchedule(batchId: String, scheduleId: String)

    /**
     * @param batchId    id of the batch
     * @param scheduleId id of the schedule to attach
     */
    @Query("UPDATE batches SET schedule = '0', last_modified = 0 WHERE _id = :batchId AND schedule = :scheduleId")
    fun removeSchedule(batchId: String, scheduleId: String)

    /**
     * Delete a vaccination_schedule by name.
     */
    @Query("UPDATE vaccination_schedule SET status = 'trash' WHERE _id = :vaccination_scheduleId")
    fun deleteScheduleById(vaccination_scheduleId: String)

    /* * Delete a vaccination_schedule by name.
     */
    @Query("UPDATE vaccination SET status = 'trash' WHERE schedule_id = :vaccination_scheduleId")
    fun deleteScheduleItems(vaccination_scheduleId: String)

    /**
     * Delete all vaccination_schedule.
     */
    @Query("DELETE FROM vaccination_schedule")
    fun deleteAllSchedules()

    /**
     * Permanently deleteCategory all trashed entries
     */
    @Query("DELETE FROM vaccination_schedule WHERE status = 'trash'")
    fun clearTrash()

}

