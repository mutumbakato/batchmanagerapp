package com.mutumbakato.batchmanager.data.remote;


import android.os.Handler;
import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.BaseDataSource;
import com.mutumbakato.batchmanager.data.EggsDataSource;
import com.mutumbakato.batchmanager.data.models.Eggs;

import java.util.LinkedHashMap;
import java.util.Map;

public class EggsRemoteDataSource implements EggsDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 3000;
    private final static Map<String, Eggs> EGGS_SERVICE_DATA;
    private static EggsRemoteDataSource INSTANCE = null;

    static {
        EGGS_SERVICE_DATA = new LinkedHashMap<>(2);
//        addEggs("1", "2017-08-08", 20, 2, 4);
//        addEggs("1", "2017-07-07", 10, 0, 13);
    }

    private EggsRemoteDataSource() {

    }

    private static void addEggs(String batchId
            , String date, int treys, int eggs, int damage, int count) {
        Eggs newEggs = new Eggs(batchId, date, treys, eggs, damage, count);
        EGGS_SERVICE_DATA.put(newEggs.getId(), newEggs);
    }

    public static EggsRemoteDataSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new EggsRemoteDataSource();
        return INSTANCE;
    }


    @Override
    public void getAll(@NonNull String batchId, @NonNull final BaseDataSource.DataLoadCallback<Eggs> callback) {
        callback.onEmptyData();
    }

    @Override
    public void getOne(@NonNull final String id, @NonNull String batchId, @NonNull final BaseDataSource.DataItemCallback<Eggs> callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.onDataItemLoaded(EGGS_SERVICE_DATA.get(id));
            }
        }, SERVICE_LATENCY_IN_MILLIS);
    }

    @Override
    public void saveData(@NonNull Eggs data) {
        EGGS_SERVICE_DATA.put(data.getId(), data);
    }

    @Override
    public void updateData(@NonNull Eggs data) {
        EGGS_SERVICE_DATA.put(data.getId(), data);
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllData(@NonNull String batchId) {
        EGGS_SERVICE_DATA.clear();
    }

    @Override
    public void deleteDataItem(@NonNull Eggs item, @NonNull String batchId) {
        EGGS_SERVICE_DATA.remove(item);
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void observeData(DataLoadCallback<Eggs> callback, String... referenceIds) {

    }

    @Override
    public void getTotalEggs(String batchId, OnEggsTotal callback) {
        //Eggs total is only implemented by the local data source
    }
}


