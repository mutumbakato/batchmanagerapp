package com.mutumbakato.batchmanager.data.models

import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.utils.DateUtils

data class Filter constructor(var name: String = "Farm",
                              var id: String = PreferenceUtils.farmId,
                              var type: String = "Batch",
                              var from: String = DateUtils.monthAgo(),
                              var to: String = DateUtils.today(),
                              var rangeName: String = "Last 30 days") {

    override fun toString(): String {
        return "$id,$from,$to,$type,$name,$rangeName"
    }

    companion object {
        const val FILTER_NAME = 4
        const val FILTER_ID = 0
        const val FILTER_FROM = 1
        const val FILTER_TO = 2
        const val FILTER_TYPE = 3
        const val FILTER_RANGE_NAME = 5
    }
}