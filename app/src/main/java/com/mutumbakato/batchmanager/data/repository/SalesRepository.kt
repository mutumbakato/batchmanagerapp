package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.SalesDataSource
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.Logger
import com.mutumbakato.batchmanager.utils.Currency
import java.util.*

class SalesRepository private constructor(private val mSalesRemoteDataSource: SalesDataSource, private val mSalesLocalDataSource: SalesDataSource) : SalesDataSource {

    private var mListener: SalesDataSource.DataUpdatedListener? = null

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    private var mCachedSales: MutableMap<String, MutableMap<String, Sale>>? = null

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    private var mCacheIsDirty = false

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Sale>) {
        observeData(callback, batchId)
        if (mCachedSales != null && mCachedSales!![batchId] != null && !mCacheIsDirty) {
            callback.onDataLoaded(ArrayList(mCachedSales!![batchId]!!.values))
            return
        }
        if (mCacheIsDirty) {
            getSalesFromRemoteSource(batchId, callback)
        } else {
            mSalesLocalDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Sale> {
                override fun onDataLoaded(data: List<Sale>) {
                    refreshCache(batchId, data)
                    callback.onDataLoaded(data)
                }

                override fun onEmptyData() {
                    getSalesFromRemoteSource(batchId, callback)
                }

                override fun onError(message: String) {
                    callback.onError(message)
                }
            })
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Sale>) {
        if (mCachedSales != null && mCachedSales!![batchId] != null) {
            mCachedSales!![batchId]!![id]?.let { callback.onDataItemLoaded(it) }
        } else {
            mSalesLocalDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Sale> {
                override fun onDataItemLoaded(data: Sale) {
                    addCacheItem(data)
                    callback.onDataItemLoaded(data)
                }

                override fun onDataItemNotAvailable() {
                    mSalesRemoteDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Sale> {
                        override fun onDataItemLoaded(data: Sale) {
                            addCacheItem(data)
                            callback.onDataItemLoaded(data)
                        }

                        override fun onDataItemNotAvailable() {
                            callback.onDataItemNotAvailable()
                        }

                        override fun onError(message: String) {
                            callback.onError(message)
                        }
                    })
                }

                override fun onError(message: String) {

                }
            })
        }
    }

    override fun saveData(data: Sale) {
        Logger.log(data.batchId, Logger.ACTION_ADDED, data.item
                + " for "
                + " " + Currency.format(data.rate * data.quantity)
                + " to Sales")
        mSalesLocalDataSource.saveData(data)
        mSalesRemoteDataSource.saveData(data)
        addCacheItem(data)
        notifyDataChanged()
    }

    override fun updateData(data: Sale) {
        Logger.log(data.batchId, Logger.ACTION_UPDATED, data.item
                + " for "
                + Currency.format(data.rate * data.quantity)
                + " in sales")
        mSalesLocalDataSource.updateData(data)
        mSalesRemoteDataSource.updateData(data)
        addCacheItem(data)
        notifyDataChanged()
    }

    override fun refreshData() {
        mCacheIsDirty = true
    }

    override fun deleteAllData(batchId: String) {
        mSalesRemoteDataSource.deleteAllData(batchId)
        mSalesLocalDataSource.deleteAllData(batchId)
        if (mCachedSales != null) {
            mCachedSales!!.clear()
        }
        notifyDataChanged()
    }

    override fun deleteDataItem(item: Sale, batchId: String) {
        Logger.log(batchId, Logger.ACTION_DELETED,
                item.item
                        + " of " + PreferenceUtils.currency
                        + " " + Currency.format(item.rate * item.quantity)
                        + " on " + item.date + " from Sales")
        mSalesLocalDataSource.deleteDataItem(item, batchId)
        mSalesRemoteDataSource.deleteDataItem(item, batchId)
        if (isCacheAvailable(batchId))
            mCachedSales!![batchId]!!.remove(item.id)
        notifyDataChanged()
    }

    override fun clearCache() {
        mCachedSales = null
        notifyDataChanged()
    }

    override fun notifyDataChanged() {
        if (mListener != null)
            mListener!!.onDataUpdated()
    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Sale>, vararg referenceIds: String) {
        mSalesRemoteDataSource.observeData(object : BaseDataSource.DataLoadCallback<Sale> {
            override fun onDataLoaded(data: List<Sale>) {
                callback.onDataLoaded(data)
                refreshLocalDataSource(referenceIds[0], data)
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        }, *referenceIds)
    }

    private fun getSalesFromRemoteSource(batchId: String, callback: BaseDataSource.DataLoadCallback<Sale>) {
        mSalesRemoteDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Sale> {
            override fun onDataLoaded(data: List<Sale>) {
                if (data.isNotEmpty()) {
                    refreshLocalDataSource(batchId, data)
                    callback.onDataLoaded(data)
                } else
                    callback.onEmptyData()
                mCacheIsDirty = false
            }

            override fun onEmptyData() {
                callback.onEmptyData()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })

    }

    private fun addCacheItem(sale: Sale) {
        if (isCacheAvailable(sale.batchId))
            mCachedSales!![sale.batchId]!![sale.id] = sale
    }

    private fun refreshLocalDataSource(batchId: String, sales: List<Sale>) {
        mCachedSales = null
        mSalesLocalDataSource.deleteAllData(batchId)
        for (sale in sales) {
            mSalesLocalDataSource.saveData(sale)
        }
        refreshCache(batchId, sales)
    }

    private fun refreshCache(batchId: String, sales: List<Sale>) {
        for (sale in sales) {
            if (isCacheAvailable(sale.batchId))
                mCachedSales!![batchId]!![sale.id] = sale
        }
    }

    override fun getTotalSales(batchId: String, callback: SalesDataSource.OnTotalSales) {
        getAll(batchId, object : BaseDataSource.DataLoadCallback<Sale> {
            override fun onDataLoaded(data: List<Sale>) {
                var total = 0f
                for (sale in data) {
                    total += sale.quantity * sale.rate
                }
                callback.onTotal(total)
            }

            override fun onEmptyData() {
                callback.onTotal(0f)
            }

            override fun onError(message: String) {
                callback.onTotal(0f)
            }
        })
    }

    fun setDataUpdatedListener(listener: SalesDataSource.DataUpdatedListener) {
        mListener = listener
    }

    private fun isCacheAvailable(batchId: String): Boolean {
        if (mCachedSales == null) {
            mCachedSales = LinkedHashMap()
            mCachedSales!![batchId] = LinkedHashMap()
        }

        if (mCachedSales!![batchId] == null) {
            mCachedSales!![batchId] = LinkedHashMap()
        }

        return true
    }

    companion object {

        private var INSTANCE: SalesRepository? = null

        fun getInstance(salesRemoteDataSource: SalesDataSource, salesLocalDataSource: SalesDataSource): SalesRepository {
            if (INSTANCE == null)
                INSTANCE = SalesRepository(salesRemoteDataSource, salesLocalDataSource)
            return INSTANCE as SalesRepository
        }
    }
}
