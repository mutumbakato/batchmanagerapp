package com.mutumbakato.batchmanager.data.models;

import com.google.firebase.auth.FirebaseUser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by cato on 10/24/17.
 */
public class User {

    private String id;
    private String uuid = null;
    private String name;
    private String email;
    private String phone;
    private String password;

    public User(String id, String name, String email, String password, String phone) {
        this.email = email;
        this.name = name;
        this.password = password;
        this.phone = phone;
        this.id = id;
    }

    public User(String userInfo) throws JSONException {
        JSONObject json = new JSONObject(userInfo);
        this.name = json.getString("username");
        this.email = json.getString("email");
        this.id = json.getString("id");
    }

    public User(FirebaseUser u, String id) {
        this.name = u.getDisplayName();
        this.email = u.getEmail();
        this.phone = u.getPhoneNumber();
        this.uuid = u.getUid();
        this.id = id;
    }

    public User() {

    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getId() {
        return id == null ? uuid : id;
    }

    public String getUuid() {
        return uuid;
    }
}
