package com.mutumbakato.batchmanager.data.models;

public class EggCount {
    public int total;
    public int damage;

    @Override
    public String toString() {
        return total + "Eggs and " + damage + "Damage";
    }
}
