package com.mutumbakato.batchmanager.data.local;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.mutumbakato.batchmanager.data.BaseDataSource;
import com.mutumbakato.batchmanager.data.EggsDataSource;
import com.mutumbakato.batchmanager.data.local.dao.EggsDao;
import com.mutumbakato.batchmanager.data.models.EggCount;
import com.mutumbakato.batchmanager.data.models.Eggs;
import com.mutumbakato.batchmanager.data.sync.Sync;
import com.mutumbakato.batchmanager.data.sync.Syncable;
import com.mutumbakato.batchmanager.utils.executors.AppExecutors;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class EggsLocalDataSource implements EggsDataSource, Syncable<Eggs> {

    private static final String TAG = EggsLocalDataSource.class.getSimpleName();
    private static volatile EggsLocalDataSource INSTANCE;
    private EggsDao mEggsDao;
    private AppExecutors mAppExecutors;

    //  private EggsRepository repository;
    private EggsLocalDataSource(AppExecutors appExecutors, EggsDao eggsDao) {
        mEggsDao = eggsDao;
        mAppExecutors = appExecutors;
//  repository = EggsRepository.getInstance(EggsRemoteDataSource.getInstance(), this);
    }

    public static EggsLocalDataSource getInstance(AppExecutors appExecutors, EggsDao eggsDao) {
        if (INSTANCE == null)
            INSTANCE = new EggsLocalDataSource(appExecutors, eggsDao);
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void getAll(@NonNull final String batchId, @NonNull final BaseDataSource.DataLoadCallback<Eggs> callback) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final List<Eggs> eggs = mEggsDao.getAllEggs(batchId);
                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (eggs.size() > 0) {
                            callback.onDataLoaded(eggs);
                        } else {
                            // This will be called if the table is new or just empty.
                            callback.onEmptyData();
                        }
                    }
                });
            }
        };
        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void getOne(@NonNull final String id, @NonNull String batchId, @NonNull final BaseDataSource.DataItemCallback<Eggs> callback) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                final Eggs eggs = mEggsDao.getEggsById(id);
                mAppExecutors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        if (eggs != null) {
                            callback.onDataItemLoaded(eggs);

                        } else {
                            // This will be called if the table is new or just empty.
                            callback.onDataItemNotAvailable();
                        }
                    }
                });
            }
        };
        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void saveData(@NonNull final Eggs data) {
        Runnable saveRunnable = () -> {
            mEggsDao.insertEggs(data);
            sync();
        };
        mAppExecutors.diskIO().execute(saveRunnable);

    }

    @Override
    public void updateData(@NonNull final Eggs data) {
        Runnable runnable = () -> {
            mEggsDao.insertEggs(data);
            sync();
        };
        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllData(@NonNull final String batchId) {
        Runnable runnable = () -> mEggsDao.deleteEggs(batchId);
        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void deleteDataItem(@NonNull final Eggs item, @NonNull String batchId) {
        Runnable runnable = () -> {
            mEggsDao.deleteEggsById(item.getId());
            sync();
        };
        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void observeData(DataLoadCallback<Eggs> callback, String... referenceIds) {

    }

    @Override
    public void getTotalEggs(final String batchId, final OnEggsTotal callback) {
        final Runnable runnable = () -> {
            final EggCount eggs = mEggsDao.totalEggs(batchId);
            mAppExecutors.mainThread().execute(() -> {
                if (eggs != null) {
                    callback.onTotal(eggs);
                }
            });
        };
        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public String getTable() {
        return Eggs.TABLE_NAME;
    }

    @Override
    public List<Eggs> getNew() {
        return mEggsDao.getNew();
    }

    @Override
    public List<Eggs> getUpdated() {
        return mEggsDao.getUpdated();
    }

    @Override
    public List<Eggs> getLocalData() {
        return mEggsDao.getLocal();
    }

    @Override
    public List<Long> getTrash() {
        return mEggsDao.getTrash();
    }

    @Override
    public long getLastServerId() {
        return mEggsDao.getLastServerId();
    }

    @Override
    public void handleNewData(JSONArray newData) {
        Gson gson = new Gson();
        for (int n = 0; n < newData.length(); n++) {
            try {
                Eggs eggs = gson.fromJson(newData.get(n).toString(), Eggs.class);
                Eggs newEggs = new Eggs(eggs.getId(), eggs.getBatchId(), eggs.getDate(), eggs.getEggs(),
                        eggs.getDamage(), eggs.getCount(), eggs.getServerId(), eggs.getStatus(), eggs.getLastModified());
                mEggsDao.insertEggs(newEggs);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleModified(JSONArray modified) {
        Gson gson = new Gson();
        for (int n = 0; n < modified.length(); n++) {
            try {
                Eggs eggs = gson.fromJson(modified.get(n).toString(), Eggs.class);
                mEggsDao.updateEggs(eggs);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSynced(JSONArray synced) {
        for (int n = 0; n < synced.length(); n++) {

            try {
                String id = synced.getJSONObject(n).getString("id");
                long serverId = synced.getJSONObject(n).getLong("serverId");
                long lastMod = synced.getJSONObject(n).getLong("lastModified");

                Eggs eggs = mEggsDao.getEggsById(id);
                Eggs syncedEggs = new Eggs(eggs.getId(), eggs.getBatchId(), eggs.getDate(), eggs.getEggs(),
                        eggs.getDamage(), eggs.getCount(), serverId, eggs.getStatus(), lastMod);

                mEggsDao.updateEggs(syncedEggs);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleUpdates(JSONArray updates) {
        for (int n = 0; n < updates.length(); n++) {
            try {
                String id = updates.getJSONObject(n).getString("id");
                Long last_mod = updates.getJSONObject(n).getLong("lastModified");
                Eggs eggs = mEggsDao.getEggsById(id);
                Eggs updatedEggs = new Eggs(eggs.getId(), eggs.getBatchId(), eggs.getDate(), eggs.getEggs(),
                        eggs.getDamage(), eggs.getCount(), eggs.getServerId(), eggs.getStatus(), last_mod);

                mEggsDao.updateEggs(updatedEggs);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleTrash(String[] trash) {
        for (String aTrash : trash) {
            mEggsDao.deleteEggsById(aTrash);
        }
        mEggsDao.clearTrash();
    }

    @Override
    public void notifyRepository() {
//        repository.clearCache();
    }

    private void sync() {
        Sync.getInstance().sync(this, false);
    }
}
