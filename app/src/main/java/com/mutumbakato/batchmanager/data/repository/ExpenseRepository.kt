package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.ExpenseDataSource
import com.mutumbakato.batchmanager.data.local.ExpenseLocalDataSource
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.utils.Logger
import java.util.*

class   ExpenseRepository private constructor(private val mExpensesRemoteDataSource: ExpenseDataSource, private val mExpensesLocalDataSource: ExpenseLocalDataSource) : ExpenseDataSource {

    private var mListener: ExpenseDataSource.DataUpdatedListener? = null
    private var mCachedExpenses: MutableMap<String, MutableMap<String, Expense>>? = null
    private var mCacheIsDirty = false

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Expense>) {
        observeData(callback, batchId)
        if (mCachedExpenses != null && mCachedExpenses!![batchId] != null && !mCacheIsDirty) {
            callback.onDataLoaded(ArrayList(mCachedExpenses!![batchId]!!.values))
            return
        }

        if (mCacheIsDirty) {
            getExpensesFromRemoteDataSource(batchId, callback)
        } else
            mExpensesLocalDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Expense> {
                override fun onDataLoaded(data: List<Expense>) {
                    refreshCache(batchId, data)
                    callback.onDataLoaded(data)
                    mCacheIsDirty = false
                }

                override fun onEmptyData() {
                    getExpensesFromRemoteDataSource(batchId, callback)
                }

                override fun onError(message: String) {
                    callback.onError(message)
                }
            })
    }

    override fun getOne(id: String, batchId: String,
                        callback: BaseDataSource.DataItemCallback<Expense>) {
        if (mCachedExpenses != null && mCachedExpenses!![batchId] != null && !mCacheIsDirty) {
            mCachedExpenses!![batchId]!![id]?.let { callback.onDataItemLoaded(it) }
        } else {
            mExpensesLocalDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Expense> {
                override fun onDataItemLoaded(data: Expense) {
                    if (isCacheAvailable(batchId)) {
                        mCachedExpenses!![batchId]!![id] = data
                    }
                    callback.onDataItemLoaded(data)
                }

                override fun onDataItemNotAvailable() {
                    mExpensesRemoteDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Expense> {
                        override fun onDataItemLoaded(data: Expense) {
                            if (isCacheAvailable(batchId)) {
                                mCachedExpenses!![batchId]!![id] = data
                            }
                            callback.onDataItemLoaded(data)
                        }

                        override fun onDataItemNotAvailable() {
                            callback.onDataItemNotAvailable()
                        }

                        override fun onError(message: String) {
                            callback.onError(message)
                        }
                    })
                }

                override fun onError(message: String) {

                }
            })
        }
    }

    override fun saveData(data: Expense) {
        Logger.log(data.batchId, "added", data.description + " to Expenses")
        mExpensesLocalDataSource.saveData(data)
        mExpensesRemoteDataSource.saveData(data)
        mExpensesLocalDataSource.getOne(data.id, data.batchId, object : BaseDataSource.DataItemCallback<Expense> {
            override fun onDataItemLoaded(data: Expense) {
                if (isCacheAvailable(data.batchId)) {
                    mCachedExpenses!![data.batchId]!![data.id] = data
                }
                if (mListener != null)
                    mListener!!.onDataUpdated()
            }

            override fun onDataItemNotAvailable() {

            }

            override fun onError(message: String) {

            }
        })
    }

    override fun updateData(data: Expense) {
        Logger.log(data.batchId, "updated", data.description + " in Expenses")
        mExpensesLocalDataSource.updateData(data)
        mExpensesRemoteDataSource.updateData(data)
        mExpensesLocalDataSource.getOne(data.id, data.batchId, object : BaseDataSource.DataItemCallback<Expense> {
            override fun onDataItemLoaded(data: Expense) {

                if (isCacheAvailable(data.batchId)) {
                    mCachedExpenses!![data.batchId]!![data.id] = data
                }

                if (mListener != null)
                    mListener!!.onDataUpdated()
            }

            override fun onDataItemNotAvailable() {}

            override fun onError(message: String) {

            }
        })
    }

    override fun refreshData() {
        mCacheIsDirty = true
    }

    override fun deleteAllData(batchId: String) {
        mExpensesLocalDataSource.deleteAllData(batchId)
        mExpensesRemoteDataSource.deleteAllData(batchId)
        if (isCacheAvailable(batchId)) {
            mCachedExpenses!![batchId]!!.clear()
        }
    }

    override fun deleteDataItem(item: Expense, batchId: String) {
        Logger.log(batchId, Logger.ACTION_DELETED, item.description + " from Expenses")
        mExpensesLocalDataSource.deleteDataItem(item, batchId)
        mExpensesRemoteDataSource.deleteDataItem(item, batchId)
        if (isCacheAvailable(batchId)) {
            mCachedExpenses!![batchId]!!.remove(item.id)
        }
        notifyDataChanged()
    }

    override fun clearCache() {
        mCachedExpenses = null
        notifyDataChanged()
    }

    override fun notifyDataChanged() {
        if (mListener != null)
            mListener!!.onDataUpdated()
    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Expense>, vararg referenceIds: String) {
        mExpensesRemoteDataSource.observeData(object : BaseDataSource.DataLoadCallback<Expense> {
            override fun onDataLoaded(data: List<Expense>) {
                callback.onDataLoaded(data)
                refreshLocalDataSource(referenceIds[0], data)
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        }, referenceIds[0])

    }

    private fun getExpensesFromRemoteDataSource(batchId: String, callback: BaseDataSource.DataLoadCallback<Expense>) {
        mExpensesRemoteDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Expense> {
            override fun onDataLoaded(data: List<Expense>) {
                refreshLocalDataSource(batchId, data)
                callback.onDataLoaded(data)
                mCacheIsDirty = false
            }

            override fun onEmptyData() {
                callback.onEmptyData()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })
    }

    private fun refreshLocalDataSource(batchId: String, expenses: List<Expense>) {
        mExpensesLocalDataSource.deleteAllData(batchId)
        if (mCachedExpenses != null)
            mCachedExpenses!!.clear()
        for (expense in expenses) {
            mExpensesLocalDataSource.saveData(expense)
        }
        refreshCache(batchId, expenses)
    }

    private fun refreshCache(batchId: String, expenses: List<Expense>) {
        for (expense in expenses) {
            if (isCacheAvailable(batchId))
                mCachedExpenses!![batchId]!!.put(expense.id, expense)
        }
        mCacheIsDirty = false
    }

    override fun getTotalExpenses(batchId: String, callback: ExpenseDataSource.ExpenseTotalCallback) {
        getAll(batchId, object : BaseDataSource.DataLoadCallback<Expense> {
            override fun onDataLoaded(data: List<Expense>) {
                var total = 0f
                for (expense in data) {
                    total += expense.amount
                }
                callback.onTotal(total)
            }

            override fun onEmptyData() {
                callback.onTotal(0f)
            }

            override fun onError(message: String) {
                callback.onTotal(0f)
            }
        })
    }

    fun setOnDataUpdatedListener(listener: ExpenseDataSource.DataUpdatedListener) {
        mListener = listener
    }

    private fun isCacheAvailable(batchId: String): Boolean {
        if (mCachedExpenses == null) {
            mCachedExpenses = linkedMapOf()
            mCachedExpenses!![batchId] = LinkedHashMap()
        } else if (mCachedExpenses!![batchId] == null) {
            mCachedExpenses!![batchId] = LinkedHashMap()
        }
        return true
    }

    companion object {

        private var INSTANCE: ExpenseRepository? = null

        fun getInstance(expenseRemoteDataSource: ExpenseDataSource,
                        expenseLocalDataSource: ExpenseLocalDataSource): ExpenseRepository {
            if (INSTANCE == null)
                INSTANCE = ExpenseRepository(expenseRemoteDataSource, expenseLocalDataSource)

            return INSTANCE!!
        }
    }
}
