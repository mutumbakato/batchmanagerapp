package com.mutumbakato.batchmanager.data.models;

import java.util.Map;

/**
 * Created by cato on 1/6/18.
 */

public class Ingredients {

    private String id;
    private String name;
    private Map<String, String> nutrients;

    public Ingredients(String id, String name, Map<String, String> nutrients) {
        this.id = id;
        this.name = name;
        this.nutrients = nutrients;
    }


}
