package com.mutumbakato.batchmanager.data.models;

import androidx.room.Ignore;

import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by cato on 27/05/18.
 */
public class ActivityLog {

    private String id;
    private String action;
    private String description;
    private String actor;
    private String time;
    private String batchId;
    private String farmId;
    private String batchName;
    private String createdAt = null;

    public ActivityLog(String id, String action, String description, String actor, String batchId, String createdAt) {
        this.id = id;
        this.action = action;
        this.description = description;
        this.actor = actor;
        Calendar calendar = Calendar.getInstance();
        this.time = String.format(Locale.UK, " %1$tY-%1$tm-%1$td %1$tH:%1$tM", calendar);
        this.batchId = batchId;
        this.farmId = PreferenceUtils.INSTANCE.getFarmId();
        this.createdAt = createdAt;
    }

    @Ignore
    public ActivityLog() {
    }

    public String getId() {
        return id;
    }

    public String getAction() {
        return action;
    }

    public String getDescription() {
        return description;
    }

    public String getActor() {
        return actor;
    }

    public String getTime() {
        return time;
    }

    public String getCreatedAt() {
        if (createdAt != null)
            return createdAt;
        else return time;
    }

    public String getBatchId() {
        return batchId;
    }

    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }

    public String getBatchName() {
        return this.batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }
}
