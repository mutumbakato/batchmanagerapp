package com.mutumbakato.batchmanager.data.models

import androidx.lifecycle.LiveData

data class DataResource<T>(val data: LiveData<T>, val status: LiveData<Status>, val isLoading: LiveData<Boolean>)