package com.mutumbakato.batchmanager.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

const val TABLE_STORES = "stores"

@Entity(tableName = TABLE_STORES)
data class Store(@PrimaryKey @ColumnInfo(name = "_id") val id: String,
                 @ColumnInfo(name = "farm_id") val farmId: String,
                 val name: String,
                 val type: String,
                 @ColumnInfo(name = "created_at") val createdAt: String,
                 @ColumnInfo(name = "last_modified") val lastModified: Long)
