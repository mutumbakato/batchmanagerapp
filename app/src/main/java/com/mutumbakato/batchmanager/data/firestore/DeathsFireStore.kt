package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.DeathDataSource
import com.mutumbakato.batchmanager.data.models.Death
import java.util.*

/**
 * Created by cato on 3/26/18.
 */
class DeathsFireStore : DeathDataSource {

    private val db = FirebaseFirestore.getInstance().collection("deaths")

    override fun getMortalityRate(batchId: String, callback: DeathDataSource.OnMortalityRate) {
        //Implemented in the repository
    }

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Death>) {
        db.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val deaths = ArrayList<Death>()
                for (data in task.result!!) {
                    deaths.add(data.toObject(Death::class.java))
                }
                if (deaths.size > 0)
                    callback.onDataLoaded(deaths)
                else
                    callback.onEmptyData()
            } else {
                callback.onEmptyData()
            }
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Death>) {
        db.document(id).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callback.onDataItemLoaded(task.result!!.toObject(Death::class.java)!!)
            } else {
                callback.onDataItemNotAvailable()
            }
        }
    }

    override fun saveData(data: Death) {
        db.document(data.id).set(data)
    }

    override fun updateData(data: Death) {
        db.document(data.id).set(data)
    }

    override fun refreshData() {
        //Implemented in the repository
    }

    override fun deleteAllData(batchId: String) {
        //Don't do it!!!!
    }

    override fun deleteDataItem(item: Death, batchId: String) {
        db.document(item.id).delete()
    }

    override fun clearCache() {
        //Implemented in the repository
    }

    override fun notifyDataChanged() {

    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Death>, vararg referenceIds: String) {
        db.whereEqualTo("batchId", referenceIds[0]).addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val deaths = ArrayList<Death>()
                for (data in snapshots) {
                    deaths.add(data.toObject(Death::class.java))
                }
                callback.onDataLoaded(deaths)
            }
        }
    }
}
