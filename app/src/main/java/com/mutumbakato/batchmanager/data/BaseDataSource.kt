package com.mutumbakato.batchmanager.data


interface BaseDataSource<T> {

    fun getAll(batchId: String, callback: DataLoadCallback<T>)

    fun getOne(id: String, batchId: String, callback: DataItemCallback<T>)

    fun saveData(data: T)

    fun updateData(data: T)

    fun refreshData()

    fun deleteAllData(batchId: String)

    fun deleteDataItem(item: T, batchId: String)

    fun clearCache()

    fun notifyDataChanged()

    fun observeData(callback: DataLoadCallback<T>, vararg referenceIds: String)

    interface DataLoadCallback<T> {

        fun onDataLoaded(data: List<T>)

        fun onEmptyData()

        fun onError(message: String)
    }

    interface DataItemCallback<T> {

        fun onDataItemLoaded(data: T)

        fun onDataItemNotAvailable()

        fun onError(message: String)
    }
}
