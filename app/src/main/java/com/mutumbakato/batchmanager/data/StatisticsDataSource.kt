package com.mutumbakato.batchmanager.data

import androidx.lifecycle.LiveData
import com.mutumbakato.batchmanager.data.local.dao.StatisticsDao
import com.mutumbakato.batchmanager.data.models.*
import com.mutumbakato.batchmanager.data.models.exports.*
import com.mutumbakato.batchmanager.ui.StatisticsContract
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class StatisticsDataSource(private val statisticsDao: StatisticsDao) : StatisticsContract {

    fun getStatistics(batchId: String): Statistics {
        val statistics = Statistics()
        statistics.batch = getBatch(batchId)
        statistics.count = getCount(batchId)
        statistics.totalSales = totalSales(batchId)
        statistics.totalExpenditure = totalExpenses(batchId)
        statistics.mortality = mortality(batchId)
        statistics.eggCount = getEggCount(batchId)
        statistics.sales = sales(batchId)
        statistics.expenses = dailyExpenses(batchId)
        statistics.deaths = deaths(batchId)
        statistics.eggs = dailyEggs(batchId)
        statistics.henDay = dailyHenDay(batchId)
        statistics.sold = getSold(batchId)
        statistics.totalFeeds = totalFeeds(batchId)
        statistics.totalWater = totalWater(batchId)
        statistics.water = dailyWater(batchId)
        statistics.feeding = dailyFeeding(batchId)
        statistics.weight = dailyAverageWeight(batchId)
        statistics.batchCount = getBatchCount(batchId)
        statistics.avgWeight = averageWeight(batchId)
        statistics.costPerBird = costPerBird(batchId)
        statistics.lastFeedsTotal = getLastFeedsTotal(batchId)
        statistics.lastWaterTotal = getLastWaterTotal(batchId)
        statistics.eggsPercentage = getEggsPercentage(batchId)
        statistics.weightUniformityData = statisticsDao.getWeightUniformityData(batchId)
        return statistics
    }

    override fun getBatch(batchId: String): LiveData<Batch> {
        return statisticsDao.getLiveBatchById(batchId)
    }

    override fun getCount(batchId: String): LiveData<Int> {
        return statisticsDao.count(batchId)
    }

    override fun getBatchCount(batchId: String): LiveData<BatchCount> {
        return statisticsDao.batchCount(batchId)
    }

    override fun getSold(batchId: String): LiveData<Int> {
        return statisticsDao.soldBird(batchId)
    }

    override fun mortality(batchId: String): LiveData<Int> {
        return statisticsDao.mortalityRate(batchId)
    }

    override fun totalSales(batchId: String): LiveData<Float> {
        return statisticsDao.totalSales(batchId)
    }

    override fun totalExpenses(batchId: String): LiveData<Float> {
        return statisticsDao.getTotalExpenses(batchId)
    }

    override fun totalFeeds(batchId: String): LiveData<Float> {
        return statisticsDao.getTotalFeeds(batchId)
    }

    override fun totalWater(batchId: String): LiveData<Float> {
        return statisticsDao.getTotalWater(batchId)
    }

    override fun getEggCount(batchID: String): LiveData<EggCount> {
        return statisticsDao.totalEggs(batchID)
    }

    override fun sales(batchId: String): LiveData<List<EntryItem>> {
        return statisticsDao.geDailySales(batchId)
    }

    override fun deaths(batchId: String): LiveData<List<EntryItem>> {
        return statisticsDao.getDailyDeaths(batchId)
    }

    override fun dailyExpenses(batchID: String): LiveData<List<EntryItem>> {
        return statisticsDao.getDailyExpenses(batchID)
    }

    override fun dailyEggs(batchId: String): LiveData<List<EntryItem>> {
        return statisticsDao.getDailyEggs(batchId)
    }


    override fun dailyHenDay(batchId: String): LiveData<List<EntryItem>> {
        return statisticsDao.getDailyHenDay(batchId)
    }

    override fun dailyFeeding(batchId: String): LiveData<List<EntryItem>> {
        return statisticsDao.getDailyFeeds(batchId)
    }

    override fun dailyWater(batchId: String): LiveData<List<EntryItem>> {
        return statisticsDao.getDailyWater(batchId)
    }

    override fun dailyAverageWeight(batchId: String): LiveData<List<EntryItem>> {
        return statisticsDao.getDailyWeight(batchId)
    }

    override fun averageWeight(batchId: String): LiveData<Float> {
        return statisticsDao.getAvgWeight(batchId)
    }

    override fun costPerBird(batchId: String): LiveData<Float> {
        return statisticsDao.getCostPerBird(batchId)
    }

    override fun getLastFeedsTotal(batchId: String): LiveData<Float> {
        return statisticsDao.getLastFeedsTotal(batchId)
    }

    override fun getLastWaterTotal(batchId: String): LiveData<Float> {
        return statisticsDao.getLastWaterTotal(batchId)
    }

    override fun getEggsPercentage(batchId: String): LiveData<Float> {
        return statisticsDao.getEggsPercentage(batchId)
    }

    override fun getExportData(config: ExportConfig, onFinish: (data: ExportData) -> Unit, onError: (error: String) -> Unit) {

        var expenses: List<Expense>
        var sales: List<Sale>
        var deaths: List<DeathExport>
        var feeds: List<FeedsExport>
        var eggs: List<EggsExport>
        var weight: List<WeightExport>
        var water: List<WaterExport>

        doAsync {

            val batch = statisticsDao.getBatchById(config.batchId)
            deaths = statisticsDao.getBatchDeaths(config.batchId)
            expenses = statisticsDao.getBatchExpenses(config.batchId)
            feeds = statisticsDao.getBatchFeeding(config.batchId)
            sales = statisticsDao.getBatchSales(config.batchId)
            water = statisticsDao.getBatchWater(config.batchId)
            weight = statisticsDao.getBatchWeight(config.batchId)
            eggs = statisticsDao.getBatchEggs(config.batchId)

            uiThread {
                val exportData = ExportData(
                        deaths = deaths,
                        expenses = expenses,
                        sales = sales,
                        feeds = feeds,
                        eggs = eggs,
                        water = water,
                        weight = weight,
                        batch = batch
                )
                onFinish(exportData)
            }
        }
    }
}
