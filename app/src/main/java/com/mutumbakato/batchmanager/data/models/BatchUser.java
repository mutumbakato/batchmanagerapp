package com.mutumbakato.batchmanager.data.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import static com.mutumbakato.batchmanager.data.models.BatchUser.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class BatchUser {

    public static final String TABLE_NAME = "batch_users";

    public static final String[] ROLES = {"Admin", "Worker", "Guest"};

    public static final String BATCH_USER_TYPE_EMPLOYEE = "employee";

    public static final String BATCH_USER_TYPE_PBM_USER = "pbm_user";

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "_id")
    private String id;
    @ColumnInfo(name = "batch_id")
    private String batchId;
    @ColumnInfo(name = "user_id")
    private String userId;
    private String name;
    private String role;
    private String addedBy;
    private String date;
    private String extra;

    public BatchUser(@NonNull String id, String batchId, String userId,
                     String name, String role, String extra, String addedBy, String date) {
        this.id = id;
        this.batchId = batchId;
        this.userId = userId;
        this.role = role;
        this.addedBy = addedBy;
        this.date = date;
        this.extra = extra;
        this.name = name;
    }

    @Ignore
    public BatchUser() {

    }

    @Ignore
    public BatchUser(String batchId, String userId) {
        this.batchId = batchId;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public String getBatchId() {
        return batchId;
    }

    public String getUserId() {
        return userId;
    }

    public String getRole() {
        return role;
    }

    public String getAddedBy() {
        return addedBy;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getExtra() {
        return extra;
    }
}
