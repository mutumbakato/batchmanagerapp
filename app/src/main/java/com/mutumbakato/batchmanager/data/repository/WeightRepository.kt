package com.mutumbakato.batchmanager.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mutumbakato.batchmanager.data.WeightDataSource
import com.mutumbakato.batchmanager.data.local.WeightLocalDataSource
import com.mutumbakato.batchmanager.data.models.Weight
import com.mutumbakato.batchmanager.data.models.WeightResource
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.Logger
import com.mutumbakato.batchmanager.utils.DateUtils

class WeightRepository constructor(private val localDataSource: WeightLocalDataSource, private val remoteDataSource: WeightDataSource) {

    private val isLoading = MutableLiveData<Boolean>()
    private val error = MutableLiveData<String>()
    private var isOnline: Boolean = false

    fun createWeight(weight: Weight) {
        Logger.log(weight.batchId, Logger.ACTION_ADDED, " ${weight.weight} ${PreferenceUtils.weightUnits} to weight")
        isLoading.postValue(true)
        localDataSource.insert(weight) {
            isLoading.postValue(false)
        }
        remoteDataSource.createWeight(weight) {
            isLoading.postValue(false)
        }
    }

    fun listWeights(batchId: String): WeightResource {
        if (!isOnline) {
            listFromRemote(batchId)
            isOnline = true
        }
        val weights = localDataSource.listWeight(batchId)
        return WeightResource(weights, error, isLoading)
    }

    fun getWeightById(id: String): LiveData<Weight> {
        return localDataSource.getWeightById(id)
    }

    fun updateWeight(weight: Weight) {
        Logger.log(weight.batchId, Logger.ACTION_UPDATED, " weight for " + DateUtils.parseDateToddMMyyyy(weight.date))
        isLoading.postValue(true)
        localDataSource.update(weight) {
            isLoading.postValue(false)
        }
        remoteDataSource.updateWeight(weight) {
        }
    }

    fun deleteWeight(weight: Weight) {
        Logger.log(weight.batchId, Logger.ACTION_DELETED, " weight for " + DateUtils.parseDateToddMMyyyy(weight.date))
        isLoading.postValue(true)
        localDataSource.deleteWeight(weight) {
            isLoading.postValue(false)
        }
        remoteDataSource.deleteWeight(weight) {
            isLoading.postValue(false)
        }
    }

    private fun listFromRemote(batchId: String) {
        isLoading.postValue(true)
        remoteDataSource.listWeights(batchId) {
            localDataSource.deleteAllWeight(batchId)
            localDataSource.insertAll(it) {
                isLoading.postValue(false)
            }
        }
    }

    companion object {

        @Volatile
        private var INSTANCE: WeightRepository? = null

        fun getInstance(localDataSource: WeightLocalDataSource, remoteDataSource: WeightDataSource): WeightRepository {
            if (INSTANCE == null)
                INSTANCE = WeightRepository(localDataSource, remoteDataSource)
            return INSTANCE!!
        }
    }
}