package com.mutumbakato.batchmanager.data.remote;


import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.BaseDataSource;
import com.mutumbakato.batchmanager.data.VaccinationDataSource;
import com.mutumbakato.batchmanager.data.models.Vaccination;
import com.mutumbakato.batchmanager.data.models.VaccinationCheck;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VaccinationRemoteDataSource implements VaccinationDataSource {

    private static VaccinationRemoteDataSource INSTANCE;
    private VaccinationEndPoints mApi;

    private VaccinationRemoteDataSource(VaccinationEndPoints api) {
        mApi = api;
    }

    public static VaccinationRemoteDataSource getInstance(VaccinationEndPoints api) {
        if (INSTANCE == null) {
            INSTANCE = new VaccinationRemoteDataSource(api);
        }
        return INSTANCE;
    }

    @Override
    public void listVaccinations(String scheduleId, @NonNull final BaseDataSource.DataLoadCallback<Vaccination> callback) {
        mApi.fetchVaccinations(scheduleId).enqueue(new Callback<List<Vaccination>>() {
            @Override
            public void onResponse(@NonNull Call<List<Vaccination>> call, @NonNull Response<List<Vaccination>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    callback.onDataLoaded(response.body());
                } else {
                    callback.onEmptyData();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Vaccination>> call, @NonNull Throwable t) {
                callback.onError("Failed to fetch Vaccinations");
            }
        });
    }

    @Override
    public void getCheckList(String bachId, BaseDataSource.DataLoadCallback<VaccinationCheck> callback) {

    }

    @Override
    public void getVaccination(String scheduleId, @NonNull String vaccinationId, @NonNull BaseDataSource.DataItemCallback<Vaccination> callback) {
        callback.onDataItemNotAvailable();
    }

    @Override
    public void createVaccination(@NonNull Vaccination vaccination) {

    }

    @Override
    public void updateVaccination(@NonNull Vaccination vaccination) {
    }

    @Override
    public void removeFromComplete(String batchId, Vaccination vaccination) {

    }

    @Override
    public void addCheckItem(VaccinationCheck check) {

    }

    @Override
    public void clearCache() {

    }

    @Override
    public void observeVaccination(BaseDataSource.DataLoadCallback<Vaccination> callback, String scheduleId) {

    }

    @Override
    public void observeCheckList(String bachId, BaseDataSource.DataLoadCallback<VaccinationCheck> callback) {

    }

    @Override
    public void refreshVaccinations() {

    }

    @Override
    public void deleteVaccinations(String schedule, String id) {

    }

    @Override
    public void deleteAllVaccinations() {
    }

    @Override
    public void deleteAllChecks(String batchId) {

    }

    @Override
    public void addToComplete(String batchId, Vaccination vaccination) {

    }
}

