package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import com.mutumbakato.batchmanager.data.models.Store

@Dao
interface StoreDao {

    @Insert
    fun insert(store: Store)

    @Insert
    fun insertAll(stores: List<Store>)
    
}