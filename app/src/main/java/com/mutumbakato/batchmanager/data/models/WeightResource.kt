package com.mutumbakato.batchmanager.data.models

import androidx.lifecycle.LiveData

data class WeightResource(val data: LiveData<List<Weight>>, val error: LiveData<String>, val isLoading: LiveData<Boolean>)