package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

import com.mutumbakato.batchmanager.data.models.VaccinationCheck

@Dao
interface VaccinationCheckDao {

    /**
     * Select all vaccination_checks from the vaccination_check table.
     *
     * @return all vaccination_checks.
     */
    @get:Query("SELECT * FROM vaccination_check WHERE last_modified != 0 AND server_id != 0 AND status != 'trash'")
    val local: List<VaccinationCheck>

    /**
     * Select new vaccination_check
     *
     * @return vaccination_checks that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM vaccination_check WHERE server_id = 0")
    val new: List<VaccinationCheck>

    /**
     * Select locally updated vaccination_checks since the last synchronisation
     *
     * @return updated vaccination_check  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM vaccination_check WHERE server_id != 0 AND last_modified = 0 AND status != 'trash'")
    val updated: List<VaccinationCheck>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM vaccination_check ORDER BY server_id DESC LIMIT 1")
    val lastServerId: Long

    /**
     * Select vaccination_checks deleted from the locally
     *
     * @return server ids of vaccination_checks where status = trash ;
     */
    @get:Query("SELECT server_id FROM vaccination_check WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * @return list of Vaccination schedule objects
     */
    @Query("SELECT * FROM vaccination_check WHERE batch_id = :batchId AND status != 'trash' ")
    fun listChecks(batchId: String): List<VaccinationCheck>

    /**
     * Select a vaccination by name.
     *
     * @param checkId the vaccination name.
     * @return the vaccination with vaccinationId.
     */
    @Query("SELECT * FROM vaccination_check WHERE _id = :checkId AND status != 'trash' ")
    fun getChecksById(checkId: String): VaccinationCheck

    /**
     * Insert a schedule in the database. If the vaccination already exists, replace it.
     *
     * @param schedule the vaccination  schedule to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCheck(schedule: VaccinationCheck): Long

    /**
     * Update a vaccination schedule.
     *
     * @param check vaccination to be updated
     * @return the number of vaccination  schedules updated. This should always be 1.
     */
    @Update
    fun updateCheck(check: VaccinationCheck): Int

    /**
     * Delete a vaccination_check by id.
     */
    @Query("UPDATE vaccination_check SET status = 'trash' WHERE _id = :vaccination_checkId")
    fun deleteCheckById(vaccination_checkId: String)

    /**
     * Permanently deleteCategory all trashed entries
     */
    @Query("DELETE FROM vaccination_check WHERE status = 'trash'")
    fun clearTrash()

}

