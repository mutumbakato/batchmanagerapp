package com.mutumbakato.batchmanager.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mutumbakato.batchmanager.data.models.DataResource
import com.mutumbakato.batchmanager.data.models.Store

class StoresRepository {

    fun createStore(store: Store, onFinish: ((store: Store) -> Unit)? = null, onError: ((error: String) -> Unit)? = null) {

    }

    fun getStore(id: String): LiveData<Store> {
        return MutableLiveData()
    }

    fun listStores(farmId: String): DataResource<List<Store>> {
        TODO("List store implementation not available")
    }

    fun updateStore(store: Store, onFinish: ((store: Store) -> Unit)? = null, onError: ((error: String) -> Unit)? = null) {

    }

    fun deleteStore(store: Store) {

    }

}