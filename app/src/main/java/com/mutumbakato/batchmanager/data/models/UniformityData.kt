package com.mutumbakato.batchmanager.data.models

data class UniformityData(var date: String, var data: String)