package com.mutumbakato.batchmanager.data.sync;

/**
 * Created by cato on 11/4/17.
 */

public interface SyncListener {

    void onSuccess();

    void onFail();

}
