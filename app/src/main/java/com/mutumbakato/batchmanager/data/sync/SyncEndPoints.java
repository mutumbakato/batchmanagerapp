package com.mutumbakato.batchmanager.data.sync;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by cato on 10/21/17.
 */

public interface SyncEndPoints {
    @POST("sync")
    Call<String> sync(@Header("Authorization") String token, @Body String body);
}
