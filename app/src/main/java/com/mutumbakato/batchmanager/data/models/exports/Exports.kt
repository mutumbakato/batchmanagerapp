package com.mutumbakato.batchmanager.data.models.exports

import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.utils.DateUtils

data class Exports(val batch: Batch, val sortedData: MutableMap<String, Map<Int, Float>> = hashMapOf(), val isLaying: Boolean) {

    val days: Map<Int, String>
        get() {
            val value = hashMapOf<Int, String>()
            DateUtils.getDailyTimeline(batch.date!!, batch.closeDate, batch.type).forEach {
                value[it] = DateUtils.simpleDateFromAgeDays(batch.date ?: "", it)
            }
            return value
        }

    companion object {
        const val FEEDS = "feeds"
        const val WATER = "water"
        const val DEATHS = "deaths"
        const val WEIGHT = "weight"
        const val EGGS = "eggs"
        const val HEN_DAY = "hen_day"
        const val CUM_FEEDS = "cum_feeds"
        const val CUM_WATER = "cum_water"
        const val CUM_EGGS = "cum_eggs"
        const val CUM_DEATHS = "cum_deaths"
        const val WEIGHT_GAIN = "weight_gain"
        const val TOTAL_FEEDS = "total_feeds"
        const val TOTAL_WATER = "total_water"
    }
}
