package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.ExpenseCategory

interface ExpenseCategoriesDataSource {

    fun getAllCategories(callback: CategoriesLoadCallback)

    fun saveCategory(category: ExpenseCategory)

    fun updateCategory(category: ExpenseCategory)

    fun deleteCategory(id: String)

    fun deleteAll()

    fun clearCache()

    fun observeData(callback: CategoriesLoadCallback, vararg referenceIds: String)


    interface CategoriesLoadCallback {
        fun onCategoriesLoaded(categories: List<ExpenseCategory>)

        fun onCategoriesNotAvailable()
    }
}
