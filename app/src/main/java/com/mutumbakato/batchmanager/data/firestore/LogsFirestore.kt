package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.firestore.Source
import com.mutumbakato.batchmanager.data.ActivityLogDataSource
import com.mutumbakato.batchmanager.data.models.ActivityLog
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.utils.DateUtils
import java.util.*

class LogsFirestore : ActivityLogDataSource {

    private val batchCollection = FirebaseFirestore.getInstance().collection("batches")
    private val farmCollection = FirebaseFirestore.getInstance().collection("farms")

    override fun log(log: ActivityLog) {
        if (log.batchId != null && log.batchId.isNotEmpty()) {
            batchCollection.document(log.batchId).get().addOnSuccessListener {
                val batch = it.toObject(Batch::class.java)
                batch?.let {
                    log.batchName = batch.name
                    log.farmId = batch.farmId
                    farmCollection.document(log.farmId).collection("logs").document(log.id).set(log)
                }
            }
        } else {
            log.batchName = "Farm"
            log.farmId = PreferenceUtils.farmId
            farmCollection.document(log.farmId).collection("logs").document(log.id).set(log)
        }
    }

    override fun getLogs(farmId: String, callBack: ActivityLogDataSource.LogsCallBack) {
        farmCollection.document(farmId)
                .collection("logs")
                .orderBy("time", Query.Direction.DESCENDING)
                .limit(30).get(Source.CACHE)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        callBack.onLoad(ArrayList(task.result!!.toObjects(ActivityLog::class.java)), 0)
                    } else {
                        callBack.onError("Failed to get logs")
                    }
                }
    }

    override fun setListener(farmId: String, callBack: ActivityLogDataSource.LogsCallBack) {
        farmCollection.document(farmId)
                .collection("logs")
                .orderBy("time", Query.Direction.DESCENDING)
                .limit(30)
                .addSnapshotListener { snapshots, _ ->
                    var newCount = 0
                    val logs = ArrayList<ActivityLog>()
                    if (snapshots != null) {
                        logs.addAll(snapshots.toObjects(ActivityLog::class.java))
                        for (log in logs) {
                            if (DateUtils.cameAfter(log.time)) {
                                newCount++
                            }
                        }
                    }
                    callBack.onLoad(logs, newCount)
                }
        migrateLogs(farmId)
    }

    override fun loadMore(farmId: String, lastDate: String, callBack: ActivityLogDataSource.LogsCallBack) {
        val logs = ArrayList<ActivityLog>()
        farmCollection.document(farmId)
                .collection("logs")
                .orderBy("time", Query.Direction.DESCENDING)
                .startAfter(lastDate)
                .limit(30)
                .get()
                .addOnCompleteListener { task ->
                    if (task.result != null)
                        logs.addAll(task.result!!.toObjects(ActivityLog::class.java))
                    callBack.onLoad(logs, 0)
                }
    }

    private fun migrateLogs(farmId: String) {
        batchCollection.whereEqualTo("farmId", farmId).get().addOnSuccessListener {
            val batches = it.toObjects(Batch::class.java)
            for (batch in batches) {
                moveLogs(batch)
            }
        }
    }

    private fun moveLogs(batch: Batch) {
        batchCollection.document(batch.id).collection("logs").get().addOnSuccessListener {
            val logs = it.toObjects(ActivityLog::class.java)
            for (log in logs) {
                log.farmId = batch.farmId
                log.batchName = batch.name
                farmCollection.document(batch.farmId).collection("logs").document(log.id).set(log)
                //Delete log after migration
                batchCollection.document(batch.id).collection("logs").document(log.id).delete()
            }
        }
    }

    companion object {

        internal val instance = LogsFirestore()
    }
}
