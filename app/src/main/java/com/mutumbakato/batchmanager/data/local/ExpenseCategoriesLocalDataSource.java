package com.mutumbakato.batchmanager.data.local;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.mutumbakato.batchmanager.data.ExpenseCategoriesDataSource;
import com.mutumbakato.batchmanager.data.local.dao.ExpenseCategoriesDao;
import com.mutumbakato.batchmanager.data.models.ExpenseCategory;
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;
import com.mutumbakato.batchmanager.data.sync.Syncable;
import com.mutumbakato.batchmanager.utils.executors.AppExecutors;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;


public class ExpenseCategoriesLocalDataSource implements ExpenseCategoriesDataSource, Syncable<ExpenseCategory> {

    private static ExpenseCategoriesLocalDataSource INSTANCE = null;
    private AppExecutors mAppExecutors;
    private ExpenseCategoriesDao mExpenseCategoriesDao;

    private ExpenseCategoriesLocalDataSource(AppExecutors appExecutors, ExpenseCategoriesDao expenseCategoriesDao) {
        mAppExecutors = appExecutors;
        mExpenseCategoriesDao = expenseCategoriesDao;
    }

    public static ExpenseCategoriesLocalDataSource getInstance(AppExecutors appExecutors, ExpenseCategoriesDao expenseCategoriesDao) {
        if (INSTANCE == null)
            INSTANCE = new ExpenseCategoriesLocalDataSource(appExecutors, expenseCategoriesDao);
        return INSTANCE;
    }

    @Override
    public void getAllCategories(@NonNull final CategoriesLoadCallback callback) {
        Runnable runnable = () -> {
            final List<ExpenseCategory> categories = mExpenseCategoriesDao.getCategories();
            mAppExecutors.mainThread().execute(() -> {
                if (categories.size() > 0) {
                    callback.onCategoriesLoaded(categories);
                } else
                    callback.onCategoriesNotAvailable();
            });
        };
        mAppExecutors.diskIO().execute(runnable);
    }

    @Override
    public void saveCategory(@NonNull final ExpenseCategory category) {
        mAppExecutors.diskIO().execute(() -> mExpenseCategoriesDao.insertCategory(category));
    }

    @Override
    public void updateCategory(@NonNull final ExpenseCategory category) {
        mAppExecutors.diskIO().execute(() -> mExpenseCategoriesDao.updateExpense(category));
    }

    @Override
    public void deleteCategory(@NonNull final String id) {
        mAppExecutors.diskIO().execute(() -> mExpenseCategoriesDao.deleteCategoryById(id));
    }

    @Override
    public void deleteAll() {
        mAppExecutors.diskIO().execute(() -> mExpenseCategoriesDao.deleteCategories());
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void observeData(CategoriesLoadCallback callback, String... referenceIds) {

    }

    @Override
    public String getTable() {
        return ExpenseCategory.TABLE_NAME;
    }

    @Override
    public List<ExpenseCategory> getNew() {
        return mExpenseCategoriesDao.getNew();
    }

    @Override
    public List<ExpenseCategory> getUpdated() {
        return mExpenseCategoriesDao.getUpdated();
    }

    @Override
    public List<ExpenseCategory> getLocalData() {
        return mExpenseCategoriesDao.getLocal();
    }

    @Override
    public List<Long> getTrash() {
        return mExpenseCategoriesDao.getTrash();
    }

    @Override
    public long getLastServerId() {
        return mExpenseCategoriesDao.getLastServerId();
    }

    @Override
    public void handleNewData(JSONArray newData) {
        Gson gson = new Gson();
        for (int n = 0; n < newData.length(); n++) {
            try {
                ExpenseCategory expense = gson.fromJson(newData.get(n).toString(), ExpenseCategory.class);
                ExpenseCategory newExpense = new ExpenseCategory(expense.getId(), expense.getName(),
                        expense.getServerId(), expense.getStatus(), expense.getLastModified(), PreferenceUtils.INSTANCE.getFarmId());
                mExpenseCategoriesDao.insertCategory(newExpense);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleModified(JSONArray modified) {
        Gson gson = new Gson();
        for (int n = 0; n < modified.length(); n++) {
            try {
                ExpenseCategory expense = gson.fromJson(modified.get(n).toString(),
                        ExpenseCategory.class);
                mExpenseCategoriesDao.updateExpense(expense);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSynced(JSONArray synced) {
        for (int n = 0; n < synced.length(); n++) {
            try {
                String id = synced.getJSONObject(n).getString("id");
                long serverId = synced.getJSONObject(n).getLong("serverId");
                long lastMod = synced.getJSONObject(n).getLong("lastModified");
                ExpenseCategory expense = mExpenseCategoriesDao.getExpenseById(id);
                ExpenseCategory syncedExpense = new ExpenseCategory(expense.getId(), expense.getName(),
                        serverId, expense.getStatus(), lastMod, PreferenceUtils.INSTANCE.getFarmId());
                mExpenseCategoriesDao.updateExpense(syncedExpense);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleUpdates(JSONArray updates) {
        for (int n = 0; n < updates.length(); n++) {
            try {
                String id = updates.getJSONObject(n).getString("id");
                long last_mod = updates.getJSONObject(n).getLong("lastModified");
                ExpenseCategory expense = mExpenseCategoriesDao.getExpenseById(id);
                ExpenseCategory newExpense = new ExpenseCategory(expense.getId(), expense.getName(),
                        expense.getServerId(), expense.getStatus(), last_mod, PreferenceUtils.INSTANCE.getFarmId());
                mExpenseCategoriesDao.updateExpense(newExpense);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleTrash(String[] trash) {
        for (String aTrash : trash) {
            mExpenseCategoriesDao.deleteCategoryById(aTrash);
        }
        mExpenseCategoriesDao.clearTrash();
    }

    @Override
    public void notifyRepository() {

    }
}
