package com.mutumbakato.batchmanager.data.models;

import androidx.lifecycle.LiveData;

import java.util.List;

public class Statistics {

    public LiveData<Batch> batch;

    public LiveData<Integer> count;

    public LiveData<BatchCount> batchCount;

    public LiveData<Integer> mortality;

    public LiveData<Integer> sold;

    public LiveData<Float> totalExpenditure;

    public LiveData<Float> totalSales;

    public LiveData<Float> costPerBird;

    public LiveData<Float> totalFeeds;

    public LiveData<Float> totalWater;

    public LiveData<EggCount> eggCount;

    public LiveData<List<EntryItem>> sales;

    public LiveData<List<EntryItem>> expenses;

    public LiveData<List<EntryItem>> eggs;

    public LiveData<List<EntryItem>> henDay;

    public LiveData<List<EntryItem>> deaths;

    public LiveData<List<EntryItem>> feeding;

    public LiveData<List<EntryItem>> water;

    public LiveData<List<EntryItem>> weight;

    public LiveData<Float> avgWeight;

    public LiveData<Float> lastWaterTotal;

    public LiveData<Float> lastFeedsTotal;

    public LiveData<Float> eggsPercentage;

    public LiveData<List<UniformityData>> eggsUniformityData;

    public LiveData<List<UniformityData>> weightUniformityData;

}
