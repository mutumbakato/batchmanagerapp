package com.mutumbakato.batchmanager.data.remote

import android.location.Location
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.pixplicity.easyprefs.library.Prefs

import org.json.JSONException
import org.json.JSONObject

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by cato on 10/12/17.
 */
class UserRemoteDataSource private constructor(private val mApi: UserEndPoints) : UserDataSource {
    private var counter = 0
    private var counterLogin = 0

    override fun login(email: String, password: String, callBack: UserDataSource.UserCallBack) {
        mApi.login(email, password).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    if (response.body() == null && counterLogin < 4) {
                        counterLogin++
                        login(email, password, callBack)
                        return
                    } else {
                        counterLogin = 0
                    }
                    val jsonObject = JSONObject(response.body())
                    if (!jsonObject.getBoolean("error")) {
                        callBack.onSuccess(handleUserData(jsonObject.getJSONObject("data")))
                    } else {
                        callBack.onError(jsonObject.getString("message"))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    callBack.onError("Error while logging in")
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                t.printStackTrace()
                callBack.onError("Error while logging in")
            }
        })
    }

    override fun register(user: User, callBack: UserDataSource.UserCallBack) {
        mApi.register(user.name,
                user.email,
                user.phone,
                user.password).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                //Try again if it failed less 4 times
                if (response.body() == null && counter < 4) {
                    counter++
                    register(user, callBack)
                    return
                } else {
                    counter = 0
                }
                try {
                    val jsonObject = JSONObject(response.body())
                    if (!jsonObject.getBoolean("error")) {
                        handleUserData(jsonObject.getJSONObject("data"))
                        callBack.onSuccess(null)
                    } else {
                        callBack.onError(jsonObject.getString("message"))
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    callBack.onError("Error while logging in")
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                t.printStackTrace()
                callBack.onError("Error while logging in")
            }
        })
    }

    override fun logOut(callBack: UserDataSource.UserCallBack) {
        mApi.logOut(PreferenceUtils.userToken).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                callBack.onSuccess(null)
            }

            override fun onFailure(call: Call<String>, throwable: Throwable) {
                callBack.onSuccess(null)
            }
        })
    }

    override fun resetPassword(email: String, callBack: UserDataSource.PasswordCallBack) {
        mApi.reset(email).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                try {
                    val jsonObject = JSONObject(response.body())
                    if (jsonObject.getBoolean("error"))
                        callBack.onSuccess(jsonObject.getString("message"))
                    else
                        callBack.onSuccess(jsonObject.getString("data"))
                } catch (e: Exception) {
                    e.printStackTrace()
                    callBack.onError("Unknown response please try again")
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                callBack.onError("There was a connection problem")
            }
        })
    }

    override fun getUserById(id: String, callBack: UserDataSource.UserCallBack) {
        mApi.getUserById(id).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                val res = response.body()
                try {
                    val json = JSONObject(res)
                    if (json.getBoolean("error")) {
                        callBack.onError(json.getString("message"))
                    } else {
                        val user = User(json.getJSONObject("data").toString())
                        callBack.onSuccess(user)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    callBack.onError("Failed to get user")
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                callBack.onError("Failed to get user, please try again!")
            }
        })
    }

    override fun getUserByEmail(email: String, callBack: UserDataSource.UserCallBack) {
        mApi.getUserByEmail(email).enqueue(object : Callback<String> {
            override fun onResponse(call: Call<String>, response: Response<String>) {
                val res = response.body()
                try {
                    val json = JSONObject(res)
                    if (json.getBoolean("error")) {
                        callBack.onError("Failed, " + json.getString("message"))
                    } else {
                        val user = User(json.getJSONObject("data").toString())
                        callBack.onSuccess(user)
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                    callBack.onError("Failed to get user")
                }

            }

            override fun onFailure(call: Call<String>, t: Throwable) {
                callBack.onError("Failed to get user, please try again!")
            }
        })
    }

    override fun updateUser(user: User, callBack: UserDataSource.UserCallBack) {

    }

    override fun deleteAccount(user: User, callBack: UserDataSource.UserCallBack) {

    }

    override fun setLocation(uid: String, location: Location) {

    }

    @Throws(JSONException::class)
    private fun handleUserData(data: JSONObject): User {

        val id = data.getString("id")
        val username = data.getString("username")
        val email = data.getString("email")
        val token = data.getString("token")
        val licence = data.getString("licence")

        Prefs.putString(PreferenceUtils.USER_ID, id)
        Prefs.putString(PreferenceUtils.USER, username)
        Prefs.putString(PreferenceUtils.USER_TOKEN, token)
        Prefs.putString(PreferenceUtils.USER_EMAIL, email)
        Prefs.putString(PreferenceUtils.LICENCE, licence)

        return User(data.toString())
    }

    companion object {

        private var INSTANCE: UserRemoteDataSource? = null

        fun getInstance(api: UserEndPoints): UserRemoteDataSource {
            if (INSTANCE == null) {
                INSTANCE = UserRemoteDataSource(api)
            }
            return INSTANCE as UserRemoteDataSource
        }
    }

}
