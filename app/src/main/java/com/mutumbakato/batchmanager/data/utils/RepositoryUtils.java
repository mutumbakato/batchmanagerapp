package com.mutumbakato.batchmanager.data.utils;

import android.content.Context;

import com.mutumbakato.batchmanager.data.StatisticsDataSource;
import com.mutumbakato.batchmanager.data.UserDataSource;
import com.mutumbakato.batchmanager.data.firestore.BatchFireStore;
import com.mutumbakato.batchmanager.data.firestore.DeathsFireStore;
import com.mutumbakato.batchmanager.data.firestore.EggsFireStore;
import com.mutumbakato.batchmanager.data.firestore.ExpensesFireStore;
import com.mutumbakato.batchmanager.data.firestore.FarmFireStore;
import com.mutumbakato.batchmanager.data.firestore.FeedingFireStore;
import com.mutumbakato.batchmanager.data.firestore.FeedsFireStore;
import com.mutumbakato.batchmanager.data.firestore.SalesFireStore;
import com.mutumbakato.batchmanager.data.firestore.UserFireStore;
import com.mutumbakato.batchmanager.data.firestore.VaccinationFireStore;
import com.mutumbakato.batchmanager.data.firestore.WaterFirestore;
import com.mutumbakato.batchmanager.data.firestore.WeightFireStore;
import com.mutumbakato.batchmanager.data.local.BatchLocalDataSource;
import com.mutumbakato.batchmanager.data.local.DeathLocalDataSource;
import com.mutumbakato.batchmanager.data.local.EggsLocalDataSource;
import com.mutumbakato.batchmanager.data.local.ExpenseCategoriesLocalDataSource;
import com.mutumbakato.batchmanager.data.local.ExpenseLocalDataSource;
import com.mutumbakato.batchmanager.data.local.FarmLocalDataSource;
import com.mutumbakato.batchmanager.data.local.FeedingLocalDataSource;
import com.mutumbakato.batchmanager.data.local.FeedsLocalDataSource;
import com.mutumbakato.batchmanager.data.local.SalesLocalSource;
import com.mutumbakato.batchmanager.data.local.UserLocalDataSource;
import com.mutumbakato.batchmanager.data.local.VaccinationCheckLocalDataSource;
import com.mutumbakato.batchmanager.data.local.VaccinationLocalDataSource;
import com.mutumbakato.batchmanager.data.local.VaccinationScheduleLocalDataSource;
import com.mutumbakato.batchmanager.data.local.WaterLocalDataSource;
import com.mutumbakato.batchmanager.data.local.WeightLocalDataSource;
import com.mutumbakato.batchmanager.data.local.db.BManagerDatabase;
import com.mutumbakato.batchmanager.data.remote.UserEndPoints;
import com.mutumbakato.batchmanager.data.remote.UserRemoteDataSource;
import com.mutumbakato.batchmanager.data.repository.BatchRepository;
import com.mutumbakato.batchmanager.data.repository.DeathRepository;
import com.mutumbakato.batchmanager.data.repository.EggsRepository;
import com.mutumbakato.batchmanager.data.repository.ExpenseCategoriesRepository;
import com.mutumbakato.batchmanager.data.repository.ExpenseRepository;
import com.mutumbakato.batchmanager.data.repository.ExpensesLiveRepository;
import com.mutumbakato.batchmanager.data.repository.FarmRepository;
import com.mutumbakato.batchmanager.data.repository.FeedingRepository;
import com.mutumbakato.batchmanager.data.repository.FeedsRepository;
import com.mutumbakato.batchmanager.data.repository.SalesLiveRepository;
import com.mutumbakato.batchmanager.data.repository.SalesRepository;
import com.mutumbakato.batchmanager.data.repository.UserRepository;
import com.mutumbakato.batchmanager.data.repository.VaccinationRepository;
import com.mutumbakato.batchmanager.data.repository.VaccinationScheduleRepository;
import com.mutumbakato.batchmanager.data.repository.WaterRepository;
import com.mutumbakato.batchmanager.data.repository.WeightRepository;
import com.mutumbakato.batchmanager.utils.executors.AppExecutors;

/**
 * Created by cato on 3/25/18.
 */
public class RepositoryUtils {

    public static BatchRepository getBatchRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return BatchRepository.Companion.getInstance(new BatchFireStore(),
                BatchLocalDataSource.Companion.getInstance(new AppExecutors(), db.batchDao()));
    }

    public static UserRepository getUserRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        UserRemoteDataSource remoteDataSource = UserRemoteDataSource.Companion.getInstance(
                (UserEndPoints) ApiUtils.getEndPoints(UserEndPoints.class));
        UserDataSource firebaseDataSource = new UserFireStore();
        UserLocalDataSource localDataSource = UserLocalDataSource.Companion.getInstance(db.userDao(), new AppExecutors());
        return UserRepository.Companion.getInstance(localDataSource, remoteDataSource, firebaseDataSource);
    }

    public static VaccinationRepository getVaccinationRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return VaccinationRepository.Companion.getInstance(new VaccinationFireStore(),
                VaccinationLocalDataSource.getInstance(new AppExecutors(), db.vaccinationDao(),
                        VaccinationCheckLocalDataSource.getInstance(db.vaccinationCheckDao())));
    }

    public static VaccinationScheduleRepository getScheduleRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return VaccinationScheduleRepository.Companion.getInstance(new VaccinationFireStore(),
                VaccinationScheduleLocalDataSource.getInstance(new AppExecutors(), db.vaccinationScheduleDao()));
    }

    public static ExpenseRepository getExpensesRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return ExpenseRepository.Companion.getInstance(new ExpensesFireStore(),
                ExpenseLocalDataSource.Companion.getInstance(new AppExecutors(), db.expenseDao()));
    }

    public static ExpensesLiveRepository getExpensesLiveRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return ExpensesLiveRepository.Companion.getInstance(ExpenseLocalDataSource.Companion.getInstance(new AppExecutors(), db.expenseDao()),
                new ExpensesFireStore());
    }

    public static ExpenseCategoriesRepository getExpenseCategoryRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return ExpenseCategoriesRepository.Companion.getInstance(new ExpensesFireStore(),
                ExpenseCategoriesLocalDataSource.getInstance(new AppExecutors(),
                        db.expenseCategoriesDao()));
    }

    public static EggsRepository getEggsRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return EggsRepository.Companion.getInstance(new EggsFireStore(),
                EggsLocalDataSource.getInstance(new AppExecutors(), db.eggsDao()));

    }

    public static StatisticsDataSource getStatisticsDataSource(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return new StatisticsDataSource(db.statisticsDao());
    }

    public static FeedingRepository getFeedingRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return FeedingRepository.Companion.getInstance(new FeedingFireStore(),
                FeedingLocalDataSource.getInstance(new AppExecutors(), db.feedingDao()));
    }

    public static FeedsRepository getFeedsRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return FeedsRepository.Companion.getInstance(new FeedsLocalDataSource(db.feedsDao(), new AppExecutors()),
                new FeedsFireStore());
    }

    public static SalesRepository getSalesRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return SalesRepository.Companion.getInstance(new SalesFireStore(),
                SalesLocalSource.Companion.getInstance(new AppExecutors(), db.salesDao()));

    }

    public static SalesLiveRepository getSalesLiveRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return SalesLiveRepository.Companion.getInstance(
                SalesLocalSource.Companion.getInstance(new AppExecutors(), db.salesDao()), new SalesFireStore());

    }

    public static DeathRepository getDeathRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return DeathRepository.Companion.getInstance(new DeathsFireStore(),
                DeathLocalDataSource.getInstance(new AppExecutors(), db.deathDao()));

    }

    public static FarmRepository getFarmRepo(Context context) {
        BManagerDatabase db = BManagerDatabase.getInstance(context);
        return FarmRepository.Companion.getInstance(new FarmFireStore(), FarmLocalDataSource.Companion.getInstance(
                db.farmDao()));
    }

    public static WeightRepository getWeightRepository(Context context) {
        AppExecutors executors = new AppExecutors();
        WeightLocalDataSource ldc = new WeightLocalDataSource(BManagerDatabase.getInstance(context).weightDao(), executors);
        return WeightRepository.Companion.getInstance(ldc, new WeightFireStore());
    }

    public static WaterRepository getWaterRepository(Context context) {
        AppExecutors executors = new AppExecutors();
        WaterLocalDataSource ldc = new WaterLocalDataSource(BManagerDatabase.getInstance(context).waterDao(), executors);
        return WaterRepository.Companion.getInstance(ldc, new WaterFirestore());
    }


}
