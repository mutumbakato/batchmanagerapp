package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.ExpenseCategoriesDataSource
import com.mutumbakato.batchmanager.data.models.ExpenseCategory
import java.util.*

class ExpenseCategoriesRepository private constructor(private val mExpenseCategoriesRemoteDataSource: ExpenseCategoriesDataSource, private val mExpensesCategoriesLocalDataSource: ExpenseCategoriesDataSource) : ExpenseCategoriesDataSource {
    private var mCachedCategories: MutableMap<String, ExpenseCategory>? = null
    private var mCacheIsDirty = false

    override fun getAllCategories(callback: ExpenseCategoriesDataSource.CategoriesLoadCallback) {
        observeData(callback)
        if (mCachedCategories != null && !mCacheIsDirty) {
            callback.onCategoriesLoaded(ArrayList(mCachedCategories!!.values))
        } else {
            mExpensesCategoriesLocalDataSource.getAllCategories(object : ExpenseCategoriesDataSource.CategoriesLoadCallback {
                override fun onCategoriesLoaded(categories: List<ExpenseCategory>) {
                    refreshCache(categories)
                    callback.onCategoriesLoaded(categories)
                }

                override fun onCategoriesNotAvailable() {
                    getCategoriesFromRemoteSource(callback)
                }
            })
        }
    }

    override fun saveCategory(category: ExpenseCategory) {
        mExpensesCategoriesLocalDataSource.saveCategory(category)
        mExpenseCategoriesRemoteDataSource.saveCategory(category)
        if (mCachedCategories == null)
            mCachedCategories = LinkedHashMap()
        mCachedCategories!![category.id] = category
    }

    override fun updateCategory(category: ExpenseCategory) {
        mExpensesCategoriesLocalDataSource.updateCategory(category)
        mExpenseCategoriesRemoteDataSource.updateCategory(category)
        if (mCachedCategories == null)
            mCachedCategories = LinkedHashMap()
        mCachedCategories!![category.id] = category
    }

    override fun deleteCategory(id: String) {
        mExpensesCategoriesLocalDataSource.deleteCategory(id)
        mExpenseCategoriesRemoteDataSource.deleteCategory(id)
        if (mCachedCategories == null)
            mCachedCategories = LinkedHashMap()
        mCachedCategories!!.remove(id)
    }

    override fun deleteAll() {
        mExpensesCategoriesLocalDataSource.deleteAll()
        mExpenseCategoriesRemoteDataSource.deleteAll()
        if (mCachedCategories == null)
            mCachedCategories = LinkedHashMap()
        mCachedCategories!!.clear()
    }

    override fun clearCache() {

    }

    override fun observeData(callback: ExpenseCategoriesDataSource.CategoriesLoadCallback, vararg referenceIds: String) {
        mExpenseCategoriesRemoteDataSource.getAllCategories(object : ExpenseCategoriesDataSource.CategoriesLoadCallback {
            override fun onCategoriesLoaded(categories: List<ExpenseCategory>) {
                callback.onCategoriesLoaded(categories)
                refreshLocalDataSource(categories)
            }

            override fun onCategoriesNotAvailable() {

            }
        })
    }

    private fun getCategoriesFromRemoteSource(callback: ExpenseCategoriesDataSource.CategoriesLoadCallback) {
        mExpenseCategoriesRemoteDataSource.getAllCategories(object : ExpenseCategoriesDataSource.CategoriesLoadCallback {
            override fun onCategoriesLoaded(categories: List<ExpenseCategory>) {
                if (categories.isNotEmpty()) {
                    refreshLocalDataSource(categories)
                    callback.onCategoriesLoaded(categories)
                } else
                    callback.onCategoriesNotAvailable()
            }

            override fun onCategoriesNotAvailable() {
                callback.onCategoriesNotAvailable()
            }
        })
    }

    private fun refreshCache(categories: List<ExpenseCategory>) {
        if (mCachedCategories == null)
            mCachedCategories = LinkedHashMap()
        for (category in categories) {
            mCachedCategories!![category.id] = category
        }
        mCacheIsDirty = false
    }

    private fun refreshLocalDataSource(categories: List<ExpenseCategory>) {
        mExpensesCategoriesLocalDataSource.deleteAll()
        for (category in categories) {
            mExpensesCategoriesLocalDataSource.saveCategory(category)
        }
        refreshCache(categories)
    }

    companion object {

        private var INSTANCE: ExpenseCategoriesRepository? = null

        fun getInstance(expenseCategoriesRemoteDataSource: ExpenseCategoriesDataSource, expenseCategoriesLocalDataSource: ExpenseCategoriesDataSource): ExpenseCategoriesRepository {
            if (INSTANCE == null)
                INSTANCE = ExpenseCategoriesRepository(expenseCategoriesRemoteDataSource, expenseCategoriesLocalDataSource)
            return INSTANCE as ExpenseCategoriesRepository
        }
    }
}
