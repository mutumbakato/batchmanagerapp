package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.*
import com.mutumbakato.batchmanager.data.models.*

/**
 * Data Access Object for the batches table.
 */
@Dao
interface BatchDao {
    /**
     * Select all batches from the batches table.
     *
     * @return all batches.
     */
    @get:Query("SELECT * FROM batches WHERE (status != 'trash' AND status != 'closed')")
    val allBatches: List<Batch>

    /**
     * Select all batches from the batches table.
     *
     * @return all batches.
     */
    @get:Query("SELECT * FROM batches WHERE last_modified != 0 AND server_id != 0")
    val local: List<Batch>

    /**
     * Select new batches
     *
     * @return batches that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM batches WHERE server_id = 0")
    val new: List<Batch>

    /**
     * Select locally updated batches since the last synchronisation
     *
     * @return updated batches  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM batches WHERE server_id != 0 AND last_modified = 0")
    val updated: List<Batch>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM batches ORDER BY server_id DESC LIMIT 1")
    val lastServerId: Long

    /**
     * Select batches deleted from the locally
     *
     * @return server ids of batches where status = trash ;
     */
    @get:Query("SELECT server_id FROM batches WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * Select all batches from the batches table.
     *
     * @return all batches.
     */
    @Query("SELECT * FROM batches WHERE farmId = :farmId AND (status != 'trash' AND status != 'closed')")
    fun getAllBatches(farmId: String): List<Batch>

    /**
     * Select all batches from the batches table.
     *
     * @return all batches.
     */
    @Query("SELECT * FROM batches WHERE farmId =:farmId AND  status = 'closed'")
    fun getArchivedBatches(farmId: String): List<Batch>

    /**
     * Select a batch by name.
     *
     * @param batchId the batch name.
     * @return the batch with batchId.
     */
    @Query("SELECT * FROM batches WHERE _id = :batchId")
    fun getBatchById(batchId: String): Batch

    /**
     * Insert a batch in the database. If the batch already exists, replace it.
     *
     * @param batch the batch to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBatch(batch: Batch): Long

    /**
     * Update a batch.
     *
     * @param batch batch to be updated
     * @return the number of batches updated. This should always be 1.
     */
    @Update
    fun updateBatch(batch: Batch): Int

    /**
     * Delete a batch by name.
     *
     * @return the number of batches deleted. This should always be 1.
     */
    @Query("UPDATE batches SET status = 'trash' WHERE _id = :batchId")
    fun deleteBatchById(batchId: String): Int

    /**
     * Delete all batches.
     */
    @Query("DELETE FROM batches WHERE farmId = :farmId")
    fun deleteAllMyBatches(farmId: String)

    @Query("DELETE FROM batches WHERE farmId != :farmId")
    fun deleteAllOtherBatches(farmId: String)

    /**
     * @param batchUser batch user
     * @return
     */
    @Insert
    fun addBatchUser(batchUser: BatchUser): Long

    @Query("DELETE FROM batch_users WHERE batch_id = :batchId AND user_id = :userId")
    fun removeBatchUser(batchId: String, userId: String): Int

    @Query("SELECT * FROM batch_users WHERE batch_id = :batchId AND user_id = :userId")
    fun getBatchUser(batchId: String, userId: String): BatchUser

    @Query("SELECT * FROM batch_users WHERE batch_id = :batchId")
    fun getBatchUsers(batchId: String): List<BatchUser>

    @Update
    fun updateBatchUser(user: BatchUser): Int

    @Query("DELETE FROM batch_users WHERE batch_id = :batchId")
    fun deleteALlBatchUsers(batchId: String): Int

    @Query("UPDATE expenses SET status = 'trash' WHERE batch_id = :batchId")
    fun deleteExpenses(batchId: String)

    @Query("UPDATE  sales SET status= 'trash' WHERE batch_id = :batchId")
    fun deleteSales(batchId: String)

    @Query("UPDATE  vaccination_check SET status= 'trash' WHERE batch_id = :batchId")
    fun deleteVaccinationChecks(batchId: String)

    @Query("UPDATE  death SET status= 'trash' WHERE batch_id = :batchId")
    fun deleteDeaths(batchId: String)

    @Query("UPDATE  feeding SET status= 'trash' WHERE batch_id = :batchId")
    fun deleteFeeds(batchId: String)

    @Query("UPDATE  eggs SET status= 'trash' WHERE batch_id = :batchId")
    fun deleteEggs(batchId: String)

    @Query("SELECT * FROM feeds WHERE batch_id= :batchId and STRFTIME('%Y-%m-%d',date)  = :date")
    fun todayFeeds(batchId: String, date: String): List<Feeds>

    @Query("SELECT * FROM water WHERE batch_id= :batchId and STRFTIME('%Y-%m-%d',date)  = :date")
    fun todayWater(batchId: String, date: String): List<Water>

    @Query("SELECT * FROM death WHERE batch_id= :batchId and STRFTIME('%Y-%m-%d',date)  = :date")
    fun todayDeaths(batchId: String, date: String): List<Death>

    @Query("SELECT * FROM weight WHERE batch_id= :batchId and STRFTIME('%Y-%m-%d',date)  = :date")
    fun todayWeight(batchId: String, date: String): List<Weight>

    @Query("SELECT * FROM eggs WHERE batch_id= :batchId and STRFTIME('%Y-%m-%d',date)  = :date")
    fun todayEggs(batchId: String, date: String): List<Eggs>
}
