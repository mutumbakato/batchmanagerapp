package com.mutumbakato.batchmanager.data.remote;

import android.os.Handler;

import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.ExpenseDataSource;
import com.mutumbakato.batchmanager.data.models.Expense;

import java.util.LinkedHashMap;
import java.util.Map;


public class ExpenseRemoteDataSource implements ExpenseDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 3000;
    private final static Map<String, Expense> EXPENSES_SERVICE_DATA;
    private static ExpenseRemoteDataSource INSTANCE = null;

    static {
        EXPENSES_SERVICE_DATA = new LinkedHashMap<>(2);
    }

    private ExpenseRemoteDataSource() {

    }

    public static ExpenseRemoteDataSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new ExpenseRemoteDataSource();
        return INSTANCE;
    }

    @Override
    public void getAll(@NonNull String batchId, @NonNull final DataLoadCallback<Expense> callback) {
        callback.onEmptyData();
    }

    @Override
    public void getOne(@NonNull final String id, @NonNull String batchId, @NonNull final DataItemCallback<Expense> callback) {
        new Handler().postDelayed(() -> callback.onDataItemLoaded(EXPENSES_SERVICE_DATA.get(id)), SERVICE_LATENCY_IN_MILLIS);
    }

    @Override
    public void saveData(@NonNull Expense data) {
        EXPENSES_SERVICE_DATA.put(data.getId(), data);
    }

    @Override
    public void updateData(@NonNull Expense data) {
        EXPENSES_SERVICE_DATA.put(data.getId(), data);
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllData(@NonNull String batchId) {
        EXPENSES_SERVICE_DATA.clear();
    }

    @Override
    public void deleteDataItem(@NonNull Expense item, @NonNull String batchId) {
        EXPENSES_SERVICE_DATA.remove(item);
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void observeData(DataLoadCallback<Expense> callback, String... referenceIds) {

    }

    @Override
    public void getTotalExpenses(String batchId, ExpenseTotalCallback callback) {
        //Total should always be fetched from the local source
    }
}

