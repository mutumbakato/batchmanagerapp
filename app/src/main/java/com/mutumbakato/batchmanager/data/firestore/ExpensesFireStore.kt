package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.ExpenseCategoriesDataSource
import com.mutumbakato.batchmanager.data.ExpenseDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.models.ExpenseCategory
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import java.util.*
/**
 * Created by cato on 3/26/18.
 */
class ExpensesFireStore : ExpenseDataSource, ExpenseCategoriesDataSource {

    private val db = FirebaseFirestore.getInstance().collection("expenses")
    private val categoryDb = FirebaseFirestore.getInstance().collection("expense_categories")
    private val batchDb = FirebaseFirestore.getInstance().collection("batches")

    override fun getTotalExpenses(batchId: String, callback: ExpenseDataSource.ExpenseTotalCallback) {
        //This is implemented in the repository
    }

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Expense>) {
        db.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val expenses = ArrayList<Expense>()
                for (data in task.result!!) {
                    expenses.add(data.toObject(Expense::class.java))
                }
                if (expenses.size > 0)
                    callback.onDataLoaded(expenses)
                else
                    callback.onEmptyData()
            } else {
                if (task.result!!.size() == 0) {
                    callback.onEmptyData()
                } else {
                    callback.onError("No Expenses were found")
                }
            }
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Expense>) {
        db.document(id).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callback.onDataItemLoaded(task.result!!.toObject(Expense::class.java)!!)
            } else {
                callback.onError("Expense not found")
            }
        }
    }

    override fun saveData(data: Expense) {
        db.document(data.id).set(data)
    }

    override fun updateData(data: Expense) {
        db.document(data.id).set(data)
    }

    override fun refreshData() {
        //Implemented in the repository
    }

    override fun deleteAllData(batchId: String) {
        //Don't do this!!!!
    }

    override fun deleteDataItem(item: Expense, batchId: String) {
        db.document(item.id).delete()
    }

    override fun getAllCategories(callback: ExpenseCategoriesDataSource.CategoriesLoadCallback) {
        categoryDb.whereEqualTo("farmId", PreferenceUtils.farmId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val categories = ArrayList<ExpenseCategory>()
                for (data in task.result!!) {
                    categories.add(data.toObject(ExpenseCategory::class.java))
                }
                callback.onCategoriesLoaded(categories)
            } else {
                callback.onCategoriesNotAvailable()
            }
        }
    }

    override fun saveCategory(category: ExpenseCategory) {
        categoryDb.document(category.id).set(category)
    }

    override fun updateCategory(category: ExpenseCategory) {
        categoryDb.document(category.id).set(category)
    }

    override fun deleteCategory(id: String) {
        categoryDb.document(id).delete()
    }

    override fun deleteAll() {
        //Don't do this
    }

    override fun clearCache() {
        //Implemented in the repository
    }

    override fun observeData(callback: ExpenseCategoriesDataSource.CategoriesLoadCallback, vararg referenceIds: String) {
        db.whereEqualTo("batchId", referenceIds[0]).addSnapshotListener { snapshots, _ ->
            val categories = ArrayList<ExpenseCategory>()
            if (snapshots != null) {
                for (data in snapshots) {
                    categories.add(data.toObject(ExpenseCategory::class.java))
                }
                callback.onCategoriesLoaded(categories)
            }
        }
    }

    override fun notifyDataChanged() {

    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Expense>, vararg referenceIds: String) {
        db.whereEqualTo("farmId", referenceIds[0]).addSnapshotListener { snapshots, _ ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val dataItems = ArrayList<Expense>()
                for (data in snapshots) {
                    dataItems.add(data.toObject(Expense::class.java))
                }
                callback.onDataLoaded(dataItems)
            }
        }
        //Add farmId to old expenses
        migrateExpenses(referenceIds[0])
    }

    private fun migrateExpenses(farmId: String) {
        batchDb.whereEqualTo("farmId", farmId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                for (data in task.result!!) {
                    updateExpenses(data.toObject(Batch::class.java))
                }
            }
        }
    }

    private fun updateExpenses(batch: Batch) {
        getAll(batch.id, object : BaseDataSource.DataLoadCallback<Expense> {
            override fun onDataLoaded(data: List<Expense>) {
                for (expense in data) {
                    if (expense.farmId == null || expense.farmId == "") {
                        expense.farmId = batch.farmId
                        updateData(expense)
                    }
                }
            }

            override fun onEmptyData() {
            }

            override fun onError(message: String) {
            }
        })
    }
}
