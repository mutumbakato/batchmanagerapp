package com.mutumbakato.batchmanager.data.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.mutumbakato.batchmanager.data.utils.Data;

import java.util.UUID;

import static com.mutumbakato.batchmanager.data.models.Expense.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class Expense {

    public static final String TABLE_NAME = "expenses";

    @Expose
    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "_id")
    private String id;

    @Expose
    @ColumnInfo(name = "date")
    private String date;

    @Expose
    @ColumnInfo(name = "category")
    private String category;

    @Expose
    @ColumnInfo(name = "description")
    private String description;

    @Expose
    @ColumnInfo(name = "amount")
    private float amount;

    @Expose
    @ColumnInfo(name = "batch_id")
    private String batchId;

    @Expose
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Expose
    @ColumnInfo(name = "status")
    private String status;

    @ColumnInfo(name = "farm_id")
    private String farmId;

    @Ignore
    public Expense() {

    }

    public Expense(@NonNull String id, String batchId, String farmId, String date, String category, String description,
                   float amount, long serverId, String status, long lastModified) {
        this.id = id;
        this.batchId = batchId;
        this.farmId = farmId;
        this.date = date;
        this.category = category;
        this.description = description;
        this.amount = amount;
        this.serverId = serverId;
        this.status = status;
        this.lastModified = lastModified;
    }

    @Ignore
    public Expense(@NonNull String id, String batchId, String date, String category, String description,
                   float amount, long serverId, String status, long lastModified) {
        this(id, batchId, "", date, category, description, amount, serverId,
                status, lastModified);
    }

    @Ignore
    public Expense(String batchId, String date, String category, String description, float amount) {
        this(UUID.randomUUID().toString(), batchId, date, category, description, amount, 0,
                Data.STATUS_NORMAL, 0);
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

    public float getAmount() {
        return amount;
    }

    public String getBatchId() {
        return batchId;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }

    public String getFarmId() {
        return farmId;
    }

    @Ignore
    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }
}
