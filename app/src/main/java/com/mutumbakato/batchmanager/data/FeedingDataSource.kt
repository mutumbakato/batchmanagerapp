package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Feeding

interface FeedingDataSource : BaseDataSource<Feeding> {

    fun getMortalityRate(batchId: String, callback: OnMortalityRate)

    interface OnMortalityRate {
        fun onMortalityLoaded(rate: Int)
    }

    interface DataUpdatedListener {
        fun onDataUpdated()
    }

}
