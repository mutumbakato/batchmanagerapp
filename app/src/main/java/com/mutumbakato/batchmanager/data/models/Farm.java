package com.mutumbakato.batchmanager.data.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.UUID;

import static com.mutumbakato.batchmanager.data.models.Farm.TABLE_NAME;

/**
 * Created by cato on 5/10/18.
 */
@Entity(tableName = TABLE_NAME)
public class Farm {

    public static final String TABLE_NAME = "farms";

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "_id")
    private String id = UUID.randomUUID().toString();

    private String name;

    private String contact;

    @ColumnInfo(name = "user_id")
    private String userId;

    private String currency;

    private String location;

    private String units;

    private String status;

    @ColumnInfo(name = "eggs_in_tray")
    private int eggsInTray;

    @ColumnInfo(name = "water_units")
    private String waterUnits;

    @Ignore
    private Map<String, Employee> workers;
    @Ignore
    private Map<String, ActivityLog> activities;

    public Farm(@NonNull String id, String userId, String name, String location, String contact, String units, String currency, String waterUnits, int eggsInTray) {
        this.id = id;
        this.name = name;
        this.contact = contact;
        this.units = units;
        this.userId = userId;
        this.currency = currency;
        this.location = location;
        this.waterUnits = waterUnits;
        this.eggsInTray = eggsInTray;
    }

    @Ignore
    public Farm(String userId, String name, String location, String contact, String units, String currency) {
        this(UUID.randomUUID().toString(), userId, name, location, contact, units, currency, "ltr", 30);
    }

    @Ignore
    public Farm(String userId, String name) {
        this(UUID.randomUUID().toString(), userId, name, "", "", "Kgs", "UGX", "ltr", 30);
    }

    @Ignore
    public Farm(@NotNull String farmId, @Nullable String userId, @NotNull String name, @NotNull String location, @NotNull String contact, @NotNull String units, @NotNull String currency) {
        this(farmId, userId, name, location, contact, units, currency, "ltr", 30);
    }

    @Ignore
    public Farm() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getContact() {
        return contact;
    }

    public String getUnits() {
        return units;
    }

    public String getUserId() {
        return userId;
    }

    public String getCurrency() {
        return currency;
    }

    public String getLocation() {
        return location;
    }

    @Ignore
    public Map<String, Employee> getWorkers() {
        return workers;
    }

    @Ignore
    public void setWorkers(Map<String, Employee> workers) {
        this.workers = workers;
    }

    @Ignore
    public void addWorker(Employee employee) {
        this.workers.put(employee.getId(), employee);
    }

    @Ignore
    public Map<String, ActivityLog> getActivities() {
        return activities;
    }

    @Ignore
    public void setActivities(Map<String, ActivityLog> activities) {
        this.activities = activities;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWaterUnits() {
        return waterUnits;
    }

    public int getEggsInTray() {
        return eggsInTray;
    }

    public void setWaterUnits(String waterUnits) {
        this.waterUnits = waterUnits;
    }

    public void setEggsInTray(int eggsInTray) {
        this.eggsInTray = eggsInTray;
    }
}
