package com.mutumbakato.batchmanager.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mutumbakato.batchmanager.data.WaterDataSource
import com.mutumbakato.batchmanager.data.local.WaterLocalDataSource
import com.mutumbakato.batchmanager.data.models.DataResource
import com.mutumbakato.batchmanager.data.models.Status
import com.mutumbakato.batchmanager.data.models.Water
import com.mutumbakato.batchmanager.data.utils.Logger
import com.mutumbakato.batchmanager.utils.DateUtils

class WaterRepository constructor(private val localDataSource: WaterLocalDataSource, private val remoteDataSource: WaterDataSource) {

    private val isLoading = MutableLiveData<Boolean>()
    private val error = MutableLiveData<Status>()
    private var isOnline: Boolean = false

    fun createWater(water: Water) {
        Logger.log(water.batchId, Logger.ACTION_ADDED, "${water.quantity} ltr to water consumption")
        isLoading.postValue(true)
        localDataSource.insert(water) {
            isLoading.postValue(false)
        }
        remoteDataSource.createWater(water) {
            isLoading.postValue(false)
        }
    }

    fun listWaters(batchId: String): DataResource<List<Water>> {
        if (!isOnline) {
            listFromRemote(batchId)
            isOnline = true
        }
        val waters = localDataSource.listWater(batchId)
        return DataResource(waters, error, isLoading)
    }

    fun getWaterById(id: String): LiveData<Water> {
        return localDataSource.getWaterById(id)
    }

    fun updateWater(water: Water) {
        Logger.log(water.batchId, Logger.ACTION_UPDATED, "water for ${DateUtils.parseDateToddMMyyyy(water.date)}")
        isLoading.postValue(true)
        localDataSource.update(water) {
            isLoading.postValue(false)
        }
        remoteDataSource.updateWater(water) {
            isLoading.postValue(false)
        }
    }

    fun deleteWater(water: Water) {
        Logger.log(water.batchId, Logger.ACTION_DELETED, "${water.quantity} ltr of water for ${DateUtils.parseDateToddMMyyyy(water.date)}")
        isLoading.postValue(true)
        localDataSource.deleteWater(water) {
            isLoading.postValue(false)
        }
        remoteDataSource.deleteWater(water) {
            isLoading.postValue(false)
        }
    }

    private fun listFromRemote(batchId: String) {
        isLoading.postValue(true)
        remoteDataSource.listWaters(batchId) {
            localDataSource.deletAllWater(batchId)
            localDataSource.insertAll(it) {
                isLoading.postValue(false)
            }
        }
    }

    companion object {

        @Volatile
        private var INSTANCE: WaterRepository? = null

        fun getInstance(localDataSource: WaterLocalDataSource, remoteDataSource: WaterDataSource): WaterRepository {
            if (INSTANCE == null)
                INSTANCE = WaterRepository(localDataSource, remoteDataSource)
            return INSTANCE!!
        }
    }
}