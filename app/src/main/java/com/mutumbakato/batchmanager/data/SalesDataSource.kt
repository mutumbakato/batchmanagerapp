package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Sale

interface SalesDataSource : BaseDataSource<Sale> {

    fun getTotalSales(batchId: String, callback: OnTotalSales)

    interface DataUpdatedListener {
        fun onDataUpdated()
    }

    interface OnTotalSales {
        fun onTotal(total: Float)
    }
}
