package com.mutumbakato.batchmanager.data.local;


import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.mutumbakato.batchmanager.data.BaseDataSource;
import com.mutumbakato.batchmanager.data.VaccinationDataSource;
import com.mutumbakato.batchmanager.data.local.dao.VaccinationDao;
import com.mutumbakato.batchmanager.data.models.Vaccination;
import com.mutumbakato.batchmanager.data.models.VaccinationCheck;
import com.mutumbakato.batchmanager.data.sync.Sync;
import com.mutumbakato.batchmanager.data.sync.Syncable;
import com.mutumbakato.batchmanager.utils.executors.AppExecutors;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;
import java.util.UUID;

public class VaccinationLocalDataSource implements VaccinationDataSource, Syncable<Vaccination> {

    private static volatile VaccinationLocalDataSource INSTANCE;
    private VaccinationDao vaccinationDao;
    private VaccinationCheckLocalDataSource vaccinationCheckLocalDataSource;
    private AppExecutors mAppExecutors;
//    private VaccinationRepository mRepo;

    // Prevent direct instantiation.
    private VaccinationLocalDataSource(@NonNull AppExecutors appExecutors,
                                       @NonNull VaccinationDao vaccinationDao, VaccinationCheckLocalDataSource vaccinationCheckLocalDataSource) {
        mAppExecutors = appExecutors;
        this.vaccinationDao = vaccinationDao;
        this.vaccinationCheckLocalDataSource = vaccinationCheckLocalDataSource;
//        mRepo = VaccinationRepository.getInstance(VaccinationRemoteDataSource.getInstance((VaccinationEndPoints) ApiUtils.getEndPoints(VaccinationEndPoints.class)), this);
    }

    public static VaccinationLocalDataSource getInstance(@NonNull AppExecutors appExecutors,
                                                         @NonNull VaccinationDao vaccinationDao,
                                                         VaccinationCheckLocalDataSource vaccinationCheckLocalDataSource) {
        if (INSTANCE == null) {
            synchronized (VaccinationLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new VaccinationLocalDataSource(appExecutors, vaccinationDao, vaccinationCheckLocalDataSource);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public void listVaccinations(final String scheduleId, @NonNull final BaseDataSource.DataLoadCallback<Vaccination> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final List<Vaccination> vaccinations = vaccinationDao.listVaccinations(scheduleId);
            mAppExecutors.mainThread().execute(() -> {
                if (vaccinations.isEmpty()) {
                    callback.onEmptyData();
                } else {
                    callback.onDataLoaded(vaccinations);
                }
            });
        });
    }

    @Override
    public void getCheckList(final String bachId, final BaseDataSource.DataLoadCallback<VaccinationCheck> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final List<VaccinationCheck> vaccinations = vaccinationDao.getCheKList(bachId);
            mAppExecutors.mainThread().execute(() -> {
                if (vaccinations.isEmpty()) {
                    callback.onEmptyData();
                } else {
                    callback.onDataLoaded(vaccinations);
                }
            });
        });
    }

    @Override
    public void getVaccination(String scheduleId, @NonNull final String vaccinationId, @NonNull final BaseDataSource.DataItemCallback<Vaccination> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final Vaccination vaccination = vaccinationDao.getVaccinationById(vaccinationId);
            mAppExecutors.mainThread().execute(() -> {
                if (vaccination != null) {
                    callback.onDataItemLoaded(vaccination);
                } else {
                    callback.onDataItemNotAvailable();
                }
            });
        });
    }

    @Override
    public void createVaccination(@NonNull final Vaccination vaccination) {
        mAppExecutors.diskIO().execute(() -> vaccinationDao.insertVaccination(vaccination));
        sync();
    }

    @Override
    public void updateVaccination(@NonNull final Vaccination vaccination) {
        mAppExecutors.diskIO().execute(() -> vaccinationDao.updateVaccination(vaccination));
        sync();
    }

    @Override
    public void refreshVaccinations() {

    }

    @Override
    public void deleteVaccinations(String schedule, final String id) {
        mAppExecutors.diskIO().execute(() -> vaccinationDao.deleteVaccination(id));
    }

    @Override
    public void deleteAllVaccinations() {
        mAppExecutors.diskIO().execute(() -> vaccinationDao.deleteAllVaccinations());
        sync();
    }

    @Override
    public void deleteAllChecks(String batchId) {
        mAppExecutors.diskIO().execute(() -> vaccinationDao.deleteAllChecks(batchId));
    }

    @Override
    public void addToComplete(final String batchId, final Vaccination vaccination) {
        mAppExecutors.diskIO().execute(() -> {
            VaccinationCheck check = new VaccinationCheck(UUID.randomUUID().toString(),
                    batchId, vaccination.getScheduleId(), vaccination.getId(), 0, "normal", 0);
            vaccinationDao.addToComplete(check);
            sync();
        });
    }

    @Override
    public void removeFromComplete(String batchID, final Vaccination vaccination) {
        mAppExecutors.diskIO().execute(() -> {
            vaccinationDao.removeFromComplete(vaccination.getId());
            sync();
        });
    }

    @Override
    public void addCheckItem(final VaccinationCheck check) {
        mAppExecutors.diskIO().execute(() -> vaccinationDao.addToComplete(check));
    }

    @Override
    public void clearCache() {
    }

    @Override
    public void observeVaccination(BaseDataSource.DataLoadCallback<Vaccination> callback, String scheduleId) {

    }

    @Override
    public void observeCheckList(String bachId, BaseDataSource.DataLoadCallback<VaccinationCheck> callback) {

    }

    @Override
    public String getTable() {
        return Vaccination.TABLE_NAME;
    }

    @Override
    public List<Vaccination> getNew() {
        return vaccinationDao.getNew();
    }

    @Override
    public List<Vaccination> getUpdated() {
        return vaccinationDao.getUpdated();
    }

    @Override
    public List<Vaccination> getLocalData() {
        return vaccinationDao.getLocal();
    }

    @Override
    public List<Long> getTrash() {
        return vaccinationDao.getTrash();
    }

    @Override
    public long getLastServerId() {
        return vaccinationDao.getLastServerId();
    }

    @Override
    public void handleNewData(JSONArray newData) {
        Gson gson = new Gson();
        for (int n = 0; n < newData.length(); n++) {
            try {
                Vaccination vaccination = gson.fromJson(newData.get(n).toString(),
                        Vaccination.class);
                Vaccination newVaccination = new Vaccination(vaccination.getId(), vaccination.getScheduleId(),
                        vaccination.getAge(), vaccination.getThenEvery(), vaccination.getInfection(),
                        vaccination.getVaccine(), vaccination.getMethod(), vaccination.isRecurrent(),
                        vaccination.getServerId(), vaccination.getStatus(), vaccination.getLastModified());
                vaccinationDao.insertVaccination(newVaccination);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleModified(JSONArray modified) {
        Gson gson = new Gson();
        for (int n = 0; n < modified.length(); n++) {
            try {
                Vaccination schedule = gson.fromJson(modified.get(n).toString(), Vaccination.class);
                vaccinationDao.updateVaccination(schedule);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSynced(JSONArray synced) {
        for (int n = 0; n < synced.length(); n++) {
            try {

                String id = synced.getJSONObject(n).getString("id");
                long serverId = synced.getJSONObject(n).getLong("serverId");
                long lastMod = synced.getJSONObject(n).getLong("lastModified");
                Vaccination vaccination = vaccinationDao.getVaccinationById(id);
                Vaccination syncedVaccination = new Vaccination(vaccination.getId(), vaccination.getScheduleId(),
                        vaccination.getAge(), vaccination.getThenEvery(), vaccination.getInfection(),
                        vaccination.getVaccine(), vaccination.getMethod(), vaccination.isRecurrent(),
                        serverId, vaccination.getStatus(), lastMod);
                vaccinationDao.updateVaccination(syncedVaccination);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleUpdates(JSONArray updates) {
        for (int n = 0; n < updates.length(); n++) {
            try {
                String id = updates.getJSONObject(n).getString("id");
                Long last_mod = updates.getJSONObject(n).getLong("lastModified");
                Vaccination vaccination = vaccinationDao.getVaccinationById(id);
                Vaccination newVaccination = new Vaccination(vaccination.getId(), vaccination.getScheduleId(),
                        vaccination.getAge(), vaccination.getThenEvery(), vaccination.getInfection(),
                        vaccination.getVaccine(), vaccination.getMethod(), vaccination.isRecurrent(),
                        vaccination.getServerId(), vaccination.getStatus(), last_mod);
                vaccinationDao.updateVaccination(newVaccination);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleTrash(String[] trash) {
        for (String aTrash : trash) {
            vaccinationDao.deleteVaccination(aTrash);
        }
        vaccinationDao.clearTrash();
    }

    @Override
    public void notifyRepository() {
//        mRepo.clearCache();
    }

    private void sync() {
        Sync.getInstance().sync(this);
        if (vaccinationCheckLocalDataSource != null)
            Sync.getInstance().sync(vaccinationCheckLocalDataSource);
    }
}
