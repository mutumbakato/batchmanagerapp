package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchRecords
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.Logger
import java.util.*

class BatchRepository private constructor(private val mBatchRemoteDataSource: BatchDataSource,
                                          private val mBatchLocalDataSource: BatchDataSource) : BatchDataSource {

    private var mListener: BatchDataSource.DataUpdatedListener? = null
    private var mCachedBatch: MutableMap<String, MutableMap<String, Batch>>? = null
    private var mCachedBatchUsers: MutableMap<String, MutableMap<String, BatchUser>>? = null
    private var mCacheIsDirty = false

    override fun getAllBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        // Respond immediately with cache if available and not dirty
        if (mCachedBatch != null && mCachedBatch!![farmId] != null && !mCacheIsDirty) {
            val map = HashMap<String, Batch>()
            for (b in mCachedBatch!!.values) {
                map.putAll(b)
            }
            val batches = ArrayList(map.values)
            callback.onBatchesLoaded(ArrayList(batches))
            return
        }

        // Query the local storage if available. If not, query the network.
        mBatchLocalDataSource.getAllBatches(farmId, object : BatchDataSource.LoadBatchesCallback {
            override fun onBatchesLoaded(batches: List<Batch>) {
                refreshCache(batches)
                callback.onBatchesLoaded(batches)
                for (batch in batches) {
                    if (batch.userId == null) {
                        batch.userId = PreferenceUtils.userId
                        val users = ArrayList<String>()
                        users.add(PreferenceUtils.userId)
                        batch.users = users
                        updateBatch(batch)
                    }
                }
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
        observeMyBatches(farmId, callback)
        observeOtherBatches(farmId, callback)
    }

    override fun getArchivedBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        mBatchLocalDataSource.getArchivedBatches(farmId, object : BatchDataSource.LoadBatchesCallback {
            override fun onBatchesLoaded(batches: List<Batch>) {
                callback.onBatchesLoaded(batches)
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    override fun saveBatch(batch: Batch, callback: BatchDataSource.CreateBatchCallback) {
        Logger.log(batch.id, Logger.ACTION_CREATED, "this batch")
        mBatchLocalDataSource.saveBatch(batch, callback)
        mBatchRemoteDataSource.saveBatch(batch, callback)
        // Do in memory cache update to keep the app UI up to date
        addToCache(batch)
        notifyDataChanged()
    }

    override fun updateBatch(batch: Batch) {
        Logger.log(batch.id, Logger.ACTION_UPDATED, "this batch")
        mCachedBatch!!.clear()
        mBatchLocalDataSource.updateBatch(batch)
        mBatchRemoteDataSource.updateBatch(batch)
        notifyDataChanged()
    }

    override fun closeBatch(batchId: String) {
        //Implemented in update
    }

    override fun getBatch(batchId: String, callback: BatchDataSource.GetBatchCallback) {
        val cachedBatch = getBatchWithId(batchId)
        // Respond immediately with cache if available
        if (cachedBatch != null) {
            callback.onBatchLoaded(cachedBatch)
            return
        }
        // Load from server/persisted if needed.
        // Is the batch in the local data source? If not, query the network.
        mBatchLocalDataSource.getBatch(batchId, object : BatchDataSource.GetBatchCallback {
            override fun onBatchLoaded(batch: Batch) {
                callback.onBatchLoaded(batch)
            }

            override fun onDataNotAvailable() {
                mBatchRemoteDataSource.getBatch(batchId, object : BatchDataSource.GetBatchCallback {
                    override fun onBatchLoaded(batch: Batch) {
                        // Do in memory cache update to keep the app UI up to date
                        addToCache(batch)
                        callback.onBatchLoaded(batch)
                    }

                    override fun onDataNotAvailable() {
                        callback.onDataNotAvailable()
                    }
                })
            }
        })
    }

    override fun refreshBatches() {
        mCacheIsDirty = true
    }

    override fun deleteAllMyBatches(farmId: String) {
        //Don't do this!!
    }

    override fun deleteAllOtherBatches(farmId: String) {
        //Don't do this!!
    }

    override fun deleteBatch(batchId: String) {
        mBatchLocalDataSource.deleteBatch(batchId)
        mBatchRemoteDataSource.deleteBatch(batchId)
        clearMemoryCache(false)
    }

    override fun clearMemoryCache(notify: Boolean) {
        mCachedBatch = null
        if (notify)
            notifyDataChanged()
    }

    override fun notifyDataChanged() {
        if (mListener != null) {
            mListener!!.onDataUpdated()
        }
    }

    override fun observeMyBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        mBatchRemoteDataSource.observeMyBatches(farmId, object : BatchDataSource.LoadBatchesCallback {
            override fun onBatchesLoaded(batches: List<Batch>) {
                refreshMyLocalDataSource(farmId, true, batches, callback)
            }

            override fun onDataNotAvailable() {
                refreshMyLocalDataSource(farmId, true, ArrayList(), callback)
            }
        })
    }

    override fun observeOtherBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        mBatchRemoteDataSource.observeOtherBatches(farmId, object : BatchDataSource.LoadBatchesCallback {
            override fun onBatchesLoaded(batches: List<Batch>) {
                refreshMyLocalDataSource(farmId, false, batches, callback)
            }

            override fun onDataNotAvailable() {
                refreshMyLocalDataSource(farmId, false, ArrayList(), callback)
            }
        })
    }

    override fun addUser(users: BatchUser, callback: BaseDataSource.DataItemCallback<String>) {
        Logger.log(users.batchId, Logger.ACTION_ADDED, users.name + " to this batch")
        if (mCachedBatchUsers != null)
            mCachedBatchUsers!!.clear()
        mBatchLocalDataSource.addUser(users, object : BaseDataSource.DataItemCallback<String> {
            override fun onDataItemLoaded(data: String) {
                callback.onDataItemLoaded(data)
                addToCache(users)
            }

            override fun onDataItemNotAvailable() {}

            override fun onError(message: String) {
                callback.onError(message)
            }
        })
        mBatchRemoteDataSource.addUser(users, object : BaseDataSource.DataItemCallback<String> {
            override fun onDataItemLoaded(data: String) {

            }

            override fun onDataItemNotAvailable() {

            }

            override fun onError(message: String) {

            }
        })
    }

    override fun removeUser(user: BatchUser, callback: BaseDataSource.DataItemCallback<String>) {
        Logger.log(user.batchId, Logger.ACTION_REMOVED, user.name + " from this batch")
        if (mCachedBatchUsers != null) {
            mCachedBatchUsers!!.clear()
        }
        mBatchLocalDataSource.removeUser(user, callback)
        mBatchRemoteDataSource.removeUser(user, object : BaseDataSource.DataItemCallback<String> {
            override fun onDataItemLoaded(data: String) {

            }

            override fun onDataItemNotAvailable() {

            }

            override fun onError(message: String) {

            }
        })
    }

    override fun getBatchUsers(batchId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>) {
        observeBatchesUsers(batchId, callback)
        if (mCachedBatchUsers != null && mCachedBatchUsers!![batchId] != null) {
            callback.onDataLoaded(ArrayList(mCachedBatchUsers!![batchId]!!.values))
        } else {
            mBatchLocalDataSource.getBatchUsers(batchId, object : BaseDataSource.DataLoadCallback<BatchUser> {
                override fun onDataLoaded(data: List<BatchUser>) {
                    if (data.isNotEmpty()) {
                        callback.onDataLoaded(data)
                        refreshUserCache(data)
                    } else {
                        fetchBatchUsersFromRemote(batchId, callback)
                    }
                }

                override fun onEmptyData() {
                    fetchBatchUsersFromRemote(batchId, callback)
                }

                override fun onError(message: String) {
                    callback.onError(message)
                }
            })
        }
    }

    private fun fetchBatchUsersFromRemote(batchId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>) {
        mBatchRemoteDataSource.getBatchUsers(batchId, object : BaseDataSource.DataLoadCallback<BatchUser> {
            override fun onDataLoaded(data: List<BatchUser>) {
                if (data.isNotEmpty()) {
                    refreshUserLocalDataSource(data)
                    callback.onDataLoaded(data)
                } else
                    callback.onEmptyData()
            }

            override fun onEmptyData() {
                callback.onEmptyData()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })
    }

    override fun getBatchUser(batchId: String, userId: String, callback: BaseDataSource.DataItemCallback<BatchUser>) {
        observeBatchesUsers(batchId, object : BaseDataSource.DataLoadCallback<BatchUser> {
            override fun onDataLoaded(data: List<BatchUser>) {
            }

            override fun onEmptyData() {
            }

            override fun onError(message: String) {
            }
        })

        if (mCachedBatchUsers != null && mCachedBatchUsers!![batchId] != null && mCachedBatchUsers!![batchId]!![userId] != null) {
            callback.onDataItemLoaded(mCachedBatchUsers!![batchId]!![userId]!!)
        } else {
            mBatchLocalDataSource.getBatchUser(batchId, userId, object : BaseDataSource.DataItemCallback<BatchUser> {
                override fun onDataItemLoaded(data: BatchUser) {
                    addToCache(data)
                    callback.onDataItemLoaded(data)
                }

                override fun onDataItemNotAvailable() {
                    getBatchUserFromRemote(batchId, userId, callback)
                }

                override fun onError(message: String) {
                    callback.onError(message)
                }
            })
        }
    }

    private fun getBatchUserFromRemote(batchId: String, userId: String, callback: BaseDataSource.DataItemCallback<BatchUser>) {
        mBatchRemoteDataSource.getBatchUser(batchId, userId, object : BaseDataSource.DataItemCallback<BatchUser> {
            override fun onDataItemLoaded(data: BatchUser) {
                callback.onDataItemLoaded(data)
            }

            override fun onDataItemNotAvailable() {
                callback.onDataItemNotAvailable()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })
    }

    override fun updateBatchUser(user: BatchUser, callback: BaseDataSource.DataItemCallback<BatchUser>) {
        if (mCachedBatchUsers != null)
            mCachedBatchUsers!!.clear()
        mBatchLocalDataSource.updateBatchUser(user, callback)
        mBatchRemoteDataSource.updateBatchUser(user, object : BaseDataSource.DataItemCallback<BatchUser> {
            override fun onDataItemLoaded(data: BatchUser) {

            }

            override fun onDataItemNotAvailable() {

            }

            override fun onError(message: String) {

            }
        })
    }

    override fun observeBatchesUsers(batchId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>) {
        mBatchRemoteDataSource.observeBatchesUsers(batchId, object : BaseDataSource.DataLoadCallback<BatchUser> {
            override fun onDataLoaded(data: List<BatchUser>) {
                refreshUserLocalDataSource(data)
            }

            override fun onEmptyData() {
                refreshUserLocalDataSource(ArrayList())
            }

            override fun onError(message: String) {

            }
        })
    }

    override fun deleteAllBatchUsers(batchId: String) {

    }

    override fun getTodayRecords(onResults: (records: BatchRecords) -> Unit) {
        mBatchLocalDataSource.getTodayRecords(onResults)
    }

    private fun getBatchFromRemoteDataSource(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        mBatchRemoteDataSource.getAllBatches(farmId, object : BatchDataSource.LoadBatchesCallback {
            override fun onBatchesLoaded(batches: List<Batch>) {
                refreshMyLocalDataSource(farmId, true, batches, callback)
                mCacheIsDirty = false
            }

            override fun onDataNotAvailable() {
                callback.onDataNotAvailable()
            }
        })
    }

    private fun addToCache(batch: Batch?) {
        if (batch == null)
            return
        if (mCachedBatch == null) {
            mCachedBatch = LinkedHashMap()
        }

        if (mCachedBatch!![batch.farmId] == null) {
            mCachedBatch!![batch.farmId] = HashMap()
        }

        mCachedBatch!![batch.farmId]!![batch.id] = batch
    }

    private fun addToCache(batchUser: BatchUser?) {
        if (mCachedBatchUsers == null) {
            mCachedBatchUsers = LinkedHashMap()
        }
        if (mCachedBatchUsers!![batchUser!!.batchId] == null) {
            mCachedBatchUsers!![batchUser.batchId] = HashMap()
        }
        mCachedBatchUsers!![batchUser.batchId]!![batchUser.id] = batchUser
    }

    private fun refreshCache(batches: List<Batch>) {

        if (mCachedBatch == null) {
            mCachedBatch = LinkedHashMap()
        }
        mCachedBatch!!.clear()

        for (batch in batches) {
            addToCache(batch)
        }
        mCacheIsDirty = false
    }

    private fun refreshUserCache(batchUsers: List<BatchUser>) {
        if (mCachedBatch == null) {
            mCachedBatch = LinkedHashMap()
        }
        mCachedBatch!!.clear()
        for (batch in batchUsers) {
            addToCache(batch)
        }
        mCacheIsDirty = false
    }

    private fun refreshMyLocalDataSource(farmId: String, isMine: Boolean, batches: List<Batch>, callback: BatchDataSource.LoadBatchesCallback) {
        mCachedBatch = null

        if (isMine) {
            mBatchLocalDataSource.deleteAllMyBatches(farmId)
        } else {
            mBatchLocalDataSource.deleteAllOtherBatches(farmId)
        }

        for (batch in batches) {
            mBatchLocalDataSource.saveBatch(batch, object : BatchDataSource.CreateBatchCallback {
                override fun onBatchCreated(batchId: String) {
                }
            })
        }

        mBatchLocalDataSource.getAllBatches(farmId, object : BatchDataSource.LoadBatchesCallback {
            override fun onBatchesLoaded(batches: List<Batch>) {
                refreshCache(batches)
                callback.onBatchesLoaded(batches)
            }

            override fun onDataNotAvailable() {

            }
        })
    }

    private fun refreshUserLocalDataSource(batchUsers: List<BatchUser>) {
        mCachedBatchUsers = null
        mBatchLocalDataSource.deleteAllBatchUsers(if (batchUsers.isNotEmpty()) batchUsers[0].batchId else "0")
        for (batch in batchUsers) {
            mBatchLocalDataSource.addUser(batch, object : BaseDataSource.DataItemCallback<String> {
                override fun onDataItemLoaded(data: String) {

                }

                override fun onDataItemNotAvailable() {

                }

                override fun onError(message: String) {

                }
            })
        }
        refreshUserCache(batchUsers)
    }

    private fun getBatchWithId(id: String): Batch? {
        if (mCachedBatch == null || mCachedBatch!!.isEmpty()) {
            return null
        } else {
            val batches = ArrayList<Batch>()
            for (b in ArrayList(mCachedBatch!!.values)) {
                batches.addAll(b.values)
            }
            for (bb in batches) {
                if (bb.id == id) {
                    return bb
                }
            }
            return null
        }
    }

    fun setDataUpdatedListener(listener: BatchDataSource.DataUpdatedListener) {
        mListener = listener
    }

    companion object {

        private var INSTANCE: BatchRepository? = null

        fun getInstance(batchesRemoteDataSource: BatchDataSource,
                        batchesLocalDataSource: BatchDataSource): BatchRepository {
            if (INSTANCE == null) {
                INSTANCE = BatchRepository(batchesRemoteDataSource, batchesLocalDataSource)
            }
            return INSTANCE as BatchRepository
        }

        fun getTestInstance(batchesRemoteDataSource: BatchDataSource,
                            batchesLocalDataSource: BatchDataSource): BatchRepository {
            return BatchRepository(batchesRemoteDataSource, batchesLocalDataSource)
        }
    }
}
