package com.mutumbakato.batchmanager.data.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;

import java.util.Map;

import static com.mutumbakato.batchmanager.data.models.VaccinationSchedule.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class VaccinationSchedule {

    public static final String TABLE_NAME = "vaccination_schedule";

    @NonNull
    @PrimaryKey
    @Expose
    @ColumnInfo(name = "_id")
    private String id;

    @Expose
    @ColumnInfo(name = "user_id")
    private String userId;

    @Expose
    @ColumnInfo(name = "batch_type")
    private String title;

    @Expose
    @ColumnInfo(name = "description")
    private String description;

    @Expose
    @ColumnInfo(name = "publicity")
    private String publicity;

    @Expose
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @ColumnInfo(name = "status")
    private String status;

    private String farmId;

    @Ignore
    private Map<String, Vaccination> vaccinations;

    @Ignore
    public VaccinationSchedule() {

    }

    public VaccinationSchedule(@NonNull String id, String userId, String title, String description,
                               String publicity, long serverId, String status, long lastModified, String farmId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.publicity = publicity;
        this.userId = userId;
        this.serverId = serverId;
        this.status = status;
        this.lastModified = lastModified;
        this.farmId = farmId;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPublicity() {
        return publicity;
    }

    public String getUserId() {
        return userId;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }

    public Map<String, Vaccination> getVaccinations() {
        return vaccinations;
    }

    public String getFarmId() {
        return farmId;
    }
}
