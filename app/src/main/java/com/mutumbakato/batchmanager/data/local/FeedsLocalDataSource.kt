package com.mutumbakato.batchmanager.data.local

import androidx.lifecycle.LiveData
import com.mutumbakato.batchmanager.data.local.dao.FeedsDao
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.models.Feeds
import com.mutumbakato.batchmanager.utils.executors.AppExecutors

class FeedsLocalDataSource constructor(val dao: FeedsDao, val executors: AppExecutors) {

    fun insert(feeds: Feeds, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.insert(feeds)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun insertAll(feeds: List<Feeds>, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.insertAll(feeds)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun insertAllOldFeeds(feeds: List<Feeding>) {
        executors.diskIO().execute {
            dao.insertAllOldFeeds(feeds)
        }
    }

    fun update(feeds: Feeds, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.update(feeds)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun listFeeds(batchId: String): LiveData<List<Feeds>> {
        return dao.listFeeds(batchId)
    }

    fun listOldFeeds(batchId: String): LiveData<List<Feeding>> {
        return dao.listOldFeeds(batchId)
    }

    fun getFeedsById(id: String): LiveData<Feeds> {
        return dao.getFeedsById(id)
    }

    fun deleteFeeds(feeds: Feeds, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.deleteFeeds(feeds)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun deleteAllFeeds(batchId: String) {
        executors.diskIO().execute {
            deleteAllFeeds(batchId)
        }
    }
}