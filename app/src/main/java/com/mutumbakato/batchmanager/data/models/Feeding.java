package com.mutumbakato.batchmanager.data.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.firebase.firestore.Exclude;
import com.google.gson.annotations.Expose;
import com.mutumbakato.batchmanager.data.utils.Data;

import java.util.UUID;

import static com.mutumbakato.batchmanager.data.models.Feeding.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class Feeding {

    public static final String TABLE_NAME = "feeding";

    @NonNull
    @Expose
    @PrimaryKey
    @ColumnInfo(name = "_id")
    private String id = "";

    @Expose
    @ColumnInfo(name = "batch_id")
    private String batchId;

    @Expose
    @ColumnInfo(name = "date")
    private String date;

    @Expose
    @ColumnInfo(name = "quantity")
    private float quantity;

    @Expose
    @ColumnInfo(name = "item")
    private String item;

    @Exclude
    @Expose
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Expose
    @ColumnInfo(name = "status")
    private String status;

//    @ColumnInfo(name = "batch_count")
//    private int batchCount;

    @Ignore
    public Feeding() {
    }

    public Feeding(@NonNull String id, String batchId, String date, float quantity, String item, long serverId,
                   String status, long lastModified) {
        this.id = id;
        this.date = date;
        this.quantity = quantity;
        this.item = item;
        this.batchId = batchId;
        this.serverId = serverId;
        this.status = status;
        this.lastModified = lastModified;
    }

    @Ignore
    public Feeding(String batchId, String date, float quantity, String item) {
        this(UUID.randomUUID().toString(), batchId, date, quantity, item, Data.SERVER_ID_NEW, Data.STATUS_NORMAL, 0);
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public float getQuantity() {
        return quantity;
    }

    public String getItem() {
        return item;
    }

    public String getBatchId() {
        return batchId;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }
}
