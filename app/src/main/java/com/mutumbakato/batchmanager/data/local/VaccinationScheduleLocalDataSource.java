package com.mutumbakato.batchmanager.data.local;

import androidx.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.mutumbakato.batchmanager.data.BaseDataSource;
import com.mutumbakato.batchmanager.data.VaccinationScheduleDataSource;
import com.mutumbakato.batchmanager.data.local.dao.VaccinationScheduleDao;
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule;
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;
import com.mutumbakato.batchmanager.data.sync.Sync;
import com.mutumbakato.batchmanager.data.sync.Syncable;
import com.mutumbakato.batchmanager.utils.executors.AppExecutors;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

/**
 * Created by cato on 11/2/17.
 */

public class VaccinationScheduleLocalDataSource implements VaccinationScheduleDataSource, Syncable<VaccinationSchedule> {

    private static final String TAG = VaccinationScheduleDataSource.class.getSimpleName();
    private static volatile VaccinationScheduleLocalDataSource INSTANCE;

    private VaccinationScheduleDao vaccinationScheduleDao;
    private AppExecutors mAppExecutors;
//    private VaccinationScheduleRepository mRepo;

    // Prevent direct instantiation.
    private VaccinationScheduleLocalDataSource(@NonNull AppExecutors appExecutors,
                                               @NonNull VaccinationScheduleDao vaccinationScheduleDao) {
        mAppExecutors = appExecutors;
        this.vaccinationScheduleDao = vaccinationScheduleDao;
//        mRepo = VaccinationScheduleRepository.getInstance(VaccinationScheduleRemoteDataSource.getInstance((VaccinationEndPoints) ApiUtils.getEndPoints(VaccinationEndPoints.class)), this);
    }

    public static VaccinationScheduleLocalDataSource getInstance(@NonNull AppExecutors appExecutors,
                                                                 @NonNull VaccinationScheduleDao vaccinationDao) {
        if (INSTANCE == null) {
            synchronized (VaccinationLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new VaccinationScheduleLocalDataSource(appExecutors, vaccinationDao);
                }
            }
        }
        return INSTANCE;
    }


    @Override
    public void getAllSchedules(@NonNull String batchId, @NonNull final BaseDataSource.DataLoadCallback<VaccinationSchedule> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final List<VaccinationSchedule> schedules = vaccinationScheduleDao.listSchedules();
            mAppExecutors.mainThread().execute(() -> {
                if (schedules.size() > 0) {
                    callback.onDataLoaded(schedules);
                } else {
                    callback.onEmptyData();
                }
            });
        });
    }

    @Override
    public void getSchedule(@NonNull final String id, @NonNull String batchId, @NonNull final BaseDataSource.DataItemCallback<VaccinationSchedule> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final VaccinationSchedule schedule = vaccinationScheduleDao.getSchedulesById(id);
            mAppExecutors.mainThread().execute(() -> {
                if (schedule != null) {
                    callback.onDataItemLoaded(schedule);
                } else {
                    callback.onDataItemNotAvailable();
                }
            });
        });
    }

    @Override
    public void saveSchedule(@NonNull final VaccinationSchedule schedule) {
        mAppExecutors.diskIO().execute(() -> vaccinationScheduleDao.insertSchedule(schedule));
        sync();
    }

    @Override
    public void updateSchedule(@NonNull final VaccinationSchedule schedule) {
        mAppExecutors.diskIO().execute(() -> vaccinationScheduleDao.updateSchedule(schedule));
        sync();
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllSchedules(@NonNull String batchId) {
        mAppExecutors.diskIO().execute(() -> {
            //vaccinationScheduleDao.deleteAllSchedules();
        });
    }

    @Override
    public void deleteSchedule(@NonNull final String id, @NonNull String batchId) {
        mAppExecutors.diskIO().execute(() -> {
            vaccinationScheduleDao.deleteScheduleById(id);
            vaccinationScheduleDao.deleteScheduleItems(id);
        });
        sync();
    }

    @Override
    public void clearCache() {
    }

    @Override
    public void notifyDataChanged() {
    }

    @Override
    public String getTable() {
        return VaccinationSchedule.TABLE_NAME;
    }

    @Override
    public List<VaccinationSchedule> getNew() {
        return vaccinationScheduleDao.getNew();
    }

    @Override
    public List<VaccinationSchedule> getUpdated() {
        return vaccinationScheduleDao.getUpdated();
    }

    @Override
    public List<VaccinationSchedule> getLocalData() {
        return vaccinationScheduleDao.getLocal();
    }

    @Override
    public List<Long> getTrash() {
        return vaccinationScheduleDao.getTrash();
    }

    @Override
    public long getLastServerId() {
        return vaccinationScheduleDao.getLastServerId();
    }

    @Override
    public void handleNewData(JSONArray newData) {
        Gson gson = new Gson();
        for (int n = 0; n < newData.length(); n++) {
            try {
                VaccinationSchedule schedule = gson.fromJson(newData.get(n).toString(),
                        VaccinationSchedule.class);

                VaccinationSchedule newSchedule = new VaccinationSchedule(schedule.getId(),
                        schedule.getUserId(), schedule.getTitle(), schedule.getDescription(),
                        schedule.getPublicity(), schedule.getServerId(), schedule.getStatus(),
                        schedule.getLastModified(), PreferenceUtils.INSTANCE.getFarmId());

                Log.d(TAG, "handleNewData: " + schedule.getId());
                vaccinationScheduleDao.insertSchedule(newSchedule);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleModified(JSONArray modified) {
        Gson gson = new Gson();
        for (int n = 0; n < modified.length(); n++) {
            try {
                VaccinationSchedule schedule = gson.fromJson(modified.get(n).toString(), VaccinationSchedule.class);
                vaccinationScheduleDao.updateSchedule(schedule);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSynced(JSONArray synced) {
        for (int n = 0; n < synced.length(); n++) {
            try {
                String id = synced.getJSONObject(n).getString("id");
                long serverId = synced.getJSONObject(n).getLong("serverId");
                long lastMod = synced.getJSONObject(n).getLong("lastModified");
                VaccinationSchedule schedule = vaccinationScheduleDao.getSchedulesById(id);
                VaccinationSchedule syncedSchedule = new VaccinationSchedule(schedule.getId(),
                        schedule.getUserId(), schedule.getTitle(), schedule.getDescription(),
                        schedule.getPublicity(), serverId, schedule.getStatus(), lastMod, PreferenceUtils.INSTANCE.getFarmId());
                vaccinationScheduleDao.updateSchedule(syncedSchedule);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleUpdates(JSONArray updates) {
        for (int n = 0; n < updates.length(); n++) {
            try {
                String id = updates.getJSONObject(n).getString("id");
                Long last_mod = updates.getJSONObject(n).getLong("lastModified");
                VaccinationSchedule schedule = vaccinationScheduleDao.getSchedulesById(id);
                VaccinationSchedule newSchedule = new VaccinationSchedule(schedule.getId(), schedule.getUserId(),
                        schedule.getTitle(), schedule.getDescription(), schedule.getPublicity(), schedule.getServerId(),
                        schedule.getStatus(), last_mod, PreferenceUtils.INSTANCE.getFarmId());
                vaccinationScheduleDao.updateSchedule(newSchedule);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleTrash(String[] trash) {
        for (String aTrash : trash) {
            vaccinationScheduleDao.deleteScheduleById(aTrash);
        }
        vaccinationScheduleDao.clearTrash();
    }

    @Override
    public void notifyRepository() {
//        mRepo.clearCache();
    }

    @Override
    public void useSchedule(final String batchId, final VaccinationSchedule schedule) {
        mAppExecutors.diskIO().execute(() -> vaccinationScheduleDao.useSchedule(batchId, schedule.getId()));
    }

    @Override
    public void removeSchedule(final String batchId, final String scheduleId) {
        Log.d(TAG, "removeSchedule: Remove schedule");
        mAppExecutors.diskIO().execute(() -> {
            vaccinationScheduleDao.removeSchedule(batchId, scheduleId);
            mAppExecutors.mainThread().execute(() -> {
//                        mRepo.notifyDataChanged();
            });
        });
    }

    @Override
    public void observeSchedule(BaseDataSource.DataLoadCallback<VaccinationSchedule> callback, String userId) {

    }

    private void sync() {
        Sync.getInstance().sync(this);
    }
}
