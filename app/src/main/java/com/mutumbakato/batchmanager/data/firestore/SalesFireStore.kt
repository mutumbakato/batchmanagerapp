package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.SalesDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.Sale
import java.util.*

/**
 * Created by cato on 3/26/18.
 */

class SalesFireStore : SalesDataSource {

    private val db = FirebaseFirestore.getInstance().collection("sales")
    private val batchDb = FirebaseFirestore.getInstance().collection("batches")

    override fun getTotalSales(batchId: String, callback: SalesDataSource.OnTotalSales) {
        //Implemented in the repository
    }

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Sale>) {
        db.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val sales = ArrayList<Sale>()
                for (data in task.result!!) {
                    sales.add(data.toObject(Sale::class.java))
                }
                if (sales.size > 0)
                    callback.onDataLoaded(sales)
                else
                    callback.onEmptyData()
            } else {
                callback.onEmptyData()
            }
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Sale>) {
        db.document(id).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callback.onDataItemLoaded(task.result!!.toObject(Sale::class.java)!!)
            } else {
                callback.onDataItemNotAvailable()
            }
        }
    }

    override fun saveData(data: Sale) {
        db.document(data.id).set(data)
    }

    override fun updateData(data: Sale) {
        db.document(data.id).set(data)
    }

    override fun refreshData() {
        //Implemented in the repository
    }

    override fun deleteAllData(batchId: String) {
        //Don't do this
    }

    override fun deleteDataItem(item: Sale, batchId: String) {
        db.document(item.id).delete()
    }

    override fun clearCache() {
        //Implemented in the repository
    }

    override fun notifyDataChanged() {

    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Sale>, vararg referenceIds: String) {
        db.whereEqualTo("farmId", referenceIds[0]).addSnapshotListener { snapshots, _ ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val dataItems = ArrayList<Sale>()
                for (data in snapshots) {
                    dataItems.add(data.toObject(Sale::class.java))
                }
                callback.onDataLoaded(dataItems)
            }
        }
        migrateExpenses(referenceIds[0])
    }

    private fun migrateExpenses(farmId: String) {
        batchDb.whereEqualTo("farmId", farmId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                for (data in task.result!!) {
                    updateExpenses(data.toObject(Batch::class.java))
                }
            }
        }
    }

    private fun updateExpenses(batch: Batch) {
        getAll(batch.id, object : BaseDataSource.DataLoadCallback<Sale> {
            override fun onDataLoaded(data: List<Sale>) {
                for (sale in data) {
                    if (sale.farmId == null || sale.farmId == "") {
                        sale.farmId = batch.farmId
                        updateData(sale)
                    }
                }
            }

            override fun onEmptyData() {
            }

            override fun onError(message: String) {
            }
        })
    }
}
