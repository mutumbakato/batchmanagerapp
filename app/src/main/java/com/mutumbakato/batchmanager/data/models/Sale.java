package com.mutumbakato.batchmanager.data.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.firebase.firestore.Exclude;
import com.google.gson.annotations.Expose;
import com.mutumbakato.batchmanager.data.utils.Data;

import java.util.UUID;

import static com.mutumbakato.batchmanager.data.models.Sale.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class Sale {

    public static final String TABLE_NAME = "sales";

    @Expose
    @PrimaryKey
    @ColumnInfo(name = "_id")
    @NonNull
    private String id;

    @Expose
    @ColumnInfo(name = "batch_id")
    private String batchId;

    @ColumnInfo(name = "farm_id")
    private String farmId;

    @ColumnInfo(name = "store_id")
    private String storeId;

    @Expose
    @ColumnInfo(name = "date")
    private String date;

    @Expose
    @ColumnInfo(name = "item")
    private String item;

    @Expose
    @ColumnInfo(name = "rate")
    private float rate;

    @Expose
    @ColumnInfo(name = "quantity")
    private int quantity;

    @Expose
    @ColumnInfo(name = "customer")
    private String customer;

    @Expose
    @Exclude
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Expose
    @ColumnInfo(name = "status")
    private String status;

    @Ignore
    public Sale() {

    }

    public Sale(@NonNull String id, String batchId, String farmId, String storeId, String date, String item, float rate, int quantity,
                String customer, long serverId, String status, long lastModified) {
        this.id = id;
        this.batchId = batchId;
        this.farmId = farmId;
        this.storeId = storeId;
        this.date = date;
        this.item = item;
        this.rate = rate;
        this.quantity = quantity;
        this.customer = customer;
        this.serverId = serverId;
        this.status = status;
        this.lastModified = lastModified;
    }

    @Ignore
    public Sale(@NonNull String id, String batchId, String date, String item, float rate, int quantity,
                String customer, long serverId, String status, long lastModified) {
        this(id, batchId, "", "", date, item, rate, quantity, customer, serverId, status, lastModified);
    }

    @Ignore
    public Sale(String batchId, String date, String item, float rate, int quantity, String customer) {
        this(UUID.randomUUID().toString(), batchId, date, item, rate, quantity, customer, Data.SERVER_ID_NEW, Data.STATUS_NORMAL, 0);
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getBatchId() {
        return batchId;
    }

    public String getItem() {
        return item;
    }

    public float getRate() {
        return rate;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getCustomer() {
        return customer;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }

    public String getDate() {
        return date;
    }

    public String getFarmId() {
        return farmId;
    }

    public String getStoreId() {
        return storeId;
    }

    public float getTotalAmount() {
        return quantity * rate;
    }

    @Ignore
    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }
}
