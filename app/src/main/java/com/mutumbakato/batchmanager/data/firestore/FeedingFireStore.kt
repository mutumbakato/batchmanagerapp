package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.FeedingDataSource
import com.mutumbakato.batchmanager.data.models.Feeding
import java.util.*

/**
 * Created by cato on 3/26/18.
 */

class FeedingFireStore : FeedingDataSource {

    private val db = FirebaseFirestore.getInstance().collection("feeds")

    override fun getMortalityRate(batchId: String, callback: FeedingDataSource.OnMortalityRate) {

    }

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Feeding>) {
        db.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val feedings = ArrayList<Feeding>()
                for (data in task.result!!) {
                    feedings.add(data.toObject(Feeding::class.java))
                }
                if (feedings.size > 0) {
                    callback.onDataLoaded(feedings)
                } else {
                    callback.onEmptyData()
                }
            } else {
                callback.onEmptyData()
            }
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Feeding>) {
        db.document(id).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callback.onDataItemLoaded(task.result!!.toObject(Feeding::class.java)!!)
            } else {
                callback.onDataItemNotAvailable()
            }
        }
    }

    override fun saveData(data: Feeding) {
        db.document(data.id).set(data)
    }

    override fun updateData(data: Feeding) {
        db.document(data.id).set(data)
    }

    override fun refreshData() {
        //Implemented in the repository
    }

    override fun deleteAllData(batchId: String) {
        //Don't do it !!!
    }

    override fun deleteDataItem(item: Feeding, batchId: String) {
        db.document(item.id).delete()
    }

    override fun clearCache() {

    }

    override fun notifyDataChanged() {

    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Feeding>, vararg referenceIds: String) {
        db.whereEqualTo("batchId", referenceIds[0]).addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val dataItems = ArrayList<Feeding>()
                for (data in snapshots) {
                    dataItems.add(data.toObject(Feeding::class.java))
                }
                callback.onDataLoaded(dataItems)
            }
        }
    }
}
