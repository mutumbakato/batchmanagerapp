package com.mutumbakato.batchmanager.data.local

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.SalesDataSource
import com.mutumbakato.batchmanager.data.local.dao.SalesDao
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.sync.Sync
import com.mutumbakato.batchmanager.data.sync.Syncable
import com.mutumbakato.batchmanager.utils.executors.AppExecutors
import org.json.JSONArray
import org.json.JSONException

class SalesLocalSource
//    private SalesRepository repository;

private constructor(private val mAppExecutors: AppExecutors, private val mSalesDao: SalesDao)
    : SalesDataSource, Syncable<Sale> {

    fun insertSale(expense: Sale, onFinish: (() -> Unit)? = null) {
        mAppExecutors.diskIO().execute {
            mSalesDao.insertSale(expense)
            if (onFinish != null)
                mAppExecutors.mainThread().execute(onFinish)
        }
    }

    fun insertAllSales(expenses: List<Sale>) {
        mAppExecutors.diskIO().execute {
            mSalesDao.insertAllSales(expenses)
        }
    }

    fun listFarmSales(farmId: String): LiveData<List<Sale>> {
        return mSalesDao.listFarmSales(farmId)
    }

    fun filterSales(filter: Filter): LiveData<List<Sale>> {
        return mSalesDao.filterSales(filter.id, filter.from, filter.to)
    }

    fun listBatchSales(batchId: String): LiveData<List<Sale>> {
        return mSalesDao.listBatchSales(batchId)
    }

    fun getSaleById(id: String): LiveData<Sale> {
        return mSalesDao.getLiveSaleById(id)
    }

    fun updateSale(expense: Sale, onFinish: (() -> Unit)? = null) {
        mAppExecutors.diskIO().execute {
            mSalesDao.updateSale(expense)
            if (onFinish != null)
                mAppExecutors.mainThread().execute(onFinish)
        }
    }

    fun deleteAllForBatch(batchId: String) {
        mAppExecutors.diskIO().execute {
            mSalesDao.deleteAllForBatch(batchId)
        }
    }

    fun deleteAllForFarm(farmId: String) {
        mAppExecutors.diskIO().execute {
            mSalesDao.deleteFarmSales(farmId)
        }
    }

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Sale>) {
        mAppExecutors.diskIO().execute {
            val sales = mSalesDao.getSales(batchId)
            mAppExecutors.mainThread().execute {
                if (sales.isNotEmpty()) {
                    callback.onDataLoaded(sales)
                } else {
                    callback.onEmptyData()
                }
            }
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Sale>) {
        mAppExecutors.diskIO().execute {
            val sale = mSalesDao.getSaleById(id)
            mAppExecutors.mainThread().execute { callback.onDataItemLoaded(sale) }
        }
    }

    override fun saveData(data: Sale) {
        mAppExecutors.diskIO().execute { mSalesDao.insertSale(data) }
        sync()
    }

    override fun updateData(data: Sale) {
        mAppExecutors.diskIO().execute { mSalesDao.updateSale(data) }
        sync()
    }

    override fun refreshData() {

    }

    override fun deleteAllData(batchId: String) {
        mAppExecutors.diskIO().execute { mSalesDao.deleteSales(batchId) }
    }

    override fun deleteDataItem(item: Sale, batchId: String) {
        mAppExecutors.diskIO().execute { mSalesDao.deleteSaleById(item.id) }
        sync()
    }

    override fun clearCache() {}

    override fun notifyDataChanged() {

    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Sale>, vararg referenceIds: String) {

    }

    override fun getTotalSales(batchId: String, callback: SalesDataSource.OnTotalSales) {
        mAppExecutors.diskIO().execute {
            val totalSales = mSalesDao.totalSales(batchId)
            mAppExecutors.mainThread().execute {
                if (totalSales >= 0)
                    callback.onTotal(totalSales)
            }
        }
    }

    override fun getTable(): String {
        return Sale.TABLE_NAME
    }

    override fun getNew(): List<Sale> {
        return mSalesDao.new
    }

    override fun getUpdated(): List<Sale> {
        return mSalesDao.updated
    }

    override fun getLocalData(): List<Sale> {
        return mSalesDao.local
    }

    override fun getTrash(): List<Long> {
        return mSalesDao.trash
    }

    override fun getLastServerId(): Long {
        return mSalesDao.lastServerId
    }

    override fun handleNewData(newData: JSONArray) {
        val gson = Gson()
        for (n in 0 until newData.length()) {
            try {
                val sale = gson.fromJson(newData.get(n).toString(), Sale::class.java)
                val newSale = Sale(sale.id, sale.batchId, sale.date, sale.item, sale.rate, sale.quantity, sale
                        .customer, sale.serverId, sale.status, sale.lastModified)
                mSalesDao.insertSale(newSale)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    override fun handleModified(modified: JSONArray) {
        val gson = Gson()
        for (n in 0 until modified.length()) {
            try {
                val item = gson.fromJson(modified.get(n).toString(), Sale::class.java)
                mSalesDao.updateSale(item)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    override fun handleSynced(synced: JSONArray) {
        for (n in 0 until synced.length()) {
            try {
                val id = synced.getJSONObject(n).getString("id")
                val serverId = synced.getJSONObject(n).getLong("serverId")
                val lastMod = synced.getJSONObject(n).getLong("lastModified")

                val sale = mSalesDao.getSaleById(id)
                val syncedSale = Sale(sale.id, sale.batchId, sale.date, sale.item, sale.rate, sale.quantity, sale
                        .customer, serverId, sale.status, lastMod)
                mSalesDao.updateSale(syncedSale)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    override fun handleUpdates(updates: JSONArray) {
        for (n in 0 until updates.length()) {
            try {

                val id = updates.getJSONObject(n).getString("id")
                val lastModified = updates.getJSONObject(n).getLong("lastModified")

                val sale = mSalesDao.getSaleById(id)
                val newSale = Sale(sale.id, sale.batchId, sale.date, sale.item, sale.rate, sale.quantity, sale
                        .customer, sale.serverId, sale.status, lastModified)

                mSalesDao.updateSale(newSale)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    override fun handleTrash(trash: Array<String>) {
        for (aTrash in trash) {
            mSalesDao.deleteSaleById(aTrash)
        }
        mSalesDao.clearTrash()
    }

    override fun notifyRepository() {
        //        repository.clearCache();
    }

    private fun sync() {
        Sync.getInstance().sync(this, false)
    }

    companion object {

        @Volatile
        private var INSTANCE: SalesLocalSource? = null

        fun getInstance(appExecutors: AppExecutors, salesDao: SalesDao): SalesLocalSource {
            if (INSTANCE == null)
                INSTANCE = SalesLocalSource(appExecutors, salesDao)
            return INSTANCE as SalesLocalSource
        }
    }
}
