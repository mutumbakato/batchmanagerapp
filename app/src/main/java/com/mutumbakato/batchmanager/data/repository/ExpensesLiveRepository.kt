package com.mutumbakato.batchmanager.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.ExpenseDataSource
import com.mutumbakato.batchmanager.data.local.ExpenseLocalDataSource
import com.mutumbakato.batchmanager.data.models.DataResource
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.models.Status
import com.mutumbakato.batchmanager.data.utils.Logger

class ExpensesLiveRepository constructor(val local: ExpenseLocalDataSource,
                                         val remote: ExpenseDataSource) {

    private val isLoading = MutableLiveData<Boolean>()
    private val error = MutableLiveData<Status>()
    private var isOnLine: Boolean = false

    fun create(expense: Expense, onFinish: (() -> Unit)? = null) {
        Logger.log(expense.batchId, Logger.ACTION_ADDED, expense.description + " to Expenses")
        remote.saveData(expense)
        local.insertExpense(expense, onFinish)
    }

    fun listFarmExpenses(farmId: String): DataResource<List<Expense>> {
        val expenses = local.listFarmExpenses(farmId)
        if (!isOnLine) {
            observeRemote(farmId)
            isOnLine = true
        }
        return DataResource(expenses, error, isLoading)
    }

    fun filter(filter: Filter): DataResource<List<Expense>> {
        if (filter.type == "Farm") {
            val farmId = filter.id
            if (!isOnLine) {
                observeRemote(farmId)
                isOnLine = true
            }
        }
        val expenses = local.filter(filter)
        return DataResource(expenses, error, isLoading)
    }

    fun listExpensesMonths(farmId: String): LiveData<List<String>> {
        return local.listExpensesMonths(farmId)
    }

    fun listBatchExpenses(batchId: String): DataResource<List<Expense>> {
        return DataResource(local.listBatchExpenses(batchId), error, isLoading)
    }

    fun getExpenseById(expenseId: String): DataResource<Expense> {
        val expense = local.getExpenseById(expenseId)
        return DataResource(expense, error, isLoading)
    }

    fun update(expense: Expense, onFinish: (() -> Unit)? = null) {
        Logger.log(expense.batchId, Logger.ACTION_UPDATED, expense.description + " in Expenses")
        remote.updateData(expense)
        local.updateExpense(expense, onFinish)
    }

    fun deleteExpenses(expense: Expense) {
        Logger.log(expense.batchId, Logger.ACTION_DELETED, expense.description + " from Expenses")
        remote.deleteDataItem(expense, "")
        local.deleteDataItem(expense, "")
    }

    private fun observeRemote(farmId: String) {
        remote.observeData(object : BaseDataSource.DataLoadCallback<Expense> {

            override fun onDataLoaded(data: List<Expense>) {
                local.deleteAllForFarm(farmId)
                local.insertAllExpenses(data)
            }

            override fun onEmptyData() {}
            override fun onError(message: String) {}
        }, farmId, "0")
    }

    companion object {

        @Volatile
        private var INSTANCE: ExpensesLiveRepository? = null

        fun getInstance(local: ExpenseLocalDataSource, remote: ExpenseDataSource): ExpensesLiveRepository {
            if (INSTANCE == null)
                INSTANCE = ExpensesLiveRepository(local, remote)
            return INSTANCE!!
        }
    }
}