package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Source
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.FarmDataSource
import com.mutumbakato.batchmanager.data.models.ActivityLog
import com.mutumbakato.batchmanager.data.models.Employee
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import java.util.*

/**
 * Created by cato on 3/28/18.
 */
class FarmFireStore : FarmDataSource {

    private val db = FirebaseFirestore.getInstance().collection("farms")

    override fun setCurrency(farmId: String, currency: String, callback: FarmDataSource.FarmCallback) {
        db.document(farmId)
                .set(LinkedHashMap<String, Any>().put("currency", currency)!!)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        getFarm(farmId, callback)
                    } else {
                        callback.onFail("Failed to create farm")
                    }
                }
    }

    override fun getFarms(userId: String, callbacks: FarmDataSource.FarmListCallback) {
        db.whereEqualTo("userId", userId).get(Source.SERVER).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val farms = ArrayList<Farm>()
                for (data in task.result!!) {
                    farms.add(data.toObject(Farm::class.java))
                }
                if (farms.size > 0) {
                    callbacks.onLoad(farms)
                } else {
                    callbacks.onFail("No farms found!")
                }
            } else {
                callbacks.onFail("Failed to fetch farm data")
            }
        }
    }

    override fun getFarm(id: String, callback: FarmDataSource.FarmCallback) {
        db.document(id).get(Source.SERVER).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callback.onLoad(task.result!!.toObject(Farm::class.java)!!)
            } else {
                callback.onFail("Failed to get farm")
            }
        }
    }

    override fun createFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        db.document(farm.id).set(farm).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                getFarm(farm.id, callBack)
            } else {
                callBack.onFail("Failed to create farm")
            }
        }
    }

    override fun updateFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        db.document(farm.id).set(farm).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                getFarm(farm.id, callBack)
            } else {
                callBack.onFail("Failed to update farm!")
            }
        }
    }

    override fun deleteFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        db.document(farm.id)
                .delete()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        callBack.onLoad(farm)
                    } else {
                        callBack.onFail("Failed to delete farm")
                    }
                }
    }

    override fun addEmployee(employee: Employee, callback: FarmDataSource.FarmCallback) {
        db.document(employee.farmId)
                .collection("workers")
                .document(employee.id)
                .set(employee)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        getFarm(employee.farmId, callback)
                    } else {
                        callback.onFail("Failed to add employee")
                    }
                }
    }

    override fun getEmployees(farmId: String, callback: BaseDataSource.DataLoadCallback<Employee>) {
        db.document(farmId).collection("employees").get(Source.SERVER)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val employees = ArrayList<Employee>()
                        for (data in task.result!!) {
                            employees.add(data.toObject(Employee::class.java))
                            if (employees.size > 0) {
                                callback.onDataLoaded(employees)
                            } else {
                                callback.onEmptyData()
                            }
                        }
                    } else {
                        callback.onError("Failed to get employees")
                    }
                }
    }

    override fun removeEmployee(employee: Employee, callback: FarmDataSource.FarmCallback) {
        db.document(employee.farmId)
                .collection("workers")
                .document(employee.id)
                .delete()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        getFarm(employee.farmId, callback)
                    } else {
                        callback.onFail("Failed to remove employee!")
                    }
                }
    }

    override fun log(log: ActivityLog) {
        db.document(log.batchId).collection("logs").document(log.id).set(log)
    }

    override fun refresh() {

    }

    override fun observeFarms(callback: FarmDataSource.FarmListCallback, vararg referenceIds: String) {
        observeCurrentFarm()
        db.whereEqualTo("userId", referenceIds[0]).addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val dataItems = ArrayList<Farm>()
                for (data in snapshots) {
                    dataItems.add(data.toObject(Farm::class.java))
                }
                callback.onLoad(dataItems)
            }
        }
    }

    private fun observeCurrentFarm() {
        if (PreferenceUtils.farmId.isNotEmpty())
            db.document(PreferenceUtils.farmId).addSnapshotListener { snapshots, e ->
                if (snapshots != null) {
                    val farm = snapshots.toObject(Farm::class.java)
                    if (farm != null)
                        PreferenceUtils.setFarm(farm)
                }
            }
    }
}
