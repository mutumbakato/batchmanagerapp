package com.mutumbakato.batchmanager.data.firestore

import android.text.TextUtils
import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.VaccinationDataSource
import com.mutumbakato.batchmanager.data.VaccinationScheduleDataSource
import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationCheck
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import java.util.*

/**
 * Created by cato on 3/28/18.
 */

class VaccinationFireStore : VaccinationDataSource, VaccinationScheduleDataSource {

    private val batchDb = FirebaseFirestore.getInstance().collection("batches")
    private val scheduleDb = FirebaseFirestore.getInstance().collection("vaccination_schedules")

    override fun listVaccinations(scheduleId: String, callback: BaseDataSource.DataLoadCallback<Vaccination>) {
        if (TextUtils.isEmpty(scheduleId)) {
            callback.onEmptyData()
            return
        }
        scheduleDb.document(scheduleId).collection("vaccinations").get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val vaccinations = ArrayList<Vaccination>()
                for (data in task.result!!) {
                    vaccinations.add(data.toObject(Vaccination::class.java))
                }
                if (vaccinations.size > 0)
                    callback.onDataLoaded(vaccinations)
                else
                    callback.onEmptyData()
            } else {
                callback.onEmptyData()
            }
        }
    }

    override fun getCheckList(bachId: String, callback: BaseDataSource.DataLoadCallback<VaccinationCheck>) {
        batchDb.document(bachId).collection("vaccination").get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val checks = ArrayList<VaccinationCheck>()
                for (data in task.result!!) {
                    checks.add(data.toObject(VaccinationCheck::class.java))
                }
                if (checks.size > 0)
                    callback.onDataLoaded(checks)
                else
                    callback.onEmptyData()
            } else {
                callback.onEmptyData()
            }
        }
    }

    override fun getVaccination(mScheduleId: String, vaccinationId: String,
                                callback: BaseDataSource.DataItemCallback<Vaccination>) {
        if (TextUtils.isEmpty(mScheduleId)) {
            callback.onDataItemNotAvailable()
            return
        }
        scheduleDb.document(mScheduleId)
                .collection("vaccinations")
                .document(vaccinationId)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        callback.onDataItemLoaded(task.result!!.toObject(Vaccination::class.java)!!)
                    } else {
                        callback.onDataItemNotAvailable()

                    }
                }
    }

    override fun createVaccination(vaccination: Vaccination) {
        scheduleDb.document(vaccination.scheduleId)
                .collection("vaccinations")
                .document(vaccination.id)
                .set(vaccination)
    }

    override fun updateVaccination(vaccination: Vaccination) {
        if (vaccination.id != null && vaccination.id.isNotEmpty())
            scheduleDb.document(vaccination.scheduleId)
                    .collection("vaccinations")
                    .document(vaccination.id)
                    .set(vaccination)
    }

    override fun refreshVaccinations() {

    }

    override fun deleteVaccinations(schedule: String, id: String) {
        scheduleDb.document(schedule)
                .collection("vaccinations")
                .document(id)
                .delete()
    }

    override fun deleteAllVaccinations() {
        //Don't do this!
    }

    override fun deleteAllChecks(batchId: String) {
        //Don't do this!!
    }

    override fun addToComplete(batchId: String, vaccination: Vaccination) {
        val check = VaccinationCheck(UUID.randomUUID().toString(), batchId,
                vaccination.scheduleId, vaccination.id, 0, "normal", 0)
        batchDb.document(batchId)
                .collection("vaccination")
                .document(vaccination.id)
                .set(check)
    }

    override fun removeFromComplete(batchId: String, vaccination: Vaccination) {
        batchDb.document(batchId)
                .collection("vaccination")
                .document(vaccination.id)
                .delete()
    }

    override fun addCheckItem(check: VaccinationCheck) {
        batchDb.document(check.batchId)
                .collection("vaccination")
                .document(check.vaccinationId)
                .set(check)
    }

    override fun clearCache() {

    }

    override fun useSchedule(batchId: String, schedule: VaccinationSchedule) {
        batchDb.document(batchId).update("vaccinationSchedule", schedule.id)
    }

    override fun observeVaccination(callback: BaseDataSource.DataLoadCallback<Vaccination>, scheduleId: String) {
        if (TextUtils.isEmpty(scheduleId)) {
            callback.onEmptyData()
            return
        }
        scheduleDb.document(scheduleId).collection("vaccinations").addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val vaccinations = ArrayList<Vaccination>()
                for (data in snapshots) {
                    vaccinations.add(data.toObject(Vaccination::class.java))
                }
                callback.onDataLoaded(vaccinations)
            }
        }
    }

    override fun observeCheckList(bachId: String, callback: BaseDataSource.DataLoadCallback<VaccinationCheck>) {
        batchDb.document(bachId).collection("vaccination").addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val dataItems = ArrayList<VaccinationCheck>()
                for (data in snapshots) {
                    dataItems.add(data.toObject(VaccinationCheck::class.java))
                }
                callback.onDataLoaded(dataItems)
            }
        }
    }


    override fun observeSchedule(callback: BaseDataSource.DataLoadCallback<VaccinationSchedule>, userId: String) {
        scheduleDb.whereEqualTo("farmId", PreferenceUtils.farmId).addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val schedules = ArrayList<VaccinationSchedule>()
                for (data in snapshots) {
                    schedules.add(data.toObject(VaccinationSchedule::class.java))
                }
                callback.onDataLoaded(schedules)
            }
        }
    }

    override fun removeSchedule(batchId: String, scheduleId: String) {
        batchDb.document(batchId).update("vaccinationSchedule", "")
    }

    override fun getAllSchedules(batchId: String, callback: BaseDataSource.DataLoadCallback<VaccinationSchedule>) {
        scheduleDb.whereEqualTo("farmId", PreferenceUtils.farmId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val schedules = ArrayList<VaccinationSchedule>()
                for (data in task.result!!) {
                    schedules.add(data.toObject(VaccinationSchedule::class.java))
                }
                if (schedules.size > 0)
                    callback.onDataLoaded(schedules)
                else
                    callback.onEmptyData()
            } else {
                callback.onEmptyData()
            }
        }
    }

    override fun getSchedule(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<VaccinationSchedule>) {
        if (TextUtils.isEmpty(id)) {
            callback.onDataItemNotAvailable()
            return
        }
        scheduleDb.document(id).get().addOnCompleteListener { task ->
            if (task.isSuccessful && task.result != null) {
                val schedule = task.result?.toObject(VaccinationSchedule::class.java)
                if (schedule != null) {
                    callback.onDataItemLoaded(schedule)
                } else {
                    callback.onDataItemNotAvailable()
                }
            } else {
                callback.onDataItemNotAvailable()
            }
        }
    }

    override fun saveSchedule(data: VaccinationSchedule) {
        scheduleDb.document(data.id).set(data)
    }

    override fun updateSchedule(data: VaccinationSchedule) {
        scheduleDb.document(data.id).set(data)
    }

    override fun refreshData() {

    }

    override fun deleteAllSchedules(batchId: String) {
        //Don't do this
    }

    override fun deleteSchedule(id: String, batchId: String) {
        scheduleDb.document(id).collection("vaccinations").document().delete()
        scheduleDb.document(id).delete()
    }

    override fun notifyDataChanged() {
    }
}
