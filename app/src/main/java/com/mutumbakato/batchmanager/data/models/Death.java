package com.mutumbakato.batchmanager.data.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.firebase.firestore.Exclude;
import com.google.gson.annotations.Expose;
import com.mutumbakato.batchmanager.data.utils.Data;

import java.util.UUID;

import static com.mutumbakato.batchmanager.data.models.Death.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class Death {

    public static final String TABLE_NAME = "death";

    @Expose
    @PrimaryKey
    @ColumnInfo(name = "_id")
    @NonNull
    private String id;

    @Expose
    @ColumnInfo(name = "batch_id")
    private String batchId;

    @Expose
    @ColumnInfo(name = "date")
    private String date;

    @Expose
    @ColumnInfo(name = "count")
    private int count;

    @Expose
    @ColumnInfo(name = "comment")
    private String comment;

    @Expose
    @Exclude
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Expose
    @ColumnInfo(name = "status")
    private String status;

    @Ignore
    public Death() {
    }

    public Death(@NonNull String id, String batchId, String date, int count, String comment, long serverId,
                 String status, long lastModified) {
        this.id = id;
        this.date = date;
        this.count = count;
        this.comment = comment;
        this.batchId = batchId;
        this.serverId = serverId;
        this.status = status;
        this.lastModified = lastModified;
    }

    @Ignore
    public Death(String batchId, String date, int count, String comment) {
        this(UUID.randomUUID().toString(), batchId, date, count, comment, Data.SERVER_ID_NEW, Data.STATUS_NORMAL, 0);
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public int getCount() {
        return count;
    }

    public String getComment() {
        return comment;
    }

    public String getBatchId() {
        return batchId;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }
}
