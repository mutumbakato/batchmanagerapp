package com.mutumbakato.batchmanager.data.repository

import androidx.lifecycle.MutableLiveData
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.SalesDataSource
import com.mutumbakato.batchmanager.data.local.SalesLocalSource
import com.mutumbakato.batchmanager.data.models.DataResource
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.models.Status
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.Logger
import com.mutumbakato.batchmanager.utils.Currency

class SalesLiveRepository constructor(val local: SalesLocalSource, val remote: SalesDataSource) {

    private val isLoading = MutableLiveData<Boolean>()
    private val error = MutableLiveData<Status>()
    private var isOnLine: Boolean = false

    fun create(sale: Sale, onFinish: (() -> Unit)? = null) {
        Logger.log(sale.batchId, Logger.ACTION_ADDED, sale.item
                + " for "
                + " " + Currency.format(sale.totalAmount)
                + " to Sales")
        remote.saveData(sale)
        local.insertSale(sale, onFinish)
    }

    fun listFarmSales(farmId: String): DataResource<List<Sale>> {
        if (!isOnLine) {
            observeRemote(farmId)
            isOnLine = true
        }
        val sales = local.listFarmSales(farmId)
        return DataResource(sales, error, isLoading)
    }

    fun filterSales(filter: Filter): DataResource<List<Sale>> {
        if (filter.type == "Farm") {
            if (!isOnLine) {
                observeRemote(filter.id)
                isOnLine = true
            }
        }
        val sales = local.filterSales(filter)
        return DataResource(sales, error, isLoading)
    }

    fun listBatchSales(batchId: String): DataResource<List<Sale>> {
        val sales = local.listBatchSales(batchId)
        return DataResource(sales, error, isLoading)
    }

    fun getSaleById(id: String?): DataResource<Sale> {
        val sales = local.getSaleById(id ?: "0")
        return DataResource(sales, error, isLoading)
    }

    fun update(sale: Sale, onFinish: (() -> Unit)? = null) {
        Logger.log(sale.batchId, Logger.ACTION_UPDATED, sale.item
                + " for "
                + Currency.format(sale.totalAmount)
                + " in Sales")
        remote.updateData(sale)
        local.updateSale(sale, onFinish)
    }

    fun deleteSales(sale: Sale) {
        Logger.log(sale.batchId, Logger.ACTION_DELETED,
                sale.item
                        + " of " + PreferenceUtils.currency
                        + " " + Currency.format(sale.totalAmount)
                        + " on " + sale.date + " from Sales")
        remote.deleteDataItem(sale, "")
        local.deleteDataItem(sale, "")
    }

    private fun observeRemote(farmId: String) {
        remote.observeData(object : BaseDataSource.DataLoadCallback<Sale> {
            override fun onDataLoaded(data: List<Sale>) {
                local.deleteAllForFarm(farmId)
                local.insertAllSales(data)
            }

            override fun onEmptyData() {}
            override fun onError(message: String) {}
        }, farmId)
    }

    companion object {

        @Volatile
        private var INSTANCE: SalesLiveRepository? = null

        fun getInstance(local: SalesLocalSource, remote: SalesDataSource): SalesLiveRepository {
            if (INSTANCE == null)
                INSTANCE = SalesLiveRepository(local, remote)
            return INSTANCE!!
        }
    }
}