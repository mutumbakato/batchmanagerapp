package com.mutumbakato.batchmanager.data.local


import android.location.Location
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.local.dao.UserDao
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.utils.executors.AppExecutors

/**
 * Created by cato on 10/12/17.
 */

class UserLocalDataSource private constructor(private val dao: UserDao, private val appExecutors: AppExecutors) : UserDataSource {

    override fun login(email: String, password: String, callBack: UserDataSource.UserCallBack) {

    }

    override fun register(user: User, callBack: UserDataSource.UserCallBack) {

    }

    override fun logOut(callBack: UserDataSource.UserCallBack) {
        appExecutors.diskIO().execute {
            clearData()
            appExecutors.mainThread().execute { callBack.onSuccess(null) }
        }
    }

    override fun resetPassword(email: String, callBack: UserDataSource.PasswordCallBack) {

    }

    override fun getUserById(id: String, callBack: UserDataSource.UserCallBack) {

    }

    override fun getUserByEmail(email: String, callBack: UserDataSource.UserCallBack) {

    }

    override fun updateUser(user: User, callBack: UserDataSource.UserCallBack) {

    }

    override fun deleteAccount(user: User, callBack: UserDataSource.UserCallBack) {

    }

    override fun setLocation(uid: String, location: Location) {
        TODO("Not yet implemented")
    }

    private fun clearData() {
        dao.clearDeaths()
        dao.clearBatches()
        dao.clearExpenses()
        dao.clearFeeds()
        dao.clearSales()
        dao.clearVaccinationChecks()
        dao.clearExpensesCategories()
        dao.clearVaccinations()
        dao.clearVaccinationSchedules()
        dao.clearEggs()
        dao.clearFarms()
    }

    companion object {

        private var INSTANCE: UserLocalDataSource? = null

        fun getInstance(dao: UserDao, appExecutors: AppExecutors): UserLocalDataSource {
            if (INSTANCE == null) {
                INSTANCE = UserLocalDataSource(dao, appExecutors)
            }
            return INSTANCE as UserLocalDataSource
        }
    }
}
