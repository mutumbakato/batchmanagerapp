package com.mutumbakato.batchmanager.data.models

data class BatchCount(val count: Int = 0, val dead: Int = 0) {

    val remaining: Int
        get() = count - dead

    val mortalityRate: Float
        get() = ((dead * 100f) / count)
}