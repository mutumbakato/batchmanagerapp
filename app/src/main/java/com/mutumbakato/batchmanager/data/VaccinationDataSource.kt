package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationCheck

interface VaccinationDataSource {

    fun listVaccinations(scheduleId: String, callback: BaseDataSource.DataLoadCallback<Vaccination>)

    fun getCheckList(bachId: String, callback: BaseDataSource.DataLoadCallback<VaccinationCheck>)

    fun getVaccination(mScheduleId: String, vaccinationId: String, callback: BaseDataSource.DataItemCallback<Vaccination>)

    fun createVaccination(vaccination: Vaccination)

    fun updateVaccination(vaccination: Vaccination)

    fun refreshVaccinations()

    fun deleteVaccinations(schedule: String, id: String)

    fun deleteAllVaccinations()

    fun deleteAllChecks(batchId: String)

    fun addToComplete(batchId: String, vaccination: Vaccination)

    fun removeFromComplete(batchID: String, vaccination: Vaccination)

    fun addCheckItem(check: VaccinationCheck)

    fun clearCache()

    fun observeVaccination(callback: BaseDataSource.DataLoadCallback<Vaccination>, scheduleId: String)

    fun observeCheckList(bachId: String, callback: BaseDataSource.DataLoadCallback<VaccinationCheck>)


}
