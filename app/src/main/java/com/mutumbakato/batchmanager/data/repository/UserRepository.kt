package com.mutumbakato.batchmanager.data.repository

import android.location.Location
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.models.User

/**
 * Created by cato on 10/31/17.
 */
class UserRepository private constructor(private val localDataSource: UserDataSource, private val remoteDataSource: UserDataSource,
                                         private val firebaseDataSource: UserDataSource) : UserDataSource {

    override fun login(email: String, password: String, callBack: UserDataSource.UserCallBack) {
        firebaseDataSource.getUserByEmail(email, object : UserDataSource.UserCallBack {
            override fun onSuccess(user: User?) {
                var pwd = password
                if (pwd.length < 6) {
                    pwd = lengthenPassword(pwd)
                }
                firebaseDataSource.login(email, pwd, callBack)
            }

            override fun onError(message: String) {
                remoteDataSource.login(email, password, object : UserDataSource.UserCallBack {
                    override fun onSuccess(user: User?) {
                        var pwd = password
                        if (pwd.length < 6) {
                            pwd = lengthenPassword(password)
                        }
                        user?.password = pwd
                        firebaseDataSource.register(user!!, callBack)
                    }

                    override fun onError(message: String) {
                        callBack.onError(message)
                    }
                })
            }
        })
    }

    override fun register(user: User, callBack: UserDataSource.UserCallBack) {
        firebaseDataSource.register(user, callBack)
    }

    override fun logOut(callBack: UserDataSource.UserCallBack) {
        localDataSource.logOut(object : UserDataSource.UserCallBack {
            override fun onSuccess(user: User?) {
                firebaseDataSource.logOut(callBack)
            }

            override fun onError(message: String) {

            }
        })
    }

    override fun resetPassword(email: String, callBack: UserDataSource.PasswordCallBack) {
        firebaseDataSource.getUserByEmail(email, object : UserDataSource.UserCallBack {
            override fun onSuccess(user: User?) {
                firebaseDataSource.resetPassword(email, callBack)
            }

            override fun onError(message: String) {
                remoteDataSource.resetPassword(email, callBack)
            }
        })
    }

    override fun getUserById(id: String, callBack: UserDataSource.UserCallBack) {
        firebaseDataSource.getUserById(id, callBack)
    }

    override fun getUserByEmail(email: String, callBack: UserDataSource.UserCallBack) {
        firebaseDataSource.getUserByEmail(email, callBack)
    }

    override fun updateUser(user: User, callBack: UserDataSource.UserCallBack) {
        firebaseDataSource.updateUser(user, callBack)
    }

    override fun deleteAccount(user: User, callBack: UserDataSource.UserCallBack) {
        firebaseDataSource.deleteAccount(user, callBack)
    }

    override fun setLocation(uid: String, location: Location) {
//        TODO("Not yet implemented")
    }

    private fun lengthenPassword(password: String): String {
        var pwd = password
        val len = password.length
        val builder = StringBuilder(pwd)
        if (len < 6) {
            for (i in len..5) {
                builder.append("_")
            }
        }
        pwd = builder.toString()
        return pwd
    }

    companion object {

        private var INSTANCE: UserRepository? = null

        fun getInstance(localDataSource: UserDataSource,
                        remoteDataSource: UserDataSource,
                        firebaseDataSource: UserDataSource): UserRepository {
            if (INSTANCE == null)
                INSTANCE = UserRepository(localDataSource, remoteDataSource, firebaseDataSource)
            return INSTANCE as UserRepository
        }
    }
}
