package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.VaccinationDataSource
import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationCheck
import com.mutumbakato.batchmanager.data.utils.Logger
import java.util.*

class VaccinationRepository private constructor(private val mVaccinationRemoteDataSource: VaccinationDataSource,
                                                private val mVaccinationLocalDataSource: VaccinationDataSource) : VaccinationDataSource {

    private var mCachedVaccinations: MutableMap<String, MutableMap<String, Vaccination>>? = null
    private var mCheckListCache: MutableMap<String, MutableList<VaccinationCheck>>? = null
    private var mCacheIsDirty = false

    override fun listVaccinations(scheduleId: String, callback: BaseDataSource.DataLoadCallback<Vaccination>) {
        observeVaccination(callback, scheduleId)
        if (mCachedVaccinations != null && mCachedVaccinations!![scheduleId] != null && !mCacheIsDirty) {
            callback.onDataLoaded(ArrayList(mCachedVaccinations!![scheduleId]!!.values))
        } else {
            mVaccinationLocalDataSource.listVaccinations(scheduleId, object : BaseDataSource.DataLoadCallback<Vaccination> {
                override fun onDataLoaded(data: List<Vaccination>) {
                    refreshCache(data)
                    callback.onDataLoaded(data)
                }

                override fun onEmptyData() {
                    getVaccinationsFromRemoteSource(scheduleId, callback)
                }

                override fun onError(message: String) {
                    callback.onError(message)
                }
            })
        }
    }

    override fun getCheckList(bachId: String, callback: BaseDataSource.DataLoadCallback<VaccinationCheck>) {
        observeCheckList(bachId, callback)
        mVaccinationLocalDataSource.getCheckList(bachId, object : BaseDataSource.DataLoadCallback<VaccinationCheck> {
            override fun onDataLoaded(data: List<VaccinationCheck>) {
                callback.onDataLoaded(data)
                refreshCheckCache(bachId, data)
            }

            override fun onEmptyData() {
                mVaccinationRemoteDataSource.getCheckList(bachId, object : BaseDataSource.DataLoadCallback<VaccinationCheck> {
                    override fun onDataLoaded(data: List<VaccinationCheck>) {
                        refreshCheckLocalDataSource(bachId, data)
                        callback.onDataLoaded(data)
                    }

                    override fun onEmptyData() {
                        callback.onEmptyData()
                    }

                    override fun onError(message: String) {
                        callback.onError(message)
                    }
                })
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })
    }

    private fun refreshCheckCache(batchId: String, data: List<VaccinationCheck>) {
        if (mCheckListCache == null) {
            mCheckListCache = LinkedHashMap()
        }
        mCheckListCache!![batchId] = data as MutableList
    }

    override fun getVaccination(mScheduleId: String, vaccinationId: String, callback: BaseDataSource.DataItemCallback<Vaccination>) {
        if (mCachedVaccinations != null && mCachedVaccinations!![mScheduleId] != null && mCacheIsDirty) {
            mCachedVaccinations!![mScheduleId]!![vaccinationId]?.let { callback.onDataItemLoaded(it) }
        } else {
            mVaccinationLocalDataSource.getVaccination(mScheduleId, vaccinationId, object : BaseDataSource.DataItemCallback<Vaccination> {
                override fun onDataItemLoaded(data: Vaccination) {
                    addToCache(data)
                    callback.onDataItemLoaded(data)
                }

                override fun onDataItemNotAvailable() {
                    callback.onDataItemNotAvailable()
                }

                override fun onError(message: String) {
                    callback.onError(message)
                }
            })
        }
    }

    override fun createVaccination(vaccination: Vaccination) {
        mVaccinationLocalDataSource.createVaccination(vaccination)
        mVaccinationRemoteDataSource.createVaccination(vaccination)
        addToCache(vaccination)
    }

    override fun updateVaccination(vaccination: Vaccination) {
        mVaccinationLocalDataSource.updateVaccination(vaccination)
        mVaccinationRemoteDataSource.updateVaccination(vaccination)
        //addToCache(vaccination);
        if (mCachedVaccinations != null && mCachedVaccinations!![vaccination.scheduleId] != null) {
            mCachedVaccinations!![vaccination.scheduleId]!![vaccination.id] = vaccination
        }
    }

    override fun refreshVaccinations() {
        mCacheIsDirty = true
    }

    override fun deleteVaccinations(schedule: String, id: String) {
        mVaccinationLocalDataSource.deleteVaccinations(schedule, id)
        mVaccinationRemoteDataSource.deleteVaccinations(schedule, id)
        if (mCachedVaccinations != null && mCachedVaccinations!![schedule] != null) {
            mCachedVaccinations!![schedule]!!.remove(id)
        }
    }

    override fun deleteAllVaccinations() {
        mVaccinationLocalDataSource.deleteAllVaccinations()
        if (mCachedVaccinations != null) {
            mCachedVaccinations!!.clear()
        }
    }

    override fun deleteAllChecks(batchId: String) {
        //mVaccinationLocalDataSource.deleteAllChecks(batchId, vaccinationId);
        //mVaccinationRemoteDataSource.deleteAllChecks(batchId);
    }

    override fun addToComplete(batchId: String, vaccination: Vaccination) {
        Logger.log(batchId, Logger.ACTION_COMPLETED, vaccination.infection + " vaccination")
        val check = VaccinationCheck(UUID.randomUUID().toString(),
                batchId, vaccination.scheduleId, vaccination.id, 0, "normal", 0)
        mVaccinationLocalDataSource.addToComplete(batchId, vaccination)
        mVaccinationRemoteDataSource.addToComplete(batchId, vaccination)

        if (mCheckListCache == null) {
            mCheckListCache = LinkedHashMap()
        }

        if (mCheckListCache!![batchId] == null) {
            mCheckListCache!![batchId] = ArrayList()
        }
        mCheckListCache!![batchId]!!.add(check)
    }

    override fun removeFromComplete(batchID: String, vaccination: Vaccination) {
        Logger.log(batchID, Logger.ACTION_UNCOMPLETED, vaccination.infection + " vaccination")
        mVaccinationLocalDataSource.removeFromComplete(batchID, vaccination)
        mVaccinationRemoteDataSource.removeFromComplete(batchID, vaccination)
        if (mCheckListCache != null && mCheckListCache!![batchID] != null) {
            mCheckListCache!!.remove(batchID)
        }
    }

    override fun addCheckItem(check: VaccinationCheck) {
        //
    }

    override fun clearCache() {
        mCachedVaccinations = null
    }

    override fun observeVaccination(callback: BaseDataSource.DataLoadCallback<Vaccination>, scheduleId: String) {
        mVaccinationRemoteDataSource.observeVaccination(object : BaseDataSource.DataLoadCallback<Vaccination> {
            override fun onDataLoaded(data: List<Vaccination>) {
                callback.onDataLoaded(data)
                refreshLocalDataSource(data)
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        }, scheduleId)
    }

    override fun observeCheckList(bachId: String, callback: BaseDataSource.DataLoadCallback<VaccinationCheck>) {
        mVaccinationRemoteDataSource.observeCheckList(bachId, object : BaseDataSource.DataLoadCallback<VaccinationCheck> {
            override fun onDataLoaded(data: List<VaccinationCheck>) {
                callback.onDataLoaded(data)
                refreshCheckLocalDataSource(bachId, data)
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        })
    }

    private fun getVaccinationsFromRemoteSource(scheduleId: String, callback: BaseDataSource.DataLoadCallback<Vaccination>) {
        mVaccinationRemoteDataSource.listVaccinations(scheduleId, object : BaseDataSource.DataLoadCallback<Vaccination> {
            override fun onDataLoaded(data: List<Vaccination>) {
                refreshLocalDataSource(data)
                callback.onDataLoaded(data)
            }

            override fun onEmptyData() {
                callback.onEmptyData()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })

    }

    private fun refreshLocalDataSource(vaccinations: List<Vaccination>) {
        mVaccinationLocalDataSource.deleteAllVaccinations()
        for (vaccination in vaccinations) {
            mVaccinationLocalDataSource.createVaccination(vaccination)
        }
        refreshCache(vaccinations)
    }

    private fun refreshCheckLocalDataSource(batchId: String, checks: List<VaccinationCheck>) {
        mVaccinationLocalDataSource.deleteAllChecks(batchId)
        for (check in checks) {
            mVaccinationLocalDataSource.addCheckItem(check)
        }
        if (checks.isNotEmpty())
            refreshCheckCache(checks[0].batchId, checks)
        else
            mCheckListCache = null
    }

    private fun refreshCache(vaccinations: List<Vaccination>) {
        for (vaccination in vaccinations) {
            addToCache(vaccination)
        }
    }

    private fun addToCache(vaccination: Vaccination) {
        if (mCachedVaccinations == null)
            mCachedVaccinations = LinkedHashMap()
        if (mCachedVaccinations!![vaccination.scheduleId] == null) {
            mCachedVaccinations!![vaccination.scheduleId] = LinkedHashMap()
        }
        mCachedVaccinations!![vaccination.scheduleId]!!.put(vaccination.id, vaccination)
    }

    companion object {

        private var INSTANCE: VaccinationRepository? = null

        /**
         * Returns the single instance of this class, creating it if necessary.
         *
         * @return the [VaccinationRepository] instance
         */
        fun getInstance(batchesRemoteDataSource: VaccinationDataSource,
                        batchesLocalDataSource: VaccinationDataSource): VaccinationRepository {
            if (INSTANCE == null) {
                INSTANCE = VaccinationRepository(batchesRemoteDataSource,
                        batchesLocalDataSource)
            }
            return INSTANCE as VaccinationRepository
        }
    }

}
