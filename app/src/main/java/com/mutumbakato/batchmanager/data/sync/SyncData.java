package com.mutumbakato.batchmanager.data.sync;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

public class SyncData<T> {

    @Expose
    public String table;
    @Expose
    public List<T> newData;
    @Expose
    public List<T> updated;
    @Expose
    public List<Long> trash;

    private SyncData(List<T> newData, List<T> updated, List<T> localData,
                     List<Long> trash, long lastServerId, String table) {
        this.table = table;
        this.newData = newData;
        this.updated = updated;
        this.trash = trash;
    }

    public static ArrayList<SyncData<Object>> build(Syncable<Object> syncable) {
        ArrayList<SyncData<Object>> data = new ArrayList<>();
        data.add(new SyncData<>(syncable.getNew(), syncable.getUpdated(),
                syncable.getLocalData(), syncable.
                getTrash(), syncable.getLastServerId(),
                syncable.getTable()));
        return data;
    }

    public static ArrayList<SyncData<Object>> build(List<Syncable<Object>> syncables) {
        ArrayList<SyncData<Object>> data = new ArrayList<>();
        for (int i = 0; i < syncables.size(); i++) {
            data.add(new SyncData<>(syncables.get(i).getNew(), syncables.get(i).getUpdated(),
                    syncables.get(i).getLocalData(), syncables.get(i).
                    getTrash(), syncables.get(i).getLastServerId(),
                    syncables.get(i).getTable()));
        }
        return data;
    }

    public static class Builder {
        public static ArrayList<SyncData> build(Syncable<Object> syncable) {
            ArrayList<SyncData> data = new ArrayList<>();
            data.add(new SyncData<>(syncable.getNew(), syncable.getUpdated(),
                    syncable.getLocalData(), syncable.
                    getTrash(), syncable.getLastServerId(),
                    syncable.getTable()));
            return data;
        }

        public static ArrayList<SyncData> build(List<Syncable> syncables) {
            ArrayList<SyncData> data = new ArrayList<>();
            for (int i = 0; i < syncables.size(); i++) {
                data.add(new SyncData<>(syncables.get(i).getNew(), syncables.get(i).getUpdated(),
                        syncables.get(i).getLocalData(), syncables.get(i).
                        getTrash(), syncables.get(i).getLastServerId(),
                        syncables.get(i).getTable()));
            }
            return data;
        }

    }
}
