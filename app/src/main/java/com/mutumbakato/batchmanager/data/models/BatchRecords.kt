package com.mutumbakato.batchmanager.data.models

data class BatchRecords(
        var batches: List<Batch> = arrayListOf(),
        val feeds: MutableList<Feeds> = arrayListOf(),
        val weight: MutableList<Weight> = arrayListOf(),
        val mortality: MutableList<Death> = arrayListOf(),
        val water: MutableList<Water> = arrayListOf(),
        val eggs: MutableList<Eggs> = arrayListOf()) {

    val hasBatch: Boolean
        get() = batches.isNotEmpty()

    val hasFeeds: Boolean
        get() = feeds.isNotEmpty()

    val hasOthers: Boolean
        get() = weight.isNotEmpty() || mortality.isNotEmpty() || water.isNotEmpty() || eggs.isNotEmpty()
}