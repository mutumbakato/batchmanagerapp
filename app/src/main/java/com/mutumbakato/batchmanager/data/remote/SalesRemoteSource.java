package com.mutumbakato.batchmanager.data.remote;

import android.os.Handler;
import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.SalesDataSource;
import com.mutumbakato.batchmanager.data.models.Sale;

import java.util.LinkedHashMap;
import java.util.Map;


public class SalesRemoteSource implements SalesDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 3000;
    private final static Map<String, Sale> SALES_SERVICE_DATA;
    private static SalesRemoteSource INSTANCE = null;

    static {
        SALES_SERVICE_DATA = new LinkedHashMap<>(2);
//        addSale("1", "2017-08-15", "Chicken", 9000, 50, "Unknown");
//        addSale("1", "2017-07-07", "Chicken", 10000, 23, "Unknown");
    }

    private static void addSale(String batch, String date, String item, float rate, int quantity, String customer) {
        Sale newSale = new Sale(batch, date, item, rate, quantity, customer);
        SALES_SERVICE_DATA.put(newSale.getId(), newSale);
    }

    public static SalesRemoteSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new SalesRemoteSource();
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    @Override
    public void getAll(@NonNull String batchId, @NonNull final DataLoadCallback<Sale> callback) {
        callback.onEmptyData();
    }

    @Override
    public void getOne(@NonNull final String id, @NonNull final String batchId, @NonNull final DataItemCallback<Sale> callback) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.onDataItemLoaded(SALES_SERVICE_DATA.get(id));
            }
        }, SERVICE_LATENCY_IN_MILLIS);
    }

    @Override
    public void saveData(@NonNull Sale data) {
        SALES_SERVICE_DATA.put(data.getId(), data);
    }

    @Override
    public void updateData(@NonNull Sale data) {
        SALES_SERVICE_DATA.put(data.getId(), data);
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllData(@NonNull String batchId) {
        SALES_SERVICE_DATA.clear();
    }

    @Override
    public void deleteDataItem(@NonNull Sale item, @NonNull String batchId) {
        SALES_SERVICE_DATA.remove(item);
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void observeData(DataLoadCallback<Sale> callback, String... referenceIds) {

    }

    @Override
    public void getTotalSales(String batchId, OnTotalSales callback) {
        //Total sales only implemented by local source
    }
}
