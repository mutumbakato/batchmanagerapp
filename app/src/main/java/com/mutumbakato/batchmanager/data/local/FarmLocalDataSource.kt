package com.mutumbakato.batchmanager.data.local

import android.annotation.SuppressLint
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.FarmDataSource
import com.mutumbakato.batchmanager.data.local.dao.FarmDao
import com.mutumbakato.batchmanager.data.models.ActivityLog
import com.mutumbakato.batchmanager.data.models.Employee
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.utils.executors.AppExecutors
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FarmLocalDataSource private constructor(private val farmDao: FarmDao, val executors: AppExecutors = AppExecutors()) : FarmDataSource {

    override fun setCurrency(id: String, currency: String, callback: FarmDataSource.FarmCallback) {}

    @SuppressLint("CheckResult")
    override fun getFarms(userId: String, callbacks: FarmDataSource.FarmListCallback) {
        farmDao.getAllFarms(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ callbacks.onLoad(it) }, { callbacks.onFail("No users found") })
    }

    @SuppressLint("CheckResult")
    override fun getFarm(id: String, callback: FarmDataSource.FarmCallback) {
        farmDao.getFarmById(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ callback.onLoad(it) }, { callback.onFail("Farm not found") })
    }

    @SuppressLint("CheckResult")
    override fun createFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        Completable.fromAction { farmDao.createFarm(farm) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onComplete() {
                        farmDao.getFarmById(farm.id)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ callBack.onLoad(it) }, { it.printStackTrace() })
                    }

                    override fun onError(e: Throwable) {
                        callBack.onFail("Failed to create farm")
                    }
                })
    }

    @SuppressLint("CheckResult")
    override fun updateFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        Completable.fromAction { farmDao.updateFarm(farm) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onComplete() {
                        farmDao.getFarmById(farm.id)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ callBack.onLoad(it) }, { it.printStackTrace() })
                    }

                    override fun onError(e: Throwable) {

                    }
                })
    }

    override fun deleteFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        Completable.fromAction { farmDao.deleteFarmById(farm.id) }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : CompletableObserver {
                    override fun onSubscribe(d: Disposable) {

                    }

                    override fun onComplete() {
                        callBack.onLoad(farm)
                    }

                    override fun onError(e: Throwable) {
                        callBack.onFail("Failed to delete farm")
                    }
                })
    }

    override fun addEmployee(employee: Employee, callback: FarmDataSource.FarmCallback) {

    }

    override fun getEmployees(farmId: String, callback: BaseDataSource.DataLoadCallback<Employee>) {

    }

    override fun removeEmployee(employee: Employee, callback: FarmDataSource.FarmCallback) {

    }

    override fun log(log: ActivityLog) {

    }

    override fun refresh() {

    }

    override fun observeFarms(callback: FarmDataSource.FarmListCallback, vararg referenceIds: String) {

    }

    companion object {

        private var INSTANCE: FarmLocalDataSource? = null

        fun getInstance(dao: FarmDao): FarmLocalDataSource {
            if (INSTANCE == null) {
                INSTANCE = FarmLocalDataSource(dao)
            }
            return INSTANCE as FarmLocalDataSource
        }
    }
}
