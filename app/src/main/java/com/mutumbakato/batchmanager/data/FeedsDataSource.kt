package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.models.Feeds

interface FeedsDataSource {

    fun createFeeds(feeds: Feeds, onSuccess: (Feeds) -> Unit)

    fun listFeeds(batchId: String, onSuccess: (List<Feeds>) -> Unit)

    fun listOldFeeds(batchId: String, onSuccess: (List<Feeding>) -> Unit)

    fun getFeeds(id: String, onSuccess: (Feeds) -> Unit)

    fun updateFeeds(feeds: Feeds, onSuccess: (Feeds) -> Unit)

    fun deleteFeeds(feeds: Feeds, onSuccess: () -> Unit)

}