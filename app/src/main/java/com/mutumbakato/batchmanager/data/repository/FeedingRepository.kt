package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.FeedingDataSource
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.utils.Logger
import java.util.*

class FeedingRepository private constructor(private val mFeedsRemoteDataSource: FeedingDataSource, private val mFeedsLocalDataSource: FeedingDataSource) : FeedingDataSource {

    private var mListener: FeedingDataSource.DataUpdatedListener? = null

    /**
     * This variable has package local visibility so it can be accessed from tests.
     */
    private var mCachedFeeds: MutableMap<String, MutableMap<String, Feeding>>? = null
    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */
    private var mCacheIsDirty = false

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Feeding>) {

        observeData(callback, batchId)
        if (mCachedFeeds != null && mCachedFeeds!![batchId] != null && !mCacheIsDirty) {
            callback.onDataLoaded(ArrayList(mCachedFeeds!![batchId]!!.values))
            return
        }

        if (mCacheIsDirty) {
            getFeedsFromRemoteSource(batchId, callback)
        } else {
            mFeedsLocalDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Feeding> {
                override fun onDataLoaded(data: List<Feeding>) {
                    refreshCache(batchId, data)
                    callback.onDataLoaded(data)
                }

                override fun onEmptyData() {
                    getFeedsFromRemoteSource(batchId, callback)
                }

                override fun onError(message: String) {
                }
            })
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Feeding>) {
        if (mCachedFeeds != null && mCachedFeeds!![batchId] != null && !mCacheIsDirty) {
            mCachedFeeds!![batchId]!![id]?.let { callback.onDataItemLoaded(it) }
        } else {
            mFeedsRemoteDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Feeding> {
                override fun onDataItemLoaded(data: Feeding) {
                    if (mCachedFeeds == null)
                        mCachedFeeds = LinkedHashMap()
                    mCachedFeeds!![batchId]!![data.id] = data
                    callback.onDataItemLoaded(data)
                }

                override fun onDataItemNotAvailable() {
                    mFeedsRemoteDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Feeding> {
                        override fun onDataItemLoaded(data: Feeding) {
                            if (mCachedFeeds == null)
                                mCachedFeeds = LinkedHashMap()
                            mCachedFeeds!![batchId]!![data.id] = data
                            callback.onDataItemLoaded(data)
                        }

                        override fun onDataItemNotAvailable() {
                            if (mCachedFeeds == null)
                                mCachedFeeds = LinkedHashMap()
                            mCachedFeeds!!.remove(id)
                            callback.onDataItemNotAvailable()
                        }

                        override fun onError(message: String) {

                        }
                    })
                }

                override fun onError(message: String) {

                }
            })
        }
    }

    override fun saveData(data: Feeding) {
        Logger.log(data.batchId, Logger.ACTION_ADDED, data.quantity.toString() + (if (data.item == "WaterActivity") "Ltr" else "kg") + " of " + data.item + " to Feeding")
        mFeedsLocalDataSource.saveData(data)
        mFeedsRemoteDataSource.saveData(data)
        if (isCacheAvailable(data.batchId)) {
            mCachedFeeds!![data.batchId]!![data.id] = data
        }
        notifyDataChanged()
    }

    override fun updateData(data: Feeding) {
        Logger.log(data.batchId, Logger.ACTION_UPDATED, data.item + " for " + data.date + " in feeding records")
        mFeedsLocalDataSource.updateData(data)
        mFeedsRemoteDataSource.updateData(data)
        if (isCacheAvailable(data.batchId))
            mCachedFeeds!![data.batchId]!![data.id] = data
        notifyDataChanged()
    }

    override fun refreshData() {
        mCacheIsDirty = true
    }

    override fun deleteAllData(batchId: String) {
        mFeedsLocalDataSource.deleteAllData(batchId)
        mFeedsRemoteDataSource.deleteAllData(batchId)

        if (isCacheAvailable(batchId))
            mCachedFeeds!![batchId]!!.clear()
        notifyDataChanged()
    }


    override fun deleteDataItem(item: Feeding, batchId: String) {
        Logger.log(batchId, Logger.ACTION_DELETED, item.item + " on " + item.date + " from feeding records")
        mFeedsLocalDataSource.deleteDataItem(item, batchId)
        mFeedsRemoteDataSource.deleteDataItem(item, batchId)
        if (isCacheAvailable(batchId)) {
            mCachedFeeds!![batchId]!!.remove(item.id)
        }
        notifyDataChanged()
    }

    override fun clearCache() {
        mCachedFeeds = null
        notifyDataChanged()
    }

    override fun notifyDataChanged() {
        if (mListener != null) {
            mListener!!.onDataUpdated()
        }
    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Feeding>, vararg referenceIds: String) {
        mFeedsRemoteDataSource.observeData(object : BaseDataSource.DataLoadCallback<Feeding> {
            override fun onDataLoaded(data: List<Feeding>) {
                callback.onDataLoaded(data)
                refreshLocalData(referenceIds[0], data)
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        }, *referenceIds)
    }

    private fun getFeedsFromRemoteSource(batchId: String, callback: BaseDataSource.DataLoadCallback<Feeding>) {
        mFeedsRemoteDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Feeding> {
            override fun onDataLoaded(data: List<Feeding>) {
                refreshLocalData(batchId, data)
                callback.onDataLoaded(data)
                mCacheIsDirty = false
            }

            override fun onEmptyData() {
                callback.onEmptyData()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })
    }

    private fun refreshLocalData(batchId: String, data: List<Feeding>) {
        mCachedFeeds = null
        mFeedsLocalDataSource.deleteAllData(batchId)
        for (feeding in data) {
            mFeedsLocalDataSource.saveData(feeding)
        }
        refreshCache(batchId, data)
    }


    private fun refreshCache(batchId: String, data: List<Feeding>) {
        for (feeding in data) {
            if (isCacheAvailable(batchId)) {
                mCachedFeeds!![batchId]!![feeding.id] = feeding
            }
        }
    }

    override fun getMortalityRate(batchId: String, callback: FeedingDataSource.OnMortalityRate) {
        mFeedsLocalDataSource.getMortalityRate(batchId, object : FeedingDataSource.OnMortalityRate {
            override fun onMortalityLoaded(rate: Int) {
                if (rate >= 0)
                    callback.onMortalityLoaded(rate)
                else
                    callback.onMortalityLoaded(0)
            }
        })
    }

    fun setDataUpdateCallback(listener: FeedingDataSource.DataUpdatedListener) {
        mListener = listener
    }

    private fun isCacheAvailable(batchId: String): Boolean {
        if (mCachedFeeds == null) {
            mCachedFeeds = LinkedHashMap()
            mCachedFeeds!![batchId] = LinkedHashMap()
        } else if (mCachedFeeds!![batchId] == null) {
            mCachedFeeds!![batchId] = LinkedHashMap()
        }
        return true
    }

    companion object {

        private var INSTANCE: FeedingRepository? = null

        fun getInstance(remoteSource: FeedingDataSource, localSource: FeedingDataSource): FeedingRepository {
            if (INSTANCE == null)
                INSTANCE = FeedingRepository(remoteSource, localSource)
            return INSTANCE as FeedingRepository
        }
    }
}
