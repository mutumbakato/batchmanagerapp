package com.mutumbakato.batchmanager.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.Map;
import java.util.UUID;

import static com.mutumbakato.batchmanager.data.utils.TableNames.TABLE_NAME_WORKERS;

/**
 * Created by cato on 5/10/18.
 */
@Entity(tableName = TABLE_NAME_WORKERS)
public class Employee {

    @NonNull
    @PrimaryKey
    private String id;

    private String name;

    private String permissions;

    private String farmId;

    private String contact;

    private Map<String, String> batches;

    @Ignore
    public Employee() {

    }

    @Ignore
    private Employee(String name, String permissions, String farmId, String contact, Map<String, String> batches) {
        new Employee(UUID.randomUUID().toString(), name, permissions, farmId, batches);
    }

    public Employee(@NonNull String id, String name, String permissions, String farmId, String contact, Map<String, String> batches) {
        this.id = id;
        this.name = name;
        this.permissions = permissions;
        this.farmId = farmId;
        this.batches = batches;
        this.contact = contact;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPermissions() {
        return permissions;
    }

    public String getFarmId() {
        return farmId;
    }

    public Map<String, String> getBatches() {
        return batches;
    }
}
