package com.mutumbakato.batchmanager.data.models.exports

import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils

data class EggsExport(val date: String, val eggs: Int, val count: Int, val damage: Int) {

    val trays: Int
        get() = eggs % PreferenceUtils.eggsInATray

    val total
        get() = eggs + damage

}