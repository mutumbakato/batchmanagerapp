package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchRecords
import com.mutumbakato.batchmanager.data.models.BatchUser

interface BatchDataSource {

    fun getAllBatches(farmId: String, callback: LoadBatchesCallback)

    fun getArchivedBatches(farmId: String, callback: LoadBatchesCallback)

    fun getBatch(batchId: String, callback: GetBatchCallback)

    fun saveBatch(batch: Batch, callback: CreateBatchCallback)

    fun updateBatch(batch: Batch)

    fun closeBatch(batchId: String)

    fun refreshBatches()

    fun deleteAllMyBatches(farmId: String)

    fun deleteAllOtherBatches(farmId: String)

    fun deleteBatch(batchId: String)

    fun clearMemoryCache(thenNotify: Boolean)

    fun notifyDataChanged()

    fun observeMyBatches(farmId: String, callback: LoadBatchesCallback)

    fun observeOtherBatches(farmId: String, callback: LoadBatchesCallback)

    fun addUser(users: BatchUser, callback: BaseDataSource.DataItemCallback<String>)

    fun removeUser(user: BatchUser, callback: BaseDataSource.DataItemCallback<String>)

    fun getBatchUsers(batchId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>)

    fun getBatchUser(batchId: String, userId: String, callback: BaseDataSource.DataItemCallback<BatchUser>)

    fun updateBatchUser(user: BatchUser, callback: BaseDataSource.DataItemCallback<BatchUser>)

    fun observeBatchesUsers(farmId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>)

    fun deleteAllBatchUsers(batchId: String)


    fun getTodayRecords(onResults: (records: BatchRecords) -> Unit)

    interface DataUpdatedListener {
        fun onDataUpdated()
    }

    interface LoadBatchesCallback {

        fun onBatchesLoaded(batches: List<Batch>)

        fun onDataNotAvailable()
    }

    interface GetBatchCallback {

        fun onBatchLoaded(batch: Batch)

        fun onDataNotAvailable()
    }

    interface CreateBatchCallback {
        fun onBatchCreated(batchId: String)
    }
}
