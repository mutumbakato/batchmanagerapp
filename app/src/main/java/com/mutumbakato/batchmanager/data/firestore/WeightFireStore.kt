package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.WeightDataSource
import com.mutumbakato.batchmanager.data.models.Weight

class WeightFireStore : WeightDataSource {

    private val db = FirebaseFirestore.getInstance().collection("weight")

    override fun createWeight(weight: Weight, onSuccess: (Weight) -> Unit) {
        db.document(weight.id).set(weight)
    }

    override fun listWeights(batchId: String, onSuccess: (List<Weight>) -> Unit) {
        db.whereEqualTo("batchId", batchId).addSnapshotListener { snapshots, _ ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val dataItems = ArrayList<Weight>()
                for (data in snapshots) {
                    dataItems.add(data.toObject(Weight::class.java))
                }
                onSuccess(dataItems)
            }
        }
    }

    override fun getWeight(id: String, onSuccess: (Weight) -> Unit) {

    }

    override fun updateWeight(weight: Weight, onSuccess: (Weight) -> Unit) {
        db.document(weight.id).set(weight)
    }

    override fun deleteWeight(weight: Weight, onSuccess: () -> Unit) {
        db.document(weight.id).delete()
    }
}