package com.mutumbakato.batchmanager.data.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;
import com.mutumbakato.batchmanager.data.utils.Data;

import java.util.UUID;

import static com.mutumbakato.batchmanager.data.models.Eggs.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class Eggs {

    public static final String TABLE_NAME = "eggs";
    public static final int EGGS_IN_A_TREY = PreferenceUtils.INSTANCE.getEggsInATray();

    @Expose
    @PrimaryKey
    @ColumnInfo(name = "_id")

    @NonNull
    private String id = UUID.randomUUID().toString();

    @Expose
    @ColumnInfo(name = "batch_id")
    private String batchId;

    @ColumnInfo(name = "store_id")
    private String storeId;

    @Expose
    @ColumnInfo(name = "date")
    private String date;

    @Expose
    @ColumnInfo(name = "eggs")
    private int eggs;

    @Expose
    @ColumnInfo(name = "damage")
    private int damage;

    @Expose
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Expose
    @ColumnInfo(name = "status")
    private String status;

    @Expose
    @ColumnInfo(name = "count")
    private int count;

    @Ignore
    public Eggs() {

    }

    public Eggs(@NonNull String id, String batchId, String storeId, String date, int eggs,
                int damage, int count, long serverId, String status, long lastModified) {
        this.id = id;
        this.batchId = batchId;
        this.storeId = storeId;
        this.date = date;
        this.eggs = eggs;
        this.damage = damage;
        this.serverId = serverId;
        this.status = status;
        this.lastModified = lastModified;
        this.count = count;
    }

    @Ignore
    public Eggs(@NonNull String id, String batchId, String date, int eggs,
                int damage, int count, long serverId, String status, long lastModified) {
        this(id, batchId, "", date, eggs, damage, count, serverId,
                status, lastModified);
    }

    @Ignore
    public Eggs(String id, String batchId, String date, int treys, int eggs, int damage, int count, long serverId,
                String status, long lastModified) {
        this(id, batchId, date, (eggs + (treys * EGGS_IN_A_TREY)), damage, count, serverId, status, lastModified);
    }

    @Ignore
    public Eggs(String batchId, String date, int treys, int eggs, int damage, int count) {
        this(UUID.randomUUID().toString(), batchId, date, treys, eggs, damage, count, Data.SERVER_ID_NEW,
                Data.STATUS_NORMAL, 0);
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getBatchId() {
        return batchId;
    }

    public String getDate() {
        return date;
    }

    @Ignore
    public int getTreys() {
        return eggs / EGGS_IN_A_TREY;
    }

    public int getEggs() {
        return eggs;
    }

    public int getRemainderEggs() {
        return (eggs % EGGS_IN_A_TREY);
    }

    public int getDamage() {
        return damage;
    }

    public float getPercentage() {
        if (count == 0) {
            count = 2000;
        }
        int total = getEggs() + getDamage();
        return (total * 100.0f) / count;
    }

    public String toString() {
        return "Eggs on " + date;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }

    public int getCount() {
        return count;
    }

    public String getStoreId() {
        return storeId;
    }
}
