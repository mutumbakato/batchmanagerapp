package com.mutumbakato.batchmanager.data.local

import androidx.lifecycle.LiveData
import com.google.gson.Gson
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.ExpenseDataSource
import com.mutumbakato.batchmanager.data.local.dao.ExpenseDao
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.sync.Sync
import com.mutumbakato.batchmanager.data.sync.Syncable
import com.mutumbakato.batchmanager.utils.executors.AppExecutors
import org.json.JSONArray
import org.json.JSONException

class ExpenseLocalDataSource
private constructor(private val mAppExecutors: AppExecutors, private val mExpenseDao: ExpenseDao)
    : ExpenseDataSource, Syncable<Expense> {

    fun insertExpense(expense: Expense, onFinish: (() -> Unit)? = null) {
        mAppExecutors.diskIO().execute {
            mExpenseDao.insertExpense(expense)
            if (onFinish != null)
                mAppExecutors.mainThread().execute(onFinish)
        }
    }

    fun insertAllExpenses(expenses: List<Expense>) {
        mAppExecutors.diskIO().execute {
            mExpenseDao.insertAllExpenses(expenses)
        }
    }

    fun listFarmExpenses(farmId: String): LiveData<List<Expense>> {
        return mExpenseDao.listFarmExpenses(farmId)
    }

    fun listBatchExpenses(batchId: String): LiveData<List<Expense>> {
        return mExpenseDao.listBatchExpenses(batchId)
    }

    fun listExpensesMonths(farmId: String): LiveData<List<String>> {
        return mExpenseDao.listExpensesMonths(farmId)
    }

    fun getExpenseById(id: String): LiveData<Expense> {
        return mExpenseDao.getLiveExpenseById(id)
    }

    fun updateExpense(expense: Expense, onFinish: (() -> Unit)? = null) {
        mAppExecutors.diskIO().execute {
            mExpenseDao.updateExpense(expense)
            if (onFinish != null)
                mAppExecutors.mainThread().execute(onFinish)
        }
    }

    fun filter(filter: Filter): LiveData<List<Expense>> {
        return mExpenseDao.filterFarmExpenses(filter.id, filter.from, filter.to)
    }

    fun deleteAllForFarm(farmId: String) {
        mAppExecutors.diskIO().execute {
            mExpenseDao.deleteFarmExpenses(farmId)
        }
    }

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Expense>) {
        val runnable = {
            val expenses = mExpenseDao.listExpenses(batchId)
            mAppExecutors.mainThread().execute {
                if (expenses.isNotEmpty()) {
                    callback.onDataLoaded(expenses)
                } else {
                    callback.onEmptyData()
                }
            }
        }
        mAppExecutors.diskIO().execute(runnable)
    }
    
    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Expense>) {
        val runnable = {
            val expenses = mExpenseDao.getExpenseById(id)
            mAppExecutors.mainThread().execute { callback.onDataItemLoaded(expenses) }
        }
        mAppExecutors.diskIO().execute(runnable)
    }

    override fun saveData(data: Expense) {
        val saveRunnable = {
            mExpenseDao.insertExpense(data)
            sync()
        }
        mAppExecutors.diskIO().execute(saveRunnable)
    }

    override fun updateData(data: Expense) {
        val runnable = {
            mExpenseDao.updateExpense(data)
            sync()
        }
        mAppExecutors.diskIO().execute(runnable)
    }

    override fun refreshData() {

    }

    override fun deleteAllData(batchId: String) {
        val runnable = Runnable { mExpenseDao.deleteExpenses(batchId) }
        mAppExecutors.diskIO().execute(runnable)
    }

    override fun deleteDataItem(item: Expense, batchId: String) {
        val runnable = {
            mExpenseDao.deleteExpenseById(item.id)
            sync()
        }
        mAppExecutors.diskIO().execute(runnable)
    }

    override fun clearCache() {

    }

    override fun notifyDataChanged() {

    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Expense>, vararg referenceIds: String) {

    }

    override fun getTotalExpenses(batchId: String, callback: ExpenseDataSource.ExpenseTotalCallback) {
        val runnable = {
            val total = mExpenseDao.getTotalExpenses(batchId)
            mAppExecutors.mainThread().execute { callback.onTotal(total) }
        }
        mAppExecutors.diskIO().execute(runnable)
    }

    override fun getTable(): String {
        return Expense.TABLE_NAME
    }

    override fun getNew(): List<Expense> {
        return mExpenseDao.new
    }

    override fun getUpdated(): List<Expense> {
        return mExpenseDao.updated
    }

    override fun getLocalData(): List<Expense> {
        return mExpenseDao.local
    }

    override fun getTrash(): List<Long> {
        return mExpenseDao.trash
    }

    override fun getLastServerId(): Long {
        return mExpenseDao.lastServerId
    }

    override fun handleNewData(newData: JSONArray) {
        val gson = Gson()
        for (n in 0 until newData.length()) {
            try {
                val expense = gson.fromJson(newData.get(n).toString(), Expense::class.java)
                val newExpense = Expense(expense.id, expense.batchId, expense.date, expense.category, expense.description,
                        expense.amount, expense.serverId, expense.status, expense.lastModified)
                mExpenseDao.insertExpense(newExpense)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    override fun handleModified(modified: JSONArray) {
        val gson = Gson()
        for (n in 0 until modified.length()) {
            try {
                val expense = gson.fromJson(modified.get(n).toString(), Expense::class.java)
                mExpenseDao.updateExpense(expense)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    override fun handleSynced(synced: JSONArray) {
        for (n in 0 until synced.length()) {
            try {
                val id = synced.getJSONObject(n).getString("id")
                val serverId = synced.getJSONObject(n).getLong("serverId")
                val lastMod = synced.getJSONObject(n).getLong("lastModified")
                val expense = mExpenseDao.getExpenseById(id)
                val syncedExpense = Expense(expense.id, expense.batchId, expense.date, expense.category, expense.description,
                        expense.amount, serverId, expense.status, lastMod)
                mExpenseDao.updateExpense(syncedExpense)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    override fun handleUpdates(updates: JSONArray) {
        for (n in 0 until updates.length()) {
            try {
                val id = updates.getJSONObject(n).getString("id")
                val lastModified = updates.getJSONObject(n).getLong("lastModified")
                val expense = mExpenseDao.getExpenseById(id)
                val newExpense = Expense(expense.id, expense.batchId, expense.date, expense.category, expense.description,
                        expense.amount, expense.serverId, expense.status, lastModified)
                mExpenseDao.updateExpense(newExpense)
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    override fun handleTrash(trash: Array<String>) {
        for (aTrash in trash) {
            mExpenseDao.deleteExpenseById(aTrash)
        }
        mExpenseDao.clearTrash()
    }

    override fun notifyRepository() {
        //        Log.d(TAG, "notifyRepository: Sync complete: Notifying repositosy");
        //        repository.clearCache();
    }

    private fun sync() {
        //        Log.d(TAG, "sync: Synchronising expenses");
        Sync.getInstance().sync(this, false)
    }

    companion object {

        @Volatile
        private var INSTANCE: ExpenseLocalDataSource? = null

        fun getInstance(appExecutors: AppExecutors, expenseDao: ExpenseDao): ExpenseLocalDataSource {
            if (INSTANCE == null)
                INSTANCE = ExpenseLocalDataSource(appExecutors, expenseDao)
            return INSTANCE as ExpenseLocalDataSource
        }
    }
}
