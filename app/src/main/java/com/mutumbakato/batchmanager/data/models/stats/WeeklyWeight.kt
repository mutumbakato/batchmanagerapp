package com.mutumbakato.batchmanager.data.models.stats

data class WeeklyWeight(val days: Int, val aveWeight: Float, val aveWeightGain: Float)