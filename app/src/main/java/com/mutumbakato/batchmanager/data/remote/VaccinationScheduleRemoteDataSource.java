package com.mutumbakato.batchmanager.data.remote;

import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.BaseDataSource;
import com.mutumbakato.batchmanager.data.VaccinationScheduleDataSource;
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VaccinationScheduleRemoteDataSource implements VaccinationScheduleDataSource {

    private static VaccinationScheduleRemoteDataSource INSTANCE;
    private VaccinationEndPoints mApi;

    private VaccinationScheduleRemoteDataSource(VaccinationEndPoints api) {
        mApi = api;
    }

    public static VaccinationScheduleRemoteDataSource getInstance(VaccinationEndPoints api) {
        if (INSTANCE == null) {
            INSTANCE = new VaccinationScheduleRemoteDataSource(api);
        }
        return INSTANCE;
    }


    @Override
    public void getAllSchedules(@NonNull String userId, @NonNull final BaseDataSource.DataLoadCallback<VaccinationSchedule> callback) {
        mApi.fetchSchedules().enqueue(new Callback<List<VaccinationSchedule>>() {
            @Override
            public void onResponse(@NonNull Call<List<VaccinationSchedule>> call,
                                   @NonNull Response<List<VaccinationSchedule>> response) {
                if (response.body() != null && response.body().size() > 0) {
                    callback.onDataLoaded(response.body());
                } else {
                    callback.onEmptyData();
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<VaccinationSchedule>> call, @NonNull Throwable t) {
                callback.onError("Failed to get Vaccination Schedules");
                t.printStackTrace();
            }
        });
    }

    @Override
    public void getSchedule(@NonNull String id, @NonNull String batchId, @NonNull BaseDataSource.DataItemCallback<VaccinationSchedule> callback) {
        callback.onDataItemNotAvailable();
    }

    @Override
    public void saveSchedule(@NonNull VaccinationSchedule data) {

    }

    @Override
    public void updateSchedule(@NonNull VaccinationSchedule data) {

    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllSchedules(@NonNull String batchId) {

    }

    @Override
    public void deleteSchedule(@NonNull String id, @NonNull String batchId) {

    }

    @Override
    public void clearCache() {

    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void useSchedule(String batchId, VaccinationSchedule schedule) {

    }

    @Override
    public void removeSchedule(String batchId, String scheduleId) {

    }

    @Override
    public void observeSchedule(BaseDataSource.DataLoadCallback<VaccinationSchedule> callback, String userId) {

    }

}
