package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

import com.mutumbakato.batchmanager.data.models.EggCount
import com.mutumbakato.batchmanager.data.models.Eggs

@Dao
interface EggsDao {

    /**
     * Select all eggs from the eggs table.
     *
     * @return all eggs.
     */
    @get:Query("SELECT * FROM eggs WHERE last_modified != 0 AND server_id != 0 AND status !='trash'")
    val local: List<Eggs>

    /**
     * Select new eggs
     *
     * @return eggs that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM eggs WHERE server_id = 0")
    val new: List<Eggs>

    /**
     * Select locally updated eggs since the last synchronisation
     *
     * @return updated eggs  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM eggs WHERE server_id != 0 AND last_modified = 0")
    val updated: List<Eggs>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM eggs ORDER BY server_id DESC LIMIT 1")
    val lastServerId: Long

    /**
     * Select eggs deleted from the locally
     *
     * @return server ids of eggs where status = trash ;
     */
    @get:Query("SELECT server_id FROM eggs WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * Select all eggs from the eggs table.
     *
     * @return all eggs.
     */
    @Query("SELECT * FROM eggs WHERE batch_id = :batchId AND status != 'trash' ORDER BY DATE DESC")
    fun getAllEggs(batchId: String): List<Eggs>

    /**
     * Select a eggs by name.
     *
     * @param eggsId the eggs name.
     * @return the eggs with eggsId.
     */
    @Query("SELECT * FROM eggs WHERE _id = :eggsId")
    fun getEggsById(eggsId: String): Eggs

    /**
     * Insert a eggs in the database. If the eggs already exists, replace it.
     *
     * @param eggs the eggs to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEggs(eggs: Eggs): Long

    /**
     * Update a eggs.
     *
     * @param eggs eggs to be updated
     * @return the number of eggs updated. This should always be 1.
     */
    @Update
    fun updateEggs(eggs: Eggs): Int

    /**
     * Delete a eggs by name.
     *
     * @return the number of eggs deleted. This should always be 1.
     */
    @Query("UPDATE eggs SET status = 'trash' WHERE _id = :eggsId")
    fun deleteEggsById(eggsId: String): Int

    /**
     * Delete all eggs.
     */
    @Query("DELETE FROM eggs WHERE batch_id = :batchId")
    fun deleteEggs(batchId: String)

    /**
     * Permanently deleteCategory entries
     */
    @Query("DELETE FROM eggs WHERE status = 'trash' ")
    fun clearTrash()

    /**
     * @param batchId id of the batch
     * @return combined total eggs and damage
     */
    @Query("SELECT SUM(eggs) AS total, SUM(damage) as damage from eggs WHERE batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun totalEggs(batchId: String): EggCount
}
