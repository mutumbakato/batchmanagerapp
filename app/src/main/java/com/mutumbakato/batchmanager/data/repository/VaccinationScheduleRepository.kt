package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.VaccinationScheduleDataSource
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import java.util.*

/**
 * Created by cato on 11/2/17.
 */
class VaccinationScheduleRepository private constructor(private val mRemoteDataSource: VaccinationScheduleDataSource,
                                                        private val mLocalDataSource: VaccinationScheduleDataSource) : VaccinationScheduleDataSource {
    private var mCachedSchedules: MutableMap<String, VaccinationSchedule>? = null
    private var mListener: VaccinationScheduleDataSource.DataUpdatedListener? = null
    private var isCacheDirty = false

    override fun getAllSchedules(batchId: String, callback: BaseDataSource.DataLoadCallback<VaccinationSchedule>) {
        observeSchedule(callback, batchId)
        if (mCachedSchedules != null && !isCacheDirty) {
            callback.onDataLoaded(ArrayList(mCachedSchedules!!.values))
            return
        }
        if (isCacheDirty) {
            getSchedulesFromRemoteSource(batchId, callback)
        } else {
            mLocalDataSource.getAllSchedules(batchId, object : BaseDataSource.DataLoadCallback<VaccinationSchedule> {
                override fun onDataLoaded(data: List<VaccinationSchedule>) {
                    callback.onDataLoaded(data)
                    refreshScheduleCache(data)
                }

                override fun onEmptyData() {
                    getSchedulesFromRemoteSource(batchId, callback)
                }

                override fun onError(message: String) {
                    callback.onError(message)
                }
            })
        }
    }

    override fun getSchedule(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<VaccinationSchedule>) {
        if (mCachedSchedules != null && mCachedSchedules!![id] != null) {
            callback.onDataItemLoaded(mCachedSchedules!![id]!!)
        } else {
            mLocalDataSource.getSchedule(id, batchId, object : BaseDataSource.DataItemCallback<VaccinationSchedule> {
                override fun onDataItemLoaded(data: VaccinationSchedule) {
                    callback.onDataItemLoaded(data)
                }

                override fun onDataItemNotAvailable() {
                    mRemoteDataSource.getSchedule(id, batchId, object : BaseDataSource.DataItemCallback<VaccinationSchedule> {
                        override fun onDataItemLoaded(data: VaccinationSchedule) {
                            callback.onDataItemLoaded(data)
                        }

                        override fun onDataItemNotAvailable() {
                            callback.onDataItemNotAvailable()
                        }

                        override fun onError(message: String) {
                            callback.onError(message)
                        }
                    })
                }

                override fun onError(message: String) {
                    callback.onError(message)
                }
            })
        }
    }

    override fun saveSchedule(data: VaccinationSchedule) {
        mLocalDataSource.saveSchedule(data)
        mRemoteDataSource.saveSchedule(data)
        if (mCachedSchedules != null) {
            mCachedSchedules!![data.id] = data
        }
        notifyDataChanged()
    }

    override fun updateSchedule(data: VaccinationSchedule) {
        mLocalDataSource.updateSchedule(data)
        mRemoteDataSource.updateSchedule(data)
        if (mCachedSchedules != null) {
            mCachedSchedules!![data.id] = data
        }
        notifyDataChanged()
    }

    override fun refreshData() {
        isCacheDirty = true
    }

    override fun deleteAllSchedules(farmId: String) {
        mLocalDataSource.deleteAllSchedules(farmId)
        mRemoteDataSource.deleteAllSchedules(farmId)
        if (mCachedSchedules != null) {
            mCachedSchedules!!.clear()
        }
    }

    override fun deleteSchedule(id: String, batchId: String) {
        mLocalDataSource.deleteSchedule(id, batchId)
        mRemoteDataSource.deleteSchedule(id, batchId)
        if (mCachedSchedules != null) {
            mCachedSchedules!!.remove(id)
        }
    }

    override fun clearCache() {
        mCachedSchedules = null
    }

    override fun notifyDataChanged() {
        if (mListener != null) {
            mListener!!.onDataUpdated()
        }
    }

    private fun getSchedulesFromRemoteSource(farmId: String, callback: BaseDataSource.DataLoadCallback<VaccinationSchedule>) {
        mRemoteDataSource.getAllSchedules(farmId, object : BaseDataSource.DataLoadCallback<VaccinationSchedule> {
            override fun onDataLoaded(data: List<VaccinationSchedule>) {
                refreshScheduleLocalDataSource(farmId, data)
                callback.onDataLoaded(data)
                isCacheDirty = false
            }

            override fun onEmptyData() {
                callback.onEmptyData()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })
    }

    private fun refreshScheduleLocalDataSource(farmId: String, schedules: List<VaccinationSchedule>) {
        mCachedSchedules = null
        mLocalDataSource.deleteAllSchedules(farmId)
        for (schedule in schedules) {
            mLocalDataSource.saveSchedule(schedule)
        }
        refreshScheduleCache(schedules)
    }

    private fun refreshScheduleCache(schedules: List<VaccinationSchedule>) {

        if (mCachedSchedules == null)
            mCachedSchedules = LinkedHashMap()

        for (schedule in schedules) {
            mCachedSchedules!![schedule.id] = schedule
        }
    }

    override fun useSchedule(batchId: String, schedule: VaccinationSchedule) {
        mLocalDataSource.useSchedule(batchId, schedule)
        mRemoteDataSource.useSchedule(batchId, schedule)
    }

    override fun removeSchedule(batchId: String, scheduleId: String) {
        mLocalDataSource.removeSchedule(batchId, scheduleId)
        mRemoteDataSource.removeSchedule(batchId, scheduleId)
    }

    override fun observeSchedule(callback: BaseDataSource.DataLoadCallback<VaccinationSchedule>, farmId: String) {
        mRemoteDataSource.observeSchedule(object : BaseDataSource.DataLoadCallback<VaccinationSchedule> {
            override fun onDataLoaded(data: List<VaccinationSchedule>) {
                callback.onDataLoaded(data)
                refreshScheduleLocalDataSource(farmId, data)
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        }, farmId)
    }

    fun setDataUpdateCallback(listener: VaccinationScheduleDataSource.DataUpdatedListener) {
        mListener = listener
    }

    companion object {

        private var INSTANCE: VaccinationScheduleRepository? = null

        fun getInstance(batchesRemoteDataSource: VaccinationScheduleDataSource,
                        batchesLocalDataSource: VaccinationScheduleDataSource): VaccinationScheduleRepository {
            if (INSTANCE == null) {
                INSTANCE = VaccinationScheduleRepository(batchesRemoteDataSource,
                        batchesLocalDataSource)
            }
            return INSTANCE as VaccinationScheduleRepository
        }
    }
}
