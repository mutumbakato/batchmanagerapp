package com.mutumbakato.batchmanager.data

import android.location.Location
import com.mutumbakato.batchmanager.data.models.User

/**
 * Created by cato on 10/31/17.
 */

interface UserDataSource {

    fun login(email: String, password: String, callBack: UserCallBack)

    fun register(user: User, callBack: UserCallBack)

    fun logOut(callBack: UserCallBack)

    fun resetPassword(email: String, callBack: PasswordCallBack)

    fun getUserById(id: String, callBack: UserCallBack)

    fun getUserByEmail(email: String, callBack: UserCallBack)

    fun updateUser(user: User, callBack: UserCallBack)

    fun deleteAccount(user: User, callBack: UserCallBack)

    fun setLocation(uid: String, location: Location)

    interface UserCallBack {

        fun onSuccess(user: User?)

        fun onError(message: String)
    }

    interface PasswordCallBack {

        fun onSuccess(message: String)

        fun onError(message: String)
    }
}

