package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.*
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.models.exports.*
import io.reactivex.Single

@Dao
interface FarmDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun createFarm(farm: Farm)

    @Query("SELECT * FROM farms WHERE _id = :farmId")
    fun getFarmById(farmId: String): Single<Farm>

    @Query("SELECT * FROM batches WHERE _id =:id")
    fun getBatchById(id: String): Batch

    @Query("SELECT * FROM farms WHERE user_id = :userId")
    fun getAllFarms(userId: String): Single<List<Farm>>

    @Update
    fun updateFarm(farm: Farm): Int

    @Query("UPDATE farms SET status = 'trash' WHERE _id = :farmId")
    fun deleteFarmById(farmId: String): Int

    // ------------------------------Export data---------------------------------------------------
    @Query("SELECT * FROM expenses WHERE batch_id = :batchId ORDER BY date ASC")
    fun getBatchExpenses(batchId: String): List<Expense>

    @Query("SELECT * FROM sales WHERE batch_id = :batchId ORDER BY date ASC")
    fun getBatchSales(batchId: String): List<Sale>

    @Query("SELECT date, SUM(count) AS count, comment AS cause FROM death WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchDeaths(batchId: String): List<DeathExport>

    @Query("SELECT date, SUM(quantity) AS total, AVG(batch_count) AS count ,ROUND(((SUM(quantity)/AVG(batch_count)) * 1000),1) AS gpb FROM feeds WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchFeeding(batchId: String): List<FeedsExport>

    @Query("SELECT date, ROUND(AVG(weight),0)  AS avg FROM weight WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchWeight(batchId: String): List<WeightExport>

    @Query("SELECT date, SUM(quantity) AS  total, AVG(batch_count) AS count FROM water WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchWater(batchId: String): List<WaterExport>

    @Query("SELECT date, SUM(eggs) AS eggs, SUM(damage) AS damage, AVG(count) AS count FROM eggs WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchEggs(batchId: String): List<EggsExport>
}
