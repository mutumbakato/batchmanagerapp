package com.mutumbakato.batchmanager.data.models;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.firebase.firestore.Exclude;
import com.google.gson.annotations.Expose;

import static com.mutumbakato.batchmanager.data.models.VaccinationCheck.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class VaccinationCheck {

    public static final String TABLE_NAME = "vaccination_check";

    @NonNull
    @Expose
    @PrimaryKey
    @ColumnInfo(name = "_id")
    private String id;

    @Expose
    @ColumnInfo(name = "batch_id")
    private String batchId;

    @Expose
    @ColumnInfo(name = "schedule_id")
    private String scheduleId;

    @Expose
    @ColumnInfo(name = "vaccination_id")
    private String vaccinationId;

    @Expose
    @Exclude
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Expose
    @ColumnInfo(name = "status")
    private String status;

    @Ignore
    public VaccinationCheck() {

    }

    public VaccinationCheck(@NonNull String id, String batchId, String scheduleId, String vaccinationId, long serverId, String status, long lastModified) {

        this.id = id;
        this.batchId = batchId;
        this.scheduleId = scheduleId;
        this.vaccinationId = vaccinationId;
        this.serverId = serverId;
        this.status = status;
        this.lastModified = lastModified;

    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getBatchId() {
        return batchId;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public String getVaccinationId() {
        return vaccinationId;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }
}
