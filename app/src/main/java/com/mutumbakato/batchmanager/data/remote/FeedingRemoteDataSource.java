package com.mutumbakato.batchmanager.data.remote;

import android.os.Handler;
import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.FeedingDataSource;
import com.mutumbakato.batchmanager.data.models.Feeding;

import java.util.LinkedHashMap;
import java.util.Map;

public class FeedingRemoteDataSource implements FeedingDataSource {
    private static final int SERVICE_LATENCY_IN_MILLIS = 3000;
    private final static Map<String, Feeding> DEATH_SERVICE_DATA;
    private static FeedingRemoteDataSource INSTANCE = null;

    static {
        DEATH_SERVICE_DATA = new LinkedHashMap<>(2);
//        addDeath("1", "2017-08-04", 10, "Accident");
//        addDeath("1", "2017-07-07", 3, "Coldness");
//        addDeath("1", "2017-07-09", 5, "Heat");
    }

    private FeedingRemoteDataSource() {

    }

    private static void addDeath(String batch, String date, int count, String comment) {
        Feeding newFeeding = new Feeding(batch, date, count, comment);
        DEATH_SERVICE_DATA.put(newFeeding.getId(), newFeeding);
    }

    public static FeedingRemoteDataSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new FeedingRemoteDataSource();
        return INSTANCE;
    }

    @Override
    public void getAll(@NonNull String batchId, @NonNull final DataLoadCallback<Feeding> callback) {
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                callback.onDataLoaded(new ArrayList<>(DEATH_SERVICE_DATA.values()));
//            }
//        }, SERVICE_LATENCY_IN_MILLIS);
        callback.onEmptyData();
    }

    @Override
    public void getOne(@NonNull final String id, @NonNull final String batchId, @NonNull final DataItemCallback<Feeding> callback) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.onDataItemLoaded(DEATH_SERVICE_DATA.get(id));
            }
        }, SERVICE_LATENCY_IN_MILLIS);
    }

    @Override
    public void saveData(@NonNull Feeding data) {
        DEATH_SERVICE_DATA.put(data.getId(), data);
    }

    @Override
    public void updateData(@NonNull Feeding data) {
        DEATH_SERVICE_DATA.put(data.getId(), data);

    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllData(@NonNull String batchI) {
        DEATH_SERVICE_DATA.clear();
    }

    @Override
    public void deleteDataItem(@NonNull Feeding item, @NonNull String batchId) {
        DEATH_SERVICE_DATA.remove(item);
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void observeData(DataLoadCallback<Feeding> callback, String... referenceIds) {

    }

    @Override
    public void getMortalityRate(String batchId, OnMortalityRate callback) {
        //Mortality rate is only provided by local data source
    }
}
