package com.mutumbakato.batchmanager.data.models

data class Status(val isError: Boolean, val message: String, val type: String)