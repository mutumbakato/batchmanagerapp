/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mutumbakato.batchmanager.data.remote

import com.mutumbakato.batchmanager.data.BaseDataSource.DataItemCallback
import com.mutumbakato.batchmanager.data.BaseDataSource.DataLoadCallback
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource.*
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchRecords
import com.mutumbakato.batchmanager.data.models.BatchUser
import java.util.*

/**
 * Implementation of the data source that adds a latency simulating network.
 */
class BatchRemoteDataSource  // Prevent direct instantiation.
private constructor() : BatchDataSource {
    companion object {
        private const val SERVICE_LATENCY_IN_MILLIS = 3000
        private var TASKS_SERVICE_DATA: MutableMap<String, Batch>? = null
        private var INSTANCE: BatchRemoteDataSource? = null
        val instance: BatchRemoteDataSource?
            get() {
                if (INSTANCE == null) {
                    INSTANCE = BatchRemoteDataSource()
                }
                return INSTANCE
            }

        private fun addBatch(date: String, name: String, supplier: String, quantity: Int, rate: Double, type: String) {
//        Batch newBatch = new Batch(date, name, supplier, quantity, rate, type);
//        TASKS_SERVICE_DATA.put(newBatch.getId(), newBatch);
        }

        init {
            TASKS_SERVICE_DATA = LinkedHashMap(2)
            addBatch("2017-08-08", "House One", "Biyinzika", 2500, 4000.0, "Layers")
            addBatch("2017-07-07", "House Two", "UgaChick", 2000, 3000.0, "Broilers")
        }
    }

    /**
     * Note: [LoadBatchesCallback.onDataNotAvailable] is never fired. In a real remote data
     * source implementation, this would be fired if the server can't be contacted or the server
     * returns an error.
     */
    override fun getAllBatches(farmId: String, callback: LoadBatchesCallback) {
//        // Simulate network by delaying the execution.
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                callback.onBatchesLoaded(new ArrayList<>(TASKS_SERVICE_DATA.values()));
//            }
//        }, SERVICE_LATENCY_IN_MILLIS);
        callback.onDataNotAvailable()
    }

    override fun getArchivedBatches(farmId: String, callback: LoadBatchesCallback) {}

    /**
     * Note: [GetBatchCallback.onDataNotAvailable] is never fired. In a real remote data
     * source implementation, this would be fired if the server can't be contacted or the server
     * returns an error.
     */
    override fun getBatch(batchId: String, callback: GetBatchCallback) {
        val batch = TASKS_SERVICE_DATA!![batchId]
        //
//        // Simulate network by delaying the execution.
//        Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                callback.onBatchLoaded(batch);
//            }
//        }, SERVICE_LATENCY_IN_MILLIS);
    }

    override fun saveBatch(batch: Batch, callback: CreateBatchCallback) {
        TASKS_SERVICE_DATA!![batch.id] = batch
    }

    override fun updateBatch(batch: Batch) {
        TASKS_SERVICE_DATA!![batch.id] = batch
    }

    override fun closeBatch(batchId: String) {}
    override fun refreshBatches() {
        // Not required because the {@link BatchRepository} handles the logic of refreshing the
        // batches from all the available data sources.
    }

    override fun deleteAllMyBatches(farmId: String) {}
    override fun deleteAllOtherBatches(farmId: String) {}
    override fun deleteBatch(batchId: String) {
        TASKS_SERVICE_DATA!!.remove(batchId)
    }

    override fun clearMemoryCache(notify: Boolean) {}
    override fun notifyDataChanged() {}
    override fun observeMyBatches(farmId: String, callback: LoadBatchesCallback) {}
    override fun observeOtherBatches(farmId: String, callback: LoadBatchesCallback) {}
    override fun addUser(users: BatchUser, callback: DataItemCallback<String>) {}
    override fun removeUser(user: BatchUser, callback: DataItemCallback<String>) {}
    override fun getBatchUsers(batchId: String, callback: DataLoadCallback<BatchUser>) {}
    override fun getBatchUser(batchId: String, userId: String, callback: DataItemCallback<BatchUser>) {}
    override fun updateBatchUser(user: BatchUser, callback: DataItemCallback<BatchUser>) {}
    override fun observeBatchesUsers(farmId: String, callback: DataLoadCallback<BatchUser>) {}
    override fun deleteAllBatchUsers(batchId: String) {}
    override fun getTodayRecords(onResults: (records: BatchRecords) -> Unit) {
        TODO("Not yet implemented")
    }

}