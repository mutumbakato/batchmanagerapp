package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.FarmDataSource
import com.mutumbakato.batchmanager.data.models.ActivityLog
import com.mutumbakato.batchmanager.data.models.Employee
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import java.util.*

/**
 * Created by cato on 26/05/18.
 */
class FarmRepository private constructor(private val remoteDataSource: FarmDataSource, private val localDataSource: FarmDataSource) : FarmDataSource {

    private var mFarmCache: MutableMap<String, Farm>? = null

    override fun setCurrency(farmId: String, currency: String, callback: FarmDataSource.FarmCallback) {
        remoteDataSource.setCurrency(farmId, currency, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                addToCache(farm)
                callback.onLoad(farm)
            }

            override fun onFail(message: String) {
                callback.onFail(message)
            }
        })
    }

    private fun addToCache(farm: Farm) {
        if (mFarmCache == null) {
            mFarmCache = LinkedHashMap()
        }
        mFarmCache!![farm.id] = farm
    }

    override fun getFarms(userId: String, callbacks: FarmDataSource.FarmListCallback) {
        observeFarms(callbacks, userId)
        if (mFarmCache != null && mFarmCache!!.isNotEmpty()) {
            callbacks.onLoad(ArrayList(mFarmCache!!.values))
        } else {
            localDataSource.getFarms(userId, object : FarmDataSource.FarmListCallback {
                override fun onLoad(farms: List<Farm>) {
                    for (farm in farms) {
                        addToCache(farm)
                    }
                    callbacks.onLoad(farms)
                }

                override fun onFail(message: String) {
                    getFromRemote(userId, callbacks)
                }
            })
        }
    }

    private fun getFromRemote(userId: String, callbacks: FarmDataSource.FarmListCallback) {
        remoteDataSource.getFarms(userId, object : FarmDataSource.FarmListCallback {
            override fun onLoad(farms: List<Farm>) {
                if (farms.isNotEmpty()) {
                    refreshLocalData(farms)
                    callbacks.onLoad(farms)
                } else {
                    callbacks.onFail("No farms found")
                }
            }

            override fun onFail(message: String) {
                callbacks.onFail(message)
            }
        })
    }

    override fun getFarm(id: String, callback: FarmDataSource.FarmCallback) {
        if (mFarmCache != null && mFarmCache!![id] != null) {
            callback.onLoad(mFarmCache!![id]!!)
        } else {
            localDataSource.getFarm(id, object : FarmDataSource.FarmCallback {
                override fun onLoad(farm: Farm) {
                    addToCache(farm)
                    callback.onLoad(farm)
                }

                override fun onFail(message: String) {
                    callback.onFail(message)
                }
            })
        }
    }

    override fun createFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        localDataSource.createFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                addToCache(farm)
                callBack.onLoad(farm)
            }

            override fun onFail(message: String) {
                callBack.onFail(message)
            }
        })
        remoteDataSource.createFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {

            }

            override fun onFail(message: String) {

            }
        })
    }

    override fun updateFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        localDataSource.updateFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                addToCache(farm)
                callBack.onLoad(farm)
                if (PreferenceUtils.farmId == farm.id)
                    PreferenceUtils.setFarm(farm)
            }

            override fun onFail(message: String) {
                callBack.onFail(message)
            }
        })
        remoteDataSource.updateFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {

            }

            override fun onFail(message: String) {

            }
        })
    }

    override fun deleteFarm(farm: Farm, callBack: FarmDataSource.FarmCallback) {
        localDataSource.deleteFarm(farm, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                if (mFarmCache != null) {
                    mFarmCache!!.remove(farm.id)
                }
                callBack.onLoad(farm)
            }

            override fun onFail(message: String) {
                callBack.onFail(message)
            }
        })
    }

    override fun addEmployee(employee: Employee, callback: FarmDataSource.FarmCallback) {
        remoteDataSource.addEmployee(employee, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                addToCache(farm)
                callback.onLoad(farm)
            }

            override fun onFail(message: String) {
                callback.onFail(message)
            }
        })
    }

    override fun getEmployees(farmId: String, callback: BaseDataSource.DataLoadCallback<Employee>) {

    }

    override fun removeEmployee(employee: Employee, callback: FarmDataSource.FarmCallback) {
        remoteDataSource.removeEmployee(employee, object : FarmDataSource.FarmCallback {
            override fun onLoad(farm: Farm) {
                addToCache(farm)
                callback.onLoad(farm)
            }

            override fun onFail(message: String) {
                callback.onFail(message)
            }
        })
    }

    override fun log(log: ActivityLog) {
        remoteDataSource.log(log)
    }

    override fun refresh() {

    }

    override fun observeFarms(callback: FarmDataSource.FarmListCallback, vararg referenceIds: String) {
        remoteDataSource.observeFarms(object : FarmDataSource.FarmListCallback {
            override fun onLoad(farms: List<Farm>) {
                callback.onLoad(farms)
                refreshLocalData(farms)
            }

            override fun onFail(message: String) {

            }
        }, *referenceIds)
    }

    fun clearCache() {
        if (mFarmCache != null)
            mFarmCache!!.clear()
    }

    private fun refreshLocalData(farms: List<Farm>) {
        for (f in farms) {
            localDataSource.createFarm(f, object : FarmDataSource.FarmCallback {
                override fun onLoad(farm: Farm) {

                }

                override fun onFail(message: String) {

                }
            })
            addToCache(f)
        }
    }

    companion object {

        private var INSTANCE: FarmRepository? = null

        fun getInstance(remoteDataSource: FarmDataSource, localDataSource: FarmDataSource): FarmRepository {
            if (INSTANCE == null) {
                INSTANCE = FarmRepository(remoteDataSource, localDataSource)
            }
            return INSTANCE as FarmRepository
        }
    }

}
