package com.mutumbakato.batchmanager.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import com.mutumbakato.batchmanager.data.models.*
import com.mutumbakato.batchmanager.data.models.exports.*

@Dao
interface StatisticsDao {

    @Query("SELECT * FROM batches WHERE _id = :batchId")
    fun getLiveBatchById(batchId: String): LiveData<Batch>

    @Query("SELECT * FROM batches WHERE _id = :batchId")
    fun getBatchById(batchId: String): Batch

    @Query("SELECT  SUM(quantity*rate) FROM sales WHERE batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun totalSales(batchId: String): LiveData<Float>

    @Query("SELECT SUM(eggs) AS total, SUM(damage) as damage from eggs WHERE batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun totalEggs(batchId: String): LiveData<EggCount>

    @Query("SELECT SUM(amount) AS total FROM expenses WHERE  batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun getTotalExpenses(batchId: String): LiveData<Float>

    @Query("SELECT SUM(quantity) AS total FROM feeds WHERE  batch_id = :batchId GROUP BY batch_id")
    fun getTotalFeeds(batchId: String): LiveData<Float>

    @Query("SELECT SUM(quantity) AS total FROM water WHERE  batch_id = :batchId GROUP BY batch_id")
    fun getTotalWater(batchId: String): LiveData<Float>

    @Query("SELECT SUM(count) AS rate FROM death WHERE batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun mortalityRate(batchId: String): LiveData<Int>

    @Query("SELECT (b.quantity - COALESCE(d.dead,0)) AS count FROM batches b LEFT JOIN (SELECT batch_id, SUM(count) AS dead FROM death WHERE batch_id = :batchId GROUP BY batch_id) AS d ON b._id = d.batch_id WHERE b._id = :batchId")
    fun count(batchId: String): LiveData<Int>

    @Query("SELECT b.quantity as count, COALESCE(d.dead,0) AS dead FROM batches b LEFT JOIN (SELECT batch_id, COALESCE(SUM(count),0) AS dead FROM death WHERE batch_id = :batchId GROUP BY batch_id) AS d ON b._id = d.batch_id WHERE b._id = :batchId")
    fun batchCount(batchId: String): LiveData<BatchCount>

    @Query("SELECT SUM(quantity) AS sold from sales WHERE batch_id = :batchId AND item= 'Chicken'")
    fun soldBird(batchId: String): LiveData<Int>

    @Query("SELECT SUM(quantity) AS sold from sales WHERE batch_id = :batchId AND item= 'Eggs'")
    fun soldEggs(batchId: String): LiveData<Int>

    @Query("SELECT  SUM(quantity*rate) AS y, STRFTIME('%Y-%m-%d',date) AS x FROM sales WHERE batch_id = :batchId AND status != 'trash' GROUP BY STRFTIME('%Y-%m-%d',date) ")
    fun geDailySales(batchId: String): LiveData<List<EntryItem>>

    @Query("SELECT  SUM(amount) AS y, STRFTIME('%Y-%m-%d',date) AS x FROM expenses WHERE batch_id = :batchId AND status != 'trash' GROUP BY STRFTIME('%Y-%m-%d',date) ")
    fun getDailyExpenses(batchId: String): LiveData<List<EntryItem>>

    @Query("SELECT  SUM(eggs+damage) AS y, STRFTIME('%Y-%m-%d',date) AS x FROM eggs WHERE batch_id = :batchId AND status != 'trash' GROUP BY STRFTIME('%Y-%m-%d',date) ")
    fun getDailyEggs(batchId: String): LiveData<List<EntryItem>>

    @Query("SELECT ROUND((SUM(eggs + damage) * 100.0)/AVG(count),1) AS y, STRFTIME('%Y-%m-%d',date) AS x FROM eggs WHERE batch_id = :batchId AND status != 'trash' GROUP BY STRFTIME('%Y-%m-%d',date) ")
    fun getDailyHenDay(batchId: String): LiveData<List<EntryItem>>

    @Query("SELECT  SUM(count) AS y, STRFTIME('%Y-%m-%d',date) AS x FROM death WHERE batch_id = :batchId AND status != 'trash' GROUP BY STRFTIME('%Y-%m-%d',date) ")
    fun getDailyDeaths(batchId: String): LiveData<List<EntryItem>>

    @Query("SELECT ((SUM(quantity)/batch_count) * 1000) AS y, STRFTIME('%Y-%m-%d',date) AS x FROM feeds WHERE batch_id = :batchId GROUP BY STRFTIME('%Y-%m-%d',date) ")
    fun getDailyFeeds(batchId: String): LiveData<List<EntryItem>>

    @Query("SELECT  (AVG(weight) * 1000) AS y, STRFTIME('%Y-%m-%d',date) AS x FROM weight WHERE batch_id = :batchId GROUP BY STRFTIME('%Y-%m-%d',date) ")
    fun getDailyWeight(batchId: String): LiveData<List<EntryItem>>

    @Query("SELECT  SUM(quantity) AS y, STRFTIME('%Y-%m-%d',date) AS x FROM water WHERE batch_id = :batchId GROUP BY STRFTIME('%Y-%m-%d',date) ")
    fun getDailyWater(batchId: String): LiveData<List<EntryItem>>

    @Query("SELECT  (AVG(weight) *1000) AS average  FROM weight WHERE batch_id = :batchId GROUP BY date ORDER BY date DESC LIMIT 1")
    fun getAvgWeight(batchId: String): LiveData<Float>

    @Query("SELECT ((SUM(quantity) / batch_count) * 1000) AS daily FROM feeds WHERE batch_id = :batchId GROUP BY date ORDER BY date DESC LIMIT 1")
    fun getLastFeedsTotal(batchId: String): LiveData<Float>

    @Query("SELECT ((SUM(quantity)/batch_count) * 1000) AS daily FROM water WHERE batch_id = :batchId GROUP BY date ORDER BY date DESC LIMIT 1")
    fun getLastWaterTotal(batchId: String): LiveData<Float>

    @Query("SELECT ROUND(((eggs + damage) * 100.0)/count,1) AS percentage FROM eggs WHERE batch_id = :batchId ORDER BY date DESC LIMIT 1")
    fun getEggsPercentage(batchId: String): LiveData<Float>

    @Query("""
       SELECT (e.expenses/b.quantity) AS cost FROM batches b 
       LEFT JOIN (SELECT batch_id, SUM(amount) AS expenses FROM expenses GROUP BY batch_id) AS e ON b._id = e.batch_id WHERE b._id = :batchId
       """)
    fun getCostPerBird(batchId: String): LiveData<Float>


    @Query("SELECT STRFTIME('%Y-%m-%d',date) AS date, GROUP_CONCAT(weight) AS data FROM weight WHERE batch_id = :batchId GROUP BY STRFTIME('%Y-%m-%d',date) ORDER BY data DESC")
    fun getWeightUniformityData(batchId: String): LiveData<List<UniformityData>>

//    @Query("SELECT STRFTIME('%Y-%m-%d',date) AS date, GROUP_CONCAT(weight) AS data FROM weight WHERE batch_id = :batchId GROUP BY STRFTIME('%Y-%m-%d',date) ORDER BY data DESC")
//    fun getEggUniformityData(batchId: String): LiveData<List<UniformityData>>

    // ------------------------------Export data---------------------------------------------------
    @Query("SELECT * FROM expenses WHERE batch_id = :batchId ORDER BY date ASC")
    fun getBatchExpenses(batchId: String): List<Expense>

    @Query("SELECT * FROM sales WHERE batch_id = :batchId ORDER BY date ASC")
    fun getBatchSales(batchId: String): List<Sale>

    @Query("SELECT date, SUM(count) AS count, comment AS cause FROM death WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchDeaths(batchId: String): List<DeathExport>

    @Query("SELECT date, SUM(quantity) AS total, AVG(batch_count) AS count ,ROUND(((SUM(quantity)/AVG(batch_count)) * 1000),1) AS gpb FROM feeds WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchFeeding(batchId: String): List<FeedsExport>

    @Query("SELECT date, (AVG(weight) * 1000)  AS avg FROM weight WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchWeight(batchId: String): List<WeightExport>

    @Query("SELECT date, SUM(quantity) AS  total, AVG(batch_count) AS count FROM water WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchWater(batchId: String): List<WaterExport>

    @Query("SELECT date, SUM(eggs) AS eggs, SUM(damage) AS damage, AVG(count) AS count FROM eggs WHERE batch_id = :batchId GROUP BY date ORDER BY date ASC")
    fun getBatchEggs(batchId: String): List<EggsExport>

}
