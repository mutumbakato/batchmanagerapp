package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.ActivityLog
import com.mutumbakato.batchmanager.data.models.Employee
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.models.exports.ExportConfig
import com.mutumbakato.batchmanager.data.models.exports.ExportData

/**
 * Created by cato on 5/11/18.
 */

interface FarmDataSource {

    fun setCurrency(id: String, currency: String, callback: FarmCallback)

    fun getFarms(userId: String, callbacks: FarmListCallback)

    fun getFarm(id: String, callback: FarmCallback)

    fun createFarm(farm: Farm, callBack: FarmCallback)

    fun updateFarm(farm: Farm, callBack: FarmCallback)

    fun deleteFarm(farm: Farm, callBack: FarmCallback)

    fun addEmployee(employee: Employee, callback: FarmCallback)

    fun getEmployees(farmId: String, callback: BaseDataSource.DataLoadCallback<Employee>)

    fun removeEmployee(employee: Employee, callback: FarmCallback)

    fun log(log: ActivityLog)

    fun refresh()

    fun observeFarms(callback: FarmListCallback, vararg referenceIds: String)

    interface FarmListCallback {
        fun onLoad(farms: List<Farm>)

        fun onFail(message: String)
    }

    interface FarmCallback {
        fun onLoad(farm: Farm)

        fun onFail(message: String)
    }
}
