/*
 * Copyright 2017, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mutumbakato.batchmanager.data.local.db;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.mutumbakato.batchmanager.data.local.dao.BatchDao;
import com.mutumbakato.batchmanager.data.local.dao.DeathDao;
import com.mutumbakato.batchmanager.data.local.dao.EggsDao;
import com.mutumbakato.batchmanager.data.local.dao.ExpenseCategoriesDao;
import com.mutumbakato.batchmanager.data.local.dao.ExpenseDao;
import com.mutumbakato.batchmanager.data.local.dao.FarmDao;
import com.mutumbakato.batchmanager.data.local.dao.FeedingDao;
import com.mutumbakato.batchmanager.data.local.dao.FeedsDao;
import com.mutumbakato.batchmanager.data.local.dao.SalesDao;
import com.mutumbakato.batchmanager.data.local.dao.StatisticsDao;
import com.mutumbakato.batchmanager.data.local.dao.StoreDao;
import com.mutumbakato.batchmanager.data.local.dao.UserDao;
import com.mutumbakato.batchmanager.data.local.dao.VaccinationCheckDao;
import com.mutumbakato.batchmanager.data.local.dao.VaccinationDao;
import com.mutumbakato.batchmanager.data.local.dao.VaccinationScheduleDao;
import com.mutumbakato.batchmanager.data.local.dao.WaterDao;
import com.mutumbakato.batchmanager.data.local.dao.WeightDao;
import com.mutumbakato.batchmanager.data.models.Batch;
import com.mutumbakato.batchmanager.data.models.BatchUser;
import com.mutumbakato.batchmanager.data.models.Death;
import com.mutumbakato.batchmanager.data.models.Eggs;
import com.mutumbakato.batchmanager.data.models.Expense;
import com.mutumbakato.batchmanager.data.models.ExpenseCategory;
import com.mutumbakato.batchmanager.data.models.Farm;
import com.mutumbakato.batchmanager.data.models.Feeding;
import com.mutumbakato.batchmanager.data.models.Feeds;
import com.mutumbakato.batchmanager.data.models.Sale;
import com.mutumbakato.batchmanager.data.models.Store;
import com.mutumbakato.batchmanager.data.models.Vaccination;
import com.mutumbakato.batchmanager.data.models.VaccinationCheck;
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule;
import com.mutumbakato.batchmanager.data.models.Water;
import com.mutumbakato.batchmanager.data.models.Weight;

/**
 * The Room Database that contains the Data tables.
 */
@Database(entities = {Batch.class, Expense.class, ExpenseCategory.class, Feeding.class,
        Sale.class, Eggs.class, Death.class, Vaccination.class, VaccinationSchedule.class,
        VaccinationCheck.class, Farm.class, BatchUser.class, Weight.class, Water.class, Feeds.class, Store.class}, version = 5)

public abstract class BManagerDatabase extends RoomDatabase {

    private static final Object sLock = new Object();
    private static BManagerDatabase INSTANCE;

    public static BManagerDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        BManagerDatabase.class, "BManager.db").addMigrations(MIGRATION_1_2, MIGRATION_2_3, MIGRATION_3_4, MIGRATION_4_5).build();
            }
            return INSTANCE;
        }
    }

    public abstract BatchDao batchDao();

    public abstract ExpenseDao expenseDao();

    public abstract ExpenseCategoriesDao expenseCategoriesDao();

    public abstract SalesDao salesDao();

    public abstract EggsDao eggsDao();

    public abstract DeathDao deathDao();

    public abstract VaccinationDao vaccinationDao();

    public abstract FeedingDao feedingDao();

    public abstract StatisticsDao statisticsDao();

    public abstract VaccinationScheduleDao vaccinationScheduleDao();

    public abstract VaccinationCheckDao vaccinationCheckDao();

    public abstract UserDao userDao();

    public abstract FarmDao farmDao();

    public abstract WeightDao weightDao();

    public abstract WaterDao waterDao();

    public abstract FeedsDao feedsDao();

    public abstract StoreDao storeDao();

    private static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            //create weight table
            database.execSQL("CREATE TABLE IF NOT EXISTS `weight` (`_id` TEXT NOT NULL, `batch_id` TEXT NOT NULL, `weight` REAL NOT NULL, `date` TEXT NOT NULL, `batch_count` INTEGER NOT NULL, `status` TEXT NOT NULL, `last_modified` INTEGER NOT NULL, PRIMARY KEY(`_id`))");
            //Create water table
            database.execSQL("CREATE TABLE IF NOT EXISTS `water` (`_id` TEXT NOT NULL, `batch_id` TEXT NOT NULL, `date` TEXT NOT NULL, `quantity` REAL NOT NULL, `batch_count` INTEGER NOT NULL, `status` TEXT NOT NULL, `last_modified` INTEGER NOT NULL, `created_at` TEXT NOT NULL, PRIMARY KEY(`_id`))");
            //create feeds table
            database.execSQL("CREATE TABLE IF NOT EXISTS `feeds` (`_id` TEXT NOT NULL, `batch_id` TEXT NOT NULL, `date` TEXT NOT NULL, `quantity` REAL NOT NULL, `feed_type` TEXT NOT NULL, `batch_count` INTEGER NOT NULL, `store_id` TEXT, `created_at` TEXT NOT NULL, `last_modified` TEXT NOT NULL, PRIMARY KEY(`_id`))");
            //create stores table
            database.execSQL("CREATE TABLE IF NOT EXISTS `stores` (`_id` TEXT NOT NULL, `farm_id` TEXT NOT NULL, `name` TEXT NOT NULL, `type` TEXT NOT NULL, `created_at` TEXT NOT NULL, `last_modified` INTEGER NOT NULL, PRIMARY KEY(`_id`))");
            //Add farmId and storeId to sales table
            database.execSQL("ALTER TABLE sales ADD COLUMN 'store_id' TEXT");
            database.execSQL("ALTER TABLE sales ADD COLUMN 'farm_id' TEXT");
            //add farmId to expenses table
            database.execSQL("ALTER TABLE expenses ADD COLUMN 'farm_id' TEXT");
            //add storeId to eggs table
            database.execSQL("ALTER TABLE eggs ADD COLUMN 'store_id' TEXT");
            //add  batch_count to feeds
            database.execSQL("ALTER TABLE farms ADD COLUMN 'eggs_in_tray'  INTEGER DEFAULT 30 NOT NULL");
            database.execSQL("ALTER TABLE farms ADD COLUMN 'water_units' TEXT");
            database.execSQL("ALTER TABLE batches ADD COLUMN 'close_date' TEXT");
        }
    };

    private static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            //Changed quantity to decimal real number
            database.execSQL("ALTER TABLE feeding RENAME TO _feeding_old");
            database.execSQL("CREATE TABLE IF NOT EXISTS `" + Feeding.TABLE_NAME + "` (`_id` TEXT NOT NULL, `batch_id` TEXT, `date` TEXT, `quantity` REAL NOT NULL, `item` TEXT, `server_id` INTEGER NOT NULL, `last_modified` INTEGER NOT NULL, `status` TEXT, PRIMARY KEY(`_id`))");
            database.execSQL("INSERT INTO feeding(_id,batch_id,date,quantity,item,server_id, last_modified, status) SELECT _id,batch_id,date,quantity,item,server_id, last_modified, status FROM _feeding_old");
        }
    };

    private static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `" + BatchUser.TABLE_NAME + "` (`_id` TEXT NOT NULL, `batch_id` TEXT, " +
                    "`user_id` TEXT, `name` TEXT, `role` TEXT, `addedBy` TEXT, `date` TEXT, `extra` TEXT, PRIMARY KEY(`_id`))");
        }
    };

    private static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE IF NOT EXISTS `" + Farm.TABLE_NAME + "` (`_id` TEXT NOT NULL, `name` TEXT, " +
                    "`contact` TEXT, `user_id` TEXT, `currency` TEXT, `location` TEXT, `units` TEXT, `status` TEXT, PRIMARY KEY(`_id`))");
            database.execSQL("ALTER TABLE " + Batch.TABLE_NAME + " ADD COLUMN userId TEXT");
            database.execSQL("ALTER TABLE " + Batch.TABLE_NAME + " ADD COLUMN farmId TEXT");
            database.execSQL("ALTER TABLE " + VaccinationSchedule.TABLE_NAME + " ADD COLUMN farmId TEXT");
            database.execSQL("ALTER TABLE " + ExpenseCategory.TABLE_NAME + " ADD COLUMN farmId TEXT");
        }
    };
}
