package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.FeedsDataSource
import com.mutumbakato.batchmanager.data.models.Feeding
import com.mutumbakato.batchmanager.data.models.Feeds

class FeedsFireStore : FeedsDataSource {

    private val db = FirebaseFirestore.getInstance().collection("feeding")
    private val oldDb = FirebaseFirestore.getInstance().collection("feeds")

    override fun createFeeds(feeds: Feeds, onSuccess: (Feeds) -> Unit) {
        db.document(feeds.id).set(feeds)
    }

    override fun listFeeds(batchId: String, onSuccess: (List<Feeds>) -> Unit) {
        db.whereEqualTo("batchId", batchId).addSnapshotListener { snapshots, _ ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val dataItems = snapshots.toObjects(Feeds::class.java)
                onSuccess(dataItems)
            }
        }
    }

    override fun listOldFeeds(batchId: String, onSuccess: (List<Feeding>) -> Unit) {
        oldDb.whereEqualTo("batchId", batchId).get().addOnSuccessListener {
            if (it != null) {
                val dataItems = it.toObjects(Feeding::class.java)
                onSuccess(dataItems)
            }
        }
    }

    override fun getFeeds(id: String, onSuccess: (Feeds) -> Unit) {}

    override fun updateFeeds(feeds: Feeds, onSuccess: (Feeds) -> Unit) {
        db.document(feeds.id).set(feeds)
    }

    override fun deleteFeeds(feeds: Feeds, onSuccess: () -> Unit) {
        db.document(feeds.id).delete()
    }
}