package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.DeathDataSource
import com.mutumbakato.batchmanager.data.models.Death
import com.mutumbakato.batchmanager.data.utils.Logger
import java.util.*

class DeathRepository private constructor(private val mDeathRemoteDataSource: DeathDataSource, private val mDeathLocalDataSource: DeathDataSource) : DeathDataSource {
    private var mListener: DeathDataSource.DataUpdatedListener? = null

    private var mCachedDeaths: MutableMap<String, MutableMap<String, Death>>? = null

    private var mCacheIsDirty = false

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Death>) {
        observeData(callback, batchId)
        if (mCachedDeaths != null && mCachedDeaths!![batchId] != null && !mCacheIsDirty) {
            callback.onDataLoaded(ArrayList(mCachedDeaths!![batchId]!!.values))
            return
        }

        if (mCacheIsDirty) {
            getDeathsFromRemoteSource(batchId, callback)
        } else {
            mDeathLocalDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Death> {
                override fun onDataLoaded(data: List<Death>) {
                    refreshCache(batchId, data)
                    callback.onDataLoaded(data)
                }

                override fun onEmptyData() {
                    getDeathsFromRemoteSource(batchId, callback)
                }

                override fun onError(message: String) {
                    getDeathsFromRemoteSource(batchId, callback)
                }
            })
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Death>) {
        if (mCachedDeaths != null && mCachedDeaths!![batchId] != null && !mCacheIsDirty) {
            mCachedDeaths!![batchId]!![id]?.let { callback.onDataItemLoaded(it) }
        } else {
            mDeathRemoteDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Death> {
                override fun onDataItemLoaded(data: Death) {
                    if (mCachedDeaths == null)
                        mCachedDeaths = LinkedHashMap()
                    mCachedDeaths!![batchId]!!.put(data.id, data)
                    callback.onDataItemLoaded(data)
                }

                override fun onDataItemNotAvailable() {
                    mDeathRemoteDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Death> {
                        override fun onDataItemLoaded(data: Death) {
                            if (mCachedDeaths == null)
                                mCachedDeaths = LinkedHashMap()
                            mCachedDeaths!![batchId]!!.put(data.id, data)
                            callback.onDataItemLoaded(data)
                        }

                        override fun onDataItemNotAvailable() {
                            if (mCachedDeaths == null)
                                mCachedDeaths = LinkedHashMap()
                            mCachedDeaths!!.remove(id)
                            callback.onDataItemNotAvailable()
                        }

                        override fun onError(message: String) {

                        }
                    })
                }

                override fun onError(message: String) {

                }
            })
        }
    }

    override fun saveData(data: Death) {
        Logger.log(data.batchId, "added", data.count.toString() + " birds to deaths, cause " + data.comment)
        mDeathLocalDataSource.saveData(data)
        mDeathRemoteDataSource.saveData(data)
        if (isCacheAvailable(data.batchId)) {
            mCachedDeaths!![data.batchId]!!.put(data.id, data)
        }
        notifyDataChanged()
    }

    override fun updateData(data: Death) {
        Logger.log(data.batchId, "updated", "deaths for " + data.date)
        mDeathLocalDataSource.updateData(data)
        mDeathRemoteDataSource.updateData(data)
        if (isCacheAvailable(data.batchId))
            mCachedDeaths!![data.batchId]!!.put(data.id, data)
        notifyDataChanged()
    }

    override fun refreshData() {
        mCacheIsDirty = true
    }

    override fun deleteAllData(batchId: String) {
        mDeathLocalDataSource.deleteAllData(batchId)
        mDeathRemoteDataSource.deleteAllData(batchId)

        if (isCacheAvailable(batchId))
            mCachedDeaths!![batchId]!!.clear()
        notifyDataChanged()
    }

    override fun deleteDataItem(item: Death, batchId: String) {
        Logger.log(batchId, Logger.ACTION_DELETED, "deaths for " + item.date)
        mDeathLocalDataSource.deleteDataItem(item, batchId)
        mDeathRemoteDataSource.deleteDataItem(item, batchId)
        if (isCacheAvailable(batchId)) {
            mCachedDeaths!![batchId]!!.remove(item.id)
        }
        notifyDataChanged()
    }

    override fun clearCache() {
        mCachedDeaths = null
        notifyDataChanged()
    }

    override fun notifyDataChanged() {
        if (mListener != null) {
            mListener!!.onDataUpdated()
        }
    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Death>, vararg referenceIds: String) {
        mDeathRemoteDataSource.observeData(object : BaseDataSource.DataLoadCallback<Death> {
            override fun onDataLoaded(data: List<Death>) {
                callback.onDataLoaded(data)
                refreshLocalData(referenceIds[0], data)
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        }, referenceIds[0])
    }


    fun getDeathsFromRemoteSource(batchId: String, callback: BaseDataSource.DataLoadCallback<Death>) {
        mDeathRemoteDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Death> {
            override fun onDataLoaded(data: List<Death>) {
                refreshLocalData(batchId, data)
                callback.onDataLoaded(data)
                mCacheIsDirty = false
            }

            override fun onEmptyData() {
                callback.onEmptyData()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }
        })
    }

    private fun refreshLocalData(batchId: String, data: List<Death>) {
        mCachedDeaths = null
        mDeathLocalDataSource.deleteAllData(batchId)
        for (death in data) {
            mDeathLocalDataSource.saveData(death)
        }
        refreshCache(batchId, data)
        mCacheIsDirty = false
    }


    private fun refreshCache(batchId: String, data: List<Death>) {
        for (death in data) {
            if (isCacheAvailable(batchId)) {
                mCachedDeaths!![batchId]!![death.id] = death
            }
        }
    }

    override fun getMortalityRate(batchId: String, callback: DeathDataSource.OnMortalityRate) {
        mDeathLocalDataSource.getMortalityRate(batchId, object : DeathDataSource.OnMortalityRate {
            override fun onMortalityLoaded(rate: Int) {
                if (rate >= 0)
                    callback.onMortalityLoaded(rate)
                else
                    callback.onMortalityLoaded(0)
            }
        })
    }

    fun setDataUpdateCallback(listener: DeathDataSource.DataUpdatedListener) {
        mListener = listener
    }

    private fun isCacheAvailable(batchId: String): Boolean {
        if (mCachedDeaths == null) {
            mCachedDeaths = LinkedHashMap()
            mCachedDeaths!![batchId] = LinkedHashMap()
        } else if (mCachedDeaths!![batchId] == null) {
            mCachedDeaths!![batchId] = LinkedHashMap()
        }
        return true
    }

    companion object {

        private var INSTANCE: DeathRepository? = null

        fun getInstance(remoteSource: DeathDataSource, localSource: DeathDataSource): DeathRepository {
            if (INSTANCE == null)
                INSTANCE = DeathRepository(remoteSource, localSource)
            return INSTANCE as DeathRepository
        }
    }

}
