package com.mutumbakato.batchmanager.data.sync;

import android.content.Context;
import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mutumbakato.batchmanager.data.firestore.BatchFireStore;
import com.mutumbakato.batchmanager.data.firestore.DeathsFireStore;
import com.mutumbakato.batchmanager.data.firestore.EggsFireStore;
import com.mutumbakato.batchmanager.data.firestore.ExpensesFireStore;
import com.mutumbakato.batchmanager.data.firestore.FeedingFireStore;
import com.mutumbakato.batchmanager.data.firestore.SalesFireStore;
import com.mutumbakato.batchmanager.data.firestore.VaccinationFireStore;
import com.mutumbakato.batchmanager.data.local.BatchLocalDataSource;
import com.mutumbakato.batchmanager.data.local.DeathLocalDataSource;
import com.mutumbakato.batchmanager.data.local.EggsLocalDataSource;
import com.mutumbakato.batchmanager.data.local.ExpenseCategoriesLocalDataSource;
import com.mutumbakato.batchmanager.data.local.ExpenseLocalDataSource;
import com.mutumbakato.batchmanager.data.local.FeedingLocalDataSource;
import com.mutumbakato.batchmanager.data.local.SalesLocalSource;
import com.mutumbakato.batchmanager.data.local.VaccinationCheckLocalDataSource;
import com.mutumbakato.batchmanager.data.local.VaccinationLocalDataSource;
import com.mutumbakato.batchmanager.data.local.VaccinationScheduleLocalDataSource;
import com.mutumbakato.batchmanager.data.local.db.BManagerDatabase;
import com.mutumbakato.batchmanager.data.models.Batch;
import com.mutumbakato.batchmanager.data.models.Death;
import com.mutumbakato.batchmanager.data.models.Eggs;
import com.mutumbakato.batchmanager.data.models.Expense;
import com.mutumbakato.batchmanager.data.models.ExpenseCategory;
import com.mutumbakato.batchmanager.data.models.Feeding;
import com.mutumbakato.batchmanager.data.models.Sale;
import com.mutumbakato.batchmanager.data.models.Vaccination;
import com.mutumbakato.batchmanager.data.models.VaccinationCheck;
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule;
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;
import com.mutumbakato.batchmanager.data.utils.ApiUtils;
import com.mutumbakato.batchmanager.utils.executors.AppExecutors;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;

public class Sync {

    private static Sync INSTANCE;
    private AppExecutors appExecutors;
    private List<Syncable> syncableDataSources;
    private SyncListener mListener;
    private SyncEndPoints mApi;
    private boolean isNotify;

    private Sync() {
        appExecutors = new AppExecutors();
        syncableDataSources = new ArrayList<>();
        mApi = (SyncEndPoints) ApiUtils.getEndPoints(SyncEndPoints.class);
    }

    public static Sync getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Sync();
        }
        return new Sync();
    }

    public String getSyncData(List<Syncable> dataSources) {
        ArrayList<SyncData> data = SyncData.Builder.build(dataSources);
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation()
                .serializeNulls()
                .setPrettyPrinting()
                .create();
        return gson.toJson(data);
    }

    private void runSync(String syncData) {
        if (PreferenceUtils.INSTANCE.isNotLoggedIn()) {
            return;
        }
        String token = PreferenceUtils.INSTANCE.getUserToken();

        if (token.equals("0")) {
            appExecutors.mainThread().execute(() -> {
                mListener.onSuccess();
            });
            return;
        }
        mApi.sync(token, syncData).enqueue(new retrofit2.Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response) {
                try {
                    String data = response.body();
                    SyncResults syncResults = new SyncResults();
                    syncResults.parseData(data);
                    handleSyncResults(syncResults);
                } catch (Exception e) {
                    e.printStackTrace();
                    if (mListener != null)
                        mListener.onFail();
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                t.printStackTrace();
                if (mListener != null)
                    mListener.onFail();
            }
        });
    }

    private void handleSyncResults(final SyncResults syncResults) {
        appExecutors.diskIO().execute(() -> {
            for (Syncable source : syncableDataSources) {
                try {
                    source.handleNewData(syncResults.getNew(source.getTable()));
                    source.handleModified(syncResults.getModified(source.getTable()));
                    source.handleSynced(syncResults.getSynced(source.getTable()));
                    source.handleUpdates(syncResults.getUpdates(source.getTable()));
                    source.handleTrash(syncResults.getTrash(source.getTable()));
                    if (isNotify) {
                        source.notifyRepository();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (mListener != null) {
                        mListener.onFail();
                    }
                }
            }
            migrate(syncableDataSources);
        });

    }

    public void syncAll(Context context) {
        isNotify = true;
        final BManagerDatabase db = BManagerDatabase.getInstance(context);
        appExecutors.networkIO().execute(() -> {
            syncableDataSources.add(BatchLocalDataSource.Companion.getInstance(appExecutors, db.batchDao()));
            syncableDataSources.add(ExpenseLocalDataSource.Companion.getInstance(appExecutors, db.expenseDao()));
            syncableDataSources.add(ExpenseCategoriesLocalDataSource.getInstance(appExecutors, db.expenseCategoriesDao()));
            syncableDataSources.add(DeathLocalDataSource.getInstance(appExecutors, db.deathDao()));
            syncableDataSources.add(EggsLocalDataSource.getInstance(appExecutors, db.eggsDao()));
            syncableDataSources.add(SalesLocalSource.Companion.getInstance(appExecutors, db.salesDao()));
            syncableDataSources.add(FeedingLocalDataSource.getInstance(appExecutors, db.feedingDao()));
            syncableDataSources.add(VaccinationScheduleLocalDataSource.getInstance(appExecutors, db.vaccinationScheduleDao()));
            syncableDataSources.add(VaccinationLocalDataSource.getInstance(appExecutors, db.vaccinationDao(), VaccinationCheckLocalDataSource.getInstance(db.vaccinationCheckDao())));
            syncableDataSources.add(VaccinationCheckLocalDataSource.getInstance(db.vaccinationCheckDao()));
            runSync(getSyncData(syncableDataSources));
        });
    }

    /**
     * @param syncable No longer running synchronization
     */
    @Deprecated
    public void sync(final Syncable syncable) {
//        appExecutors.networkIO().execute(() -> {
//            syncableDataSources.add(syncable);
//                runSync(getSyncData(syncableDataSources));
//        });
    }

    public void sync(final Syncable syncable, boolean isNotify) {
        this.isNotify = isNotify;
        sync(syncable);
    }

    public Sync setListener(SyncListener listener) {
        mListener = listener;
        return this;
    }

    private void migrate(List<Syncable> syncableList) {
        for (Syncable s : syncableList) {
            resolveData(s);
        }
        appExecutors.mainThread().execute(() -> {
            if (mListener != null) {
                mListener.onSuccess();
            }
        });

    }

    private void resolveData(Syncable syncable) {
        switch (syncable.getTable()) {
            case Batch.TABLE_NAME: {
                migrateBatches(syncable);
            }
            break;
            case Expense.TABLE_NAME: {
                migrateExpenses(syncable);
            }
            break;
            case Sale.TABLE_NAME: {
                migrateSales(syncable);
            }
            break;
            case Eggs.TABLE_NAME: {
                migrateEggs(syncable);
            }
            break;
            case Death.TABLE_NAME: {
                migrateDeaths(syncable);
            }
            break;
            case Vaccination.TABLE_NAME: {
                migrateVaccination(syncable);
            }
            break;
            case VaccinationCheck.TABLE_NAME: {
                migrateVaccinationChecks(syncable);
            }
            break;
            case VaccinationSchedule.TABLE_NAME: {
                migrateVaccinationSchedule(syncable);
            }
            break;
            case Feeding.TABLE_NAME: {
                migrateFeeds(syncable);
            }
            break;
            case ExpenseCategory.TABLE_NAME: {
                migrateExpenseCategories(syncable);
            }
        }
    }

    private void migrateExpenseCategories(Syncable<ExpenseCategory> syncable) {
        for (ExpenseCategory expenseCategory : syncable.getLocalData()) {
            new ExpensesFireStore().saveCategory(expenseCategory);
        }
    }

    private void migrateFeeds(Syncable<Feeding> syncable) {
        for (Feeding feeding : syncable.getLocalData()) {
            new FeedingFireStore().saveData(feeding);
        }
    }

    private void migrateVaccinationSchedule(Syncable<VaccinationSchedule> syncable) {
        for (VaccinationSchedule schedule : syncable.getLocalData()) {
            new VaccinationFireStore().saveSchedule(schedule);
        }
    }

    private void migrateVaccinationChecks(Syncable<VaccinationCheck> syncable) {
        for (VaccinationCheck check : syncable.getLocalData()) {
            new VaccinationFireStore().addCheckItem(check);
        }
    }

    private void migrateVaccination(Syncable<Vaccination> syncable) {
        for (Vaccination vaccination : syncable.getLocalData()) {
            new VaccinationFireStore().createVaccination(vaccination);
        }
    }

    private void migrateDeaths(Syncable<Death> syncable) {
        for (Death death : syncable.getLocalData()) {
            new DeathsFireStore().saveData(death);
        }
    }

    private void migrateEggs(Syncable<Eggs> syncable) {
        for (Eggs eggs : syncable.getLocalData()) {
            new EggsFireStore().saveData(eggs);
        }
    }

    private void migrateSales(Syncable<Sale> syncable) {
        for (Sale sale : syncable.getLocalData()) {
            new SalesFireStore().saveData(sale);
        }
    }

    private void migrateExpenses(Syncable<Expense> syncable) {
        for (Expense e : syncable.getLocalData()) {
            new ExpensesFireStore().saveData(e);
        }
    }

    private void migrateBatches(Syncable<Batch> syncable) {
        for (Batch b : syncable.getLocalData()) {
            b.setFarmId(PreferenceUtils.INSTANCE.getFarmId());
            new BatchFireStore().saveBatch(b, batchId -> {
            });
        }
    }

}
