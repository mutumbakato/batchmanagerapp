package com.mutumbakato.batchmanager.data.local

import com.google.gson.Gson
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.local.dao.BatchDao
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchRecords
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.sync.Sync
import com.mutumbakato.batchmanager.data.sync.Syncable
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.executors.AppExecutors
import org.json.JSONArray
import org.json.JSONException

/**
 * Concrete implementation of a data source as a db.
 */
class BatchLocalDataSource private constructor(private val mAppExecutors: AppExecutors, private val batchDao: BatchDao)
    : BatchDataSource, Syncable<Batch> {

    override fun getAllBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        val runnable = {
            val batches = batchDao.allBatches
            mAppExecutors.mainThread().execute {
                if (batches.isNotEmpty()) {
                    callback.onBatchesLoaded(batches)
                } else {
                    // This will be called if the table is new or just empty.
                    callback.onDataNotAvailable()
                }
            }
        }
        mAppExecutors.diskIO().execute(runnable)
    }

    override fun getArchivedBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        val runnable = {
            val batches = batchDao.getArchivedBatches(farmId)
            mAppExecutors.mainThread().execute {
                if (batches.isNotEmpty()) {
                    callback.onBatchesLoaded(batches)
                } else {
                    // This will be called if the table is new or just empty.
                    callback.onDataNotAvailable()
                }
            }
        }
        mAppExecutors.diskIO().execute(runnable)
        callback.onDataNotAvailable()
    }

    /**
     * Note: [GetBatchCallback.onDataNotAvailable] is fired if the [Batch] isn't
     * found.
     */
    override fun getBatch(batchId: String, callback: BatchDataSource.GetBatchCallback) {
        val runnable = {
            val batch = batchDao.getBatchById(batchId)
            mAppExecutors.mainThread().execute {
                callback.onBatchLoaded(batch)
            }
        }
        mAppExecutors.diskIO().execute(runnable)
    }

    override fun saveBatch(batch: Batch, callback: BatchDataSource.CreateBatchCallback) {
        val saveRunnable = {
            batchDao.insertBatch(batch)
            mAppExecutors.mainThread().execute { callback.onBatchCreated(batch.id) }
            sync()
        }
        mAppExecutors.diskIO().execute(saveRunnable)
    }

    override fun updateBatch(batch: Batch) {
        val activateRunnable = {
            batchDao.updateBatch(batch)
            sync()
        }
        mAppExecutors.diskIO().execute(activateRunnable)
    }

    override fun closeBatch(batchId: String) {
        //Implemented with update
    }

    override fun refreshBatches() {
        // Not required because the {@link BatchRepository} handles the logic of refreshing the
        // batches from all the available data sources.
    }

    override fun deleteAllMyBatches(farmId: String) {
        val deleteRunnable = { batchDao.deleteAllMyBatches(farmId) }
        mAppExecutors.diskIO().execute(deleteRunnable)
    }

    override fun deleteAllOtherBatches(farmId: String) {
        val deleteRunnable = { batchDao.deleteAllOtherBatches(farmId) }
        mAppExecutors.diskIO().execute(deleteRunnable)
    }

    override fun deleteBatch(batchId: String) {
        val deleteRunnable = {
            batchDao.deleteVaccinationChecks(batchId)
            batchDao.deleteBatchById(batchId)
            batchDao.deleteDeaths(batchId)
            batchDao.deleteEggs(batchId)
            batchDao.deleteExpenses(batchId)
            batchDao.deleteFeeds(batchId)
            batchDao.deleteSales(batchId)
            sync()
        }
        mAppExecutors.diskIO().execute(deleteRunnable)
    }

    override fun addUser(users: BatchUser, callback: BaseDataSource.DataItemCallback<String>) {
        mAppExecutors.diskIO().execute {
            batchDao.addBatchUser(users)
            mAppExecutors.mainThread().execute { callback.onDataItemLoaded("") }
        }
    }

    override fun removeUser(user: BatchUser, callback: BaseDataSource.DataItemCallback<String>) {
        mAppExecutors.diskIO().execute {
            batchDao.removeBatchUser(user.batchId, user.userId)
            mAppExecutors.mainThread().execute { callback.onDataItemLoaded("") }
        }
    }

    override fun getBatchUsers(batchId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>) {
        mAppExecutors.diskIO().execute {
            val batchUsers = batchDao.getBatchUsers(batchId)
            mAppExecutors.mainThread().execute { callback.onDataLoaded(batchUsers) }
        }
    }

    override fun getBatchUser(batchId: String, userId: String, callback: BaseDataSource.DataItemCallback<BatchUser>) {
        mAppExecutors.diskIO().execute {
            val b: BatchUser? = batchDao.getBatchUser(batchId, userId)

            mAppExecutors.mainThread().execute {
                if (b != null)
                    callback.onDataItemLoaded(b)
                else
                    callback.onDataItemNotAvailable()
            }

        }
    }

    override fun updateBatchUser(user: BatchUser, callback: BaseDataSource.DataItemCallback<BatchUser>) {
        mAppExecutors.diskIO().execute {
            batchDao.updateBatchUser(user)
            mAppExecutors.mainThread().execute { callback.onDataItemLoaded(user) }
        }
    }

    override fun observeBatchesUsers(farmId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>) {

    }

    override fun deleteAllBatchUsers(batchId: String) {
        mAppExecutors.diskIO().execute { batchDao.deleteALlBatchUsers(batchId) }
    }

    override fun getTodayRecords(onResults: (records: BatchRecords) -> Unit) {
        mAppExecutors.diskIO().execute {
            val today = DateUtils.today()
            val records = BatchRecords()
            records.batches = batchDao.allBatches
            records.batches.forEach {
                records.feeds.addAll(batchDao.todayFeeds(it.id, today))
                records.water.addAll(batchDao.todayWater(it.id, today))
                records.weight.addAll(batchDao.todayWeight(it.id, today))
                records.mortality.addAll(batchDao.todayDeaths(it.id, today))
                records.eggs.addAll(batchDao.todayEggs(it.id, today))
            }
            mAppExecutors.mainThread().execute {
                onResults(records)
            }
        }
    }

    override fun clearMemoryCache(notify: Boolean) {

    }

    override fun notifyDataChanged() {

    }

    override fun observeMyBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {

    }

    override fun observeOtherBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {

    }

    override fun getTable(): String {
        return Batch.TABLE_NAME
    }

    override fun getNew(): List<Batch> {
        return batchDao.new
    }

    override fun getUpdated(): List<Batch> {
        return batchDao.updated
    }

    override fun getLocalData(): List<Batch> {
        return batchDao.local
    }

    override fun getTrash(): List<Long> {
        return batchDao.trash
    }

    override fun getLastServerId(): Long {
        return batchDao.lastServerId
    }

    override fun handleNewData(newData: JSONArray) {
        val gson = Gson()
        for (n in 0 until newData.length()) {
            try {
                val batch = gson.fromJson(newData.get(n).toString(), Batch::class.java)
                val newBatch = Batch(batch.id, batch.serverId, batch.date,
                        batch.name, batch.supplier, batch.quantity, batch.rate,
                        batch.type, batch.age, batch.vaccinationSchedule,
                        batch.status, batch.lastModified, batch.userId, PreferenceUtils.farmId)
                batchDao.insertBatch(newBatch)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun handleModified(modified: JSONArray) {
        val gson = Gson()
        for (n in 0 until modified.length()) {
            try {
                val item = gson.fromJson(modified.get(n).toString(), Batch::class.java)
                batchDao.updateBatch(item)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun handleSynced(synced: JSONArray) {
        for (n in 0 until synced.length()) {
            try {
                val id = synced.getJSONObject(n).getString("id")
                val serverId = synced.getJSONObject(n).getLong("serverId")
                val lastMod = synced.getJSONObject(n).getLong("lastModified")
                val batch = batchDao.getBatchById(id)
                val syncedBatch = Batch(batch.id, serverId, batch.date, batch.name,
                        batch.supplier, batch.quantity, batch.rate, batch.type,
                        batch.age, batch.vaccinationSchedule, batch.status,
                        lastMod, batch.userId, PreferenceUtils.farmId)
                batchDao.updateBatch(syncedBatch)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun handleUpdates(updates: JSONArray) {
        for (n in 0 until updates.length()) {
            try {
                val id = updates.getJSONObject(n).getString("id")
                val lastModified = updates.getJSONObject(n).getLong("lastModified")
                val batch = batchDao.getBatchById(id)
                val newBatch = Batch(batch.id, batch.serverId,
                        batch.date, batch.name, batch.supplier,
                        batch.quantity, batch.rate, batch.type,
                        batch.age, batch.vaccinationSchedule, batch.status, lastModified,
                        batch.userId, PreferenceUtils.farmId)
                batchDao.updateBatch(newBatch)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
    }

    override fun handleTrash(trash: Array<String>) {
        for (aTrash in trash) {
            batchDao.deleteBatchById(aTrash)
        }
    }

    override fun notifyRepository() {
        //repository.clearMemoryCache(true);
    }

    private fun sync() {
        Sync.getInstance().sync(this)
    }

    companion object {

        @Volatile
        private var INSTANCE: BatchLocalDataSource? = null

        fun getInstance(appExecutors: AppExecutors, batchDao: BatchDao): BatchLocalDataSource {
            if (INSTANCE == null) {
                synchronized(BatchLocalDataSource::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = BatchLocalDataSource(appExecutors, batchDao)
                    }
                }
            }
            return INSTANCE as BatchLocalDataSource
        }
    }
}
