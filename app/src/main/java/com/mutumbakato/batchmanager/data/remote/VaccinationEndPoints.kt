package com.mutumbakato.batchmanager.data.remote

import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by cato on 11/24/17.
 */

interface VaccinationEndPoints {

    @GET("vaccination/schedules")
    fun fetchSchedules(): Call<List<VaccinationSchedule>>

    @GET("vaccination/schedule_items/{schedule_id}")
    fun fetchVaccinations(@Path("schedule_id") scheduleId: String): Call<List<Vaccination>>
}
