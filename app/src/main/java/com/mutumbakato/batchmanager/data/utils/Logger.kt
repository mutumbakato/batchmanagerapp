package com.mutumbakato.batchmanager.data.utils

import com.mutumbakato.batchmanager.data.ActivityLogDataSource
import com.mutumbakato.batchmanager.data.firestore.LogsFirestore
import com.mutumbakato.batchmanager.data.models.ActivityLog
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.utils.DateUtils
import java.util.*

object Logger {

    const val ACTION_CREATED = "created"
    const val ACTION_CLOSED = "closed"
    const val ACTION_COMPLETED = "completed"
    const val ACTION_UNCOMPLETED = "uncompleted"
    const val ACTION_DELETED = "deleted"
    const val ACTION_ADDED = "added"
    const val ACTION_UPDATED = "updated"
    const val ACTION_REMOVED = "removed"

    private var fireStore: LogsFirestore? = null
    private var logs: MutableList<ActivityLog> = ArrayList()

    private fun dataSource(): LogsFirestore {
        if (fireStore == null) {
            fireStore = LogsFirestore()
        }
        return fireStore as LogsFirestore
    }

    fun log(batchId: String?, action: String, description: String) {
        val log = ActivityLog(UUID.randomUUID().toString(), action, description, PreferenceUtils.user, batchId,DateUtils.now())
        dataSource().log(log)
    }


    fun getLogs(batchId: String, callBack: ActivityLogDataSource.LogsCallBack) {
        dataSource().setListener(batchId, object : ActivityLogDataSource.LogsCallBack {
            override fun onLoad(l: List<ActivityLog>, newCount: Int) {
                logs = ArrayList(l)
                callBack.onLoad(logs, newCount)
            }

            override fun onError(message: String) {

            }
        })

    }

    fun listen(batchId: String, callBack: ActivityLogDataSource.LogsCallBack) {
        dataSource().setListener(batchId, object : ActivityLogDataSource.LogsCallBack {
            override fun onLoad(l: List<ActivityLog>, newCount: Int) {
                logs = ArrayList(l)
                callBack.onLoad(logs, newCount)
            }

            override fun onError(message: String) {

            }
        })

    }

    fun loadMore(batchId: String, lastDate: String, callBack: ActivityLogDataSource.LogsCallBack) {
        dataSource().loadMore(batchId, lastDate, object : ActivityLogDataSource.LogsCallBack {
            override fun onLoad(l: List<ActivityLog>, newCount: Int) {
                logs.addAll(l)
                callBack.onLoad(logs, newCount)
            }

            override fun onError(message: String) {

            }
        })
    }
}
