package com.mutumbakato.batchmanager.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mutumbakato.batchmanager.data.models.Sale

@Dao
interface SalesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllSales(expenses: List<Sale>)

    @Query("SELECT * FROM sales WHERE farm_id = :farmId ORDER BY date DESC")
    fun listFarmSales(farmId: String): LiveData<List<Sale>>

    @Query("SELECT * FROM sales WHERE (farm_id = :id OR batch_id = :id) AND date BETWEEN :from AND :to ORDER BY date DESC")
    fun filterSales(id: String, from: String, to: String): LiveData<List<Sale>>

    @Query("SELECT * FROM sales WHERE batch_id = :batchId")
    fun listBatchSales(batchId: String): LiveData<List<Sale>>

    @Query("SELECT * FROM sales WHERE _id = :id")
    fun getLiveSaleById(id: String): LiveData<Sale>

    @Query("SELECT STRFTIME('%Y-%m',date) AS month FROM sales WHERE farm_id = :farmId GROUP BY month")
    fun listExpensesMonths(farmId: String): LiveData<List<String>>

    @Query("DELETE FROM sales WHERE farm_id = :farmId")
    fun deleteFarmSales(farmId: String)

    @Query("DELETE FROM sales WHERE batch_id = :batchId")
    fun deleteAllForBatch(batchId: String)

    /**
     * Select all sales from the sales table.
     *
     * @return all sales.
     */
    @get:Query("SELECT * FROM sales WHERE last_modified != 0 AND server_id != 0 AND status != 'trash' ")
    val local: List<Sale>

    /**
     * Select new sales
     *
     * @return sales that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM sales WHERE server_id = 0 AND status != 'trash' ")
    val new: List<Sale>

    /**
     * Select locally updated sales since the last synchronisation
     *
     * @return updated sales  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM sales WHERE server_id != 0 AND last_modified = 0")
    val updated: List<Sale>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM sales ORDER BY server_id DESC LIMIT 0,1")
    val lastServerId: Long

    /**
     * Select sales deleted from the locally
     *
     * @return server ids of sales where status = trash ;
     */
    @get:Query("SELECT server_id FROM sales WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * Select all sales from the sales table.
     *
     * @return all sales.
     */
    @Query("SELECT * FROM sales WHERE batch_id = :batchId AND status != 'trash' ")
    fun getSales(batchId: String): List<Sale>

    /**
     * Select a sales by name.
     *
     * @param salesId the sale name.
     * @return the sales with salesId.
     */
    @Query("SELECT * FROM sales WHERE _id = :salesId")
    fun getSaleById(salesId: String): Sale

    /**
     * Insert a sales in the database. If the sales already exists, replace it.
     *
     * @param sales the sales to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSale(sales: Sale): Long

    /**
     * Update a sales.
     *
     * @param sales sales to be updated
     * @return the number of sales updated. This should always be 1.
     */
    @Update
    fun updateSale(sales: Sale): Int

    /**
     * Delete a sales by name.
     *
     * @return the number of sales deleted. This should always be 1.
     */
    @Query("UPDATE sales SET status = 'trash' WHERE _id = :salesId")
    fun deleteSaleById(salesId: String): Int

    /**
     * Delete all sales.
     */
    @Query("DELETE FROM sales WHERE batch_id = :batchId")
    fun deleteSales(batchId: String)

    /**
     * Permanently deleteCategory all trashed entries
     */
    @Query("DELETE FROM sales WHERE status = 'trash'")
    fun clearTrash(): Int

    @Query("SELECT  SUM(quantity*rate) FROM sales WHERE batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun totalSales(batchId: String): Float

}

