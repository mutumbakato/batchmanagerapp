package com.mutumbakato.batchmanager.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

const val TABLE_WEIGHT = "weight"

@Entity(tableName = TABLE_WEIGHT)
data class Weight constructor(@PrimaryKey @ColumnInfo(name = "_id") val id: String = UUID.randomUUID().toString(),
                              @ColumnInfo(name = "batch_id") val batchId: String = "0",
                              val weight: Float = 0f,
                              val date: String = "",
                              @ColumnInfo(name = "batch_count") val batchCount: Int = 0,
                              val status: String = "normal",
                              @ColumnInfo(name = "last_modified") val lastModified: Long = 0) {
    val average: Float
        get() = weight / batchCount
}
