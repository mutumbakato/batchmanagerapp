package com.mutumbakato.batchmanager.data.local.dao


import androidx.room.*
import com.mutumbakato.batchmanager.data.models.Feeding

@Dao
interface FeedingDao {

    /**
     * Select all feedings from the feeding table.
     *
     * @return all feedings.
     */
    @get:Query("SELECT * FROM feeding WHERE last_modified != 0 AND server_id != 0 AND status != 'trash'")
    val local: List<Feeding>

    /**
     * Select new feeding
     *
     * @return feedings that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM feeding WHERE server_id = 0")
    val new: List<Feeding>

    /**
     * Select locally updated feedings since the last synchronisation
     *
     * @return updated feeding  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM feeding WHERE server_id != 0 AND last_modified = 0 AND status != 'trash'")
    val updated: List<Feeding>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM feeding ORDER BY server_id DESC LIMIT 1")
    val lastServerId: Long

    /**
     * Select feedings deleted from the locally
     *
     * @return server ids of feedings where status = trash ;
     */
    @get:Query("SELECT server_id FROM feeding WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * Select all feeding from the feeding table.
     *
     * @return all feeding.
     */
    @Query("SELECT * FROM feeding WHERE batch_id = :batchId AND status != 'trash'")
    fun getFeedings(batchId: String): List<Feeding>

    /**
     * Select a feeding by name.
     *
     * @param feedingId the feeding name.
     * @return the feeding with feedingId.
     */
    @Query("SELECT * FROM feeding WHERE _id = :feedingId")
    fun getFeedingById(feedingId: String): Feeding

    /**
     * Insert a feeding in the database. If the feeding already exists, replace it.
     *
     * @param feeding the feeding to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFeeding(feeding: Feeding): Long

    /**
     * Update a feeding.
     *
     * @param feeding feeding to be updated
     * @return the number of feeding updated. This should always be 1.
     */
    @Update
    fun updateFeeding(feeding: Feeding): Int

    /**
     * Delete a feeding by name.
     */
    @Query("UPDATE feeding SET status = 'trash' WHERE _id = :feedingId")
    fun deleteFeedingById(feedingId: String)

    /**
     * Delete all feeding.
     */
    @Query("DELETE FROM feeding WHERE batch_id = :batchId")
    fun deleteAllFeeding(batchId: String)

    /**
     * Permanently deleteCategory all trashed entries
     */
    @Query("DELETE FROM feeding WHERE status = 'trash'")
    fun clearTrash()

    @Query("SELECT SUM(quantity) AS rate FROM feeding WHERE batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun mortalityRate(batchId: String): Int

}

