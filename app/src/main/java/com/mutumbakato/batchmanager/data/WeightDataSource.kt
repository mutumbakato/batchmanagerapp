package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Weight

interface WeightDataSource {

    fun createWeight(weight: Weight, onSuccess: (Weight) -> Unit)

    fun listWeights(batchId: String, onSuccess: (List<Weight>) -> Unit)

    fun getWeight(id: String, onSuccess: (Weight) -> Unit)

    fun updateWeight(weight: Weight, onSuccess: (Weight) -> Unit)

    fun deleteWeight(weight: Weight, onSuccess: () -> Unit)

}