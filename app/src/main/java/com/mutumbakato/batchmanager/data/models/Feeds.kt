package com.mutumbakato.batchmanager.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.mutumbakato.batchmanager.utils.DateUtils
import java.util.*

const val TABLE_FEEDS: String = "feeds"

@Entity(tableName = TABLE_FEEDS)
data class Feeds constructor(@PrimaryKey
                             @ColumnInfo(name = "_id") val id: String = UUID.randomUUID().toString(),
                             @ColumnInfo(name = "batch_id") val batchId: String = "",
                             val date: String = DateUtils.today(),
                             val quantity: Float = 0f,
                             @ColumnInfo(name = "feed_type") val feedType: String = "",
                             @ColumnInfo(name = "batch_count") val batchCount: Int = 0,
                             @ColumnInfo(name = "store_id") val storeId: String? = null,
                             @ColumnInfo(name = "created_at") val createdAt: String = DateUtils.today(),
                             @ColumnInfo(name = "last_modified") val lastModified: String = DateUtils.today()) {

    val average: Float
        get() = quantity / batchCount
}