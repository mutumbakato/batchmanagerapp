package com.mutumbakato.batchmanager.data.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;

import java.util.UUID;

import static com.mutumbakato.batchmanager.data.models.ExpenseCategory.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class ExpenseCategory {

    public static final String TABLE_NAME = "expense_categories";

    @NonNull
    @Expose
    @PrimaryKey
    @ColumnInfo(name = "_id")
    private String id;

    @Expose
    @ColumnInfo(name = "name")
    private String name;

    @Expose
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Expose
    @ColumnInfo(name = "status")
    private String status;

    private String farmId;

    @Ignore
    public ExpenseCategory() {

    }

    public ExpenseCategory(@NonNull String id, String name, long serverId, String status, long lastModified, String farmId) {
        this.name = name;
        this.id = id;
        this.serverId = serverId;
        this.status = status;
        this.lastModified = lastModified;
        this.farmId = farmId;
    }

    @Ignore
    public ExpenseCategory(String name, long serverId, String farmId) {
        this(UUID.randomUUID().toString(), name, serverId, "normal", 0, farmId);
    }

    @NonNull
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return name;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }

    public String getFarmId() {
        return farmId;
    }
}
