package com.mutumbakato.batchmanager.data.sync;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class SyncResults {

    private ArrayList<SyncResultsData> syncResultsData;

    public SyncResults() {
        syncResultsData = new ArrayList<>();
    }

    public void parseData(String results) {
        try {
            JSONObject resultData = new JSONObject(results);
            JSONArray jArray = resultData.getJSONArray("data");

            for (int i = 0; i < jArray.length(); i++) {
                JSONObject syncResult = jArray.getJSONObject(i);
                String table = syncResult.getString("table");
                JSONArray newData = syncResult.getJSONArray("new");
                JSONArray updated = syncResult.getJSONArray("updates");
                JSONArray modified = syncResult.getJSONArray("modified");
                JSONArray synced = syncResult.getJSONArray("sync");
                String[] trash = new Gson().fromJson(String.valueOf(syncResult.getJSONArray("trash")), String[].class);
                SyncResultsData d = new SyncResultsData();

                d.modified = modified;
                d.updates = updated;
                d.newData = newData;
                d.synced = synced;
                d.trash = trash;
                d.table = table;

                syncResultsData.add(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONArray getSynced(String table) {
        JSONArray j = new JSONArray();
        for (int i = 0; i < syncResultsData.size(); i++) {
            if (syncResultsData.get(i).table.equals(table)) {
                j = syncResultsData.get(i).synced;
            }
        }
        return j;
    }

    public JSONArray getModified(String table) {
        JSONArray j = new JSONArray();
        for (int i = 0; i < syncResultsData.size(); i++) {
            if (syncResultsData.get(i).table.equals(table)) {
                j = syncResultsData.get(i).modified;
            }
        }
        return j;
    }

    JSONArray getUpdates(String table) {
        JSONArray j = new JSONArray();
        for (int i = 0; i < syncResultsData.size(); i++) {
            if (syncResultsData.get(i).table.equals(table)) {
                j = syncResultsData.get(i).updates;
            }
        }
        return j;
    }

    public JSONArray getNew(String table) {
        JSONArray j = new JSONArray();
        for (int i = 0; i < syncResultsData.size(); i++) {
            if (syncResultsData.get(i).table.equals(table)) {
                j = syncResultsData.get(i).newData;
            }
        }
        return j;
    }

    public String[] getTrash(String table) {
        String[] j = {};
        for (int i = 0; i < syncResultsData.size(); i++) {
            if (syncResultsData.get(i).table.equals(table)) {
                j = syncResultsData.get(i).trash;
            }
        }
        return j;
    }

    private class SyncResultsData {
        private String table;
        private JSONArray synced;
        private JSONArray modified;
        private JSONArray updates;
        private JSONArray newData;
        private String[] trash;
    }

}
