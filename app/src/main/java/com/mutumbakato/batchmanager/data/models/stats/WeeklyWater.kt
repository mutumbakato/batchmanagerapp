package com.mutumbakato.batchmanager.data.models.stats

data class WeeklyWater(val days: Int, val waterPerBird: Float, val totalWater: Float)