package com.mutumbakato.batchmanager.data.models.stats

data class WeeklyEggs(val week: Int, val count: Int)