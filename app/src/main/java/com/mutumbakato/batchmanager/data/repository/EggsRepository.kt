package com.mutumbakato.batchmanager.data.repository

import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.EggsDataSource
import com.mutumbakato.batchmanager.data.models.EggCount
import com.mutumbakato.batchmanager.data.models.Eggs
import com.mutumbakato.batchmanager.data.utils.Logger
import java.util.*

class EggsRepository private constructor(private val mEggsRemoteDataSource: EggsDataSource, private val mEggsLocalDataSource: EggsDataSource) : EggsDataSource {

    private var mListener: EggsDataSource.DataUpdatedListener? = null
    private var mCachedEggs: MutableMap<String, MutableMap<String, Eggs>>? = null
    private var mCacheIsDirty = false

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Eggs>) {
        observeData(callback, batchId)
        if (mCachedEggs != null && mCachedEggs!![batchId] != null && !mCacheIsDirty) {
            callback.onDataLoaded(ArrayList(mCachedEggs!![batchId]!!.values))
            return
        }

        if (mCacheIsDirty) {
            getBatchFromRemoteDataSource(batchId, callback)
        } else {
            mEggsLocalDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Eggs> {
                override fun onDataLoaded(data: List<Eggs>) {
                    refreshCache(data)
                    callback.onDataLoaded(data)
                }

                override fun onEmptyData() {
                    getBatchFromRemoteDataSource(batchId, callback)
                }

                override fun onError(message: String) {

                }
            })
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Eggs>) {
        if (mCachedEggs != null && mCachedEggs!![batchId] != null && !mCacheIsDirty) {
            callback.onDataItemLoaded(mCachedEggs!![batchId]!![id]!!)
        } else {
            mEggsLocalDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Eggs> {
                override fun onDataItemLoaded(data: Eggs) {
                    callback.onDataItemLoaded(data)
                    isCacheAvailable(batchId)
                    mCachedEggs!![batchId]!!.put(id, data)
                }

                override fun onDataItemNotAvailable() {
                    mEggsRemoteDataSource.getOne(id, batchId, object : BaseDataSource.DataItemCallback<Eggs> {
                        override fun onDataItemLoaded(data: Eggs) {
                            if (isCacheAvailable(batchId)) {
                                mCachedEggs!![data.batchId]!!.put(id, data)
                            }
                            callback.onDataItemLoaded(data)
                        }

                        override fun onDataItemNotAvailable() {
                            callback.onDataItemNotAvailable()
                        }

                        override fun onError(message: String) {

                        }
                    })

                }

                override fun onError(message: String) {

                }
            })
        }
    }

    override fun saveData(data: Eggs) {
        Logger.log(data.batchId, Logger.ACTION_ADDED, data.treys.toString() + " Trays, " + (data.eggs % Eggs.EGGS_IN_A_TREY) + "eggs  and " + data.damage + " damage to eggs.")
        mEggsLocalDataSource.saveData(data)
        mEggsRemoteDataSource.saveData(data)
        if (isCacheAvailable(data.batchId)) {
            mCachedEggs!![data.batchId]!!.put(data.id, data)
        }
        notifyDataChanged()
    }

    override fun updateData(data: Eggs) {
        Logger.log(data.batchId, Logger.ACTION_UPDATED, "eggs for " + data.date)
        mEggsLocalDataSource.updateData(data)
        mEggsRemoteDataSource.updateData(data)

        if (isCacheAvailable(data.batchId)) {
            mCachedEggs!![data.batchId]!!.put(data.id, data)
        }

        notifyDataChanged()
    }

    override fun refreshData() {
        mCacheIsDirty = true
    }

    override fun deleteAllData(batchId: String) {
        mEggsLocalDataSource.deleteAllData(batchId)
        mEggsRemoteDataSource.deleteAllData(batchId)
        if (isCacheAvailable(batchId)) {
            mCachedEggs!![batchId]!!.clear()
        }
        notifyDataChanged()
    }

    override fun deleteDataItem(item: Eggs, batchId: String) {
        Logger.log(batchId, Logger.ACTION_DELETED, "eggs for " + item.date)
        if (isCacheAvailable(batchId)) {
            mEggsLocalDataSource.deleteDataItem(item, batchId)
            mEggsRemoteDataSource.deleteDataItem(item, batchId)
            mCachedEggs!![batchId]!!.remove(item.id)
        }
        notifyDataChanged()
    }

    override fun clearCache() {
        mCachedEggs = null
        notifyDataChanged()
    }

    override fun notifyDataChanged() {
        if (mListener != null) {
            mListener!!.onDataUpdated()
        }
    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Eggs>, vararg referenceIds: String) {
        mEggsRemoteDataSource.observeData(object : BaseDataSource.DataLoadCallback<Eggs> {
            override fun onDataLoaded(data: List<Eggs>) {
                callback.onDataLoaded(data)
                refreshLocalDataSource(referenceIds[0], data)
            }

            override fun onEmptyData() {

            }

            override fun onError(message: String) {

            }
        }, referenceIds[0])
    }

    private fun getBatchFromRemoteDataSource(batchId: String, callback: BaseDataSource.DataLoadCallback<Eggs>) {
        mEggsRemoteDataSource.getAll(batchId, object : BaseDataSource.DataLoadCallback<Eggs> {
            override fun onDataLoaded(data: List<Eggs>) {
                refreshLocalDataSource(batchId, data)
                if (isCacheAvailable(batchId))
                    callback.onDataLoaded(ArrayList(mCachedEggs!![batchId]!!.values))
                mCacheIsDirty = false
            }

            override fun onEmptyData() {
                callback.onEmptyData()
            }

            override fun onError(message: String) {
                callback.onError(message)
            }

        })
    }

    private fun refreshLocalDataSource(batchId: String, eggs: List<Eggs>) {
        mEggsLocalDataSource.deleteAllData(batchId)
        for (egg in eggs) {
            mEggsLocalDataSource.saveData(egg)
        }
        refreshCache(eggs)
    }

    private fun refreshCache(eggs: List<Eggs>) {
        for (egg in eggs) {
            if (isCacheAvailable(egg.batchId)) {
                mCachedEggs!![egg.batchId]!!.put(egg.id, egg)
            }
        }
        mCacheIsDirty = false
    }

    override fun getTotalEggs(batchId: String, callback: EggsDataSource.OnEggsTotal) {
        mEggsLocalDataSource.getTotalEggs(batchId, object : EggsDataSource.OnEggsTotal {
            override fun onTotal(total: EggCount) {
                callback.onTotal(total)
            }
        })
    }

    fun setDataChangedListener(listener: EggsDataSource.DataUpdatedListener) {
        mListener = listener
    }

    private fun isCacheAvailable(batchId: String): Boolean {
        if (mCachedEggs == null) {
            mCachedEggs = LinkedHashMap()
            mCachedEggs!![batchId] = LinkedHashMap()
        } else if (mCachedEggs!![batchId] == null) {
            mCachedEggs!![batchId] = LinkedHashMap()
        }
        return true
    }

    companion object {

        private var INSTANCE: EggsRepository? = null

        fun getInstance(eggsRemoteDataSource: EggsDataSource, eggsLocalDataSource: EggsDataSource): EggsRepository {
            if (INSTANCE == null)
                INSTANCE = EggsRepository(eggsRemoteDataSource, eggsLocalDataSource)
            return INSTANCE as EggsRepository
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}
