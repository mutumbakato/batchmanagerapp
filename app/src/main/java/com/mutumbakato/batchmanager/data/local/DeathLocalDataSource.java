package com.mutumbakato.batchmanager.data.local;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.mutumbakato.batchmanager.data.DeathDataSource;
import com.mutumbakato.batchmanager.data.local.dao.DeathDao;
import com.mutumbakato.batchmanager.data.models.Death;
import com.mutumbakato.batchmanager.data.sync.Sync;
import com.mutumbakato.batchmanager.data.sync.Syncable;
import com.mutumbakato.batchmanager.utils.executors.AppExecutors;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;


public class DeathLocalDataSource implements DeathDataSource, Syncable<Death> {

    private static final String TAG = DeathLocalDataSource.class.getSimpleName();
    private static volatile DeathLocalDataSource INSTANCE;
    private DeathDao mDeathDao;
    private AppExecutors mAppExecutors;
//    private DeathRepository repository;

    private DeathLocalDataSource(AppExecutors appExecutors, DeathDao deathDao) {
        mAppExecutors = appExecutors;
        mDeathDao = deathDao;
//        repository = DeathRepository.getInstance(DeathRemoteDataSource.getInstance(), this);
    }

    public static DeathLocalDataSource getInstance(AppExecutors appExecutors, DeathDao deathDao) {
        if (INSTANCE == null)
            INSTANCE = new DeathLocalDataSource(appExecutors, deathDao);
        return INSTANCE;
    }

    @Override
    public void getAll(@NonNull final String batchId, @NonNull final DataLoadCallback<Death> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final List<Death> deaths = mDeathDao.getDeath(batchId);
            mAppExecutors.mainThread().execute(() -> {
                if (!deaths.isEmpty())
                    callback.onDataLoaded(deaths);
                else
                    callback.onEmptyData();
            });
        });
    }

    @Override
    public void getOne(@NonNull final String id, @NonNull final String batchId, @NonNull final DataItemCallback<Death> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final Death death = mDeathDao.getDeathById(id);
            mAppExecutors.mainThread().execute(() -> {
                if (death != null)
                    callback.onDataItemLoaded(death);
                else
                    callback.onDataItemNotAvailable();
            });
        });
    }

    @Override
    public void saveData(@NonNull final Death data) {
        mAppExecutors.diskIO().execute(() -> mDeathDao.insertDeath(data));
        sync();
    }

    @Override
    public void updateData(@NonNull final Death data) {
        mAppExecutors.diskIO().execute(() -> mDeathDao.updateDeath(data));
        sync();
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllData(@NonNull final String batchId) {
        mAppExecutors.diskIO().execute(() -> mDeathDao.deleteDeath(batchId));
    }

    @Override
    public void deleteDataItem(@NonNull final Death item, @NonNull String batchId) {
        mAppExecutors.diskIO().execute(() -> mDeathDao.deleteDeathById(item.getId()));
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void observeData(DataLoadCallback<Death> callback, String... referenceIds) {

    }

    @Override
    public void getMortalityRate(final String batchId, final OnMortalityRate callback) {

        mAppExecutors.diskIO().execute(() -> {
            final int rate = mDeathDao.mortalityRate(batchId);
            mAppExecutors.mainThread().execute(() ->
                    callback.onMortalityLoaded(rate));
        });

    }

    @Override
    public String getTable() {
        return Death.TABLE_NAME;
    }

    @Override
    public List<Death> getNew() {
        return mDeathDao.getNew();
    }

    @Override
    public List<Death> getUpdated() {
        return mDeathDao.getUpdated();
    }

    @Override
    public List<Death> getLocalData() {
        return mDeathDao.getLocal();
    }

    @Override
    public List<Long> getTrash() {
        return mDeathDao.getTrash();
    }

    @Override
    public long getLastServerId() {
        return mDeathDao.getLastServerId();
    }

    @Override
    public void handleNewData(JSONArray newData) {
        Gson gson = new Gson();
        for (int n = 0; n < newData.length(); n++) {
            try {
                Death deaths = gson.fromJson(newData.get(n).toString(), Death.class);
                Death newDeaths = new Death(deaths.getId(), deaths.getBatchId(), deaths.getDate(), deaths.getCount(),
                        deaths.getComment(), deaths.getServerId(), deaths.getStatus(), deaths.getLastModified());
                mDeathDao.insertDeath(newDeaths);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleModified(JSONArray modified) {
        Gson gson = new Gson();
        for (int n = 0; n < modified.length(); n++) {
            try {
                Death death = gson.fromJson(modified.get(n).toString(), Death.class);
                mDeathDao.updateDeath(death);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSynced(JSONArray synced) {
        for (int n = 0; n < synced.length(); n++) {
            try {
                String id = synced.getJSONObject(n).getString("id");
                long serverId = synced.getJSONObject(n).getLong("serverId");
                long lastMod = synced.getJSONObject(n).getLong("lastModified");

                Death deaths = mDeathDao.getDeathById(id);
                Death syncedDeath = new Death(deaths.getId(), deaths.getBatchId(), deaths.getDate(), deaths.getCount(),
                        deaths.getComment(), serverId, deaths.getStatus(), lastMod);
                mDeathDao.updateDeath(syncedDeath);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleUpdates(JSONArray updates) {
        for (int n = 0; n < updates.length(); n++) {
            try {
                String id = updates.getJSONObject(n).getString("id");
                Long last_mod = updates.getJSONObject(n).getLong("lastModified");
                Death deaths = mDeathDao.getDeathById(id);
                Death newDeath = new Death(deaths.getId(), deaths.getBatchId(), deaths.getDate(), deaths.getCount(),
                        deaths.getComment(), deaths.getServerId(), deaths.getStatus(), last_mod);
                mDeathDao.updateDeath(newDeath);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleTrash(String[] trash) {
        for (String aTrash : trash) {
            mDeathDao.deleteDeathById(aTrash);
        }
        mDeathDao.clearTrash();
    }

    @Override
    public void notifyRepository() {
//        repository.clearCache();
    }

    private void sync() {
        Sync.getInstance().sync(this, false);
    }
}
