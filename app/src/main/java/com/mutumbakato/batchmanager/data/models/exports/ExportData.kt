package com.mutumbakato.batchmanager.data.models.exports

import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.utils.DateUtils

data class ExportData constructor(

        val deaths: List<DeathExport> = ArrayList(),
        val feeds: List<FeedsExport> = ArrayList(),
        val weight: List<WeightExport> = ArrayList(),
        val water: List<WaterExport> = ArrayList(),
        val expenses: List<Expense> = ArrayList(),
        val sales: List<Sale> = ArrayList(),
        val eggs: List<EggsExport> = ArrayList(),
        val batch: Batch? = null) {

    private val weeks: Int
        get() = timeline.keys.size - 2

    private val lastWeek
        get() = timeline[weeks]

    val lastDay
        get() = weeks * 7 + (lastWeek?.size ?: 0 - 1)

    val layingDay
        get() = 20 * 7

    val isLaying
        get() = (batch?.type == "Layers" && weeks >= 20)

    private val timeline
        get() = DateUtils.getTimeLine(batch?.date!!, batch.closeDate)

}