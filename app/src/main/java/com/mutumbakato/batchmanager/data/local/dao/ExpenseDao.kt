package com.mutumbakato.batchmanager.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mutumbakato.batchmanager.data.models.Expense

@Dao
interface ExpenseDao {

    /**
     * Select all expenses from the expenses table.
     * @return all expenses.
     */
    @get:Query("SELECT * FROM expenses WHERE last_modified != 0 AND server_id != 0 AND status !='trash'")
    val local: List<Expense>

    /**
     * Select new expenses
     * @return expenses that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM expenses WHERE server_id = 0")
    val new: List<Expense>

    /**
     * Select locally updated expenses since the last synchronisation
     * @return updated expenses  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM expenses WHERE server_id != 0 AND last_modified = 0")
    val updated: List<Expense>

    /**
     * Select the last server_id from the server
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM expenses ORDER BY server_id DESC LIMIT 0,1")
    val lastServerId: Long

    /**
     * Select the last server_id from the server
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM expenses ORDER BY server_id DESC")
    val serverIds: List<Long>

    /**
     * Select expenses deleted from the locally
     * @return server ids of expenses where status = trash ;
     */
    @get:Query("SELECT server_id FROM expenses WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * Select all expenses from the expenses table.
     * @return all expenses.
     */
    @Query("SELECT * FROM expenses WHERE batch_id = :batchId AND status != 'trash' ORDER BY date DESC")
    fun listExpenses(batchId: String): List<Expense>


    @Query("SELECT * FROM expenses WHERE farm_id = :farmId AND status != 'trash' ORDER BY date DESC")
    fun listExpensesByFarm(farmId: String): List<Expense>

    /**
     * Expenses by farm id
     */
    @Query("SELECT * FROM expenses WHERE farm_id= :farmId AND date BETWEEN :start and :end")
    fun listFarmExpenses(farmId: String, start: String, end: String): List<Expense>

    /**
     * Expenses by farm id
     */
    @Query("SELECT * FROM expenses WHERE farm_id = :farmId AND status != 'trash' ORDER BY date DESC")
    fun listFarmExpenses(farmId: String): LiveData<List<Expense>>

    @Query("SELECT * FROM expenses WHERE (farm_id = :farmId OR batch_id = :farmId) AND date BETWEEN :from AND :to ORDER BY date DESC")
    fun filterFarmExpenses(farmId: String, from: String, to: String): LiveData<List<Expense>>

    @Query("SELECT STRFTIME('%Y-%m',date) AS month FROM expenses WHERE farm_id=:farmId GROUP BY month")
    fun listExpensesMonths(farmId: String): LiveData<List<String>>

    /**
     * Expenses by farm id
     */
    @Query("SELECT * FROM expenses WHERE batch_id= :batchId ")
    fun listBatchExpenses(batchId: String): LiveData<List<Expense>>

    /**
     * Select a expense by name.
     *
     * @param expenseId the expense name.
     * @return the expense with expenseId.
     */
    @Query("SELECT * FROM expenses WHERE _id = :expenseId AND status != 'trash' ")
    fun getExpenseById(expenseId: String): Expense

    @Query("SELECT * FROM expenses WHERE _id = :expenseId AND status != 'trash' ")
    fun getLiveExpenseById(expenseId: String): LiveData<Expense>

    /**
     * Insert a expense in the database. If the expense already exists, replace it.
     *
     * @param expense the expense to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertExpense(expense: Expense): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllExpenses(expenses: List<Expense>)

    /**
     * Update a expense.
     *
     * @param expense expense to be updated
     * @return the number of expenses updated. This should always be 1.
     */
    @Update
    fun updateExpense(expense: Expense): Int

    /**
     * Delete a expense by name.
     */
    @Query("UPDATE expenses SET status = 'trash' WHERE _id = :expenseId")
    fun deleteExpenseById(expenseId: String)

    /**
     * Delete all expenses.
     */
    @Query("DELETE FROM expenses WHERE batch_id = :batchId")
    fun deleteExpenses(batchId: String)

    @Query("DELETE FROM expenses WHERE farm_id = :farmId")
    fun deleteFarmExpenses(farmId: String)

    /**
     * @return number of items deleted
     */
    @Query("DELETE FROM expenses WHERE status = 'trash'")
    fun clearTrash(): Int

    @Query("SELECT SUM(amount) AS total FROM expenses WHERE  batch_id = :batchId AND status != 'trash' GROUP BY batch_id")
    fun getTotalExpenses(batchId: String): Float
}
