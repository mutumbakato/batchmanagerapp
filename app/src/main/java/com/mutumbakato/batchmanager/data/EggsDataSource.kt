package com.mutumbakato.batchmanager.data


import com.mutumbakato.batchmanager.data.models.EggCount
import com.mutumbakato.batchmanager.data.models.Eggs

interface EggsDataSource : BaseDataSource<Eggs> {

    fun getTotalEggs(batchId: String, callback: OnEggsTotal)

    interface DataUpdatedListener {
        fun onDataUpdated()
    }

    interface OnEggsTotal {
        fun onTotal(total: EggCount)
    }

}

