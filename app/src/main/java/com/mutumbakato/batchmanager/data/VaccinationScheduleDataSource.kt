package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.VaccinationSchedule

interface VaccinationScheduleDataSource {

    fun getAllSchedules(batchId: String, callback: BaseDataSource.DataLoadCallback<VaccinationSchedule>)

    fun getSchedule(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<VaccinationSchedule>)

    fun saveSchedule(data: VaccinationSchedule)

    fun updateSchedule(data: VaccinationSchedule)

    fun refreshData()

    fun deleteAllSchedules(farmId: String)

    fun deleteSchedule(id: String, batchId: String)

    fun clearCache()

    fun notifyDataChanged()

    fun useSchedule(batchId: String, schedule: VaccinationSchedule)

    fun removeSchedule(batchId: String, scheduleId: String)

    fun observeSchedule(callback: BaseDataSource.DataLoadCallback<VaccinationSchedule>, userId: String)

    interface DataUpdatedListener {
        fun onDataUpdated()
    }

}
