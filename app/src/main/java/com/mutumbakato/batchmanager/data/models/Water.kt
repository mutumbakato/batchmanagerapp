package com.mutumbakato.batchmanager.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.mutumbakato.batchmanager.utils.DateUtils
import java.util.*

const val TABLE_WATER = "water"

@Entity(tableName = TABLE_WATER)
data class Water(@PrimaryKey @ColumnInfo(name = "_id") val id: String = UUID.randomUUID().toString(),
                 @ColumnInfo(name = "batch_id") val batchId: String = "0",
                 val date: String = "",
                 val quantity: Float = 0f,
                 @ColumnInfo(name = "batch_count") val batchCount: Int = 0,
                 val status: String = "normal",
                 @ColumnInfo(name = "last_modified") val latsModified: Long = 0,
                 @ColumnInfo(name = "created_at") val createdAt: String = DateUtils.now()) {

    val average: Float
        get() = quantity / batchCount
}