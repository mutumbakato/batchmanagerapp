package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update

import com.mutumbakato.batchmanager.data.models.Vaccination
import com.mutumbakato.batchmanager.data.models.VaccinationCheck

@Dao
interface VaccinationDao {

    /**
     * Select all vaccinations from the vaccination table.
     *
     * @return all vaccinations.
     */
    @get:Query("SELECT * FROM vaccination WHERE last_modified != 0 AND server_id != 0 AND status != 'trash'")
    val local: List<Vaccination>

    /**
     * Select new vaccination
     *
     * @return vaccinations that are not available on the server ie with server_id = 0 ;
     */
    @get:Query("SELECT * FROM vaccination WHERE server_id = 0")
    val new: List<Vaccination>

    /**
     * Select locally updated vaccinations since the last synchronisation
     *
     * @return updated vaccination  server_id != 0 and last_modified = 0 ;
     */
    @get:Query("SELECT * FROM vaccination WHERE server_id != 0 AND last_modified = 0 AND status != 'trash'")
    val updated: List<Vaccination>

    /**
     * Select the last server_id from the server
     *
     * @return last server_id ;
     */
    @get:Query("SELECT server_id FROM vaccination ORDER BY server_id DESC LIMIT 1")
    val lastServerId: Long

    /**
     * Select vaccinations deleted from the locally
     *
     * @return server ids of vaccinations where status = trash ;
     */
    @get:Query("SELECT server_id FROM vaccination WHERE status = 'trash' ")
    val trash: List<Long>

    /**
     * Select all vaccinations from the vaccinations table.
     *
     * @return all vaccinations.
     */
    @Query("SELECT * FROM vaccination WHERE schedule_id=:scheduleId AND status != 'trash' ORDER BY age ASC")
    fun listVaccinations(scheduleId: String): List<Vaccination>

    /**
     * Select all vaccinationChecks from the vaccination_check table.
     *
     * @return all vaccinationChecks.
     */
    @Query("SELECT * FROM vaccination_check WHERE batch_id=:batchId AND status != 'trash' ")
    fun getCheKList(batchId: String): List<VaccinationCheck>

    /**
     * Select a vaccination by name.
     *
     * @param vaccinationId the vaccination name.
     * @return the vaccination with vaccinationId.
     */
    @Query("SELECT * FROM vaccination WHERE _id = :vaccinationId AND status != 'trash' ")
    fun getVaccinationById(vaccinationId: String): Vaccination

    /**
     * Insert a vaccination in the database. If the vaccination already exists, replace it.
     *
     * @param vaccination the vaccination to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVaccination(vaccination: Vaccination)

    /**
     * Insert a vaccination in the database. If the vaccination already exists, replace it.
     *
     * @param check the vaccinationCheck to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addToComplete(check: VaccinationCheck)

    /**
     * Update a vaccination.
     *
     * @param vaccination vaccination to be updated
     * @return the number of vaccinations updated. This should always be 1.
     */
    @Update
    fun updateVaccination(vaccination: Vaccination)

    /**
     * Delete a single is
     *
     * @param vaccinationId of the vaccination to deleteCategory
     */

    @Query("UPDATE vaccination SET status = 'trash' WHERE _id = :vaccinationId")
    fun deleteVaccination(vaccinationId: String)

    /**
     * Delete a single is
     *
     * @param vaccinationId of the vaccination to deleteCategory
     */

    @Query("UPDATE vaccination_check SET status = 'trash' WHERE vaccination_id = :vaccinationId")
    fun removeFromComplete(vaccinationId: String)

    /**
     * Delete all vaccinations.
     */
    @Query("DELETE FROM vaccination")
    fun deleteAllVaccinations()

    @Query("DELETE FROM vaccination_check WHERE batch_id =:batchId")
    fun deleteAllChecks(batchId: String)

    /**
     * Permanently deleteCategory all trashed entries
     */
    @Query("DELETE FROM vaccination WHERE status = 'trash'")
    fun clearTrash()

}

