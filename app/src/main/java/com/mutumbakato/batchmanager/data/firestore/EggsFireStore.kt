package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.EggsDataSource
import com.mutumbakato.batchmanager.data.models.Eggs
import java.util.*

/**
 * Created by cato on 3/26/18.
 */

class EggsFireStore : EggsDataSource {

    private val db = FirebaseFirestore.getInstance().collection("eggs")

    override fun getTotalEggs(batchId: String, callback: EggsDataSource.OnEggsTotal) {

    }

    override fun getAll(batchId: String, callback: BaseDataSource.DataLoadCallback<Eggs>) {
        db.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val eggs = ArrayList<Eggs>()
                for (data in task.result!!) {
                    eggs.add(data.toObject(Eggs::class.java))
                }
                if (eggs.size > 0)
                    callback.onDataLoaded(eggs)
                else
                    callback.onEmptyData()
            } else {
                callback.onEmptyData()
            }
        }
    }

    override fun getOne(id: String, batchId: String, callback: BaseDataSource.DataItemCallback<Eggs>) {
        db.document(id).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callback.onDataItemLoaded(task.result!!.toObject(Eggs::class.java)!!)
            } else {
                callback.onDataItemNotAvailable()
            }
        }
    }

    override fun saveData(data: Eggs) {
        db.document(data.id).set(data)
    }

    override fun updateData(data: Eggs) {
        db.document(data.id).set(data)
    }

    override fun refreshData() {
        //Implemented in the repository
    }

    override fun deleteAllData(batchId: String) {
        //Don't do it !!!
    }

    override fun deleteDataItem(item: Eggs, batchId: String) {
        db.document(item.id).delete()
    }

    override fun clearCache() {
        //Implemented in the repository
    }

    override fun notifyDataChanged() {

    }

    override fun observeData(callback: BaseDataSource.DataLoadCallback<Eggs>, vararg referenceIds: String) {
        db.whereEqualTo("batchId", referenceIds[0]).addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val eggs = ArrayList<Eggs>()
                for (data in snapshots) {
                    eggs.add(data.toObject(Eggs::class.java))
                }
                callback.onDataLoaded(eggs)
            }
        }
    }
}
