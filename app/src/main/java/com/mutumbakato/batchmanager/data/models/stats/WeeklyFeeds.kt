package com.mutumbakato.batchmanager.data.models.stats

data class WeeklyFeeds(val days: Int, val totalFeeds: Float, val feedsPerBird: Float)