package com.mutumbakato.batchmanager.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mutumbakato.batchmanager.data.models.Weight

@Dao
interface WeightDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(weight: Weight)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(weights: List<Weight>)

    @Update
    fun update(weight: Weight)

    @Query("SELECT * FROM weight WHERE batch_id = :batchId ")
    fun listWeight(batchId: String): LiveData<List<Weight>>

    @Query("SELECT * FROM weight WHERE _id = :id")
    fun getWeightById(id: String): LiveData<Weight>

    @Delete
    fun deleteWeight(weight: Weight)

    @Query("DELETE FROM weight WHERE batch_id = :batchId")
    fun deleteAllWeight(batchId: String)
}