package com.mutumbakato.batchmanager.data.firestore

import com.google.errorprone.annotations.DoNotCall
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Source
import com.mutumbakato.batchmanager.data.BaseDataSource
import com.mutumbakato.batchmanager.data.BatchDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchRecords
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import java.util.*

/**
 * Created by cato on 3/25/18.
 */
class BatchFireStore : BatchDataSource {

    //private static final String TAG = BatchFireStore.class.getSimpleName();
    private val batchCollection = FirebaseFirestore.getInstance().collection("batches")
    private val batchUserCollection = FirebaseFirestore.getInstance().collection("batch_users")
    private val salesCollection = FirebaseFirestore.getInstance().collection("sales")
    private val expensesCollection = FirebaseFirestore.getInstance().collection("expenses")
    private val eggsCollection = FirebaseFirestore.getInstance().collection("eggs")
    private val feedsCollection = FirebaseFirestore.getInstance().collection("feeds")
    private val deathsCollection = FirebaseFirestore.getInstance().collection("deaths")
    private val standardsCollection = FirebaseFirestore.getInstance().collection("standards")

    override fun getAllBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        val bb = HashMap<String, Batch>()
        batchCollection.whereEqualTo("farmId", farmId)
                .get(Source.SERVER)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        for (data in task.result!!) {
                            val b = data.toObject(Batch::class.java)
                            bb[b.id] = b
                        }
                        if (bb.values.isNotEmpty()) {
                            callback.onBatchesLoaded(ArrayList(bb.values))
                        } else {
                            callback.onDataNotAvailable()
                        }
                    }
                }
    }

    override fun observeMyBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        batchCollection.whereEqualTo("farmId", farmId).addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val batches = ArrayList(snapshots.toObjects(Batch::class.java))
                callback.onBatchesLoaded(batches)
            }
        }
    }

    override fun observeOtherBatches(userId: String, callback: BatchDataSource.LoadBatchesCallback) {
        batchCollection.whereArrayContains("users", PreferenceUtils.userId)
                .addSnapshotListener { snapshots, e ->
                    if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                        val batches = ArrayList(snapshots.toObjects(Batch::class.java))
                        if (batches.size > 0) {
                            callback.onBatchesLoaded(batches)
                        } else {
                            callback.onBatchesLoaded(ArrayList())
                        }
                    }
                }
    }

    override fun getArchivedBatches(farmId: String, callback: BatchDataSource.LoadBatchesCallback) {
        batchCollection.whereEqualTo("farmId", farmId).whereEqualTo("status", "closed").get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val batches = ArrayList<Batch>()
                for (data in task.result!!) {
                    batches.add(data.toObject(Batch::class.java))
                }
                if (batches.size > 0) {
                    callback.onBatchesLoaded(batches)

                } else {
                    callback.onDataNotAvailable()
                }
            } else {
                callback.onDataNotAvailable()
            }
        }
    }

    override fun getBatch(batchId: String, callback: BatchDataSource.GetBatchCallback) {
        batchCollection.document(batchId).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callback.onBatchLoaded(task.result!!.toObject(Batch::class.java)!!)
            } else {
                callback.onDataNotAvailable()
            }
        }
    }

    override fun saveBatch(batch: Batch, callback: BatchDataSource.CreateBatchCallback) {
        batchCollection.document(batch.id).set(batch).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callback.onBatchCreated(batch.id)
            }
        }
    }

    override fun updateBatch(batch: Batch) {
        batchCollection.document(batch.id).set(batch)
    }

    override fun closeBatch(batchId: String) {
        val value = LinkedHashMap<String, Any>()
        value["status"] = "closed"
        batchCollection.document(batchId).update(value)
    }

    override fun addUser(users: BatchUser, callback: BaseDataSource.DataItemCallback<String>) {
        batchUserCollection.document(users.id).set(users).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                batchCollection.document(users.batchId).update("users", FieldValue.arrayUnion(users.userId))
                callback.onDataItemLoaded("")
            } else {
                callback.onError("Failed to add user")
            }
        }
    }

    override fun removeUser(user: BatchUser, callback: BaseDataSource.DataItemCallback<String>) {
        batchCollection.document(user.batchId).update("users", FieldValue.arrayRemove(user.userId))
        batchUserCollection.document(user.id).delete().addOnCompleteListener { task1 -> callback.onDataItemLoaded("") }
    }

    override fun getBatchUsers(batchId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>) {
        batchUserCollection
                .whereEqualTo("batchId", batchId)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val users = ArrayList<BatchUser>()
                        for (data in task.result!!) {
                            users.add(data.toObject(BatchUser::class.java))
                        }
                        if (users.size > 0) {
                            callback.onDataLoaded(users)
                        } else {
                            callback.onEmptyData()
                        }
                    } else {
                        callback.onError("Users not found")
                    }
                }
    }

    override fun getBatchUser(batchId: String, userId: String, callback: BaseDataSource.DataItemCallback<BatchUser>) {
        batchUserCollection
                .whereEqualTo("batchId", batchId)
                .whereEqualTo("userId", userId)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        if (task.result!!.documents.size > 0)
                            callback.onDataItemLoaded(task.result!!.documents[0].toObject(BatchUser::class.java)!!)
                        else
                            callback.onDataItemNotAvailable()
                    } else {
                        callback.onError("User not found")
                    }
                }
    }

    override fun updateBatchUser(user: BatchUser, callback: BaseDataSource.DataItemCallback<BatchUser>) {
        batchUserCollection.document(user.id).set(user)
    }

    override fun observeBatchesUsers(batchId: String, callback: BaseDataSource.DataLoadCallback<BatchUser>) {
        batchUserCollection.whereEqualTo("batchId", batchId).addSnapshotListener { snapshots, e ->
            if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                val users = ArrayList(snapshots.toObjects(BatchUser::class.java))
                if (users.size > 0)
                    callback.onDataLoaded(users)
                else
                    callback.onEmptyData()
            }
        }
    }

    override fun deleteAllBatchUsers(batchId: String) {
        //Don't do this here
    }

    override fun getTodayRecords(onResults: (records: BatchRecords) -> Unit) {
    }

    override fun refreshBatches() {

    }

    @DoNotCall
    override fun deleteAllMyBatches(farmId: String) {
        //WARNING Don't do this
    }

    @DoNotCall
    override fun deleteAllOtherBatches(farmId: String) {
        //WARNING Don't do this
    }

    override fun deleteBatch(batchId: String) {
        batchCollection.document(batchId).delete()
        batchCollection.document(batchId).collection("vaccination").get().addOnCompleteListener { task ->
            for (data in task.result!!) {
                data.reference.delete()
            }
        }
        batchCollection.document(batchId).collection("logs").get().addOnCompleteListener { task ->
            for (data in task.result!!) {
                data.reference.delete()
            }
        }

        salesCollection.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            for (data in task.result!!) {
                data.reference.delete()
            }
        }
        expensesCollection.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            for (data in task.result!!) {
                data.reference.delete()
            }
        }
        deathsCollection.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            for (data in task.result!!) {
                data.reference.delete()
            }
        }
        eggsCollection.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            for (data in task.result!!) {
                data.reference.delete()
            }
        }
        feedsCollection.whereEqualTo("batchId", batchId).get().addOnCompleteListener { task ->
            for (data in task.result!!) {
                data.reference.delete()
            }
        }
    }

    override fun clearMemoryCache(thenNotify: Boolean) {
        //Implemented in the repository
    }

    override fun notifyDataChanged() {

    }

    fun getStandards(){}
}
