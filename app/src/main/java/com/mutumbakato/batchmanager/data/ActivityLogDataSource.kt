package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.ActivityLog

interface ActivityLogDataSource {

    fun log(log: ActivityLog)

    fun getLogs(farmId: String, callBack: LogsCallBack)

    fun setListener(farmId: String, callBack: LogsCallBack)

    fun loadMore(farmId: String, lastDate: String, callBack: LogsCallBack)

    interface LogsCallBack {
        fun onLoad(logs: List<ActivityLog>, newCount: Int)

        fun onError(message: String)
    }
}
