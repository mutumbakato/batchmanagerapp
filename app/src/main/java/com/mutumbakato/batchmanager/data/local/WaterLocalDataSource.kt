package com.mutumbakato.batchmanager.data.local

import androidx.lifecycle.LiveData
import com.mutumbakato.batchmanager.data.local.dao.WaterDao
import com.mutumbakato.batchmanager.data.models.Water
import com.mutumbakato.batchmanager.utils.executors.AppExecutors

class WaterLocalDataSource constructor(val dao: WaterDao, val executors: AppExecutors) {

    fun insert(water: Water, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.insert(water)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun insertAll(waters: List<Water>, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.insertAll(waters)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun update(water: Water, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.update(water)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun listWater(batchId: String): LiveData<List<Water>> {
        return dao.listWater(batchId)
    }

    fun getWaterById(id: String): LiveData<Water> {
        return dao.getWaterById(id)
    }

    fun deleteWater(water: Water, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.deleteWater(water)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun deletAllWater(batchId: String) {
        executors.diskIO().execute {
            dao.deleteAllWater(batchId)
        }
    }
}