package com.mutumbakato.batchmanager.data.remote;

import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.DeathDataSource;
import com.mutumbakato.batchmanager.data.models.Death;

public class DeathRemoteDataSource implements DeathDataSource {
    private static DeathRemoteDataSource INSTANCE = null;

    private DeathRemoteDataSource() {

    }

    private static void addDeath(String batch, String date, int count, String comment) {
    }

    public static DeathRemoteDataSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new DeathRemoteDataSource();
        return INSTANCE;
    }

    @Override
    public void getAll(@NonNull String batchId, @NonNull final DataLoadCallback<Death> callback) {
        callback.onEmptyData();
    }

    @Override
    public void getOne(@NonNull final String id, @NonNull final String batchId, @NonNull final DataItemCallback<Death> callback) {
    }

    @Override
    public void saveData(@NonNull Death data) {
    }

    @Override
    public void updateData(@NonNull Death data) {
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllData(@NonNull String batchI) {
    }

    @Override
    public void deleteDataItem(@NonNull Death item, @NonNull String batchId) {
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void observeData(DataLoadCallback<Death> callback, String... referenceIds) {

    }

    @Override
    public void getMortalityRate(String batchId, OnMortalityRate callback) {
        //Mortality rate is only provided by local data source
    }
}
