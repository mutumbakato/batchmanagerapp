package com.mutumbakato.batchmanager.data.models;

import androidx.annotation.NonNull;
import androidx.room.Ignore;

import com.mutumbakato.batchmanager.utils.DateUtils;

public class EntryItem {

    public String x;
    public float y;

    public EntryItem() {

    }

    @Ignore
    public EntryItem(float y, String x) {
        this.y = y;
        this.x = x;
    }

    @NonNull
    @Override
    public String toString() {
        return "{x:" + x + ", y:" + y + "}";
    }

    public int getWeek(String dob) {
        return DateUtils.INSTANCE.ageWeeksFromDate(dob, x);
    }
}
