package com.mutumbakato.batchmanager.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.mutumbakato.batchmanager.data.models.Water

@Dao
interface WaterDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(water: Water)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(water: List<Water>)

    @Update
    fun update(water: Water)

    @Query("SELECT * FROM water WHERE batch_id = :batchId ")
    fun listWater(batchId: String): LiveData<List<Water>>

    @Query("SELECT * FROM water WHERE _id = :id")
    fun getWaterById(id: String): LiveData<Water>

    @Delete
    fun deleteWater(water: Water)

    @Query("DELETE FROM water WHERE batch_id = :batchId")
    fun deleteAllWater(batchId: String)

}