package com.mutumbakato.batchmanager.data.firestore

import android.location.Location
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.pixplicity.easyprefs.library.Prefs
import java.util.*

/**
 * Created by cato on 3/28/18.
 */
class UserFireStore : UserDataSource {

    private val auth: FirebaseAuth = FirebaseAuth.getInstance()

    private val db = FirebaseFirestore.getInstance().collection("users")

    override fun login(email: String, password: String, callBack: UserDataSource.UserCallBack) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val u = auth.currentUser
                if (u != null) {
                    getUserByEmail(email, object : UserDataSource.UserCallBack {
                        override fun onSuccess(user: User?) {
                            //Set the user Id in preference
                            Prefs.putString(PreferenceUtils.USER_ID, user!!.id)
                            callBack.onSuccess(user)
                        }

                        override fun onError(message: String) {

                        }
                    })
                }
            } else {
                callBack.onError(task.exception!!.message!!)
            }
        }
    }

    override fun register(user: User, callBack: UserDataSource.UserCallBack) {
        auth.createUserWithEmailAndPassword(user.email, user.password).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val u = auth.currentUser
                val profile = UserProfileChangeRequest.Builder()
                        .setDisplayName(user.name)
                        .build()
                assert(u != null)
                u!!.updateProfile(profile)
                        .addOnCompleteListener { t ->
                            if (t.isSuccessful) {
                                val newUser = User(Objects.requireNonNull<FirebaseUser>(auth.currentUser), user.id)
                                //Set the user is on Preferences
                                Prefs.putString(PreferenceUtils.USER_ID, newUser.id)
                                db.document(u.uid).set(newUser)
                                callBack.onSuccess(newUser)
                            } else {
                                callBack.onSuccess(User(Objects.requireNonNull<FirebaseUser>(auth.currentUser), user.id))
                            }
                        }
            } else {
                callBack.onError(Objects.requireNonNull<Exception>(task.exception).message!!)
            }
        }
    }

    override fun logOut(callBack: UserDataSource.UserCallBack) {
        Prefs.clear()
        callBack.onSuccess(User(Objects.requireNonNull<FirebaseUser>(auth.currentUser), null))
        auth.signOut()
    }

    override fun resetPassword(email: String, callBack: UserDataSource.PasswordCallBack) {
        auth.sendPasswordResetEmail(email).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callBack.onSuccess("A reset link has been sent to $email")
            } else {
                callBack.onError("Error sending reset password.")
            }
        }
    }

    override fun getUserById(id: String, callBack: UserDataSource.UserCallBack) {
        db.whereEqualTo("id", id).limit(1).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val user = Objects.requireNonNull<QuerySnapshot>(task.result).toObjects(User::class.java)
                if (user.size > 0)
                    callBack.onSuccess(user[0])
                else
                    callBack.onError("No user found")
            } else {
                callBack.onError("No user found with!")
            }
        }
    }

    override fun getUserByEmail(email: String, callBack: UserDataSource.UserCallBack) {
        db.whereEqualTo("email", email).get().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val users = Objects.requireNonNull<QuerySnapshot>(task.result).toObjects(User::class.java)
                if (users.size > 0) {
                    callBack.onSuccess(users[0])
                } else {
                    callBack.onError("No user found!")
                }
            } else {
                callBack.onError("No user found!")
            }
        }
    }

    override fun updateUser(user: User, callBack: UserDataSource.UserCallBack) {
        val u = auth.currentUser!!
        u.updateEmail(user.email)
        val profile = UserProfileChangeRequest.Builder()
                .setDisplayName(user.name)
                .build()
        u.updateProfile(profile).addOnCompleteListener { task ->
            if (task.isSuccessful) {
                callBack.onSuccess(User(auth.currentUser!!, null))
            } else {
                callBack.onError("Failed to update user, please try angain")
            }
        }
    }

    override fun deleteAccount(user: User, callBack: UserDataSource.UserCallBack) {

    }

    override fun setLocation(uid: String, location: Location) {
        db.document(uid).update(mapOf("location" to location))
    }
}
