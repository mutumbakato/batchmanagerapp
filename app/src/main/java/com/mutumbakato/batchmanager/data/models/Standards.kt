package com.mutumbakato.batchmanager.data.models

data class Standards(val id: String, val category: String, val type: String, val values: List<Float>)