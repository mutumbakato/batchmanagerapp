package com.mutumbakato.batchmanager.data.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.firebase.firestore.Exclude;
import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.UUID;

import static com.mutumbakato.batchmanager.data.models.Batch.TABLE_NAME;

/**
 * Immutable model class for a Batch.
 */
@Entity(tableName = TABLE_NAME)
public class Batch {

    public static final String TABLE_NAME = "batches";

    @NonNull
    @Expose
    @PrimaryKey
    @ColumnInfo(name = "_id")
    private String id = UUID.randomUUID().toString();

    @Exclude
    @Expose
    @ColumnInfo(name = "server_id")
    private long serverId;

    @Expose
    @Nullable
    @ColumnInfo(name = "date")
    private String date;

    @Expose
    @ColumnInfo(name = "name")
    private String name;

    @Expose
    @Nullable
    @ColumnInfo(name = "supplier")
    private String supplier;

    @Expose
    @ColumnInfo(name = "quantity")
    private int quantity;

    @Expose
    @ColumnInfo(name = "rate")
    private double rate;

    @Expose
    @ColumnInfo(name = "type")
    private String type;

    @Expose
    @ColumnInfo(name = "age")
    private String age;

    @Expose
    @ColumnInfo(name = "schedule")
    private String vaccinationSchedule;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Expose
    @ColumnInfo(name = "status")
    private String status;

    private String userId;

    private String farmId;

    @ColumnInfo(name = "close_date")
    private String closeDate;

    @Ignore
    private String farmName;

    @Ignore
    private List<String> users;

    /**
     * Use this constructor to specify a completed Batch if the Batch already has an name (copy of
     * another Batch).
     */
    @Ignore
    public Batch() {

    }

    public Batch(@NonNull String id, long serverId, @Nullable String date, String name, @Nullable String supplier,
                 int quantity, double rate, String type, String age, String vaccinationSchedule, String status,
                 long lastModified, String userId, String farmId) {
        this.id = id;
        this.date = date;
        this.supplier = supplier;
        this.quantity = quantity;
        this.lastModified = lastModified;
        this.serverId = serverId;
        this.status = status;
        this.rate = rate;
        this.type = type;
        this.age = age;
        this.name = name;
        this.vaccinationSchedule = vaccinationSchedule;
        this.userId = userId;
        this.farmId = farmId;
    }

    @Ignore
    public Batch(@NonNull String date, String name, String supplier, int quantity,
                 double rate, String type, String age, String userId, String farmId) {
        this(UUID.randomUUID().toString(), 0, date, name, supplier, quantity,
                rate, type, age, "", "normal", 0, userId, farmId);
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @Nullable
    public String getDate() {
        return date;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getRate() {
        return rate;
    }

    public String getType() {
        return type;
    }

    @Nullable
    public String getSupplier() {
        return supplier;
    }

    @Override
    public String toString() {
        return "Batch for Date " + date;
    }

    public String getName() {
        return name;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVaccinationSchedule() {
        return vaccinationSchedule;
    }

    public String getAge() {
        return age;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFarmId() {
        return farmId;
    }

    public void setFarmId(String farmId) {
        this.farmId = farmId;
    }

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    private boolean isClosed() {
        return status.equals("closed");
    }
}
