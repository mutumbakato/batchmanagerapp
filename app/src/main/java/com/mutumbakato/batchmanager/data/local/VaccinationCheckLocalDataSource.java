package com.mutumbakato.batchmanager.data.local;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.mutumbakato.batchmanager.data.local.dao.VaccinationCheckDao;
import com.mutumbakato.batchmanager.data.models.VaccinationCheck;
import com.mutumbakato.batchmanager.data.sync.Syncable;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

/**
 * Created by cato on 11/2/17.
 */

public class VaccinationCheckLocalDataSource implements Syncable<VaccinationCheck> {

    private static volatile VaccinationCheckLocalDataSource INSTANCE;

    private VaccinationCheckDao vaccinationCheckDao;

    // Prevent direct instantiation.
    private VaccinationCheckLocalDataSource(@NonNull VaccinationCheckDao vaccinationCheckDao) {
        this.vaccinationCheckDao = vaccinationCheckDao;
    }

    public static VaccinationCheckLocalDataSource getInstance(@NonNull VaccinationCheckDao vaccinationDao) {
        if (INSTANCE == null) {
            synchronized (VaccinationLocalDataSource.class) {
                if (INSTANCE == null) {
                    INSTANCE = new VaccinationCheckLocalDataSource(vaccinationDao);
                }
            }
        }
        return INSTANCE;
    }

    @Override
    public String getTable() {
        return VaccinationCheck.TABLE_NAME;
    }

    @Override
    public List<VaccinationCheck> getNew() {
        return vaccinationCheckDao.getNew();
    }

    @Override
    public List<VaccinationCheck> getUpdated() {
        return vaccinationCheckDao.getUpdated();
    }

    @Override
    public List<VaccinationCheck> getLocalData() {
        return vaccinationCheckDao.getLocal();
    }

    @Override
    public List<Long> getTrash() {
        return vaccinationCheckDao.getTrash();
    }

    @Override
    public long getLastServerId() {
        return vaccinationCheckDao.getLastServerId();
    }

    @Override
    public void handleNewData(JSONArray newData) {
        Gson gson = new Gson();
        for (int n = 0; n < newData.length(); n++) {
            try {
                VaccinationCheck check = gson.fromJson(newData.get(n).toString(),
                        VaccinationCheck.class);
                VaccinationCheck newCheck = new VaccinationCheck(check.getId(),
                        check.getBatchId(), check.getScheduleId(), check.getVaccinationId(),
                        check.getServerId(), check.getStatus(), check.getLastModified());
                vaccinationCheckDao.insertCheck(newCheck);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleModified(JSONArray modified) {
        Gson gson = new Gson();
        for (int n = 0; n < modified.length(); n++) {
            try {
                VaccinationCheck check = gson.fromJson(modified.get(n).toString(), VaccinationCheck.class);
                vaccinationCheckDao.updateCheck(check);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSynced(JSONArray synced) {
        for (int n = 0; n < synced.length(); n++) {
            try {
                String id = synced.getJSONObject(n).getString("id");
                long serverId = synced.getJSONObject(n).getLong("serverId");
                long lastMod = synced.getJSONObject(n).getLong("lastModified");
                VaccinationCheck check = vaccinationCheckDao.getChecksById(id);
                if (check == null)
                    return;
                VaccinationCheck syncedCheck = new VaccinationCheck(check.getId(), check.getBatchId(),
                        check.getScheduleId(), check.getVaccinationId(), serverId, check.getStatus(),
                        lastMod);
                vaccinationCheckDao.updateCheck(syncedCheck);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void handleUpdates(JSONArray updates) {
        for (int n = 0; n < updates.length(); n++) {
            try {
                String id = updates.getJSONObject(n).getString("id");
                Long last_mod = updates.getJSONObject(n).getLong("lastModified");
                VaccinationCheck check = vaccinationCheckDao.getChecksById(id);
                VaccinationCheck newCheck = new VaccinationCheck(check.getId(), check.getBatchId(),
                        check.getScheduleId(), check.getVaccinationId(), check.getServerId(),
                        check.getStatus(), last_mod);
                vaccinationCheckDao.updateCheck(newCheck);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleTrash(String[] trash) {
        for (String aTrash : trash) {
            vaccinationCheckDao.deleteCheckById(aTrash);
        }
        vaccinationCheckDao.clearTrash();
    }

    @Override
    public void notifyRepository() {

    }
}
