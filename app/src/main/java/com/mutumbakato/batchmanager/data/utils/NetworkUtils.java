package com.mutumbakato.batchmanager.data.utils;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


public class NetworkUtils {

    public boolean isInternetWorking() {
        boolean success = false;
        try {
            URL url = new URL("https://google.com");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setConnectTimeout(10000);
            connection.connect();
            success = connection.getResponseCode() == 200;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return success;
    }
}
