package com.mutumbakato.batchmanager.data.local;

import androidx.annotation.NonNull;

import com.google.gson.Gson;
import com.mutumbakato.batchmanager.data.FeedingDataSource;
import com.mutumbakato.batchmanager.data.local.dao.FeedingDao;
import com.mutumbakato.batchmanager.data.models.Feeding;
import com.mutumbakato.batchmanager.data.sync.Sync;
import com.mutumbakato.batchmanager.data.sync.Syncable;
import com.mutumbakato.batchmanager.utils.executors.AppExecutors;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;


public class FeedingLocalDataSource implements FeedingDataSource, Syncable<Feeding> {

    private static final String TAG = FeedingLocalDataSource.class.getSimpleName();
    private static volatile FeedingLocalDataSource INSTANCE;
    private FeedingDao mFeedingDao;
    private AppExecutors mAppExecutors;
//    private FeedingRepository repository;

    private FeedingLocalDataSource(AppExecutors appExecutors, FeedingDao feedingDao) {
        mAppExecutors = appExecutors;
        mFeedingDao = feedingDao;
//        repository = FeedingRepository.getInstance(FeedingRemoteDataSource.getInstance(), this);
    }

    public static FeedingLocalDataSource getInstance(AppExecutors appExecutors, FeedingDao feedingDao) {
        if (INSTANCE == null)
            INSTANCE = new FeedingLocalDataSource(appExecutors, feedingDao);
        return INSTANCE;
    }

    @Override
    public void getAll(@NonNull final String batchId, @NonNull final DataLoadCallback<Feeding> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final List<Feeding> feedings = mFeedingDao.getFeedings(batchId);
            mAppExecutors.mainThread().execute(() -> {
                if (!feedings.isEmpty())
                    callback.onDataLoaded(feedings);
                else
                    callback.onEmptyData();
            });
        });
    }

    @Override
    public void getOne(@NonNull final String id, @NonNull final String batchId, @NonNull final DataItemCallback<Feeding> callback) {
        mAppExecutors.diskIO().execute(() -> {
            final Feeding feeding = mFeedingDao.getFeedingById(id);
            mAppExecutors.mainThread().execute(() -> {
                if (feeding != null)
                    callback.onDataItemLoaded(feeding);
                else
                    callback.onDataItemNotAvailable();
            });
        });
    }

    @Override
    public void saveData(@NonNull final Feeding data) {
        mAppExecutors.diskIO().execute(() -> {
            mFeedingDao.insertFeeding(data);
            sync();
        });
    }

    @Override
    public void updateData(@NonNull final Feeding data) {
        mAppExecutors.diskIO().execute(() -> {
            mFeedingDao.updateFeeding(data);
            sync();
        });
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void deleteAllData(@NonNull final String batchId) {
        mAppExecutors.diskIO().execute(() -> {
            mFeedingDao.deleteAllFeeding(batchId);
            sync();
        });
    }

    @Override
    public void deleteDataItem(@NonNull final Feeding item, @NonNull String batchId) {
        mAppExecutors.diskIO().execute(() -> {
            mFeedingDao.deleteFeedingById(item.getId());
            sync();
        });
    }

    @Override
    public void clearCache() {
    }

    @Override
    public void notifyDataChanged() {

    }

    @Override
    public void observeData(DataLoadCallback<Feeding> callback, String... referenceIds) {

    }

    @Override
    public void getMortalityRate(final String batchId, final OnMortalityRate callback) {
        mAppExecutors.diskIO().execute(() -> {
            final int rate = mFeedingDao.mortalityRate(batchId);
            mAppExecutors.mainThread().execute(() -> callback.onMortalityLoaded(rate));
        });

    }

    @Override
    public String getTable() {
        return Feeding.TABLE_NAME;
    }

    @Override
    public List<Feeding> getNew() {
        return mFeedingDao.getNew();
    }

    @Override
    public List<Feeding> getUpdated() {
        return mFeedingDao.getUpdated();
    }

    @Override
    public List<Feeding> getLocalData() {
        return mFeedingDao.getLocal();
    }

    @Override
    public List<Long> getTrash() {
        return mFeedingDao.getTrash();
    }

    @Override
    public long getLastServerId() {
        return mFeedingDao.getLastServerId();
    }

    @Override
    public void handleNewData(JSONArray newData) {
        Gson gson = new Gson();
        for (int n = 0; n < newData.length(); n++) {
            try {
                Feeding feeding = gson.fromJson(newData.get(n).toString(), Feeding.class);

                Feeding newDeaths = new Feeding(feeding.getId(), feeding.getBatchId(), feeding.getDate(), feeding.getQuantity(),
                        feeding.getItem(), feeding.getServerId(), feeding.getStatus(), feeding.getLastModified());
                mFeedingDao.insertFeeding(newDeaths);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleModified(JSONArray modified) {
        Gson gson = new Gson();
        for (int n = 0; n < modified.length(); n++) {
            try {
                Feeding feeding = gson.fromJson(modified.get(n).toString(), Feeding.class);
                mFeedingDao.updateFeeding(feeding);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleSynced(JSONArray synced) {
        for (int n = 0; n < synced.length(); n++) {
            try {
                String id = synced.getJSONObject(n).getString("id");
                long serverId = synced.getJSONObject(n).getLong("serverId");
                long lastMod = synced.getJSONObject(n).getLong("lastModified");

                Feeding feeding = mFeedingDao.getFeedingById(id);

                Feeding syncedFeeding = new Feeding(feeding.getId(), feeding.getBatchId(), feeding.getDate(), feeding.getQuantity(),
                        feeding.getItem(), serverId, feeding.getStatus(), lastMod);

                mFeedingDao.updateFeeding(syncedFeeding);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleUpdates(JSONArray updates) {
        for (int n = 0; n < updates.length(); n++) {
            try {
                String id = updates.getJSONObject(n).getString("id");
                Long last_mod = updates.getJSONObject(n).getLong("lastModified");
                Feeding feeding = mFeedingDao.getFeedingById(id);
                Feeding newFeeding = new Feeding(feeding.getId(), feeding.getBatchId(), feeding.getDate(), feeding.getQuantity(),
                        feeding.getItem(), feeding.getServerId(), feeding.getStatus(), last_mod);
                mFeedingDao.updateFeeding(newFeeding);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void handleTrash(String[] trash) {
        for (String aTrash : trash) {
            mFeedingDao.deleteFeedingById(aTrash);
        }
        mFeedingDao.clearTrash();
    }

    @Override
    public void notifyRepository() {
//        repository.clearCache();
    }

    private void sync() {
        Sync.getInstance().sync(this, false);
    }
}
