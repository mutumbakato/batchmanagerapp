package com.mutumbakato.batchmanager.data.local.dao

import androidx.room.Dao
import androidx.room.Query

/**
 * Created by cato on 11/12/17.
 */

@Dao
interface UserDao {

    @Query("DELETE FROM batches")
    fun clearBatches()

    @Query("DELETE FROM expenses")
    fun clearExpenses()

    @Query("DELETE FROM expense_categories")
    fun clearExpensesCategories()

    @Query("DELETE FROM vaccination")
    fun clearVaccinations()

    @Query("DELETE FROM sales")
    fun clearSales()

    @Query("DELETE FROM vaccination_check")
    fun clearVaccinationChecks()

    @Query("DELETE FROM vaccination_schedule")
    fun clearVaccinationSchedules()

    @Query("DELETE FROM death")
    fun clearDeaths()

    @Query("DELETE FROM feeding")
    fun clearFeeds()

    @Query("DELETE FROM eggs")
    fun clearEggs()

    @Query("DELETE FROM farms")
    fun clearFarms()
}
