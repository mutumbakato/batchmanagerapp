package com.mutumbakato.batchmanager.data.models.exports

data class WeightExport(val date: String, val avg: Float)
