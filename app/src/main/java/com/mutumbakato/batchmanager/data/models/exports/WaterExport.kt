package com.mutumbakato.batchmanager.data.models.exports

data class WaterExport(val date: String, val count: Float, val total: Float)