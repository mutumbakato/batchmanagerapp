package com.mutumbakato.batchmanager.data

import com.mutumbakato.batchmanager.data.models.Water

interface WaterDataSource {

    fun createWater(water: Water, onSuccess: (Water) -> Unit)

    fun listWaters(batchId: String, onSuccess: (List<Water>) -> Unit)

    fun getWater(id: String, onSuccess: (Water) -> Unit)

    fun updateWater(water: Water, onSuccess: (Water) -> Unit)

    fun deleteWater(water: Water, onSuccess: () -> Unit)

}