package com.mutumbakato.batchmanager.data.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;

import static com.mutumbakato.batchmanager.data.models.Vaccination.TABLE_NAME;

@Entity(tableName = TABLE_NAME)
public class Vaccination {

    public static final String TABLE_NAME = "vaccination";

    @PrimaryKey
    @ColumnInfo(name = "_id")
    @NonNull
    @Expose
    private String id;

    @Expose
    @ColumnInfo(name = "schedule_id")
    private String scheduleId;

    @Expose
    @ColumnInfo(name = "age")
    private int age;

    @Expose
    @ColumnInfo(name = "recurrent")
    private boolean recurrent;

    @Expose
    @ColumnInfo(name = "thenEvery")
    private String thenEvery;

    @Expose
    @ColumnInfo(name = "infection")
    private String infection;

    @Expose
    @ColumnInfo(name = "method")
    private String method;

    @Expose
    @ColumnInfo(name = "vaccine")
    private String vaccine;

    @Expose
    @ColumnInfo(name = "server_id")
    private long serverId;

    @ColumnInfo(name = "status")
    private String status;

    @Expose
    @ColumnInfo(name = "last_modified")
    private long lastModified;

    @Ignore
    public Vaccination() {

    }

    public Vaccination(@NonNull String id, String scheduleId, int age, String thenEvery,
                       @NonNull String infection, String vaccine, @NonNull String method,
                       boolean recurrent, long serverId, String status, long lastModified) {
        this.id = id;
        this.method = method;
        this.infection = infection;
        this.scheduleId = scheduleId;
        this.vaccine = vaccine;
        this.thenEvery = thenEvery;
        this.recurrent = recurrent;
        this.serverId = serverId;
        this.lastModified = lastModified;
        this.status = status;
        this.age = age;
    }

    @NonNull
    public String getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public String getInfection() {
        return infection;
    }

    public String getMethod() {
        return method;
    }

    public String getVaccine() {
        return vaccine;
    }

    public String getScheduleId() {
        return scheduleId;
    }

    public String getThenEvery() {
        return thenEvery;
    }

    public String getStatus() {
        return status;
    }

    public boolean isRecurrent() {
        return recurrent;
    }

    public long getServerId() {
        return serverId;
    }

    public long getLastModified() {
        return lastModified;
    }
}
