package com.mutumbakato.batchmanager.data.sync;

import org.json.JSONArray;

import java.util.List;

public interface Syncable<T> {

    /**
     * @return Returns the table name of the entity
     */
    String getTable();

    /**
     * @return newly added items.
     */
    List<T> getNew();

    /**
     * @return recently updated items
     */
    List<T> getUpdated();

    /**
     * @return ids of items already i the local database
     */
    List<T> getLocalData();

    /**
     * @return ids of locally deleted items
     */
    List<Long> getTrash();

    /**
     * @return server name
     */
    long getLastServerId();

    /**
     * Handles new data received from the server
     *
     * @param newData json array containing new data
     */
    void handleNewData(JSONArray newData);

    /**
     * Handles data found modified at the server
     *
     * @param modified jsonArray containing modified data
     */
    void handleModified(JSONArray modified);

    /**
     * Handles serverIds of data tha was sent to the server
     *
     * @param synced data ids from the server;
     */
    void handleSynced(JSONArray synced);

    /**
     * Handles data ids of updates sent to the server
     *
     * @param updates server_ids of updated data
     */
    void handleUpdates(JSONArray updates);

    /**
     * Handles data that was deleted from the server
     *
     * @param trash id of deleted data
     */
    void handleTrash(String[] trash);

    /**
     * Notify the repository to update the UI with new data
     */
    void notifyRepository();

}
