package com.mutumbakato.batchmanager.data.remote

import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by cato on 10/21/17.
 */

interface UserEndPoints {

    @FormUrlEncoded
    @POST("user/login")
    fun login(@Field("username") username: String,
              @Field("password") password: String): Call<String>

    @FormUrlEncoded
    @POST("user/register")
    fun register(@Field("username") name: String,
                 @Field("email") email: String,
                 @Field("phone") phone: String,
                 @Field("password") password: String): Call<String>

    @FormUrlEncoded
    @POST("user/logout")
    fun logOut(@Field("token") token: String): Call<String>

    @FormUrlEncoded
    @POST("user/password/forgot")
    fun reset(@Field("email") email: String): Call<String>

    @GET("user/{id}")
    fun getUserById(@Path("id") id: String): Call<String>

    @GET("user/get/email")
    fun getUserByEmail(@Query("email") email: String): Call<String>

}
