package com.mutumbakato.batchmanager.data.local

import androidx.lifecycle.LiveData
import com.mutumbakato.batchmanager.data.local.dao.WeightDao
import com.mutumbakato.batchmanager.data.models.Weight
import com.mutumbakato.batchmanager.utils.executors.AppExecutors

class WeightLocalDataSource constructor(val dao: WeightDao, val executors: AppExecutors) {

    fun insert(weight: Weight, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.insert(weight)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun insertAll(weights: List<Weight>, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.insertAll(weights)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun update(weight: Weight, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.update(weight)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun listWeight(batchId: String): LiveData<List<Weight>> {
        return dao.listWeight(batchId)
    }

    fun getWeightById(id: String): LiveData<Weight> {
        return dao.getWeightById(id)
    }

    fun deleteWeight(weight: Weight, onFinish: () -> Unit) {
        executors.diskIO().execute {
            dao.deleteWeight(weight)
            executors.mainThread().execute {
                onFinish()
            }
        }
    }

    fun deleteAllWeight(batchId: String) {
        executors.diskIO().execute {
            deleteAllWeight(batchId)
        }
    }
}