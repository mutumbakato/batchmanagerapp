package com.mutumbakato.batchmanager.data.firestore

import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.data.WaterDataSource
import com.mutumbakato.batchmanager.data.models.Water

class WaterFirestore : WaterDataSource {

    private val db = FirebaseFirestore.getInstance().collection("water")

    override fun createWater(water: Water, onSuccess: (Water) -> Unit) {
        db.document(water.id).set(water)
    }

    override fun listWaters(batchId: String, onSuccess: (List<Water>) -> Unit) {
        db.whereEqualTo("batchId", batchId).addSnapshotListener { snapshot, _ ->
            db.whereEqualTo("batchId", batchId).addSnapshotListener { snapshots, _ ->
                if (snapshots != null && !snapshots.metadata.hasPendingWrites()) {
                    val dataItems = ArrayList<Water>()
                    for (data in snapshots) {
                        dataItems.add(data.toObject(Water::class.java))
                    }
                    onSuccess(dataItems)
                }
            }
        }
    }

    override fun getWater(id: String, onSuccess: (water: Water) -> Unit) {

    }

    override fun updateWater(water: Water, onSuccess: (water: Water) -> Unit) {
        db.document(water.id).set(water)
    }

    override fun deleteWater(water: Water, onSuccess: () -> Unit) {
        db.document(water.id).delete()
    }
}