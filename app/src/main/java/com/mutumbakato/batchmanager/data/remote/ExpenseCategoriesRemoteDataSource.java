package com.mutumbakato.batchmanager.data.remote;

import android.os.Handler;
import androidx.annotation.NonNull;

import com.mutumbakato.batchmanager.data.ExpenseCategoriesDataSource;
import com.mutumbakato.batchmanager.data.models.ExpenseCategory;
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;


public class ExpenseCategoriesRemoteDataSource implements ExpenseCategoriesDataSource {

    private static final int SERVICE_LATENCY_IN_MILLIS = 5000;
    private final static Map<String, ExpenseCategory> CATEGORIES_SERVICE_DATA;
    private static ExpenseCategoriesRemoteDataSource INSTANCE = null;

    static {
        CATEGORIES_SERVICE_DATA = new LinkedHashMap<>(2);
        addCategory("Feeds");
        addCategory("Wadges");
        addCategory("Transport");
        addCategory("Utilities");
        addCategory("Maintenance");
    }

    private ExpenseCategoriesRemoteDataSource() {

    }

    private static void addCategory(String name) {
        ExpenseCategory newCategory = new ExpenseCategory(name, 0, PreferenceUtils.INSTANCE.getFarmId());
        CATEGORIES_SERVICE_DATA.put(newCategory.getId(), newCategory);
    }

    public static ExpenseCategoriesRemoteDataSource getInstance() {
        if (INSTANCE == null)
            INSTANCE = new ExpenseCategoriesRemoteDataSource();
        return INSTANCE;
    }

    @Override
    public void getAllCategories(@NonNull final CategoriesLoadCallback callback) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.onCategoriesLoaded(new ArrayList<>(CATEGORIES_SERVICE_DATA.values()));
            }
        }, SERVICE_LATENCY_IN_MILLIS);
    }

    @Override
    public void saveCategory(@NonNull ExpenseCategory category) {
        CATEGORIES_SERVICE_DATA.put(category.getId(), category);
    }

    @Override
    public void updateCategory(@NonNull ExpenseCategory category) {
        CATEGORIES_SERVICE_DATA.put(category.getId(), category);
    }

    @Override
    public void deleteCategory(@NonNull String id) {
        CATEGORIES_SERVICE_DATA.remove(id);
    }

    @Override
    public void deleteAll() {
        CATEGORIES_SERVICE_DATA.clear();
    }

    @Override
    public void clearCache() {

    }

    @Override
    public void observeData(CategoriesLoadCallback callback, String... referenceIds) {

    }
}
