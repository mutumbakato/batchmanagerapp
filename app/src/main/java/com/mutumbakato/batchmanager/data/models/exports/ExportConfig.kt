package com.mutumbakato.batchmanager.data.models.exports

data class ExportConfig(
        var batchId: String = "",
        var batchName: String = "")