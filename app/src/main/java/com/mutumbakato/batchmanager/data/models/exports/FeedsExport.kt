package com.mutumbakato.batchmanager.data.models.exports

data class FeedsExport(val date: String, val count: Float, val total: Float, val gpb: Float)