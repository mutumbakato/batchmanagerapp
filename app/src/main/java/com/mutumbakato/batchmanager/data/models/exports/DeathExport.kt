package com.mutumbakato.batchmanager.data.models.exports

data class DeathExport(val date: String, val count: Int, val cause: String)