package com.mutumbakato.batchmanager.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.batch.BatchListActivity
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import kotlin.random.Random

val NO_RECORDS_MESSAGE = intArrayOf(R.string.no_records_1, R.string.no_records_2, R.string.no_records_3)

class DailyAlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        showNotification(context, "Hello Farmer")
        RepositoryUtils.getBatchRepo(context).getTodayRecords {
            Log.d("RECORDS:::", it.toString())
            if (it.hasBatch && !it.hasFeeds && !it.hasOthers) {
                val message = context.getString(NO_RECORDS_MESSAGE[Random.nextInt(0, NO_RECORDS_MESSAGE.size - 1)])
                showNotification(context, message)
            } else if (!it.hasFeeds) {
                val message = "You have a batch which has no feeding records today, this will affect the FCR."
                showNotification(context, message)
            }
        }
    }

    private fun checkForAnyData(): String {
        TODO("Not yet implemented")
    }

    private fun showNotification(context: Context, message: String) {

        val time = System.currentTimeMillis()
        val notificationManager = context
                .getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val notificationIntent = Intent(context, BatchListActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

        val pendingIntent = PendingIntent.getActivity(context, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmSound: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        // get your quote here
        val mNotifyBuilder: NotificationCompat.Builder = NotificationCompat.Builder(context, getChannelId(context))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Batch Records")
                .setContentText(message).setSound(alarmSound)
                .setAutoCancel(true).setWhen(time)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.mipmap.ic_launcher))
                .setStyle(NotificationCompat.BigTextStyle().bigText(message))
                .setContentIntent(pendingIntent)

        notificationManager.notify(5, mNotifyBuilder.build())
    }

    private fun getChannelId(context: Context): String {

        val channelId: String = "com.mutumbakato.alarm_notification_channel"

        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(R.string.channel_name)
            val descriptionText = context.getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(channelId, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
        return channelId
    }

}