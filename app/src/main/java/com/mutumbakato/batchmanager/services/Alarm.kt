package com.mutumbakato.batchmanager.services

import android.app.AlarmManager

import android.app.PendingIntent
import android.content.Context

import android.content.Intent
import java.util.*


object Alarm {

    fun setAlarm(context: Context) {

        // Quote in Morning at 08:32:00 AM
        val calendar: Calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 18)
        calendar.set(Calendar.MINUTE, 30)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        val cur: Calendar = Calendar.getInstance()

        if (cur.after(calendar)) {
            calendar.add(Calendar.DATE, 1)
        }

        val myIntent = Intent(context, DailyAlarmReceiver::class.java)
        val ALARM1_ID = 10000
        val pendingIntent = PendingIntent.getBroadcast(
                context, ALARM1_ID, myIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, AlarmManager.INTERVAL_DAY, pendingIntent)
    }
}