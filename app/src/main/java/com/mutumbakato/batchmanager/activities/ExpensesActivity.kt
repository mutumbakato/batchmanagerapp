package com.mutumbakato.batchmanager.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Expense
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.ui.components.FilterDialog
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.utils.UserRoles
import com.mutumbakato.batchmanager.utils.licence.LicenseUtils
import com.mutumbakato.batchmanager.viewmodels.ExpensesViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.activity_expenses_list.*
import kotlinx.android.synthetic.main.message_view.*
import kotlinx.android.synthetic.main.toolbar.*

class ExpensesActivity : BaseActivity() {

    private var mBatchId: String? = null
    private var mBatchName: String? = null
    private var isEditing = false
    private lateinit var viewModel: ExpensesViewModel
    private var filter = Filter()
    private var mode = "all"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_expenses_list)
        viewModel = ViewModelProvider(this, ViewModelFactory
                .ExpensesViewModelFactory(RepositoryUtils.getExpensesLiveRepo(this),
                        RepositoryUtils.getBatchRepo(this))).get(ExpensesViewModel::class.java).apply {
            setFarmId(PreferenceUtils.farmId)
        }

        mBatchId = intent.getStringExtra(BATCH_ID_EXTRA)
        mBatchName = intent.getStringExtra(BATCH_NAME_EXTRA)
        initToolBar()
        initViewModel()

        if (LicenseUtils.isLicenceExpired()) {
            expenses_add_fab.visibility = View.GONE
        }

        expenses_add_fab.setOnClickListener {
            viewModel.isEditing.postValue(true)
        }
    }

    private fun initViewModel() {

        viewModel.isLoading.observe(this, Observer {
            showProgress(it)
        })

        viewModel.filteredExpenses.observe(this, Observer {
            if (mode == "filter")
                showEmpty(it == null || it.isEmpty())
            expense_textView_total!!.text = "${PreferenceUtils.currency} ${Currency.format(getTotalExpenses(it))}"
        })

        viewModel.expenses.observe(this, Observer {
            if (mode == "all")
                showEmpty(it == null || it.isEmpty())
        })

        viewModel.isEditing.observe(this, Observer {
            isEditing = it
            if (!it) {
                onDoneEditing()
            } else {
                onStartEditing()
            }
        })

        viewModel.filter.observe(this, Observer {
            filter = it
            it?.let {
//              expense_filter_details.text = it.rangeName
                expense_filter_name.text = "${it.name}"
            }
        })

        viewModel.mode.observe(this, Observer {
            mode = it
            expense_textView_total.visibility = if (it == "filter") View.VISIBLE else View.INVISIBLE
//          expense_filter_details.visibility = if (it == "filter") View.VISIBLE else View.GONE
        })
    }

    private fun initToolBar() {
        toolbar.title = getString(R.string.title_expenses)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBackPressed() {
        if (isEditing) {
            viewModel.isEditing.postValue(false)
        } else
            super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_filter, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.action_filter -> {
                showFilter()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showFilter() {
        FilterDialog.newInstance(getString(R.string.filter_expenses), filter.toString()).apply {
            setOnApply {
                viewModel.applyFilter(it)
            }
            onClearFilter {
                viewModel.clearFilter()
                viewModel.setFarmId(PreferenceUtils.farmId)
            }
            show(supportFragmentManager, "filter_expenses")
        }
    }

    fun showEmpty(isVisible: Boolean) {
        if (isVisible) {
            message_view!!.visibility = View.VISIBLE
            message_title!!.text = getString(R.string.no_expenses_found)
            message_image!!.setImageResource(R.drawable.no_expenses)
            message_description!!.text = getString(R.string.add_expenses_message)
        } else {
            message_view!!.visibility = View.GONE
        }
    }

    fun showProgress(isLoading: Boolean) {
        expenses_progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    @SuppressLint("RestrictedApi")
    fun applyPermissions(role: String) {
        expenses_add_fab!!.visibility = if (role == UserRoles.USER_ADMIN || role == UserRoles.USER_WORKER)
            View.VISIBLE
        else
            View.GONE
    }

    fun onDoneEditing() {
        KeyBoard.hideSoftKeyboard(this@ExpensesActivity)
        if (!LicenseUtils.isLicenceExpired())
            expenses_add_fab!!.show()
    }

    fun onStartEditing() {
        expenses_add_fab!!.hide()
    }

    private fun getTotalExpenses(expenses: List<Expense>): Float {
        var total = 0f
        expenses.forEach {
            total += it.amount
        }
        return total
    }

    companion object {

        private const val BATCH_ID_EXTRA = "batch_id"
        private const val BATCH_NAME_EXTRA = "batch_name"

        fun start(context: Context, batchId: String, batchName: String, mode: String = "all") {
            val starter = Intent(context, ExpensesActivity::class.java)
            starter.putExtra(BATCH_ID_EXTRA, batchId)
            starter.putExtra(BATCH_NAME_EXTRA, batchName)
            context.startActivity(starter)
        }

    }
}
