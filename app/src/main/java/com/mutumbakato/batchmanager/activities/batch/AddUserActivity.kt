package com.mutumbakato.batchmanager.activities.batch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.batch.BatchUserFragment
import com.mutumbakato.batchmanager.presenters.AddUserPresenter
import com.mutumbakato.batchmanager.ui.AddUserContract
import kotlinx.android.synthetic.main.activity_add_user.*

class AddUserActivity : AppCompatActivity(), BatchUserFragment.BatchUserListInteractionListener {

    private var mPresenter: AddUserPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val batchId = intent.getStringExtra(EXTRA_BATCH_ID)
        val batchName = intent.getStringExtra(EXTRA_BATCH_NAME)
        supportActionBar!!.subtitle = batchName

        mPresenter = AddUserPresenter(batchId, "",
                RepositoryUtils.getBatchRepo(this),
                RepositoryUtils.getUserRepo(this),
                supportFragmentManager.findFragmentById(R.id.add_user_fragment) as AddUserContract.View)

        add_batch_user_button.setOnClickListener {
            addUser()
        }
    }

    internal fun addUser() {
        mPresenter!!.addyEmail()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun onItemClick(user: BatchUser) {

    }

    override fun onUpdate(user: BatchUser, role: String) {
        mPresenter!!.changeRole(user, role)
    }

    override fun onRemove(user: BatchUser) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Remove " + user.name)
                .setMessage("Do you want to remove " + user.name + " from this batch?")
                .setPositiveButton("Remove") { _, _ -> mPresenter!!.removeUser(user) }
                .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
                .show()
    }

    override fun showProgress(isShow: Boolean) {
        batch_user_progress!!.visibility = if (isShow) View.VISIBLE else View.GONE
    }

    override fun showHasUsers(hasUsers: Boolean) {

    }

    companion object {

        private const val EXTRA_BATCH_ID = "batch_id"
        private const val EXTRA_BATCH_NAME = "batch_name"

        fun start(context: Context, batchId: String, batchName: String) {
            val starter = Intent(context, AddUserActivity::class.java)
            starter.putExtra(EXTRA_BATCH_ID, batchId)
            starter.putExtra(EXTRA_BATCH_NAME, batchName)
            context.startActivity(starter)
        }
    }

}
