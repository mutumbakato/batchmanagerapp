package com.mutumbakato.batchmanager.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.mutumbakato.batchmanager.R
import kotlinx.android.synthetic.main.toolbar_flat.*

class StoresActivity : AppCompatActivity() {

    private lateinit var farmId: String
    private lateinit var farmName: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stores)
        setSupportActionBar(toolbar)
        farmId = intent.getStringExtra("farm_id")
        farmName = intent.getStringExtra("farm_name")
        supportActionBar?.apply {
            title = farmName
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun starts(context: Context, farmId: String, farmName: String) {
            context.startActivity(Intent(context, StoresActivity::class.java).apply {
                putExtra("farm_id", farmId)
                putExtra("farm_name", farmName)
            })
        }
    }
}
