package com.mutumbakato.batchmanager.activities.vaccination

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.vaccination.VaccinationFormFragment
import com.mutumbakato.batchmanager.presenters.VaccinationFromPresenter

import kotlinx.android.synthetic.main.toolbar.*

class VaccinationFormActivity : BaseActivity() {

    private var mPresenter: VaccinationFromPresenter? = null
    private var vaccinationId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vaccination_form)

        val vaccinationFormFragment = VaccinationFormFragment.newInstance()
        val scheduleId = intent.getStringExtra(EXTRA_SCHEDULE_ID)
        vaccinationId = intent.getStringExtra(EXTRA_VACCINATION_ID)
        toolbar!!.title = if (vaccinationId == null) "New Vaccination" else "Edit vaccination"

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        addFragment(vaccinationFormFragment, R.id.vaccination_form_frame)
        mPresenter = VaccinationFromPresenter(scheduleId, vaccinationId, RepositoryUtils.getVaccinationRepo(this),
                vaccinationFormFragment)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_vaccination_form, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_delete_batch -> {
                mPresenter!!.deleteVaccination(vaccinationId!!)
                finish()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        private const val EXTRA_SCHEDULE_ID = "schedule_id"
        private const val EXTRA_VACCINATION_ID = "vaccination_id"

        fun start(context: Context, scheduleId: String, vaccinationId: String) {
            val starter = Intent(context, VaccinationFormActivity::class.java)
            starter.putExtra(EXTRA_SCHEDULE_ID, scheduleId)
            starter.putExtra(EXTRA_VACCINATION_ID, vaccinationId)
            context.startActivity(starter)
        }
    }
}
