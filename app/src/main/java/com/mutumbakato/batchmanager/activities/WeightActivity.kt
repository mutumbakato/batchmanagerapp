package com.mutumbakato.batchmanager.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import com.mutumbakato.batchmanager.viewmodels.WeightViewModel
import kotlinx.android.synthetic.main.activity_weight.*
import kotlinx.android.synthetic.main.record_type_view.*
import java.util.*

class WeightActivity : AppCompatActivity() {

    private lateinit var batchName: String
    private lateinit var batchId: String
    private lateinit var dateOfBirth: String
    private var isFormOpen: Boolean = false
    private lateinit var batchViewModel: BatchViewModel
    private lateinit var viewModel: WeightViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this,
                ViewModelFactory.WeightViewModelFactory(RepositoryUtils.getWeightRepository(this),
                        RepositoryUtils.getBatchRepo(this)
                )).get(WeightViewModel::class.java)

        batchViewModel = ViewModelProvider(this, ViewModelFactory.BatchViewModelFactory(RepositoryUtils.getStatisticsDataSource(this)))[BatchViewModel::class.java]
        setContentView(R.layout.activity_weight)
        setSupportActionBar(toolbar)

        batchName = intent.getStringExtra(BATCH_NAME_EXTRA) ?: ""
        batchId = intent.getStringExtra(BATCH_ID_EXTRA) ?: "0"
        dateOfBirth = intent.getStringExtra(DOB_EXTRA) ?: ""

        viewModel.setBatchId(batchId)
        batchViewModel.setBatchId(batchId)
        viewModel.setDateOfBirth(dateOfBirth)

        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.title = batchName
        }

        viewModel.isFormOpen.observe(this, Observer {
            isFormOpen = it
            if (it) {
                add_weight_fab.hide()
            } else {
                add_weight_fab.show()
            }
        })

        recordType.text = getString(R.string.body_weight)
        add_weight_fab.setOnClickListener {
            viewModel.setDate(DateUtils.today())
        }

        weight_return_fab.setOnClickListener {
            viewModel.goTo.postValue(UUID.randomUUID().toString())
            weight_return_fab.visibility = View.INVISIBLE
            add_weight_fab.visibility = View.VISIBLE
            app_bar.setExpanded(true)
        }

        button_chart_details.setOnClickListener {
            StatsDetailsActivity.start(this, batchId, "weight")
        }

        batchViewModel.avgWeight.observe(this, Observer {
            record_summary.text = String.format("Avg: %.0f gm", it)
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (isFormOpen) {
            viewModel.closeForm()
        } else
            super.onBackPressed()
    }

    companion object {

        const val BATCH_ID_EXTRA = "batch_id"
        const val BATCH_NAME_EXTRA = "batch_name"
        const val DOB_EXTRA = "date_of_birth"
        const val BATCH_COUNT_EXTRA = "batch_count"
        const val CLOSE_DATE_EXTRA = "close_date"
        const val BATCH_TYPE = "batch_type"

        fun start(context: Context, batchId: String, batchName: String, dateOfBirth: String, batchCount: Int, closeDate: String?, batchType: String) {
            val intent = Intent(context, WeightActivity::class.java).apply {
                putExtra(BATCH_ID_EXTRA, batchId)
                putExtra(BATCH_NAME_EXTRA, batchName)
                putExtra(DOB_EXTRA, dateOfBirth)
                putExtra(BATCH_COUNT_EXTRA, batchCount)
                putExtra(CLOSE_DATE_EXTRA, closeDate)
                putExtra(BATCH_TYPE, batchType)
            }
            context.startActivity(intent)
        }
    }
}
