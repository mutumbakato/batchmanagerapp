package com.mutumbakato.batchmanager.activities.batch

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem
import com.google.firebase.firestore.FirebaseFirestore
import com.mutumbakato.batchmanager.*
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.activities.ExpensesActivity
import com.mutumbakato.batchmanager.activities.SalesActivity
import com.mutumbakato.batchmanager.activities.StoresActivity
import com.mutumbakato.batchmanager.activities.farm.ExportActivity
import com.mutumbakato.batchmanager.activities.farm.FarmDetailsActivity
import com.mutumbakato.batchmanager.activities.user.AccountActivity
import com.mutumbakato.batchmanager.activities.user.LoginActivity
import com.mutumbakato.batchmanager.activities.vaccination.VaccinationScheduleActivity
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.Farm
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.RebrandFragment
import com.mutumbakato.batchmanager.fragments.batch.ActivityLogFragment
import com.mutumbakato.batchmanager.fragments.batch.BatchListFragment
import com.mutumbakato.batchmanager.fragments.farm.FarmFragment
import com.mutumbakato.batchmanager.fragments.farm.FarmListFragment
import com.mutumbakato.batchmanager.presenters.BatchListPresenter
import com.mutumbakato.batchmanager.services.Alarm
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_batch_list.*

class BatchListActivity : BaseActivity(), BatchListFragment.OnBatchListInteractionListener,
        FarmListFragment.OnListFragmentInteractionListener,
        FarmFragment.OnListFragmentInteractionListener {

    private var batchListPresenter: BatchListPresenter? = null
    private var position = 1
    private var optionMenu: Menu? = null
    private var contactNumber = "256778959764"

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_batch_list)
        Alarm.setAlarm(this)
        //if user is not logged in
        if (PreferenceUtils.isNotLoggedIn) {
            LoginActivity.start(this)
            finish()
            return
        }

        //If there is any farm selected
        if (PreferenceUtils.farmId == "") {
            SetupActivity.start(this)
            finish()
            return
        }

        if (!PreferenceUtils.isRebrand) {
            RebrandFragment.newInstance().show(supportFragmentManager, "rebrand")
        }

        toolbar!!.title = ""
        setSupportActionBar(toolbar)
        val typeface = Typeface.createFromAsset(assets, "fonts/Nunito-Black.ttf")
        toolbar_title.typeface = typeface
        new_batch_button.setOnClickListener {
            add()
        }

        initBottomNavigation()
        if (savedInstanceState != null) {
            position = savedInstanceState.getInt("position")
        }

        bottom_nav.currentItem = position
        showCurrentPosition(position)
        getContactNumber()
    }

    private fun showCurrentPosition(i: Int) {
        when (i) {
            0 -> {
                showLogs()
            }
            1 -> {
                showBatchList()
            }
            2 -> {
                showFarm()
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt("position", position)
        super.onSaveInstanceState(outState)
    }

    internal fun add() {
        batchListPresenter!!.addNewBatch()
    }

    override fun onResume() {
        super.onResume()
        if (PreferenceUtils.isNotLoggedIn) {
            LoginActivity.start(this)
            finish()
        }
    }

    override fun onBatchListFragmentInteraction(batch: Batch) {
        batchListPresenter!!.openBatchDetails(batch)
    }

    override fun showProgress(isLoading: Boolean) {
        sync_progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun showAd() {
        //adView.setVisibility(View.VISIBLE);
    }

    private fun showBatchList() {
        val batchListFragment = BatchListFragment.newInstance()
        batchListPresenter = BatchListPresenter(PreferenceUtils.farmId, false,
                RepositoryUtils.getBatchRepo(this), batchListFragment)
        supportFragmentManager.beginTransaction().replace(R.id.home_frame, batchListFragment).commitNow()
        toolbar_title.text = getString(R.string.batches)
    }

    private fun showLogs() {
        val logsFragment = ActivityLogFragment.newInstance(PreferenceUtils.farmId)
        supportFragmentManager.beginTransaction().add(R.id.home_frame, logsFragment).commitNow()
        toolbar_title.text = getString(R.string.activities)
    }

    private fun showFarm() {
        val farmFragment = FarmFragment.newInstance(1)
        supportFragmentManager.beginTransaction().add(R.id.home_frame, farmFragment).commitNow()
        toolbar_title.text = getString(R.string.farm)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_batch, menu)
        optionMenu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_logout -> logout()
            R.id.action_add_batch -> add()
            R.id.action_archived -> ArchivedBatchesActivity.start(this, PreferenceUtils.farmId)
            R.id.action_farm_settings -> SettingsActivity.start(this)
            R.id.action_farms -> FarmListFragment.newInstance().show(supportFragmentManager, "farms")
            R.id.nav_share -> share()
            R.id.nav_whatsapp -> contactUs()
            R.id.nav_about -> PrivacyPolicy.start(this)
            R.id.action_account -> AccountActivity.start(this)
        }
        return super.onOptionsItemSelected(item)
    }

    private fun logout() {
        val userRepository = RepositoryUtils.getUserRepo(this)
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Logout")
        builder.setMessage("Are you sure you want to logout? All your data will be removed from this device!")
        builder.setPositiveButton("Continue") { _, _ ->
            showProgress(true)
            userRepository.logOut(object : UserDataSource.UserCallBack {
                override fun onSuccess(user: User?) {
                    showProgress(false)
                    Prefs.clear()
                    RepositoryUtils.getBatchRepo(this@BatchListActivity).clearMemoryCache(false)
                    RepositoryUtils.getFarmRepo(this@BatchListActivity).clearCache()
                    LoginActivity.start(this@BatchListActivity)
                    finish()
                }

                override fun onError(message: String) {
                    showProgress(false)
                }
            })
        }
        builder.setNegativeButton("Cancel") { dialogInterface, _ -> dialogInterface.dismiss() }
        builder.create().show()
    }

    private fun share() {
        val appPackageName = packageName //from Context or Activity object
        val shareText = Uri.parse("Hey, I use the Poultry Batch Manager App for Android phones" +
                " to keep my poultry records and I find it very helpful, please try it out. " +
                "https://play.google.com/store/apps/details?id=" + appPackageName).toString()
        val intent = Intent()
        intent.action = Intent.ACTION_SEND
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_TEXT, shareText)
        startActivity(Intent.createChooser(intent, "Share"))
    }

    private fun contactUs() {
        val url = "https://wa.me/${contactNumber.replace("+", "")}"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    override fun onFarmListFragmentInteraction(item: Farm) {
        PreferenceUtils.setFarm(item)
        batchListPresenter!!.setFarm(item.id)
        (supportFragmentManager.findFragmentByTag("farms") as FarmListFragment).dismiss()
    }

    override fun onListFragmentInteraction(item: Features.FeatureItem?) {
        when (item!!.name) {
            "Expenses" -> ExpensesActivity.start(this, "0", "Farm")
            "Sales" -> SalesActivity.start(this, "0", "Farm")
            "Stores" -> StoresActivity.starts(this, "0", "Stores")
            "Vaccination" -> VaccinationScheduleActivity.start(this, "", "")
            "Preferences" -> FarmDetailsActivity.start(this, PreferenceUtils.farmId)
            "Export" -> ExportActivity.starts(this, PreferenceUtils.farmId)
        }
    }

    override fun onSettings(item: Farm) {
        FarmDetailsActivity.start(this, item.id)
    }

    private fun initBottomNavigation() {

        val item1 = AHBottomNavigationItem("Activities", R.drawable.ic_activity)
        val item2 = AHBottomNavigationItem("Batches", R.drawable.ic_action_batches)
        val item3 = AHBottomNavigationItem("Farm", R.drawable.ic_home)

        bottom_nav!!.addItem(item1)
        bottom_nav!!.addItem(item2)
        bottom_nav!!.addItem(item3)
        bottom_nav.currentItem = 1

        bottom_nav.setTitleTextSizeInSp(14f, 14f)
        bottom_nav!!.defaultBackgroundColor = ContextCompat.getColor(this, R.color.colorOrangeLight)
        bottom_nav!!.accentColor = ContextCompat.getColor(this, R.color.colorOrange)
        bottom_nav!!.setOnTabSelectedListener { position, _ ->
            this.position = position
            showCurrentPosition(position)
            invalidateOptionsMenu()
            true
        }
    }

    private fun getContactNumber() {
        FirebaseFirestore.getInstance().collection("contacts").document("contact").addSnapshotListener { snapshot, error ->
            if (snapshot?.exists() != null) {
                contactNumber = snapshot.data?.get("number") as String
            }
        }
    }

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, BatchListActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(starter)
        }
    }
}
