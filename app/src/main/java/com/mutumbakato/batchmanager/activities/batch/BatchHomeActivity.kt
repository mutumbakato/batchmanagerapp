package com.mutumbakato.batchmanager.activities.batch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.mutumbakato.batchmanager.Features
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.*
import com.mutumbakato.batchmanager.activities.farm.ExportActivity
import com.mutumbakato.batchmanager.activities.feeds.FeedingActivity
import com.mutumbakato.batchmanager.activities.vaccination.VaccinationActivity
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.batch.BatchHomeFragment
import com.mutumbakato.batchmanager.fragments.export.ExportFragment
import com.mutumbakato.batchmanager.fragments.stats.StatisticsFragment
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.app_bar_home.*

class BatchHomeActivity : BaseActivity(), BatchHomeFragment.OnFeaturesFragmentInteractionListener {

    private var mBatchId: String = "0"
    private var featuresFragment: BatchHomeFragment? = null
    private var statisticsFragment: StatisticsFragment? = null
    private lateinit var viewModel: BatchViewModel
    private lateinit var mBatch: Batch
    private var position = 0
    private var mCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.app_bar_home)
        mBatchId = intent.getStringExtra("batch_id") ?: "0"
        viewModel = ViewModelProvider(this, ViewModelFactory.BatchViewModelFactory(
                RepositoryUtils.getStatisticsDataSource(this)))[BatchViewModel::class.java]

        viewModel.setBatchId(mBatchId)

        if (savedInstanceState != null) {
            position = savedInstanceState.getInt(POSITION)
        }

        initToolBar()
        initViewPager()

        viewModel.batch.observe(this, Observer { batch ->
            if (batch != null) {
                mBatch = batch
                batch_title!!.text = batch.name
                batch_age!!.text = DateUtils.toAge(batch.date!!)
                batch_card.setOnClickListener {
                    BatchDetailsActivity.start(this, batch.id, batch.name, batch.userId)
                }
            } else {
                finish()
            }
        })

        viewModel.batchCount.observe(this, Observer {
            it?.let {
                mCount = it.remaining
            }
        })
    }

    private fun initViewPager() {
        tabs.setupWithViewPager(container_view_pager)
        container_view_pager.adapter = OldPageAdapter(supportFragmentManager)
        container_view_pager.currentItem = position
    }

    private fun initToolBar() {
        toolbar.title = ""
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putInt(POSITION, container_view_pager!!.currentItem)
        super.onSaveInstanceState(outState)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_delete_batch -> {
                deleteBatch()
                return true
            }
            R.id.action_edit_batch -> {
                BatchFormActivity.start(this, mBatchId)
                return true
            }
            android.R.id.home -> finish()
            R.id.action_batch_details -> {
                BatchDetailsActivity.start(this, mBatchId, mBatch.name, mBatch.userId)
                return true
            }
            R.id.action_batch_export -> {
                ExportActivity.starts(this, mBatchId)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onFeaturesFragmentInteraction(item: Features.FeatureItem) {
        when (item.name) {
            "Batch" -> {
                BatchListActivity.start(this)
            }
            "Sales" -> {
                SalesActivity.start(this, mBatch.id, mBatch.name)
            }
            "Expenses" -> {
                ExpensesActivity.start(this, mBatch.id, mBatch.name)
            }
            "Mortality" -> {
                DeathActivity.start(this, mBatch.id, mBatch.name, mBatch.date!!, mBatch.closeDate)
            }
            "Vaccination" -> {
                VaccinationActivity.start(this, mBatch.vaccinationSchedule, mBatch.id, mBatch.name, mBatch.date!!)
            }
            "Eggs" -> {
                EggsActivity.start(this, mBatch.id, mBatch.name, mCount)
            }
            "Feeding" -> run {
                FeedingActivity.start(this, mBatch.id, mBatch.name, mBatch.date!!, mCount, mBatch.closeDate, mBatch.type)
            }
            "Body Weight" -> {
                WeightActivity.start(this, mBatch.id, mBatch.name, mBatch.date!!, mCount, mBatch.closeDate, mBatch.type)
            }
            "Water" -> {
                WaterActivity.start(this, mBatch.id, mBatch.name, mBatch.date!!, mCount, mBatch.closeDate)
            }
        }
    }

    private fun deleteBatch() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Confirm")
        builder.setMessage("Are you sure you want to delete this batch?\n\nAll the information about the batch will be lost forever.")
        builder.setPositiveButton("DELETE") { _, _ ->
            RepositoryUtils.getBatchRepo(this@BatchHomeActivity).deleteBatch(mBatchId)
            finish()
        }
        builder.setNegativeButton("CANCEL") { dialogInterface, _ -> dialogInterface.dismiss() }
        builder.create().show()
    }

    private fun showFeatures(): Fragment {
        if (featuresFragment == null) {
            featuresFragment = BatchHomeFragment.newInstance()
        }
        return featuresFragment as BatchHomeFragment
    }

    private fun showStatistics(): Fragment {
        if (statisticsFragment == null) {
            statisticsFragment = StatisticsFragment.newInstance(mBatchId)
        }
        return statisticsFragment as StatisticsFragment
    }

    private fun showExports(): Fragment {
        return ExportFragment.newInstance(mBatchId)
    }


    inner class SectionsPagerAdapter : FragmentStateAdapter(this) {

        override fun createFragment(position: Int): Fragment {
            return when (position) {
                0 -> showFeatures()
                1 -> showStatistics()
                2 -> showExports()
                else -> showFeatures()
            }
        }

        override fun getItemCount(): Int {
            return 2
        }

    }

    inner class OldPageAdapter constructor(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> showFeatures()
                1 -> showStatistics()
                2 -> showExports()
                else -> showFeatures()
            }
        }

        override fun getCount(): Int {
            return 2
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return tabLabels[position]
        }

    }

    companion object {
        val tabLabels = arrayListOf("Records", "Analysis", "Export")

        private const val POSITION = "position"
        fun start(context: Context, batchId: String) {
            val starter = Intent(context, BatchHomeActivity::class.java)
            starter.putExtra("batch_id", batchId)
            context.startActivity(starter)
        }
    }
}
