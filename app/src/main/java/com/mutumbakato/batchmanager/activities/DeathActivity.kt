package com.mutumbakato.batchmanager.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.deaths.DeathFormFragment
import com.mutumbakato.batchmanager.fragments.deaths.DeathListFragment
import com.mutumbakato.batchmanager.presenters.DeathFormPresenter
import com.mutumbakato.batchmanager.presenters.DeathListPresenter
import com.mutumbakato.batchmanager.ui.DeathFormContract
import com.mutumbakato.batchmanager.ui.DeathListContract
import com.mutumbakato.batchmanager.ui.animation.Shake
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.KeyBoard
import kotlinx.android.synthetic.main.activity_death_list.*
import kotlinx.android.synthetic.main.message_view.*
import kotlinx.android.synthetic.main.record_type_view.*

class DeathActivity : BaseActivity(), DeathFormContract.View.FormInteractionListener, DeathListContract.View.ListInteractionListener {

    private var mFormPresenter: DeathFormContract.Presenter? = null
    private lateinit var mBatchId: String
    private lateinit var mBatchName: String
    private lateinit var mDateOfBirth: String
    private var mCloseDate: String? = null
    private var isEditing: Boolean = false
    private lateinit var listPresenter: DeathListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_death_list)
        recordType.text = getString(R.string.mortality)
        mBatchId = intent.getStringExtra(BATCH_ID_EXTRA)
        mBatchName = intent.getStringExtra(BATCH_NAME_EXTRA)
        mDateOfBirth = intent.getStringExtra(DATE_OF_BIRTH)
        mCloseDate = intent.getStringExtra(CLOSE_DATE)

        initList()
        initForm()
        initToolBar()

        add_death_fab.setOnClickListener {
            mFormPresenter!!.startEditing(null, DateUtils.today())
        }

        return_fab.setOnClickListener {
            listPresenter.goToWeek(DateUtils.ageWeeksFromDate(mDateOfBirth))
            return_fab.visibility = View.INVISIBLE
            add_death_fab.visibility = View.VISIBLE
            app_bar.setExpanded(true)
        }
        button_chart_details.setOnClickListener {
            StatsDetailsActivity.start(this, mBatchId, "deaths")
        }
    }

    private fun initToolBar() {
        toolbar.title = mBatchName
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    internal fun edit(date: String) {
        if (!isEditing) {
            mFormPresenter!!.startEditing(null, date)
        }
    }

    override fun onDoneEditing() {
        isEditing = false
        KeyBoard.hideSoftKeyboard(this)
        add_death_fab.show()
    }

    override fun onStartEditing() {
        isEditing = true
        add_death_fab.hide()
    }

    override fun showForm(id: String, date: String) {
        mFormPresenter!!.startEditing(id, date)
    }

    override fun add(date: String) {
        edit(date)
    }

    override fun showTotal(total: String) {
        record_summary.text = "Total: $total Birds"
        if (isEditing) {
            record_summary.startAnimation(Shake.run(this))
        }
    }

    override fun showEmpty(isVisible: Boolean) {
        if (isVisible) {
            message_view!!.visibility = View.VISIBLE
            message_title!!.text = getString(R.string.no_deaths_found)
            message_image!!.setImageResource(R.drawable.no_deaths)
            message_description!!.text = getText(R.string.tap_to_add_deaths)
        } else {
            message_view!!.visibility = View.GONE
        }
    }

    override fun showProgress(isLoading: Boolean) {
        death_progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun applyPermissions(role: String) {

    }

    override fun onBackPressed() {
        if (isEditing) {
            mFormPresenter!!.stopEditing()
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    private fun initList() {
        val mListFragment = supportFragmentManager.findFragmentById(R.id.death_list_fragment) as DeathListFragment?
        listPresenter = DeathListPresenter(mBatchId, mDateOfBirth, RepositoryUtils.getDeathRepo(this), RepositoryUtils.getBatchRepo(this), mListFragment!!)
    }

    private fun initForm() {
        val deathFormFragment = supportFragmentManager.findFragmentById(R.id.death_form_fragment) as DeathFormFragment
        mFormPresenter = DeathFormPresenter(null, mBatchId, RepositoryUtils.getDeathRepo(this), deathFormFragment)
    }

    companion object {

        const val BATCH_ID_EXTRA = "batch_id"
        const val BATCH_NAME_EXTRA = "batch_name"
        const val DATE_OF_BIRTH = "date_of_birth"
        const val CLOSE_DATE = "close_date"

        fun start(context: Context, batchId: String, batchName: String, dateOfBirth: String, closeDate: String?) {
            val starter = Intent(context, DeathActivity::class.java)
            starter.putExtra(BATCH_ID_EXTRA, batchId)
            starter.putExtra(BATCH_NAME_EXTRA, batchName)
            starter.putExtra(DATE_OF_BIRTH, dateOfBirth)
            starter.putExtra(CLOSE_DATE, closeDate)
            context.startActivity(starter)
        }
    }
}
