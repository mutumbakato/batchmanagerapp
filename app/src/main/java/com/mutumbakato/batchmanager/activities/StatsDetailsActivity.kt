package com.mutumbakato.batchmanager.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.stats.*
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.toolbar.*

class StatsDetailsActivity : AppCompatActivity() {

    private lateinit var viewModel: BatchViewModel
    private lateinit var type: String
    private lateinit var mBatchId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats_details)
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.setDisplayHomeAsUpEnabled(true)
            it.title = "Details"
        }
        mBatchId = intent.getStringExtra("batch_id") ?: "0"
        type = intent.getStringExtra("type") ?: "performance"
        viewModel = ViewModelProvider(this, ViewModelFactory.BatchViewModelFactory(RepositoryUtils.getStatisticsDataSource(this)))[BatchViewModel::class.java].apply {
            setBatchId(mBatchId)
        }
        supportFragmentManager.beginTransaction().add(R.id.stats_details_frame, StatsDetailsFragment.newInstance(type)).commitNow()
        when (type) {
            "feeds" -> showFragment(FeedsChart.newInstance())
            "deaths" -> showFragment(DeathsChart.newInstance())
            "weight" -> showFragment(WeightChart.newInstance())
            "water" -> showFragment(WaterChart.newInstance())
            "eggs" -> showFragment(EggsChart.newInstance())
        }
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().add(R.id.stats_chart_frame, fragment).commitNow()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        fun start(context: Context, batchId: String, type: String) {
            val starter = Intent(context, StatsDetailsActivity::class.java)
            starter.putExtra("batch_id", batchId)
            starter.putExtra("type", type)
            context.startActivity(starter)
        }
    }

}
