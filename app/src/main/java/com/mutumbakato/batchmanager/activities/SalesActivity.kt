package com.mutumbakato.batchmanager.activities

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.Filter
import com.mutumbakato.batchmanager.data.models.Sale
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.ui.components.FilterDialog
import com.mutumbakato.batchmanager.utils.Currency
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.utils.UserRoles
import com.mutumbakato.batchmanager.utils.licence.LicenseUtils
import com.mutumbakato.batchmanager.viewmodels.SalesViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.activity_sales_list.*
import kotlinx.android.synthetic.main.message_view.*
import kotlinx.android.synthetic.main.toolbar.*

class SalesActivity : BaseActivity() {

    private var isEditing: Boolean = false
    private var mBatchName: String? = null
    private var mBatchId: String? = null
    private var filter = Filter()
    private lateinit var viewModel: SalesViewModel
    private var mode = "all"

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sales_list)
        viewModel = ViewModelProvider(this,
                ViewModelFactory.SalesViewModelFactory(RepositoryUtils.getSalesLiveRepo(this)))
                .get(SalesViewModel::class.java).apply {
                    setFarmId(PreferenceUtils.farmId)
                }

        mBatchId = intent.getStringExtra(BATCH_ID_EXTRA)
        mBatchName = intent.getStringExtra(BATCH_NAME_EXTRA)
        initToolBar()

        if (LicenseUtils.isLicenceExpired()) {
            sales_add_fab!!.visibility = View.GONE
        }

        sales_add_fab.setOnClickListener {
            edit()
        }

        viewModel.isEditing.observe(this, Observer {
            isEditing = it
            if (!it) {
                onDoneEditing()
            } else {
                onStartEditing()
            }
        })

        viewModel.filteredSales.observe(this, Observer {
            if (mode == "filter")
                showEmpty(it == null || it.isEmpty())
            if (it != null)
                showTotal(Currency.format(getSalesTotal(it)))
        })

        viewModel.isLoading.observe(this, Observer {
            showProgress(it)
        })

        viewModel.filter.observe(this, Observer {
            filter = it
            it?.let {
                sales_filter_name.text = "${it.name}"
//              sales_filter_period.text = it.rangeName
            }
        })

        viewModel.mode.observe(this, Observer {
            mode = it
//          sales_filter_container.visibility = if (it == "filter") View.VISIBLE else View.GONE
            sales_textView_total.visibility = if (it == "filter") View.VISIBLE else View.INVISIBLE
//          sales_filter_period.visibility = if (it == "filter") View.VISIBLE else View.GONE
        })

        viewModel.sales.observe(this, Observer {
            if (mode == "all") {
                showEmpty(it == null || it.isEmpty())
            }
        })
    }

    private fun initToolBar() {
        toolbar!!.title = "Sales"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onBackPressed() {
        if (isEditing) {
            viewModel.isEditing.postValue(false)
        } else
            super.onBackPressed()
    }

    internal fun edit() {
        if (!isEditing) {
            viewModel.isEditing.postValue(true)
        }
    }

    fun showTotal(total: String) {
        sales_textView_total!!.text = "${PreferenceUtils.currency} $total"
    }

    fun showEmpty(isVisible: Boolean) {
        if (isVisible) {
            message_view!!.visibility = View.VISIBLE
            message_title!!.text = getString(R.string.no_sales_fount)
            message_image!!.setImageResource(R.drawable.no_sales)
            message_description!!.text = getString(R.string.add_sales_message)
        } else {
            message_view!!.visibility = View.GONE
        }
    }

    fun showProgress(isLoading: Boolean) {
        sales_progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    @SuppressLint("RestrictedApi")
    fun applyPermissions(role: String) {
        sales_add_fab!!.visibility = if (UserRoles.canCreate(role)) View.VISIBLE else View.GONE
    }

    fun onDoneEditing() {
        KeyBoard.hideSoftKeyboard(this)
        if (!LicenseUtils.isLicenceExpired())
            sales_add_fab!!.show()
    }

    fun onStartEditing() {
        sales_add_fab!!.hide()
    }

    private fun showFilter() {
        FilterDialog.newInstance(getString(R.string.filter_sales), filter.toString()).apply {
            setOnApply {
                viewModel.applyFilter(it)
            }
            onClearFilter {
                viewModel.clearFilter()
                viewModel.setFarmId(PreferenceUtils.farmId)
            }
            show(supportFragmentManager, "filter_sales")
        }
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_filter, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        else if (item.itemId == R.id.action_filter)
            showFilter()
        return super.onOptionsItemSelected(item)
    }

    private fun getSalesTotal(sales: List<Sale>): Float {
        var total = 0f
        sales.forEach {
            total += it.totalAmount
        }
        return total
    }

    companion object {

        private const val BATCH_ID_EXTRA = "batch_id"
        private const val BATCH_NAME_EXTRA = "batch_name"

        fun start(context: Context, batchId: String, batchName: String, mode: String = "all") {
            val starter = Intent(context, SalesActivity::class.java)
            starter.putExtra(BATCH_ID_EXTRA, batchId)
            starter.putExtra(BATCH_NAME_EXTRA, batchName)
            context.startActivity(starter)
        }
    }
}
