package com.mutumbakato.batchmanager.activities.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.viewmodels.UserViewModel
import kotlinx.android.synthetic.main.fragment_account.*

/**
 * A placeholder fragment containing a simple view.
 */
class AccountFragment : Fragment() {

    private lateinit var viewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(UserViewModel::class.java)
        viewModel.setUserId(FirebaseAuth.getInstance().currentUser?.uid ?: "")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_account, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.user.observe(this, Observer {
            if (it != null) {
                user_name.setText(it.name)
                user_email_input.setText(it.email)
            }
        })
        update_profile_button.setOnClickListener {
            val userName = user_name.text.toString().trim()
            if (userName.isNotEmpty()) {
                showProgress(true)
                viewModel.saveUserName(userName) {
                    showProgress(false)
                    Snackbar.make(update_profile_button, "Success", Snackbar.LENGTH_LONG).show()
                }
            }
        }

        password_reset_button.setOnClickListener {
            PasswordResetActivity.start(context!!, user_email_input.text.toString().trim())
        }
    }

    fun showProgress(isShowing: Boolean) {
        update_profile_button.isEnabled = !isShowing
        account_progress.visibility = if (isShowing) View.VISIBLE else View.GONE
    }
}
