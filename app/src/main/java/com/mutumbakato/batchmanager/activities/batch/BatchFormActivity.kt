package com.mutumbakato.batchmanager.activities.batch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.batch.BatchFormFragment
import com.mutumbakato.batchmanager.presenters.BatchFromPresenter
import kotlinx.android.synthetic.main.toolbar.*

class BatchFormActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_batch_form)
        toolbar.setNavigationIcon(R.drawable.ic_action_close)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val batchFormFragment = supportFragmentManager.findFragmentById(R.id.batch_form_fragment) as BatchFormFragment?
        val batchId = intent.getStringExtra(EXTRA_BATCH_ID)
        title = if (batchId == null) "New Batch" else "Update Batch"
        BatchFromPresenter(batchId, RepositoryUtils.getBatchRepo(this), batchFormFragment!!, batchId != null)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private const val EXTRA_BATCH_ID = "batch_id"

        fun start(context: Context, batchId: String?) {
            val starter = Intent(context, BatchFormActivity::class.java)
            starter.putExtra(EXTRA_BATCH_ID, batchId)
            context.startActivity(starter)
        }
    }
}
