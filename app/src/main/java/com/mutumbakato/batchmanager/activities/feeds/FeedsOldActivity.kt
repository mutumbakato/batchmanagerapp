package com.mutumbakato.batchmanager.activities.feeds

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.viewmodels.FeedsViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.toolbar_flat.*

class FeedsOldActivity : AppCompatActivity() {

    private lateinit var viewModel: FeedsViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_feeds_old)
        toolbar.title = "Old Feeds"
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        viewModel = ViewModelProvider(this,
                ViewModelFactory.FeedsViewModelFactory(RepositoryUtils.getFeedsRepo(this),
                        RepositoryUtils.getBatchRepo(this))).get(FeedsViewModel::class.java)
        viewModel.setBatchId(intent.getStringExtra("batch_id"))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_feeds_old, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        if (item.itemId == R.id.action_why_old) {
            AlertDialog.Builder(this).apply {
                setTitle("Why old feeds")
                setMessage(getString(R.string.old_feeds_description))
                setPositiveButton("Okay") { dialogInterface, _ ->
                    dialogInterface.dismiss()
                }
                show()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun start(context: Context, batchId: String) {
            context.startActivity(Intent(context, FeedsOldActivity::class.java).apply {
                putExtra("batch_id", batchId)
            })
        }
    }
}
