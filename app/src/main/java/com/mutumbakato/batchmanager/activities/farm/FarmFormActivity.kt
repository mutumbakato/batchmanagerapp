package com.mutumbakato.batchmanager.activities.farm

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.farm.FarmFormFragment
import com.mutumbakato.batchmanager.presenters.FarmFormPresenter
import kotlinx.android.synthetic.main.toolbar.*

class FarmFormActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_farm_form)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val mFarmId = intent.getStringExtra(FARM_ID)
        val farmFormFragment = supportFragmentManager.findFragmentById(R.id.farm_form_fragment) as FarmFormFragment?
        FarmFormPresenter(mFarmId, RepositoryUtils.getFarmRepo(this), farmFormFragment!!)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        private const val FARM_ID = "farm_id"

        fun start(context: Context, farmId: String?) {
            val starter = Intent(context, FarmFormActivity::class.java)
            starter.putExtra(FARM_ID, farmId)
            context.startActivity(starter)
        }
    }
}
