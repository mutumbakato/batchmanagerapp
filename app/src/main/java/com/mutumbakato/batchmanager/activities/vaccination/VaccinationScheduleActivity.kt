package com.mutumbakato.batchmanager.activities.vaccination

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.vaccination.ScheduleListFragment
import com.mutumbakato.batchmanager.presenters.VaccinationSchedulePresenter
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_vaccination_schedule.*
import kotlinx.android.synthetic.main.toolbar.*

class VaccinationScheduleActivity : AppCompatActivity(), ScheduleListFragment.OnListFragmentInteractionListener {

    private var mBatchId: String? = null
    private var dateOfBirth: String? = null
    private lateinit var scheduleListFragment: ScheduleListFragment
    private var progress: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vaccination_schedule)
        mBatchId = Prefs.getString(PreferenceUtils.CURRENT_BATCH, "")
        dateOfBirth = intent.getStringExtra(VaccinationActivity.EXTRA_DOB)
        progress = findViewById(R.id.schedule_progress)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        fab.setOnClickListener { scheduleListFragment.showAddSchedule() }
        initFragment()
    }

    override fun onListFragmentInteraction(item: VaccinationSchedule) {
        VaccinationScheduleDetailsActivity.start(this, item.id, item.title)
    }

    override fun onUseSchedule(schedule: VaccinationSchedule) {
        val i = Intent()
        setResult(Activity.RESULT_OK, i)
        finish()
    }

    override fun onRemoveSchedule(schedule: String) {
        val i = Intent()
        setResult(Activity.RESULT_OK, i)
    }

    override fun updateSchedule(schedule: VaccinationSchedule) {
    }

    override fun showProgress(isLoading: Boolean) {
        progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    private fun initFragment() {
        scheduleListFragment = supportFragmentManager.findFragmentById(R.id.schedule_fragment) as ScheduleListFragment
        VaccinationSchedulePresenter(mBatchId!!, RepositoryUtils.getScheduleRepo(this), scheduleListFragment)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menu.add("Refresh").setIcon(R.drawable.ic_action_refresh)
                .setOnMenuItemClickListener {
                    scheduleListFragment.refresh()
                    true
                }.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun start(context: Context, farmId: String, currentSchedule: String) {
            val starter = Intent(context, VaccinationScheduleActivity::class.java)
            starter.putExtra("farm_id", farmId)
            starter.putExtra(VaccinationActivity.SCHEDULE_ID_EXTRA, currentSchedule)
            context.startActivity(starter)
        }
    }
}
