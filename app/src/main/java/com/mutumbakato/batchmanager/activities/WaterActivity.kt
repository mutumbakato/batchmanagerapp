package com.mutumbakato.batchmanager.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.UserRoles
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import com.mutumbakato.batchmanager.viewmodels.WaterViewModel
import kotlinx.android.synthetic.main.record_type_view.*
import kotlinx.android.synthetic.main.water_activity.*
import java.util.*

class WaterActivity : AppCompatActivity() {

    private lateinit var batchName: String
    private lateinit var viewModel: WaterViewModel
    private lateinit var batchViewModel: BatchViewModel
    private lateinit var batchId: String
    private lateinit var dateOfBirth: String
    private var closeDate: String? = null
    private var isFormOpen: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel = ViewModelProvider(this,
                ViewModelFactory.WaterViewModelFactory(RepositoryUtils.getWaterRepository(this),
                        RepositoryUtils.getBatchRepo(this)))[WaterViewModel::class.java]
        batchViewModel = ViewModelProvider(this, ViewModelFactory.BatchViewModelFactory(RepositoryUtils.getStatisticsDataSource(this)))[BatchViewModel::class.java]

        setContentView(R.layout.water_activity)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        batchName = intent.getStringExtra(BATCH_NAME_EXTRA)
        batchId = intent.getStringExtra(BATCH_ID_EXTRA)
        dateOfBirth = intent.getStringExtra(DOB_EXTRA)
        closeDate = intent.getStringExtra(CLOSE_DATE_EXTRA)

        supportActionBar?.title = batchName
        viewModel.setBatchId(batchId)
        viewModel.setDateOfBirth(dateOfBirth)
        batchViewModel.setBatchId(batchId)

        viewModel.isFormOpen.observe(this, Observer {
            isFormOpen = it
            if (it) {
                add_water_fab.hide()
            } else {
                add_water_fab.show()
            }
        })

        viewModel.userRole.observe(this, Observer {
            add_water_fab.visibility = if (it == UserRoles.USER_GUEST) View.GONE else View.VISIBLE
        })

        batchViewModel.totalWater.observe(this, Observer {
            it?.let {
                record_summary.text = String.format("Total: %.1f Ltr", it)
            }
        })

        recordType.text = getString(R.string.water_consumption)
        add_water_fab.setOnClickListener {
            viewModel.setDate(DateUtils.today())
        }

        water_return_fab.setOnClickListener {
            viewModel.gotTo.postValue(UUID.randomUUID().toString())
            water_return_fab.visibility = View.INVISIBLE
            add_water_fab.visibility = View.VISIBLE
            app_bar.setExpanded(true)
        }

        button_chart_details.setOnClickListener {
            StatsDetailsActivity.start(this, batchId, "water")
        }
    }

    override fun onBackPressed() {
        if (isFormOpen) {
            viewModel.closeForm()
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    companion object {

        const val BATCH_ID_EXTRA = "batch_id"
        const val BATCH_NAME_EXTRA = "batch_name"
        const val DOB_EXTRA = "date_of_birth"
        const val BATCH_COUNT_EXTRA = "batch_count"
        const val CLOSE_DATE_EXTRA = "close_date"

        fun start(context: Context, batchId: String, batchName: String, dateOfBirth: String, batchCount: Int, closeDate: String?) {
            val intent = Intent(context, WaterActivity::class.java).apply {
                putExtra(BATCH_ID_EXTRA, batchId)
                putExtra(BATCH_NAME_EXTRA, batchName)
                putExtra(DOB_EXTRA, dateOfBirth)
                putExtra(BATCH_COUNT_EXTRA, batchCount)
                putExtra(CLOSE_DATE_EXTRA, closeDate)
            }
            context.startActivity(intent)
        }
    }
}
