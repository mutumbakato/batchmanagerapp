package com.mutumbakato.batchmanager.activities.feeds

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.activities.StatsDetailsActivity
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.utils.DateUtils
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.utils.UserRoles
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.FeedsViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.activity_feeding_list.*
import kotlinx.android.synthetic.main.message_view.*
import kotlinx.android.synthetic.main.record_type_view.*
import java.util.*

class FeedingActivity : BaseActivity() {

    private var mBatchId: String? = null
    private var mDateOfBirth: String? = null
    private var mCloseDate: String? = null
    private var mBatchName: String? = null
    private var isEditing: Boolean = false

    private lateinit var viewModel: FeedsViewModel
    private lateinit var batchViewModel: BatchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProvider(this, ViewModelFactory.FeedsViewModelFactory(
                RepositoryUtils.getFeedsRepo(this),
                RepositoryUtils.getBatchRepo(this)
        )).get(FeedsViewModel::class.java)

        setContentView(R.layout.activity_feeding_list)

        batchViewModel = ViewModelProvider(this, ViewModelFactory.BatchViewModelFactory(RepositoryUtils.getStatisticsDataSource(this)))[BatchViewModel::class.java]

        recordType.text = getString(R.string.feeding)
        mBatchId = intent.getStringExtra(BATCH_ID_EXTRA)
        mBatchName = intent.getStringExtra(BATCH_NAME_EXTRA)
        mDateOfBirth = intent.getStringExtra(DATE_OF_BIRTH)
        mCloseDate = intent.getStringExtra(CLOSE_DATE)

        viewModel.setBatchId(mBatchId!!)
        viewModel.setDateOfBirth(mDateOfBirth!!)
        batchViewModel.setBatchId(mBatchId!!)

        initToolBar()

        add_feeds_fab.setOnClickListener {
            edit()
        }

        feeds_return_fab.setOnClickListener {
            viewModel.goTo.postValue(UUID.randomUUID().toString())
            feeds_return_fab.visibility = View.INVISIBLE
            add_feeds_fab.visibility = View.VISIBLE
            app_bar.setExpanded(true)
        }

        button_chart_details.setOnClickListener {
            StatsDetailsActivity.start(this, mBatchId!!, "feeds")
        }

        viewModel.isLoading.observe(this, Observer {
            showProgress(it)
        })

        viewModel.isFormOpen.observe(this, Observer {
            if (it) {
                onStartEditing()
            } else {
                onDoneEditing()
            }
        })

        viewModel.userRole.observe(this, Observer {
            applyPermissions(it ?: UserRoles.USER_GUEST)
        })

        batchViewModel.totalFeeds.observe(this, Observer {
            it?.let {
                record_summary.text = String.format("Total: %.1f Kg", it)
            }
        })
    }

    private fun initToolBar() {
        toolbar.title = mBatchName
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
    }

    private fun edit() {
        if (!isEditing) {
            viewModel.setDate(DateUtils.today())
        }
    }

    fun onDoneEditing() {
        isEditing = false
        KeyBoard.hideSoftKeyboard(this)
        add_feeds_fab.show()
    }

    fun onStartEditing() {
        isEditing = true
        add_feeds_fab.hide()
    }

    fun showProgress(isLoading: Boolean) {
        feeding_progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    fun applyPermissions(role: String) {
        add_feeds_fab.visibility = if (UserRoles.canCreate(role)) View.VISIBLE else View.GONE
    }

    fun showForm(id: String?, date: String?) {
        viewModel.setDate(date)
        viewModel.setFeedsId(id)
    }

    //Never show empty when there is a timeline
    fun showEmpty(isVisible: Boolean) {
        if (isVisible) {
            message_view!!.visibility = View.VISIBLE
            message_title!!.text = getString(R.string.no_feeds_found)
            message_image!!.setImageResource(R.drawable.no_feeds)
            message_description!!.text = getString(R.string.tap_to_add_feeds)
        } else {
            message_view!!.visibility = View.GONE
        }
    }

    override fun onBackPressed() {
        if (isEditing) {
            viewModel.setDate(null)
        } else
            super.onBackPressed()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_feeds, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        } else if (item.itemId == R.id.action_old_feeds) {
            FeedsOldActivity.start(this, mBatchId!!)
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {

        private const val BATCH_ID_EXTRA = "batch_id"
        private const val BATCH_NAME_EXTRA = "batch_name"
        const val BATCH_COUNT = "batch_count"
        const val DATE_OF_BIRTH = "date_of_birth"
        const val CLOSE_DATE = "close_date"
        const val BATCH_TYPE = "batch_type"

        fun start(context: Context, batchId: String, batchName: String, dateOfBirth: String, batchCount: Int,
                  closeDate: String?, batchType: String) {
            val starter = Intent(context, FeedingActivity::class.java)
            starter.putExtra(BATCH_ID_EXTRA, batchId)
            starter.putExtra(BATCH_NAME_EXTRA, batchName)
            starter.putExtra(DATE_OF_BIRTH, dateOfBirth)
            starter.putExtra(BATCH_COUNT, batchCount)
            starter.putExtra(CLOSE_DATE, closeDate)
            starter.putExtra(BATCH_TYPE, batchType)
            context.startActivity(starter)
        }
    }
}
