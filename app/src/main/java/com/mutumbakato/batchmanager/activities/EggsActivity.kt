package com.mutumbakato.batchmanager.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.repository.EggsRepository
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.eggs.EggsFormFragment
import com.mutumbakato.batchmanager.fragments.eggs.EggsListFragment
import com.mutumbakato.batchmanager.presenters.EggsFormPresenter
import com.mutumbakato.batchmanager.presenters.EggsListPresenter
import com.mutumbakato.batchmanager.ui.EggsFormContract
import com.mutumbakato.batchmanager.ui.EggsListContract
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.mutumbakato.batchmanager.utils.UserRoles
import com.mutumbakato.batchmanager.utils.licence.LicenseUtils
import kotlinx.android.synthetic.main.activity_eggs.*
import kotlinx.android.synthetic.main.message_view.*
import kotlinx.android.synthetic.main.record_type_view.*
import kotlinx.android.synthetic.main.toolbar.*

class EggsActivity : BaseActivity(), EggsFormContract.View.FormInteractionListener,
        EggsListContract.View.ListInteractionListener {

    private var mFormPresenter: EggsFormPresenter? = null
    private var mRepo: EggsRepository? = null
    private var mBatchName: String? = null
    private var isEditing: Boolean = false
    private var mBatchId: String? = null
    private var mCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_eggs)
        mBatchId = intent.getStringExtra(BATCH_ID_EXTRA)
        mBatchName = intent.getStringExtra(BATCH_NAME_EXTRA)
        mCount = intent.getIntExtra(BATCH_COUNT_EXTRA, 0)
        mRepo = RepositoryUtils.getEggsRepo(this)

        initEggsListFragment()
        initEggsForm(null)
        initToolBar()

        if (LicenseUtils.isLicenceExpired()) {
            fab!!.visibility = View.GONE
        }

        fab.setOnClickListener {
            edit()
        }

        button_chart_details.setOnClickListener {
            StatsDetailsActivity.start(this, mBatchId ?: "0", "eggs")
        }
    }

    private fun initToolBar() {
        recordType.text = getString(R.string.egg_production)
        toolbar!!.title = mBatchName
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    internal fun edit() {
        if (!isEditing) {
            mFormPresenter!!.startEditing(null)
        } else {
            mFormPresenter!!.stopEditing()
        }
    }

    override fun onDoneEditing() {
        isEditing = false
        KeyBoard.hideSoftKeyboard(this@EggsActivity)
        if (!LicenseUtils.isLicenceExpired()) {
            fab!!.show()
        }
    }

    override fun onStartEditing() {
        isEditing = true
        fab!!.hide()
    }

    override fun showForm(id: String) {
        mFormPresenter!!.startEditing(id)
    }

    override fun showTotal(total: String) {
        record_summary!!.text = "Total: $total Eggs"
    }

    override fun showEmpty(visible: Boolean) {
        if (visible) {
            message_view!!.visibility = View.VISIBLE
            message_title!!.text = getText(R.string.no_eggs_text)
            message_image!!.setImageResource(R.drawable.no_egg)
            message_description!!.setText(R.string.create_eggs)
        } else {
            message_view!!.visibility = View.GONE
        }
    }

    override fun showProgress(isLoading: Boolean) {
        eggs_progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun applyPermissions(role: String) {
        fab!!.visibility = if (role == UserRoles.USER_ADMIN || role == UserRoles.USER_WORKER) View.VISIBLE else View.GONE
    }

    override fun onBackPressed() {
        if (isEditing) {
            mFormPresenter!!.stopEditing()
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initEggsListFragment() {
        if (mRepo == null)
            return
        val mEggsList = supportFragmentManager.findFragmentById(R.id.eggs_list_fragment) as EggsListFragment?
        EggsListPresenter(mBatchId!!, mRepo!!, RepositoryUtils.getBatchRepo(this), mEggsList!!)
    }

    private fun initEggsForm(id: String?) {
        if (mRepo == null)
            return
        val eggsFormFragment = supportFragmentManager.findFragmentById(R.id.eggs_form_fragment) as EggsFormFragment?
        mFormPresenter = EggsFormPresenter(id, mBatchId!!, mCount, mRepo!!, eggsFormFragment)
    }

    companion object {

        private const val BATCH_ID_EXTRA = "batch_id"
        private const val BATCH_NAME_EXTRA = "batch_name"
        private const val BATCH_COUNT_EXTRA = "batch_count"

        fun start(context: Context, batchId: String, batchName: String, count: Int) {
            val starter = Intent(context, EggsActivity::class.java)
            starter.putExtra(BATCH_ID_EXTRA, batchId)
            starter.putExtra(BATCH_NAME_EXTRA, batchName)
            starter.putExtra(BATCH_COUNT_EXTRA, count)
            context.startActivity(starter)
        }
    }

}
