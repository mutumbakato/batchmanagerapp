package com.mutumbakato.batchmanager.activities.user

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.repository.UserRepository
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.utils.KeyBoard
import kotlinx.android.synthetic.main.activity_password_reset_actvity.*
import kotlinx.android.synthetic.main.toolbar.*

class PasswordResetActivity : BaseActivity() {

    private var userRepository: UserRepository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_password_reset_actvity)
        userRepository = RepositoryUtils.getUserRepo(this)
        toolbar!!.title = "Reset Password"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        email_input.setText(intent.getStringExtra("email"))
        email_send.setOnClickListener {
            send()
        }
    }

    private fun send() {
        KeyBoard.hideSoftKeyboard(this)
        val email = email_input!!.text!!.toString().trim { it <= ' ' }
        if (email.isNotEmpty() && email.contains("@")) {
            showProgress(true)
            userRepository!!.resetPassword(email,
                    object : UserDataSource.PasswordCallBack {
                        override fun onSuccess(message: String) {
                            showProgress(false)
                            Snackbar.make(email_input!!, message, Snackbar.LENGTH_INDEFINITE).run {
                                setAction("OK") { dismiss() }.show()
                            }
                        }

                        override fun onError(message: String) {
                            showProgress(false)
                            Snackbar.make(email_input!!, message, Snackbar.LENGTH_LONG).run {
                                setAction("OK") { dismiss() }.show()
                            }
                        }
                    })
            email_input!!.text = null
        } else {
            Snackbar.make(email_input!!, "Please enter a valid email.", Snackbar.LENGTH_LONG).show()
        }
    }

    private fun showProgress(isVisible: Boolean) {
        password_reset_progress.visibility = if (isVisible) View.VISIBLE else View.GONE
        email_send.isEnabled = !isVisible
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun start(context: Context, email: String = "") {
            val starter = Intent(context, PasswordResetActivity::class.java)
            starter.putExtra("email", email)
            context.startActivity(starter)
        }
    }
}

