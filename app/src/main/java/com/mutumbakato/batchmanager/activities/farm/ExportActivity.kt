package com.mutumbakato.batchmanager.activities.farm

import android.Manifest.permission.WRITE_EXTERNAL_STORAGE
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.models.exports.ExportConfig
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.ui.adapters.ExportViewPagerAdapter
import com.mutumbakato.batchmanager.utils.Utils
import com.mutumbakato.batchmanager.viewmodels.ExportViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory
import kotlinx.android.synthetic.main.export_activity.*
import kotlinx.android.synthetic.main.toolbar_flat.*
import java.io.File

class ExportActivity : AppCompatActivity() {

    private lateinit var viewModel: ExportViewModel
    private var exportConfig: ExportConfig = ExportConfig()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.export_activity)
        //Check for storage permissions
        isStoragePermissionGranted()
        setSupportActionBar(toolbar)
        supportActionBar?.let {
            it.title = "Export Data"
            it.setDisplayHomeAsUpEnabled(true)
        }

        val batchId = intent.getStringExtra("batch_id")
        viewModel = ViewModelProvider(viewModelStore, ViewModelFactory.ExportViewModelFactory(RepositoryUtils.getStatisticsDataSource(this))).get(ExportViewModel::class.java)
        batchId?.let {
            viewModel.setBatchId(batchId)
        }

        export_view_pager.adapter = ExportViewPagerAdapter(this)
        export_view_pager.isUserInputEnabled = false
        export_view_pager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                when (position) {
                    0 -> {
                        export_button.visibility = View.VISIBLE
                    }
                    1 -> {
                        export_button.visibility = View.GONE
                    }
                }
            }
        })

        viewModel.exportConfig.observe(this, Observer {
            it?.let {
                exportConfig = it
            }
        })
        export_button.setOnClickListener {
            if (exportConfig.batchId.isNotEmpty()) {
                val path = "${Environment.getExternalStorageDirectory().path}/${Utils.BASE_FILE_PATH}/${exportConfig.batchName}.xls"
                viewModel.export(File(path), {
                    export_view_pager.currentItem = 3
                }, {
                    Snackbar.make(export_button, it, Snackbar.LENGTH_LONG).show()
                })
            } else {
                Snackbar.make(export_button, "No export data found", Snackbar.LENGTH_LONG).show()
            }
        }

        viewModel.exportRunning.observe(this, Observer {
            export_progress.visibility = if (it) View.VISIBLE else View.GONE
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    private fun isStoragePermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(WRITE_EXTERNAL_STORAGE), 1)
                false
            }
        } else {
            true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
            finish()
        }
    }

    companion object {
        fun starts(context: Context, batchId: String? = null) {
            context.startActivity(Intent(context, ExportActivity::class.java).apply {
                putExtra("batch_id", batchId)
            })
        }
    }
}
