package com.mutumbakato.batchmanager.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import com.mutumbakato.batchmanager.R

class About : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }

    companion object {

        fun start(context: Context) {
            val starter = Intent(context, About::class.java)
            context.startActivity(starter)
        }
    }
}
