package com.mutumbakato.batchmanager.activities

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    protected fun addFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.beginTransaction()
                .add(frameId, fragment)
                .commit()
    }

    protected fun replaceFragment(fragment: Fragment, frameId: Int) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(frameId, fragment)
                .commit()
    }

}
