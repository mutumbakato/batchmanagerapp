package com.mutumbakato.batchmanager.activities.user

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.batch.BatchListActivity
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.data.preferences.PreferenceUtils
import com.mutumbakato.batchmanager.data.repository.UserRepository
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.utils.KeyBoard
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.activity_register.*

/**
 * A login screen that offers login via email/password.
 */
class RegisterActivity : AppCompatActivity(), UserDataSource.UserCallBack {

    private var userRepository: UserRepository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        symbol!!.setFragmentManager(supportFragmentManager)
        password!!.setOnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptRegister()
            }
            false
        }

        val mEmailSignInButton = findViewById<Button>(R.id.email_sign_in_button)
        mEmailSignInButton.setOnClickListener { attemptRegister() }
        userRepository = RepositoryUtils.getUserRepo(this)
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptRegister() {

        KeyBoard.hideSoftKeyboard(this@RegisterActivity)
        // Reset errors.
        email_label!!.error = null
        password_label!!.error = null
        // Store values at the time of the login attempt.
        val email = this.email!!.text.toString().trim { it <= ' ' }
        val password = this.password!!.text.toString().trim { it <= ' ' }
        val name = this.name!!.text.toString().trim { it <= ' ' }
        val phone = "none"//mPhone.getText().toString().trim();
        var cancel = false
        var focusView: View? = null

        if (TextUtils.isEmpty(name)) {
            name_label!!.error = "Please add a valid name"
            focusView = this.name
            cancel = true
        } else {
            name_label!!.error = null
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            email_label!!.error = "Please use a valid email"
            focusView = this.email
            cancel = true
        } else if (!isEmailValid(email)) {
            email_label!!.error = "Please use a valid email"
            focusView = this.email
            cancel = true
        } else {
            email_label!!.error = null
        }

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password) && isPasswordValid(password)) {
            password_label!!.error = "Please set a valid password"
            focusView = this.password
            cancel = true
        } else if (password.length < 6) {
            password_label!!.error = "Password must be at least 6 characters"
            focusView = this.password
            cancel = true
        } else {
            password_label!!.error = null
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView!!.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
            register(name, email, phone, password)
        }
    }

    private fun register(name: String, email: String, phone: String, password: String) {
        showProgress(true)
        val user = User(null, name, email, password, phone)
        Prefs.putString(PreferenceUtils.USER_TOKEN, "0")
        userRepository!!.register(user, this)
    }

    private fun isEmailValid(email: String): Boolean {
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length >= 4
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)
        login_form!!.visibility = if (show) View.GONE else View.VISIBLE
        login_form!!.animate().setDuration(shortAnimTime.toLong()).alpha(
                (if (show) 0 else 1).toFloat()).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                login_form!!.visibility = if (show) View.GONE else View.VISIBLE
            }
        })

        login_progress!!.visibility = if (show) View.VISIBLE else View.GONE
        login_progress!!.animate().setDuration(shortAnimTime.toLong()).alpha(
                (if (show) 1 else 0).toFloat()).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                login_progress!!.visibility = if (show) View.VISIBLE else View.GONE
            }
        })
    }

    override fun onSuccess(user: User?) {
        showProgress(false)
        BatchListActivity.start(this)
        finish()
    }

    override fun onError(message: String) {
        showProgress(false)
        showError(message)
    }

    private fun showError(message: String) {
        Snackbar.make(email!!, message, Snackbar.LENGTH_LONG).show()
    }

    fun hideSoftKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    companion object {
        fun start(context: Context) {
            val starter = Intent(context, RegisterActivity::class.java)
            context.startActivity(starter)
        }
    }
}

