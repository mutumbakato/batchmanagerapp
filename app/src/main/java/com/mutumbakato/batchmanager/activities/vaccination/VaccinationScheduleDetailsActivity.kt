package com.mutumbakato.batchmanager.activities.vaccination

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.vaccination.VaccinationScheduleDetailsFragment
import com.mutumbakato.batchmanager.presenters.VaccinationPresenter
import com.mutumbakato.batchmanager.ui.VaccinationContract
import kotlinx.android.synthetic.main.activity_vaccination.*

class VaccinationScheduleDetailsActivity : BaseActivity(), VaccinationContract.View.ListInteractionListener {

    private var mScheduleId: String? = null
    private var mScheduleName: String? = null
    private var mPresenter: VaccinationPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vaccination_schedule_details)
        mScheduleId = intent.getStringExtra(EXTRA_SCHEDULE_ID)
        mScheduleName = intent.getStringExtra(SCHEDULE_NAME_EXTRA)
        vaccination_add_fab!!.setOnClickListener {
            VaccinationFormActivity.start(this@VaccinationScheduleDetailsActivity, mScheduleId!!, "")
        }
        toolbar!!.title = mScheduleName
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initVaccinationFragment()
    }

    private fun initVaccinationFragment() {
        val vaccinationFragment = supportFragmentManager.findFragmentById(R.id.vaccination_fragment) as
                VaccinationScheduleDetailsFragment
        mPresenter = VaccinationPresenter(mScheduleId, "", RepositoryUtils.getVaccinationRepo(this),
                RepositoryUtils.getScheduleRepo(this), vaccinationFragment)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            mScheduleId = data!!.getStringExtra(EXTRA_SCHEDULE_ID)
            mPresenter!!.setSchedule(mScheduleId!!)
            mPresenter!!.start()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_edit_batch) {
            return true
        } else if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showForm(id: String) {
        VaccinationFormActivity.start(this, id, "")
    }

    @SuppressLint("RestrictedApi")
    override fun hideFab(total: String) {
        //vaccination_add_fab!!.visibility = View.GONE
    }

    override fun edit(scheduleId: String, vaccinationId: String) {
        VaccinationFormActivity.start(this, scheduleId, vaccinationId)
    }

    companion object {
        const val EXTRA_SCHEDULE_ID = "schedule_id"
        const val SCHEDULE_NAME_EXTRA = "scheduleF_name"
        fun start(context: Context, scheduleId: String, scheduleName: String?) {
            val starter = Intent(context, VaccinationScheduleDetailsActivity::class.java)
            starter.putExtra(EXTRA_SCHEDULE_ID, scheduleId)
            starter.putExtra(SCHEDULE_NAME_EXTRA, scheduleName)
            context.startActivity(starter)
        }
    }
}
