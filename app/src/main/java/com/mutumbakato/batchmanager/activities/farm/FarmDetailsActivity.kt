package com.mutumbakato.batchmanager.activities.farm

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.firestore.UserFireStore
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.farm.FarmDetailsFragment
import com.mutumbakato.batchmanager.presenters.FarmDetailsPresenter
import kotlinx.android.synthetic.main.toolbar.*

class FarmDetailsActivity : AppCompatActivity() {

    private var farmId: String? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_farm_details)
        toolbar.title = getString(R.string.farm)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        farmId = intent.getStringExtra(FARM_ID_EXTRA)
        showFarmDetails()
        if (isLocationPermissionGranted()) {
            findLocation()
        }
    }

    private fun showFarmDetails() {
        val fragment =
                supportFragmentManager.findFragmentById(R.id.farm_details_fragment) as FarmDetailsFragment?
        FarmDetailsPresenter(farmId!!, RepositoryUtils.getFarmRepo(this), fragment!!)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        } else if (item.itemId == R.id.action_edit_batch) {
            farmId?.let { FarmFormActivity.start(this, it) }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.farm_details_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    private fun findLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationClient.lastLocation.addOnCompleteListener {
            if (it.isSuccessful) {
                UserFireStore().setLocation(FirebaseAuth.getInstance().currentUser!!.uid, it.result!!)
            }
        }
    }

    private fun isLocationPermissionGranted(): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION), 1)
                false
            }
        } else {
            //permission is automatically granted on sdk<23 upon installation
            true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            findLocation()
        }
    }

    companion object {
        private const val FARM_ID_EXTRA = "farm_id"
        fun start(context: Context, farmId: String) {
            val starter = Intent(context, FarmDetailsActivity::class.java)
            starter.putExtra(FARM_ID_EXTRA, farmId)
            context.startActivity(starter)
        }
    }
}
