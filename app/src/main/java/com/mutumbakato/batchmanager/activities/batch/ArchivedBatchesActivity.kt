package com.mutumbakato.batchmanager.activities.batch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.batch.BatchListFragment
import com.mutumbakato.batchmanager.presenters.BatchListPresenter
import kotlinx.android.synthetic.main.activity_archived_batches.*
import kotlinx.android.synthetic.main.toolbar_flat.*

class ArchivedBatchesActivity : BaseActivity(), BatchListFragment.OnBatchListInteractionListener {

    internal lateinit var mPresenter: BatchListPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_archived_batches)
        toolbar!!.title = "Closed Batches"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        val batchListFragment = supportFragmentManager
                .findFragmentById(R.id.batch_list_archived) as BatchListFragment?
        val mFarmId = intent.getStringExtra(EXTRA_FARM_ID)
        mPresenter = BatchListPresenter(mFarmId, true, RepositoryUtils.getBatchRepo(this), batchListFragment!!)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun onBatchListFragmentInteraction(batch: Batch) {
        mPresenter.openBatchDetails(batch)
    }

    override fun showProgress(isLoading: Boolean) {
        progress!!.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun showAd() {

    }

    companion object {

        private const val EXTRA_FARM_ID = "farm_id"

        fun start(context: Context, farmId: String) {
            val starter = Intent(context, ArchivedBatchesActivity::class.java)
            starter.putExtra(EXTRA_FARM_ID, farmId)
            context.startActivity(starter)
        }
    }
}
