package com.mutumbakato.batchmanager.activities.batch

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.data.models.Batch
import com.mutumbakato.batchmanager.data.models.BatchUser
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.batch.BatchDetailsFragment
import com.mutumbakato.batchmanager.fragments.batch.BatchUserFragment
import com.mutumbakato.batchmanager.presenters.AddUserPresenter
import com.mutumbakato.batchmanager.presenters.BatchDetailsPresenter
import com.mutumbakato.batchmanager.utils.UserRoles
import kotlinx.android.synthetic.main.activity_batch_details.*
import kotlinx.android.synthetic.main.toolbar.*

class BatchDetailsActivity : BaseActivity(), BatchDetailsFragment.OnBatchDetailsFragmentInteractionListener,
        BatchUserFragment.BatchUserListInteractionListener {

    private var mBatchId: String? = null
    private var mPresenter: BatchDetailsPresenter? = null
    private var mBatchUserPresenter: AddUserPresenter? = null
    private var status = ""
    private var role = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_batch_details)
        mBatchId = intent.getStringExtra(EXTRA_BATCH_ID)
        val mBatchOwner = intent.getStringExtra(EXTRA_BATCH_OWNER)
        toolbar!!.title = "Details"
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val batchDetailsFragment = supportFragmentManager.findFragmentById(R.id.batch_details_fragment) as BatchDetailsFragment?
        val batchUserFragment = supportFragmentManager.findFragmentById(R.id.batch_user_fragment) as BatchUserFragment?

        mPresenter = BatchDetailsPresenter(mBatchId,
                RepositoryUtils.getBatchRepo(this), batchDetailsFragment!!)

        mBatchUserPresenter = AddUserPresenter(mBatchId!!, mBatchOwner!!, RepositoryUtils.getBatchRepo(this),
                RepositoryUtils.getUserRepo(this), batchUserFragment!!)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.batch_details_menu, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menu.findItem(R.id.action_archive_batch).setIcon(if (status == "closed") R.drawable.ic_action_unarchive else R.drawable.ic_action_archive)
        menu.findItem(R.id.action_archive_batch).title = if (status == "closed") "Re-open Batch" else "Close Batch"
        menu.findItem(R.id.action_delete_batch).isVisible = role == UserRoles.USER_ADMIN
        menu.findItem(R.id.action_edit_batch).isVisible = role == UserRoles.USER_WORKER || role == UserRoles.USER_ADMIN
        menu.findItem(R.id.action_archive_batch).isVisible = role == UserRoles.USER_ADMIN
        menu.findItem(R.id.action_add_person).isVisible = role == UserRoles.USER_ADMIN
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_edit_batch) {
            BatchFormActivity.start(this, mBatchId!!)
            return true
        } else if (id == R.id.action_delete_batch) {
            deleteBatch()
            return true
        } else if (id == R.id.action_archive_batch) {
            if (status == "closed") {
                mPresenter!!.closeBatch(false, mBatchUserPresenter!!.getUsers())
                toast(getString(R.string.re_open_message))
            } else
                archiveBatch()
        } else if (id == android.R.id.home) {
            finish()
        } else if (id == R.id.action_add_person) {
            //  mPresenter.addUser();
            mBatchUserPresenter!!.addyEmail()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun archiveBatch() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.close_batch_dialog_title))
        builder.setMessage(getString(R.string.close_batch_message))
        builder.setPositiveButton(getString(R.string.action_close)) { dialogInterface, _ ->
            dialogInterface.dismiss()
            mPresenter!!.closeBatch(true, mBatchUserPresenter!!.getUsers())
            toast("Batch has been closed")
            BatchListActivity.start(this)
        }
        builder.setNegativeButton(getString(R.string.cancel)) { dialogInterface, _ -> dialogInterface.dismiss() }
        builder.create().show()
    }

    private fun deleteBatch() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Delete Batch")
        builder.setMessage("Are you sure you want to delete this batch?\n\nAll the information about the batch will be lost forever.")
        builder.setPositiveButton("DELETE") { dialogInterface, _ ->
            mPresenter!!.deleteBatch(mBatchId!!)
            dialogInterface.dismiss()
            toast("Batch has been deleted")
            finish()
        }
        builder.setNegativeButton("CANCEL") { dialogInterface, _ -> dialogInterface.dismiss() }
        builder.create().show()
    }

    override fun onBatchDetailsInteraction(batch: Batch?) {
        status = batch!!.status
        invalidateOptionsMenu()
    }

    override fun onApplyPermissions(role: String?) {
        this.role = role!!
        invalidateOptionsMenu()
        mBatchUserPresenter!!.applyPermissions(role)
    }

    private fun toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onItemClick(user: BatchUser) {

    }

    override fun onUpdate(user: BatchUser, role: String) {
        mBatchUserPresenter!!.changeRole(user, role)
    }

    override fun onRemove(user: BatchUser) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Remove " + user.name)
                .setMessage("Do you want to remove " + user.name + " from this batch?")
                .setPositiveButton("Remove") { _, _ -> mBatchUserPresenter!!.removeUser(user) }
                .setNegativeButton("Cancel") { dialog, _ -> dialog.dismiss() }
                .show()
    }

    override fun showProgress(isShow: Boolean) {
        batch_user_progress!!.visibility = if (isShow) View.VISIBLE else View.GONE
    }

    override fun showHasUsers(hasUsers: Boolean) {
        users_card!!.visibility = if (hasUsers) View.VISIBLE else View.GONE
    }

    companion object {

        private const val EXTRA_BATCH_ID = "batch_id"
        private const val EXTRA_BATCH_NAME = "batch_name"
        private const val EXTRA_BATCH_OWNER = "owner_id"

        fun start(context: Context, batchId: String, batchName: String, ownerId: String) {
            val starter = Intent(context, BatchDetailsActivity::class.java)
            starter.putExtra(EXTRA_BATCH_ID, batchId)
            starter.putExtra(EXTRA_BATCH_NAME, batchName)
            starter.putExtra(EXTRA_BATCH_OWNER, ownerId)
            context.startActivity(starter)
        }
    }
}
