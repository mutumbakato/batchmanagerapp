package com.mutumbakato.batchmanager.activities.vaccination

import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.data.models.VaccinationSchedule
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.vaccination.ScheduleListFragment
import com.mutumbakato.batchmanager.fragments.vaccination.VaccinationFragment
import com.mutumbakato.batchmanager.presenters.VaccinationPresenter
import com.mutumbakato.batchmanager.ui.VaccinationContract
import kotlinx.android.synthetic.main.activity_vaccination.*

class VaccinationActivity : BaseActivity(), VaccinationContract.View.ListInteractionListener,
        ScheduleListFragment.OnListFragmentInteractionListener {

    private var mBatchId: String? = null
    private var dateOfBirth: String? = null
    private var mBatchName: String? = null
    private var mScheduleId: String? = null
    private var mPresenter: VaccinationPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vaccination)
        dateOfBirth = intent.getStringExtra(EXTRA_DOB)
        mBatchName = intent.getStringExtra(BATCH_NAME_EXTRA)
        mBatchId = intent.getStringExtra(BATCH_ID_EXTRA)
        mScheduleId = intent.getStringExtra(SCHEDULE_ID_EXTRA)
        if (mScheduleId == null || mScheduleId!!.isEmpty()) {
            vaccination_add_fab.visibility = View.GONE
        }
        vaccination_add_fab!!.setOnClickListener {
            VaccinationFormActivity.start(this, mScheduleId!!, "")
        }
        toolbar!!.title = mBatchName
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        initVaccinationFragment()
    }

    private fun openSchedules() {
        val starter = Intent(this@VaccinationActivity, VaccinationScheduleActivity::class.java)
        starter.putExtra(EXTRA_DOB, dateOfBirth)
        starter.putExtra(BATCH_NAME_EXTRA, mBatchName)
        startActivityForResult(starter, 1)
    }

    private fun initVaccinationFragment() {
        val vaccinationFragment = supportFragmentManager.findFragmentById(R.id.vaccination_fragment) as VaccinationFragment
        mPresenter = VaccinationPresenter(mScheduleId, mBatchId, RepositoryUtils.getVaccinationRepo(this),
                RepositoryUtils.getScheduleRepo(this), vaccinationFragment)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_vaccination, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_schedules -> {
                openSchedules()
                return true
            }
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.action_remove_vaccination -> {
                AlertDialog.Builder(this).apply {
                    setTitle("Remove vaccination schedule")
                    setMessage("Do you want to stop using this vaccination schedule?")
                    setPositiveButton("Remove") { dialogInterface: DialogInterface, _: Int ->
                        dialogInterface.dismiss()
                        mPresenter!!.removeSchedule(mScheduleId!!)
                        mPresenter!!.start()
                    }
                    setNegativeButton("Cancel") { dialogInterface: DialogInterface, _: Int ->
                        dialogInterface.dismiss()
                    }
                    show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showForm(id: String) {
        VaccinationFormActivity.start(this, id, "")
    }

    @SuppressLint("RestrictedApi")
    override fun hideFab(total: String) {
        //vaccination_add_fab!!.visibility = View.GONE
    }

    override fun edit(scheduleId: String, vaccinationId: String) {
        VaccinationFormActivity.start(this, scheduleId, vaccinationId)
    }

    override fun onListFragmentInteraction(item: VaccinationSchedule) {

    }

    override fun onUseSchedule(schedule: VaccinationSchedule) {
        mPresenter?.setSchedule(schedule.id)
        mPresenter?.start()
    }

    override fun onRemoveSchedule(schedule: String) {
    }

    override fun updateSchedule(schedule: VaccinationSchedule) {
    }

    override fun showProgress(isLoading: Boolean) {
    }

    companion object {

        const val EXTRA_DOB = "date_of_birth"
        const val BATCH_NAME_EXTRA = "batch_name"
        const val BATCH_ID_EXTRA = "batch_id"
        const val SCHEDULE_ID_EXTRA = "schedule_id"

        fun start(context: Context, scheduleId: String, batchId: String, batchName: String, dob: String) {
            val starter = Intent(context, VaccinationActivity::class.java)
            starter.putExtra(EXTRA_DOB, dob)
            starter.putExtra(BATCH_NAME_EXTRA, batchName)
            starter.putExtra(BATCH_ID_EXTRA, batchId)
            starter.putExtra(SCHEDULE_ID_EXTRA, scheduleId)
            context.startActivity(starter)
        }
    }
}
