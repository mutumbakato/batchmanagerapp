package com.mutumbakato.batchmanager.activities.user

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import com.google.android.material.snackbar.Snackbar
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.activities.BaseActivity
import com.mutumbakato.batchmanager.activities.batch.BatchListActivity
import com.mutumbakato.batchmanager.data.UserDataSource
import com.mutumbakato.batchmanager.data.models.User
import com.mutumbakato.batchmanager.data.repository.UserRepository
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.utils.KeyBoard
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity(), UserDataSource.UserCallBack {

    private var userRepository: UserRepository? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        userRepository = RepositoryUtils.getUserRepo(this)
        password.setOnEditorActionListener { _, id, _ ->
            if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                attemptLogin()
            }
            false
        }
        reset_button.setOnClickListener {
            resetPassword()
        }
        email_register_button.setOnClickListener {
            register()
        }
        email_login_button.setOnClickListener {
            login()
        }
    }

    private fun login() {
        attemptLogin()
    }

    private fun register() {
        RegisterActivity.start(this)
    }

    private fun resetPassword() {
        PasswordResetActivity.start(this)
    }

    private fun attemptLogin() {
        KeyBoard.hideSoftKeyboard(this@LoginActivity)
        // Reset errors.
        email.error = null
        password.error = null

        // Store values at the time of the login attempt.
        val email = email.text.toString()
        val password = password.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            password_label.error = getString(R.string.error_invalid_password)
            focusView = this.password
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            email_label.error = getString(R.string.error_field_required)
            focusView = this.email
            cancel = true

        } else if (!isEmailValid(email)) {
            email_label.error = getString(R.string.error_invalid_email)
            focusView = this.email
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView!!.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
            login(email, password)
        }
    }

    private fun isEmailValid(email: String): Boolean {
        return email.contains("@")
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length >= 4
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)
        login_form.visibility = if (show) View.GONE else View.VISIBLE
        login_form.animate().setDuration(shortAnimTime.toLong()).alpha(
                (if (show) 0 else 1).toFloat()).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                login_form.visibility = if (show) View.GONE else View.VISIBLE
            }
        })

        login_progress.visibility = if (show) View.VISIBLE else View.GONE
        login_progress.animate().setDuration(shortAnimTime.toLong()).alpha(
                (if (show) 1 else 0).toFloat()).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                login_progress.visibility = if (show) View.VISIBLE else View.GONE
            }
        })
    }

    private fun login(username: String, password: String) {
        showProgress(true)
        userRepository!!.login(username, password, this)
    }

    private fun showError(message: String) {
        Snackbar.make(email, message, Snackbar.LENGTH_LONG).show()
    }

    override fun onSuccess(user: User?) {
        showProgress(false)
        BatchListActivity.start(this)
        finish()
    }

    override fun onError(message: String) {
        showProgress(false)
        showError(message)
    }

    fun hideSoftKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    companion object {

        fun start(context: Context) {
            val starter = Intent(context, LoginActivity::class.java)
            starter.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(starter)
        }
    }
}

