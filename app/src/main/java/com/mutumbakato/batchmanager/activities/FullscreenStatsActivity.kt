package com.mutumbakato.batchmanager.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.mutumbakato.batchmanager.R
import com.mutumbakato.batchmanager.data.utils.RepositoryUtils
import com.mutumbakato.batchmanager.fragments.stats.*
import com.mutumbakato.batchmanager.viewmodels.BatchViewModel
import com.mutumbakato.batchmanager.viewmodels.ViewModelFactory

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
class FullscreenStatsActivity : AppCompatActivity() {

    private var mBatchId: String = "0"
    private lateinit var viewModel: BatchViewModel
    private lateinit var type: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fullscreen_stats)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mBatchId = intent.getStringExtra("batch_id") ?: "0"
        type = intent.getStringExtra("type") ?: "performance"
        viewModel = ViewModelProvider(this, ViewModelFactory.BatchViewModelFactory(
                RepositoryUtils.getStatisticsDataSource(this)))[BatchViewModel::class.java]

        viewModel.setBatchId(mBatchId)

        when (type) {
            "feeds-weight" -> showFragment(WeightFeedsChart.newInstance())
            "feeds-water" -> showFragment(WaterFeedsChart.newInstance())
            "feeds" -> showFragment(FeedsChart.newInstance())
            "weight" -> showFragment(WeightChart.newInstance())
            "deaths" -> showFragment(DeathsChart.newInstance())
            "eggs" -> showFragment(EggsChart.newInstance())
            "eggs-feeds" -> showFragment(EggsFeedsChart.newInstance())
            "water" -> showFragment(WaterChart.newInstance())
        }
    }

    private fun showFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().add(R.id.charts_frame, fragment).commitNow()
    }

    companion object {

        fun start(context: Context, batchId: String, type: String) {
            val starter = Intent(context, FullscreenStatsActivity::class.java)
            starter.putExtra("batch_id", batchId)
            starter.putExtra("type", type)
            context.startActivity(starter)
        }
    }

}
