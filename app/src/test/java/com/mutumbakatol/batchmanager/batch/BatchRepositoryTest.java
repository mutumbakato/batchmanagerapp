package com.mutumbakatol.batchmanager.batch;

import com.mutumbakato.batchmanager.data.BatchDataSource;
import com.mutumbakato.batchmanager.data.local.BatchLocalDataSource;
import com.mutumbakato.batchmanager.data.models.Batch;
import com.mutumbakato.batchmanager.data.remote.BatchRemoteDataSource;
import com.mutumbakato.batchmanager.data.repository.BatchRepository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;


public class BatchRepositoryTest {

    @Mock
    BatchDataSource.LoadBatchesCallback batchCallback;
    @Mock
    private BatchRemoteDataSource remoteDataSource;
    @Mock
    private BatchLocalDataSource localDataSource;
    @Mock
    private BatchRepository batchRepository;
    @Mock
    private Batch mBatch;
    @Mock
    private BatchDataSource.CreateBatchCallback callback;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void saveBatchTest() {
        batchRepository = BatchRepository.Companion.getTestInstance(remoteDataSource, localDataSource);
        batchRepository.saveBatch(mBatch, callback);
        verify(remoteDataSource).saveBatch(mBatch, callback);
        verify(localDataSource).saveBatch(mBatch, callback);
    }

    @Test
    public void updateBatchTest() {
        batchRepository = BatchRepository.Companion.getTestInstance(remoteDataSource, localDataSource);
        batchRepository.updateBatch(mBatch);
        verify(remoteDataSource).updateBatch(mBatch);
        verify(localDataSource).updateBatch(mBatch);
    }

    @Test
    public void saveBatchWithNullBatchTest() {

    }

    @Test
    public void getBatches() {
        batchRepository = BatchRepository.Companion.getInstance(remoteDataSource, localDataSource);
        batchRepository.getAllBatches("1", batchCallback);
        verify(localDataSource).getAllBatches("1", batchCallback);
        verify(remoteDataSource).observeMyBatches("1", batchCallback);
    }

}
